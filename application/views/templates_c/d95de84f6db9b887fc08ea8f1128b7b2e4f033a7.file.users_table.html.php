<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-18 14:12:28
         compiled from "/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/users_table.html" */ ?>
<?php /*%%SmartyHeaderCode:177334768059973c1c4a5de4-49845598%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd95de84f6db9b887fc08ea8f1128b7b2e4f033a7' => 
    array (
      0 => '/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/users_table.html',
      1 => 1464652594,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177334768059973c1c4a5de4-49845598',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list_users' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_59973c1c581ca2_89587661',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59973c1c581ca2_89587661')) {function content_59973c1c581ca2_89587661($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-users fa-4x"></i> USUARIOS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Usuarios</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Usuarios</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-danger">
                        <thead>
                        <tr>
                            <th data-class="expand" style="max-width: 20px">ID</th>     
                            <th data-hide="phone">Usuario</th>
                            <th data-hide="phone">Email</th>
                            <th data-hide="phone,tablet">Rol</th>
                            <th data-hide="phone,tablet" style="min-width: 310px" class="text-center">Acciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr id="<?php echo $_smarty_tpl->tpl_vars['con']->value['user_id'];?>
">
                                <td class="text-center" style="width: 1%"><?php echo $_smarty_tpl->tpl_vars['con']->value['user_id'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['username'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['email'];?>
</td>
                                <td><?php if ($_smarty_tpl->tpl_vars['con']->value['auth_level']==1) {?>
                                		Cliente
	                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['auth_level']==3) {?>
	                                	Bodega
                                    <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['auth_level']==5) {?>
                                        Supervisor
	                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['auth_level']==6) {?>
	                                	Oficina
	                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['auth_level']==9) {?>
	                                	Administrador
	                                <?php }?>
                                </td>
                                <td class="text-center">
                                     <a href="<?php echo base_url('account/load_editUser');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['user_id'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Editar</a>
                                    <a href="<?php echo base_url('account/delete_account');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['user_id'];?>
" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
