<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-04 16:06:09
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/vendors/vendors.html" */ ?>
<?php /*%%SmartyHeaderCode:5932211115a3ae218453b05-09775177%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee34a2843642af378fa0635216a2a46029c1a144' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/vendors/vendors.html',
      1 => 1515107119,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5932211115a3ae218453b05-09775177',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a3ae2184933c3_70243145',
  'variables' => 
  array (
    'list_vendor' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3ae2184933c3_70243145')) {function content_5a3ae2184933c3_70243145($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-truck fa-4x"></i> PROVEEDORES</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Poveedor</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Provedores</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-theme">
                        <thead>
                        <tr>
                            <th class="text-center" data-hide="phone" style="width: 20px">Codigo</th>
                            <th class="text-center" data-hide="phone">Nombre</th>      
                            <th class="text-center" data-class="expand">Dirección</th>
                            <th class="text-center" data-hide="phone,tablet">Telefono</th>
                            <th class="text-center" data-hide="phone">Email</th>
                            <th class="text-center" data-hide="phone" style="width: 200px">Acciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr style="text-align: center;" id="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
">
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_address'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_phone'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_email'];?>
</td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('vendors/load_edit');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Editar</a>
                                    <a id="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
" data-vendor="<?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
"  class="btn btn-sm btn-danger btn-xs btn-push delVendor"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                        <!--tfoot section is optional-->
                       
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
