<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-23 11:18:51
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/catalogs/warehouse/newWhse.html" */ ?>
<?php /*%%SmartyHeaderCode:133824371156f2de0b64dd09-91825579%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b63c37132af51acdeb84d748233d6a070e96f200' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/catalogs/warehouse/newWhse.html',
      1 => 1458753551,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '133824371156f2de0b64dd09-91825579',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f2de0b68b060_06453128',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f2de0b68b060_06453128')) {function content_56f2de0b68b060_06453128($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i>REGISTRAR BODEGA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Registrar Bodega</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Registrar Bodega</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="item" action="<?php echo base_url('warehouse/add_warehouse');?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name" placeholder="">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dirección<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="address" >
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
