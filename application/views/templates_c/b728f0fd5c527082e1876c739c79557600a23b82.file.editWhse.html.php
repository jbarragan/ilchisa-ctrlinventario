<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-30 10:07:10
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/warehouse/editWhse.html" */ ?>
<?php /*%%SmartyHeaderCode:17018130115a70a63e13b8a7-95626351%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b728f0fd5c527082e1876c739c79557600a23b82' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/warehouse/editWhse.html',
      1 => 1513708397,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17018130115a70a63e13b8a7-95626351',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'whse' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a70a63e175595_59798589',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a70a63e175595_59798589')) {function content_5a70a63e175595_59798589($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-building fa-4x"></i>BODEGA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Bodega</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Editar Bodega</h3>
                </div><!-- /.pull-left -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whse']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                <form class="form-horizontal form-bordered" role="form" id="whse" action="<?php echo base_url('warehouse/edit_whse');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" method="post">
                
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name" value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dirección<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="address" value="<?php echo $_smarty_tpl->tpl_vars['con']->value['address'];?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                <?php } ?>
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Aceptar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
