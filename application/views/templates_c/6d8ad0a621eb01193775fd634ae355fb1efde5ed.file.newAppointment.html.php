<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-31 10:53:16
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/appointment/newAppointment.html" */ ?>
<?php /*%%SmartyHeaderCode:210734183656fd55fc673b04-38969814%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d8ad0a621eb01193775fd634ae355fb1efde5ed' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/appointment/newAppointment.html',
      1 => 1459392489,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '210734183656fd55fc673b04-38969814',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'date' => 0,
    'custom' => 0,
    'con' => 0,
    'vendor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fd55fc708208_37720910',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fd55fc708208_37720910')) {function content_56fd55fc708208_37720910($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> AGENDAR CITA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Agendar Cita</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Agendar Cita</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="appoint" action="<?php echo base_url('appointment/create');?>
" method="post">
                    <div class="form-body">
                        <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha <span class="asterisk">*</span></label>
                            <div class="col-sm-7">                         
                                 <input type="text" class="form-control input-sm" name="date" id="date" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Po No<span class="asterisk">*</span></label>
                           	<div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="po" >
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cliente<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="customer" tabindex="2">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Proveedor<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="vendor" tabindex="2">
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
