<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-22 18:57:22
         compiled from "C:\xampp\htdocs\newVicar\application\views\templates\contents\transaction\receipts\receipts.html" */ ?>
<?php /*%%SmartyHeaderCode:1179756ed9073a57314-04885287%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '308774dccdc562047d9aa94672507f76cef8b863' => 
    array (
      0 => 'C:\\xampp\\htdocs\\newVicar\\application\\views\\templates\\contents\\transaction\\receipts\\receipts.html',
      1 => 1458698240,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1179756ed9073a57314-04885287',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56ed9073a9e608_73487084',
  'variables' => 
  array (
    'list_receipts' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56ed9073a9e608_73487084')) {function content_56ed9073a9e608_73487084($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> ENTRADAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Entrada</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Entradas</h3>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-sm" data-action="refresh" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                        <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-lilac">
                        <thead>
                        <tr>
                            <th data-hide="phone">Estatus</th>  
                            <th data-hide="phone">Vicar Ctrl</th>    
                            <th data-class="expand">Fecha Registro</th>
                            <th data-hide="phone,tablet">Fecha Entrada</th>
                            <th data-hide="phone">Po. No.</th>
                            <th data-hide="phone">Cliente</th>
                            <th data-hide="phone">Producto</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">HT</th>
                            <th data-hide="phone">Etiquetas</th>
                            <th data-hide="phone">Opciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_receipts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <?php if ($_smarty_tpl->tpl_vars['con']->value['status']=="Nuevo") {?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo actualizado") {?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php } else { ?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php }?>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['system_date'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['receipt_date'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['po_no'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['ht'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['tags'];?>
</td>                                
                                <td class="text-center">
                                    <a href="#" class="btn btn-sm btn-success btn-xs btn-push btn-view" id="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_receipt'];?>
"><i class="fa fa-eye"></i> Ver Más</a>
                                <?php if ($_smarty_tpl->tpl_vars['con']->value['status']=="Nuevo") {?>
                                    <a href="load_updateRecipt/<?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Edit</a>
                                    <?php } else { ?>
                                    <a href="select_lotsReceipts/<?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Edit</a>
                                <?php }?>
                                    <a href="#" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                        <!--tfoot section is optional-->
                        <tfoot>
                        <tr> 
                            <th data-hide="phone">Estatus</th>     
                            <th data-hide="phone">Vicar Ctrl</th>      
                            <th data-class="expand">Fecha Registro</th>
                            <th data-hide="phone,tablet">Fecha Entrada</th>
                            <th data-hide="phone">Po. No.</th>
                            <th data-hide="phone">Cliente</th>
                            <th data-hide="phone">Producto</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">HT</th>
                            <th data-hide="phone">Etiquetas</th>
                            <th data-hide="phone">Opciones</th>
                        </tr>
                        </tfoot>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<!--MODAL VER-->
        <div class="modal fade bs-example-modal-lg" id="receiptView">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <iframe src=""></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<?php }} ?>
