<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-24 13:10:14
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/sign_up.html" */ ?>
<?php /*%%SmartyHeaderCode:204518631157157084bf32a0-47760554%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7113ffbbaee555ffc71291ca425deebe2bbf7d4' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/sign_up.html',
      1 => 1503277208,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '204518631157157084bf32a0-47760554',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57157084c31a06_37318880',
  'variables' => 
  array (
    'error_val' => 0,
    'Val_user' => 0,
    'user' => 0,
    'passwd' => 0,
    'passwd2' => 0,
    'Val_email' => 0,
    'email' => 0,
    'Val_rol' => 0,
    'auth_level' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57157084c31a06_37318880')) {function content_57157084c31a06_37318880($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-user fa-4x"></i> Crear Usuario</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Usuario a Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">
         <div class="panel rounded shadow">
            <div class="panel-body">
                <!-- Register form -->
                <form class="form-horizontal rounded shadow no-overflow" action="<?php echo base_url('account/sign_up');?>
" style="width:500px; margin: 0 auto;" method="post">
                    <div class="sign-body">
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $_smarty_tpl->tpl_vars['Val_user']->value;?>
">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['user']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="password" class="form-control" placeholder="Password" name="passwd">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="password" class="form-control " placeholder="Confirm Password" name="passwd2">
                                <span class="input-group-addon"><i class="fa fa-check"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd2']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_email']->value==null) {?> has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="email" class="form-control" placeholder="Your Email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['Val_email']->value;?>
">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['email']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_rol']->value==null) {?>has-error has-feedback<?php }?>">
                        <div class="input-group input-group-lg rounded no-overflow " name="username">
                            <select type="email" class="form-control" name="auth_level">
                            <option value=''>Rol de Usuario</option>
                            <option value=3>Bodega</option>
                            <option value=5>Supervisor</option>
                            <option value=6>Oficina</option>
                            <option value=9>Administrador</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-users"></i></span>
                        </div>
                        <?php if (isset($_smarty_tpl->tpl_vars['auth_level']->value)) {
echo $_smarty_tpl->tpl_vars['auth_level']->value;
}?>
                    </div>
                    </div>
                    <div class="sign-footer">
                        <div class="form-group">
                            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Sign Up</button>
                        </div>
                    </div>
                </form>
                <!--/ Register form -->
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
         <?php echo '<script'; ?>
>
        $("#logout").fadeTo(1000, 500).slideUp(500, function(){
            $("#logout").alert('close');
        });
        <?php echo '</script'; ?>
>
    </div>
</div><!-- /.row --><?php }} ?>
