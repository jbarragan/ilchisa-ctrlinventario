<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-09-14 11:13:45
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/catalogs/customers/newCustomer.html" */ ?>
<?php /*%%SmartyHeaderCode:809612257570bffcb8b7ff0-76500701%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd84adb38301ebb14f6c7a3f392b51188d6d0e29e' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/catalogs/customers/newCustomer.html',
      1 => 1503277501,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '809612257570bffcb8b7ff0-76500701',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_570bffcb91ac03_75502417',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570bffcb91ac03_75502417')) {function content_570bffcb91ac03_75502417($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-users fa-4x"></i> REGISTRAR CLIENTE</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Registrar Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="item" action="<?php echo base_url('customer/reg_customer');?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name" placeholder="">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dirección<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="address" >
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefono<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="phone" >
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="email" >
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
