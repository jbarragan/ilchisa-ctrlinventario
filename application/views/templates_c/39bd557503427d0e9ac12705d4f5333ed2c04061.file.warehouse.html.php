<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-01 10:56:45
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/catalogs/warehouse/warehouse.html" */ ?>
<?php /*%%SmartyHeaderCode:211267823456f2de0a046472-85962042%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39bd557503427d0e9ac12705d4f5333ed2c04061' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/catalogs/warehouse/warehouse.html',
      1 => 1459533320,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '211267823456f2de0a046472-85962042',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f2de0a099544_63331629',
  'variables' => 
  array (
    'list_whse' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f2de0a099544_63331629')) {function content_56f2de0a099544_63331629($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> BODEGAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Bodega</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Bodegas</h3>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-sm" data-action="refresh" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                        <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-danger">
                        <thead>
                        <tr>
                            <th data-hide="phone" class="text-center">Codigo</th>      
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone" class="text-center">Acción</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_whse']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td class="text-center" style="width: 1%"><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['address'];?>
</td>
                                <td class="text-center">
                                    <a href="load_edit/<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="delete_whse/<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                        <!--tfoot section is optional-->
                        <tfoot>
                        <tr>         
                            <th data-hide="phone" class="text-center">Codigo</th>
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone" class="text-center">Acción</th>
                        </tr>
                        </tfoot>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
