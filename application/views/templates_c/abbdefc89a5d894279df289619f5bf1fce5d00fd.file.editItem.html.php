<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-26 15:21:02
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/items/editItem.html" */ ?>
<?php /*%%SmartyHeaderCode:10767282955a42cb4eed2fc7-69303783%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'abbdefc89a5d894279df289619f5bf1fce5d00fd' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/items/editItem.html',
      1 => 1513708392,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10767282955a42cb4eed2fc7-69303783',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'editItem' => 0,
    'list_vendor' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a42cb4ef22502_78420076',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a42cb4ef22502_78420076')) {function content_5a42cb4ef22502_78420076($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> EDITAR PRODUCTO</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Producto</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
           
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="item" action="<?php echo base_url('item/edit_item');?>
/<?php echo $_smarty_tpl->tpl_vars['editItem']->value->items_code;?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="item_name" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['editItem']->value->item_name;?>
">
                            </div>
                        </div><!-- /.form-group 
                       <div class="form-group">
                            <label class="col-sm-3 control-label">Proveedor<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="item_vendor" tabindex="2">
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div>.fom-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Descripción<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="item_desc" value="<?php echo $_smarty_tpl->tpl_vars['editItem']->value->desc;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
