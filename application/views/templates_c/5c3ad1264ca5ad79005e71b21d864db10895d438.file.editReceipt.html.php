<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 09:15:46
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/receipts/editReceipt.html" */ ?>
<?php /*%%SmartyHeaderCode:15476318745702b6ac0807c3-80795453%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5c3ad1264ca5ad79005e71b21d864db10895d438' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/receipts/editReceipt.html',
      1 => 1503277372,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15476318745702b6ac0807c3-80795453',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5702b6ac151b94_61263140',
  'variables' => 
  array (
    'receipt' => 0,
    'con2' => 0,
    'date' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'items' => 0,
    'custom' => 0,
    'ht' => 0,
    'vendor' => 0,
    'tags' => 0,
    'vicarCtrl' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5702b6ac151b94_61263140')) {function content_5702b6ac151b94_61263140($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-right fa-4x"></i>ENTRADA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
            <div class="panel-body">
            		<?php  $_smarty_tpl->tpl_vars['con2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con2']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['receipt']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con2']->key => $_smarty_tpl->tpl_vars['con2']->value) {
$_smarty_tpl->tpl_vars['con2']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con2']->key;
?>
                    <?php $_smarty_tpl->tpl_vars['vicarCtrl'] = new Smarty_variable($_smarty_tpl->tpl_vars['con2']->value['vicar_ctrl'], null, 0);?>
                <form class="form-horizontal" role="form" id="receipt" action="<?php echo base_url('receipts/up_receipts');?>
/<?php echo $_smarty_tpl->tpl_vars['con2']->value['vicar_ctrl'];?>
" method="post">
                   
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Vicar Ctrl<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm" name="receipt_vicar" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['vicar_ctrl'];?>
" readonly="true" >
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;">
                            	<span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                            
                                     <input type="text" class="form-control input-sm" name="date" id="date_rec" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="whse" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['whse_code'];?>
">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['whse_code']==$_smarty_tpl->tpl_vars['con2']->value['whse_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['con2']->value['id_appointment'];?>
</div> 
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Po. No.<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="po" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['po_no'];?>
">
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="item" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['item_code'];?>
">
                                    <option value="">Selecciona Producto</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['items_code']==$_smarty_tpl->tpl_vars['con2']->value['items_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>                            
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="customer" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['customer_code'];?>
">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['con2']->value['customer_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                            <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="ht" type="checkbox" name="ht" value="1" <?php echo $_smarty_tpl->tpl_vars['ht']->value;?>
>
                                    <label for="ht" class=" control-label">HT</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Proveedor<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="vendor" tabindex="2" >
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['vendor_code']==$_smarty_tpl->tpl_vars['con2']->value['vendor_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                             <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="tags" type="checkbox" name="tags" value="1" <?php echo $_smarty_tpl->tpl_vars['tags']->value;?>
>
                                    <label for="tags" class=" control-label">Etiquetas</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>
                         <?php } ?>  
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="0">
                                <table id="datatable" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Lote</td>
                                        <td>Pallets</td>
                                        <td style="width:0px;"></td>
                                        <td>Bags</td>
                                        <td>Prod Date</td>
                                        <td>Exp Date</td>
                                        <td>Damage</td>
                                        <td>Location</td>
                                    </tr>
                                </table>
                                <button type="button" class="btn btn-theme pull-right" id="addLots">Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Notas:</label>              
                                     <textarea class="form-control" rows="5" name="notes"><?php if (isset($_smarty_tpl->tpl_vars['receipt']->value['0']['notes'])) {?> <?php echo $_smarty_tpl->tpl_vars['receipt']->value['0']['notes'];?>
 <?php }?></textarea>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Pallets:<span class="asterisk">*</span></label>              
                                     <input type="text" class="form-control input-sm" id="total" placeholder="">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10 pull-right">
                                <label class="control-label">Cargar Imagenes</label> 
                                <div action="<?php echo base_url('receipts/images');?>
/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vicarCtrl']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
" class="col-sm-12 dropzone pull-right">
                                    <div class="fallback">
                                        <input name="img" type="file" multiple />
                                    </div>
                                </div><!-- /.panel-body -->
                            </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div>
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
