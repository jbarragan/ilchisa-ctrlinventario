<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-03-02 13:39:24
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/shipments/newShipments.html" */ ?>
<?php /*%%SmartyHeaderCode:20137620685a3973890db9e0-18967272%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3d37e351ccada57df892423b9e8a86aabd5db69' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/shipments/newShipments.html',
      1 => 1520023146,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20137620685a3973890db9e0-18967272',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a397389132e64_08777450',
  'variables' => 
  array (
    'noShipm' => 0,
    'dateRec' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'custom' => 0,
    'ship_list' => 0,
    'ship' => 0,
    'imgShip' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a397389132e64_08777450')) {function content_5a397389132e64_08777450($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-left fa-4x"></i>SALIDA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Salidas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Nueva Salida</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Registrar Salida</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="shipments" action="<?php echo base_url('shipments/reg_shipments');?>
" method="post">
                            
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Salida No.<span class="asterisk">*</span></label>
                                    <input type="text" id="ship_ctrlnew" class="form-control input-sm date_rec" name="no_ship" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['noShipm']->value;?>
">
                                    <span id="errorDuplicado"></span>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;"><span class="label label-success">NUEVO</span></div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                            
                                     <input type="text" class="form-control input-sm" name="date" id="date" value="<?php echo $_smarty_tpl->tpl_vars['dateRec']->value;?>
">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="whse" tabindex="2">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>                            
                                <select  class="chosen-select mb-15" name="customer" tabindex="2" id="custShip">      
                                <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                  
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Carrier<span class="asterisk">*</span></label>
                               <input type="text" class="form-control input-sm" name="carrier" id="" >
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>                     
                                <select data-placeholder="Choose a Country" class="form-control input-sm" name="item" tabindex="2" id="itemShip">                                   
                                </select>
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                                <label class="control-label">Trailer<span class="asterisk">*</span></label>
                               <input type="text" class="form-control input-sm" name="trailer" >
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                            <label class="control-label">Ship To<span class="asterisk">*</span></label>
                                <select class="form-control mb-15" name="shipTo" tabindex="2" id=shipto> 
                                <option value="">Selecciona Ship</option>
                                <?php  $_smarty_tpl->tpl_vars['ship'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ship']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ship_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ship']->key => $_smarty_tpl->tpl_vars['ship']->value) {
$_smarty_tpl->tpl_vars['ship']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['ship']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_state'];?>
,<?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_address'];?>
</option>
                                <?php } ?>                              
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Factura<span class="asterisk">*</span></label>
                               <input type="text" class="form-control input-sm" name="seal" >
                            </div><!-- /.form-group -->
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="0">
                                <table id="table-ship" class="table table-striped table-lilac">
                                    <tr>
                                        <td>No. Ctrl</td>
                                        <td>Lote</td>
                                        <td style="dispaly:none;"></td>
                                        <td>Bags</td>
                                        <td>Fecha Prod</td>
                                        <td>Fecha cad</td>
                                        <td>Location</td>
                                        <td>storage</td>
                                    </tr>
                                    <tbody id="addrow">
                                        
                                    </tbody>
                                </table>
                                <button type="button" class="btn btn-theme pull-right" id="addreny" disabled>Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Notas:</label>              
                                     <textarea class="form-control" rows="5" name="notes"></textarea>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                <input type="text" class="form-control input-sm" id="totalBag" placeholder="" value="0">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10 pull-right">
                                <label class="control-label">Cargar Imagenes</label> 
                                <div id="my-dropzone" action="<?php echo base_url('shipments/images');?>
/<?php echo $_smarty_tpl->tpl_vars['imgShip']->value;?>
" class="col-sm-12 dropzone pull-right">
                                    <div class="fallback">
                                        <input name="img" type="file" multiple />
                                    </div>
                                </div><!-- /.panel-body -->
                            </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button id="save" type="submit" class="btn btn-theme" disabled>Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
