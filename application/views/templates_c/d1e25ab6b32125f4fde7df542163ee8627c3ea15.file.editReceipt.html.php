<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-03-02 12:42:32
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/receipts/editReceipt.html" */ ?>
<?php /*%%SmartyHeaderCode:11744716065a397c0bece716-46852568%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd1e25ab6b32125f4fde7df542163ee8627c3ea15' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/receipts/editReceipt.html',
      1 => 1520018706,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11744716065a397c0bece716-46852568',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a397c0c03e870_07834842',
  'variables' => 
  array (
    'receipt' => 0,
    'con2' => 0,
    'date' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'custom' => 0,
    'items' => 0,
    'vendor' => 0,
    'producer' => 0,
    'brand' => 0,
    'plant' => 0,
    'clasification' => 0,
    'renyCtrl' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a397c0c03e870_07834842')) {function content_5a397c0c03e870_07834842($_smarty_tpl) {?><!-- Start page header -->
<?php echo '<script'; ?>
 type="text/javascript">
    function checkSubmit() {
        document.getElementById("SaveBtn").value = "Enviando...";
        document.getElementById("SaveBtn").disabled = true;
        return true;
    }
<?php echo '</script'; ?>
>
<div class="header-content">
    <h2><i class="fa fa-arrow-right fa-4x"></i>ENTRADA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
            <div class="panel-body">
            		<?php  $_smarty_tpl->tpl_vars['con2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con2']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['receipt']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con2']->key => $_smarty_tpl->tpl_vars['con2']->value) {
$_smarty_tpl->tpl_vars['con2']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con2']->key;
?>
                    <?php $_smarty_tpl->tpl_vars['renyCtrl'] = new Smarty_variable($_smarty_tpl->tpl_vars['con2']->value['reny_ctrl'], null, 0);?>
                <form class="form-horizontal" role="form" id="receipt" action="<?php echo base_url('receipts/up_receipts');?>
/<?php echo $_smarty_tpl->tpl_vars['con2']->value['reny_ctrl'];?>
" method="post"  onsubmit="return checkSubmit();">
                   
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">reny Ctrl<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm" name="receipt_reny" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['reny_ctrl'];?>
" readonly="true" >
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;">
                            	<span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                            
                                     <input type="text" class="form-control input-sm" name="date" id="date_rec" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="whse" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['whse_code'];?>
">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['whse_code']==$_smarty_tpl->tpl_vars['con2']->value['whse_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['con2']->value['id_appointment'];?>
</div> 
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Po. No.<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="po" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['po_no'];?>
">
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>                            
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="customer" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['customer_code'];?>
">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['con2']->value['customer_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="item" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['item_code'];?>
">
                                    <option value="">Selecciona Producto</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['items_code']==$_smarty_tpl->tpl_vars['con2']->value['items_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class=" control-label">Proveedor<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="vendor" tabindex="2" >
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['vendor_code']==$_smarty_tpl->tpl_vars['con2']->value['vendor_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Productor<span class="asterisk">*</span></label>
                                <select class="form-control  mb-15" name="producer" tabindex="2">
                                    <option value="">Selecciona Productor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['producer']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_producer'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_producer']==$_smarty_tpl->tpl_vars['con2']->value['id_producer']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name_producer'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Marca<span class="asterisk">*</span></label>
                                <select class="form-control  mb-15" name="brand" tabindex="2">
                                    <option value="">Selecciona la Marca</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['brand']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_brand'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_brand']==$_smarty_tpl->tpl_vars['con2']->value['id_brand']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name_brand'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Planta<span class="asterisk">*</span></label>
                                <select class="form-control  mb-15" name="plant" tabindex="2">
                                    <option value="">Selecciona una Planta</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['plant']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_plant'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_plant']==$_smarty_tpl->tpl_vars['con2']->value['id_plant']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name_plant'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Clasificacion<span class="asterisk">*</span></label>
                                <select class="form-control  mb-15" name="clasification" tabindex="2">
                                    <option value="">Clasificación</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['clasification']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_clasification'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_clasification']==$_smarty_tpl->tpl_vars['con2']->value['id_clasification']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name_clasification'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Recibido</label>
                                <input type="text" class="form-control input-sm" name="received" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['received'];?>
">
                            </div><!-- /.form-group -->
                        </div>
                         <?php } ?>  
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="0">
                                <table id="datatable" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Lote</td>
                                        <td>KG/Bolsa</td>
                                        <td style="width:0px;"></td>
                                        <td>Bags</td>
                                        <td>Prod Date</td>
                                        <td>Exp Date</td>
                                        <td>Damage</td>
                                        <td>AMT Damage</td>
                                        <td>Location</td>
                                    </tr>
                                </table>
                                <button type="button" class="btn btn-theme pull-right" id="addLots">Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Notas:</label>              
                                     <textarea class="form-control" rows="5" name="notes"><?php if (isset($_smarty_tpl->tpl_vars['receipt']->value['0']['notes'])) {?> <?php echo $_smarty_tpl->tpl_vars['receipt']->value['0']['notes'];?>
 <?php }?></textarea>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total KG/Bags:<span class="asterisk">*</span></label>              
                                     <input type="text" class="form-control input-sm" id="total" placeholder="">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10 pull-right">
                                <label class="control-label">Cargar Imagenes</label> 
                                <div  id="my-dropzone" action="<?php echo base_url('receipts/images');?>
/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['renyCtrl']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
" class="col-sm-12 dropzone pull-right">
                                    <div class="fallback">
                                        <input name="img" type="file" multiple />
                                    </div>
                                </div><!-- /.panel-body -->
                            </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <input type="submit" class="btn btn-theme" id="SaveBtn" value="Guardar">
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div>
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
