<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-02 20:20:27
         compiled from "C:\xampp\htdocs\vic\application\views\templates\contents\catalogs\items.html" */ ?>
<?php /*%%SmartyHeaderCode:192956d62bd2433c95-11706911%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa1436ecf369ecf21a6eac4e159714a968947072' => 
    array (
      0 => 'C:\\xampp\\htdocs\\vic\\application\\views\\templates\\contents\\catalogs\\items.html',
      1 => 1456946425,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '192956d62bd2433c95-11706911',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56d62bd2466834_70637372',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d62bd2466834_70637372')) {function content_56d62bd2466834_70637372($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-table"></i> Datatable <span>responsive datatable samples</span></h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Tables</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Datatable</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Employee List <span class="label label-danger">AJAX Support</span></h3>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-sm" data-action="refresh" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                        <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                    <table id="datatable-ajax" class="table table-striped table-primary">
                        <thead>
                        <tr>
                            <th data-hide="phone">Codigo</th>      
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone,tablet">Descripción</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone">Telefono</th>
                            <th data-hide="phone">Email</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody></tbody>
                        <!--tfoot section is optional-->
                        <tfoot>
                        <tr>      
                            <th data-hide="phone">Codigo</th>      
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone,tablet">Descripción</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone">Telefono</th>
                            <th data-hide="phone">Email</th>
                        </tr>
                        </tfoot>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
