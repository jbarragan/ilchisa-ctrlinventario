<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 08:51:18
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/shipments/packingList.html" */ ?>
<?php /*%%SmartyHeaderCode:900680951570539c36dea00-84082149%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '93203718aff3d1aaac1aa079ff00dbdeb8089a77' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/shipments/packingList.html',
      1 => 1503277291,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '900680951570539c36dea00-84082149',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_570539c373c722_47917344',
  'variables' => 
  array (
    'list_packing' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570539c373c722_47917344')) {function content_570539c373c722_47917344($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-left fa-4x"></i>PACKING LIST</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Salidas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Packing List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
                <div>
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-danger">
                        <thead>
                        <tr> 
                            <th data-class="expand">No. Salida</th> 
                            <th data-hide="phone">Estatus</th>    
                            <th data-hide="phone">Fecha Registro</th>
                            <th data-hide="phone">Fecha Salida</th>
                            <th data-hide="phone">Carrier</th>
                            <th data-hide="phone">Trailer</th>
                            <th data-hide="phone">Factura</th>
                            <th data-hide="phone">Opciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_packing']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['no_shipment'];?>
</td>
                                <td style="font-size:16px; text-align:center;"><span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con']->value['statusShip'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['system_date'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['start'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['carrier'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['trailer'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['seal'];?>
</td>                                
                                <td class="text-center">
                                    <a href="select_Packing/<?php echo $_smarty_tpl->tpl_vars['con']->value['no_ship'];?>
" class="btn btn-sm btn-success btn-xs btn-push btn-view"><i class="fa fa-eye"></i> Entrar</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<!--MODAL VER-->
        <div class="modal fade bs-example-modal-lg" id="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <iframe src=""></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<?php }} ?>
