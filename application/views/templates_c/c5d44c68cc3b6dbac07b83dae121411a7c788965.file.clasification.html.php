<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-16 00:08:20
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\clasification\clasification.html" */ ?>
<?php /*%%SmartyHeaderCode:76775a341ae12112e7-74517764%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c5d44c68cc3b6dbac07b83dae121411a7c788965' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\clasification\\clasification.html',
      1 => 1513367113,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '76775a341ae12112e7-74517764',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a341ae12c1920_10235879',
  'variables' => 
  array (
    'list_clasification' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a341ae12c1920_10235879')) {function content_5a341ae12c1920_10235879($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> CLASIFICACIONES</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Clasificaciones</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Marcas</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-theme">
                        <thead>
                        <tr>
                            <th data-hide="phone" class="text-center">Codigo</th>      
                            <th data-class="expand" class="text-center">Nombre</th>
                            <th data-hide="phone" class="text-center">Acción</th>
                            <th style="display:none;"></th>
                            <th style="display:none;"></th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_clasification']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['id_clasification'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['name_clasification'];?>
</td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('clasification/load_edit');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['id_clasification'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Editar</a>
                                    <a href="<?php echo base_url('clasification/delete_clasification');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['id_clasification'];?>
" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                                <td style="display:none;"></td>
                                <td style="display:none;"></td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                        <!--tfoot section is optional-->
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
