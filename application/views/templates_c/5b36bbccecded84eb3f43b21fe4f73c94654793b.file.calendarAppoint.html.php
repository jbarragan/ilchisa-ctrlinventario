<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-03 10:06:30
         compiled from "/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/appointment/calendarAppoint.html" */ ?>
<?php /*%%SmartyHeaderCode:191070462659833bf62ce106-33005766%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b36bbccecded84eb3f43b21fe4f73c94654793b' => 
    array (
      0 => '/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/appointment/calendarAppoint.html',
      1 => 1481066755,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '191070462659833bf62ce106-33005766',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_59833bf62d4c58_43093959',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59833bf62d4c58_43093959')) {function content_59833bf62d4c58_43093959($_smarty_tpl) {?><!-- Start page header -->
                <div class="header-content">
                    <h2><i class="fa fa-calendar"></i> Calendario de Citas</h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">You are here:</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="dashboard.html">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Transacciones</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Calendario de Citas</li>
                        </ol>
                    </div><!-- /.breadcrumb-wrapper -->
                </div><!-- /.header-content -->
                <!--/ End page header -->

                <!-- Start body content -->
                <div class="body-content animated fadeIn">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <div class="calendar-toolbar">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-2">
                                        <!-- Start offcanvas btn menu calendar: This menu will take position at the top of calendar (mobile only). -->
                                        <div class="btn-group hidden-lg hidden-md">
                                            <button type="button" class="btn btn-theme btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars fa-2x"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#" data-calendar-nav="prev">Anterior</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-nav="today">Hoy</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-nav="next">Siguiente</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="hidden-sm hidden-xs">
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-nav="prev"><i class="fa fa-angle-left"></i> Anterior</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-nav="today">Hoy</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-nav="next">Siguente <i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-8 col-xs-8">
                                        <div class="page-header no-border no-margin no-padding"><h4 class="no-border no-margin no-padding text-center text-capitalize">&nbsp;</h4></div>
                                    </div>
                                    <div class="col-md-5 col-sm-2 col-xs-2">
                                        <!-- Start offcanvas btn menu calendar: This menu will take position at the top of calendar (mobile only). -->
                                        <div class="btn-group calendar-menu-mobile pull-right hidden-lg hidden-md">
                                            <button type="button" class="btn btn-theme btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars fa-2x"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#" data-calendar-view="year">Año</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-view="month">Mes</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-view="week">Semana</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-view="day">Día</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pull-right hidden-sm hidden-xs">
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-view="year">Año</button>
                                            <button class="btn btn-theme btn-sm active rounded" data-calendar-view="month">Mes</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-view="week">Semana</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-view="day">Día</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="calendar" class="rounded mb-20"></div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <table id="appotList" class="table table-striped table-danger">
                                <thead>
                                    <tr> 
                                        <th class="text-center" style="max-width: 60px; width: 60px;">ID</th> 
                                        <th class="text-center">Citas</th> 
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div><!-- /.row -->

                </div><!-- /.body-content -->
                <!--/ End body content -->
<div class="modal fade" id="events-modal">
	<div class="modal-dialog">
    	<div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Event</h3>
            </div>
            <div class="modal-body" style="height: 400px">
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Close</a>
            </div>
        </div>
    </div>
</div><?php }} ?>
