<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-17 20:32:03
         compiled from "C:\xampp\htdocs\newVicar\application\views\templates\contents\catalogs\items.html" */ ?>
<?php /*%%SmartyHeaderCode:1323456e8b7bc50e3b2-06342789%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cfbd81aea1d56e377e4d294075d0e868dd4ff252' => 
    array (
      0 => 'C:\\xampp\\htdocs\\newVicar\\application\\views\\templates\\contents\\catalogs\\items.html',
      1 => 1458243121,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1323456e8b7bc50e3b2-06342789',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e8b7bc5390e8_02678046',
  'variables' => 
  array (
    'list_items' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e8b7bc5390e8_02678046')) {function content_56e8b7bc5390e8_02678046($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> PRODUCTOS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Producto</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Productos</h3>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-sm" data-action="refresh" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Refresh"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                        <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-lilac">
                        <thead>
                        <tr>
                            <th data-hide="phone">Codigo</th>      
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone,tablet">Descripción</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone">Telefono</th>
                            <th data-hide="phone">Email</th>
                            <th data-hide="phone">Acción</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td class="text-center" style="width: 1%"><?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['desc'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_address'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_phone'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_email'];?>
</td>
                                <td class="text-center">
                                    <a href="#" class="btn btn-sm btn-success btn-xs btn-push"><i class="fa fa-eye"></i> Detail</a>
                                    <a href="#" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="#" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                        <!--tfoot section is optional-->
                        <tfoot>
                        <tr>      
                            <th data-hide="phone">Codigo</th>      
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone,tablet">Descripción</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone">Telefono</th>
                            <th data-hide="phone">Email</th>
                            <th data-hide="phone">Acción</th>
                        </tr>
                        </tfoot>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
