<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 09:14:58
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/appointment/editAppointment.html" */ ?>
<?php /*%%SmartyHeaderCode:1814643975703f4cbc4dd09-19762947%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f22898858c34db9a950899f8554224420136a87' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/appointment/editAppointment.html',
      1 => 1503277563,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1814643975703f4cbc4dd09-19762947',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5703f4cbd06b80_39724291',
  'variables' => 
  array (
    'id' => 0,
    'date' => 0,
    'po_no' => 0,
    'custom' => 0,
    'con' => 0,
    'customer' => 0,
    'vendor' => 0,
    'vend' => 0,
    'vendor_email' => 0,
    'status' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5703f4cbd06b80_39724291')) {function content_5703f4cbd06b80_39724291($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-calendar fa-4x"></i> EDITAR CITA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Cita</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Editar Cita</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="appoint" action="<?php echo base_url('appointment/edit');?>
/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" method="post">
                    <div class="form-body">
                        <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha <span class="asterisk">*</span></label>
                            <div class="col-sm-7">                         
                                 <input type="text" class="form-control input-sm" name="date" id="date" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Po No<span class="asterisk">*</span></label>
                           	<div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="po" value="<?php echo $_smarty_tpl->tpl_vars['po_no']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cliente<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <select data-placeholder="selecciona al cliente" class="chosen-select mb-15" name="customer" tabindex="2">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['customer']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Proveedor<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <select data-placeholder="Selecciona al Proveedor" class="chosen-select mb-15" id="vendor" name="vendor" tabindex="2">
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['vendor_code']==$_smarty_tpl->tpl_vars['vend']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div><!-- /.form-group -->
                       <!--  <div class="form-group">
                            <label class="col-sm-3 control-label">Enviar Correo</label>
                            <div class="col-sm-7 ckbox ckbox-theme">
                               <input id="sendMail" type="checkbox" name="sendMail" value="1" '<?php if ($_smarty_tpl->tpl_vars['vendor_email']->value==null) {?> disabled <?php }?>'>
                               <label for="sendMail" class=" control-label"></label>
                            </div>
                        </div>-->
                    <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                    <?php if ($_smarty_tpl->tpl_vars['status']->value=="agendada"||$_smarty_tpl->tpl_vars['status']->value=="reagendada") {?>
                    	<div class="col-sm-3">
                            <a href="<?php echo base_url('receipts/load_addReceipts');?>
/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="btn btn-sm btn-info btn-xs" style="height:32px;width: 100px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Crear Entrada</a>
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-warning">Reagendar</button>
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-danger" formaction="<?php echo base_url('appointment/cancel_appointment');?>
/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">Cancelar Cita</button>
                        </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="cancelada"||$_smarty_tpl->tpl_vars['status']->value=="cerrada") {?>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-danger" formaction="<?php echo base_url('appointment/delete_appointment');?>
/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">Borrar cita</button>
                        </div>
                     <?php }?>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
