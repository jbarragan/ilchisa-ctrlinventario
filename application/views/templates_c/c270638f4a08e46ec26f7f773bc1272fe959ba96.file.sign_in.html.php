<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-01 19:14:00
         compiled from "C:\xampp\htdocs\vic\application\views\templates\contents\sign\sign_in.html" */ ?>
<?php /*%%SmartyHeaderCode:874956d5dbe8d62ff1-67669977%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c270638f4a08e46ec26f7f773bc1272fe959ba96' => 
    array (
      0 => 'C:\\xampp\\htdocs\\vic\\application\\views\\templates\\contents\\sign\\sign_in.html',
      1 => 1456775592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '874956d5dbe8d62ff1-67669977',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56d5dbe8e519a6_88217508',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d5dbe8e519a6_88217508')) {function content_56d5dbe8e519a6_88217508($_smarty_tpl) {?><!-- Login form -->
<form class="sign-in form-horizontal shadow rounded no-overflow" action="<?php echo base_url('dashboard');?>
" method="post">
    <div class="sign-header">
        <div class="form-group">
            <div class="sign-text">
                <span>Member Area</span>
            </div>
        </div><!-- /.form-group -->
    </div><!-- /.sign-header -->
    <div class="sign-body">
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="text" class="form-control input-sm" placeholder="Username or email " name="username">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
        </div><!-- /.form-group -->
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="password" class="form-control input-sm" placeholder="Password" name="password">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
        </div><!-- /.form-group -->
    </div><!-- /.sign-body -->
    <div class="sign-footer">
        <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                    <div class="ckbox ckbox-theme">
                        <input id="rememberme" type="checkbox">
                        <label for="rememberme" class="rounded">Remember me</label>
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?php echo base_url('account/lost-password');?>
" title="lost password">Lost password?</a>
                </div>
            </div>
        </div><!-- /.form-group -->
        <div class="form-group">
            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Sign In</button>
        </div><!-- /.form-group -->
    </div><!-- /.sign-footer -->
</form><!-- /.form-horizontal -->
<!--/ Login form -->

<!-- Content text -->
<p class="text-muted text-center sign-link">Need an account? <a href="<?php echo base_url('account/sign-up');?>
"> Sign up free</a></p>
<!--/ Content text --><?php }} ?>
