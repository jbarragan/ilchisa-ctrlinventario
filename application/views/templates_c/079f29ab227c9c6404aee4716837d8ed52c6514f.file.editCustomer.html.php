<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-16 20:18:35
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\customers\editCustomer.html" */ ?>
<?php /*%%SmartyHeaderCode:32215a35718b7c4312-66639313%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '079f29ab227c9c6404aee4716837d8ed52c6514f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\customers\\editCustomer.html',
      1 => 1513371598,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32215a35718b7c4312-66639313',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cust' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a35718b80f770_82619707',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a35718b80f770_82619707')) {function content_5a35718b80f770_82619707($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-users fa-4x"></i> EDITAR CLIENTE</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
          
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="item" action="<?php echo base_url('customer/edit_customer');?>
/<?php echo $_smarty_tpl->tpl_vars['cust']->value['customer_code'];?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['cust']->value['first_name'];?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dirección<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="address" value="<?php echo $_smarty_tpl->tpl_vars['cust']->value['address'];?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefono<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['cust']->value['phone'];?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="email" value="<?php echo $_smarty_tpl->tpl_vars['cust']->value['email'];?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
