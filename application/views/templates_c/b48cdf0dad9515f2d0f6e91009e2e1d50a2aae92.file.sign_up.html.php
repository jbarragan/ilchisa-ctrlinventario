<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-12 20:27:20
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\sign_up.html" */ ?>
<?php /*%%SmartyHeaderCode:228795a302d9815a0d2-52190232%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b48cdf0dad9515f2d0f6e91009e2e1d50a2aae92' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\sign_up.html',
      1 => 1513038111,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '228795a302d9815a0d2-52190232',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'error_val' => 0,
    'Val_user' => 0,
    'user' => 0,
    'passwd' => 0,
    'passwd2' => 0,
    'Val_email' => 0,
    'email' => 0,
    'Val_rol' => 0,
    'auth_level' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a302d981c1483_20450321',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a302d981c1483_20450321')) {function content_5a302d981c1483_20450321($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-user fa-4x"></i> Crear Usuario</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Usuario a Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">
         <div class="panel rounded shadow">
            <div class="panel-body">
                <!-- Register form -->
                <form class="form-horizontal rounded shadow no-overflow" action="<?php echo base_url('account/sign_up');?>
" style="width:500px; margin: 0 auto;" method="post">
                    <div class="sign-body">
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $_smarty_tpl->tpl_vars['Val_user']->value;?>
">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['user']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="password" class="form-control" placeholder="Password" name="passwd">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="password" class="form-control " placeholder="Confirm Password" name="passwd2">
                                <span class="input-group-addon"><i class="fa fa-check"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd2']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_email']->value==null) {?> has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="email" class="form-control" placeholder="Your Email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['Val_email']->value;?>
">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['email']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_rol']->value==null) {?>has-error has-feedback<?php }?>">
                        <div class="input-group input-group-lg rounded no-overflow " name="username">
                            <select type="email" class="form-control" name="auth_level">
                            <option value=''>Rol de Usuario</option>
                            <option value=3>Bodega</option>
                            <option value=5>Supervisor</option>
                            <option value=6>Oficina</option>
                            <option value=9>Administrador</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-users"></i></span>
                        </div>
                        <?php if (isset($_smarty_tpl->tpl_vars['auth_level']->value)) {
echo $_smarty_tpl->tpl_vars['auth_level']->value;
}?>
                    </div>
                    </div>
                    <div class="sign-footer">
                        <div class="form-group">
                            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Sign Up</button>
                        </div>
                    </div>
                </form>
                <!--/ Register form -->
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
         <?php echo '<script'; ?>
>
        $("#logout").fadeTo(1000, 500).slideUp(500, function(){
            $("#logout").alert('close');
        });
        <?php echo '</script'; ?>
>
    </div>
</div><!-- /.row --><?php }} ?>
