<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 09:18:28
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/receipts/receiptLots.html" */ ?>
<?php /*%%SmartyHeaderCode:106983215757029fdcee31f6-68279858%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54dfa18d81d2819d5c92fabfc0ed9a49147f42df' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/receipts/receiptLots.html',
      1 => 1503277372,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '106983215757029fdcee31f6-68279858',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57029fdd16ebc5_49981228',
  'variables' => 
  array (
    'receipt' => 0,
    'con2' => 0,
    'date' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'items' => 0,
    'custom' => 0,
    'ht' => 0,
    'vendor' => 0,
    'tags' => 0,
    'numlots' => 0,
    'lots' => 0,
    'i' => 0,
    'conLots' => 0,
    'role' => 0,
    'loc' => 0,
    'x' => 0,
    'images' => 0,
    'img' => 0,
    'c' => 0,
    'extencion' => 0,
    'vicarCtrl' => 0,
    'up' => 0,
    'submit' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57029fdd16ebc5_49981228')) {function content_57029fdd16ebc5_49981228($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-right fa-4x"></i>ENTRADA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

                        <div id="success" class="alert alert-success animated fadeIn" style="display:none;">
                           
                        </div>
                        <div id="error" class="alert alert-danger animated fadeIn" style="display:none;">
                            <span class="alert-icon"><i class="fa fa-times"></i></span>
                            <strong>Error al enviar!</strong> verifique la dirección de correo electronico </a>.
                        </div>

        <!-- Start basic validation -->
            <div>
            	<?php  $_smarty_tpl->tpl_vars['con2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con2']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['receipt']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con2']->key => $_smarty_tpl->tpl_vars['con2']->value) {
$_smarty_tpl->tpl_vars['con2']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con2']->key;
?>
                <form class="form-horizontal" role="form" id="receipt"  method="post">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Vicar Ctrl<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm" name="receipt_vicar" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['vicar_ctrl'];?>
" readonly="true" >
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" >
                                <div class="row">
                                    <div class="col-sm-12 text-right" style="font-size:16px;">
                                        <?php echo $_smarty_tpl->tpl_vars['con2']->value['user'];?>

                                    </div> 
                                </div> 
                                <div class="row">
                                    <div class="col-sm-12text-right" style="font-size:20px;">
                                        <?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="nuevo actualizado") {?>
                                        	<span id="stat" class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                                        <?php } else { ?>
                                        	<span id="stat" class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                                        <?php }?>
                                    </div> 
                                </div> 
                            </div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                            
                                     <input type="text" class="form-control input-sm" name="date" id="date_rec" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="whse" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['whse_code'];?>
">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['whse_code']==$_smarty_tpl->tpl_vars['con2']->value['whse_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['con2']->value['id_appointment'];?>
</div> 
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Po. No.<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="po" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['po_no'];?>
">
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="item" tabindex="2">
                                    <option value="">Selecciona Producto</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['items_code']==$_smarty_tpl->tpl_vars['con2']->value['items_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>                            
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="customer" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['customer_code'];?>
">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['con2']->value['customer_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>

                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                            <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="ht" type="checkbox" name="ht" value="1" <?php echo $_smarty_tpl->tpl_vars['ht']->value;?>
>
                                    <label for="ht" class=" control-label">HT</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Proveedor<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="vendor" tabindex="2" >
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['vendor_code']==$_smarty_tpl->tpl_vars['con2']->value['vendor_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                             <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="tags" type="checkbox" name="tags" value="1" <?php echo $_smarty_tpl->tpl_vars['tags']->value;?>
>
                                    <label for="tags" class=" control-label">Etiquetas</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>
                         <?php } ?>  
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="<?php echo $_smarty_tpl->tpl_vars['numlots']->value;?>
">
                                <table id="datatable" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Lote</td>
                                        <td>Pallets</td>
                                        <td style="width:0px;"></td>
                                        <td>Bags</td>
                                        <td>Prod Date</td>
                                        <td>Exp Date</td>
                                        <td>Damage</td>
                                        <td>Location</td>
                                        <td>acción</td>
                                    </tr>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                    <?php $_smarty_tpl->tpl_vars['x'] = new Smarty_variable(0, null, 0);?>
                                    <?php  $_smarty_tpl->tpl_vars['conLots'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['conLots']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['lots']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['conLots']->key => $_smarty_tpl->tpl_vars['conLots']->value) {
$_smarty_tpl->tpl_vars['conLots']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['conLots']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                    <tr id="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                    	<td style="display:none;">
                                            <input type="text" name="idLots[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
">
                                        </td>
                                        <td>
                                            <input type="text" name="lots[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lot'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>'>
                                        </td>
                                        <td>
                                            <input type="number" name="pallets[]" id="pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['pallets'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>' min = '1'>
                                        </td> 
                                        <td style="width:0px">
                                            <input type="hidden" name="bags_pallets[]" id="bags_pallets" class="form-control bags_pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['bags_pallets'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>' >
                                        </td>
                                        <td>
                                            <input type="number" name="bags[]" id="bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['bags_total'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>' min = '1'>
                                        </td>
                                        <td>
                                            <input type="date" name="prod_date[]" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['prod_date'];?>
" id="prod" class="form-control" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?>  disabled <?php }?>' >
                                        </td>
                                        <td>
                                            <input type="date" name="exp_date[]" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['exp_date'];?>
" id="exp" class="form-control" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?>  disabled <?php }?>' >
                                        </td>  
                                        <td class="text-center">
                                            <select class="form-control" name="damage[]" tabindex="2" id="damage<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" style="width: 80px;">
                                                <option value="no" '<?php if ($_smarty_tpl->tpl_vars['conLots']->value['damage']=="no") {?> selected="true" <?php }?>'> &#10008</option>
                                                <option value="si" '<?php if ($_smarty_tpl->tpl_vars['conLots']->value['damage']=="si") {?> selected="true" <?php }?>'> &#10004</option>
                                            </select>
                                        </td>
                                        <td>
                                        <select class="form-control" name="whse_loc[]" tabindex="2" id="locat<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" style="width: 80px;">
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['id_location'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['conLots']->value['location'];?>
</option>
                                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['loc']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_location'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_location']==$_smarty_tpl->tpl_vars['conLots']->value['id_location']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['location'];?>
</option>
                                            <?php } ?>                                   
                                        </select>
                                        </td>
                                        <td class="text-center">
                                            <a id="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
" class="btn btn-danger btn-xs delLot" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                     <?php $_smarty_tpl->tpl_vars['x'] = new Smarty_variable($_smarty_tpl->tpl_vars['x']->value+1, null, 0);?>
                                     <?php } ?>
                                     <input type="hidden" name="index" class="" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                </table>
                                    <button type="button" class="btn btn-theme pull-right" id="addLots" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?> disabled <?php }?>'>Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Notas:</label>              
                                    <textarea class="form-control" rows="5" name="notes"><?php if (isset($_smarty_tpl->tpl_vars['receipt']->value['0']['notes'])) {?> <?php echo $_smarty_tpl->tpl_vars['receipt']->value['0']['notes'];?>
 <?php }?></textarea>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Pallets:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="total" placeholder="">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Imagenes</label>
                                        <div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(0, null, 0);?>
                                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['img']->key;
?>
                                            <div class="col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
">
                                                <?php if ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='image') {?>
                                                    <img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='pdf') {?>
                                                    <img src="<?php echo base_url('assets/global/img/pdf.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='doc') {?>
                                                    <img src="<?php echo base_url('assets/global/img/doc.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='xls') {?>
                                                    <img src="<?php echo base_url('assets/global/img/xls.png');?>
" width="100%">
                                                <?php }?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" target="_blank"> <?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
</a>
                                                <a id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" class="btn btn-danger btn-xs delImg" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar" style="position:absolute; top:1px;"><i class="fa fa-times"></i></a>
                                            </div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Cargar Imagenes</label> 
                                        <div action="<?php echo base_url('receipts/images');?>
/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vicarCtrl']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
" class="col-sm-12 dropzone pull-right">
                                            <div class="fallback">
                                                <input name="img" type="file" multiple />
                                            </div>
                                        </div><!-- /.panel-body -->
                                    </div>
                                </div>
                            </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                    <?php if ($_smarty_tpl->tpl_vars['receipt']->value[0]['status']!='Recibido') {?>
                        <?php if (!isset($_smarty_tpl->tpl_vars['receipt']->value[0]['email'])||$_smarty_tpl->tpl_vars['receipt']->value[0]['email']==null) {?>
                            <div class="callout callout-warning">
                                <strong>Aviso!</strong>Casilla <strong>Enviar Correo</strong> desabilitada, Cliente sin Email Asignado.
                            </div>
                         <?php }?>
                        <div id="boxMail" class="form-group col-sm-4">
                            <label class="col-sm-6 control-label">Enviar Correo</label>
                            <div class="col-sm-1 ckbox ckbox-theme">
                               <input id="sendMail" type="checkbox" name="sendMail" value="1" <?php if (!isset($_smarty_tpl->tpl_vars['receipt']->value[0]['email'])||$_smarty_tpl->tpl_vars['receipt']->value[0]['email']==null) {?> disabled <?php }?>>
                               <label for="sendMail" class=" control-label"></label>
                            </div>
                        </div>
                    <?php }?>
                        
                        <div id="button" class="col-sm-offset-3">
                        <?php if ($_smarty_tpl->tpl_vars['role']->value=='Supervisor') {
echo $_smarty_tpl->tpl_vars['up']->value;
}?>
                           <?php echo $_smarty_tpl->tpl_vars['submit']->value;?>

                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div>
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
