<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-13 17:26:57
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\partials\header.html" */ ?>
<?php /*%%SmartyHeaderCode:167435a300722447646-82152886%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd1b1ddc557185f1dbe15d1e8b20a010116c68049' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\partials\\header.html',
      1 => 1513182411,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167435a300722447646-82152886',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a3007224951c6_59603581',
  'variables' => 
  array (
    'employees' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3007224951c6_59603581')) {function content_5a3007224951c6_59603581($_smarty_tpl) {?><header id="header">

<!-- Start header left -->
<div class="header-left">
    <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
    <div class="navbar-minimize-mobile left">
        <i class="fa fa-bars"></i>
    </div>
    <!--/ End offcanvas left -->

    <!-- Start navbar header -->
    <div class="navbar-header">

        <!-- Start brand -->
        <a class="navbar-brand" href="<?php echo base_url('dashboard/');?>
"">
            <img class="logo" src="<?php echo base_url('assets/global/img/logo-reny-picot.png');?>
" alt="Reny Picot logo" width="220" height="65" />
        </a><!-- /.navbar-brand -->
        <!--/ End brand -->

    </div><!-- /.navbar-header -->
    <!--/ End navbar header -->

    <div class="clearfix"></div>
</div><!-- /.header-left -->
<!--/ End header left -->

<!-- Start header right -->
<div class="header-right">
<!-- Start navbar toolbar -->
<div class="navbar navbar-toolbar navbar-dark">

<!-- Start left navigation -->
<ul class="nav navbar-nav navbar-left">

    <!-- Start sidebar shrink -->
    <li class="navbar-minimize">
        <a href="javascript:void(0);" title="Minimize sidebar">
            <i class="fa fa-bars"></i>
        </a>
    </li>
    <!--/ End sidebar shrink -->

</ul><!-- /.navbar-left -->
<!--/ End left navigation -->

<!-- Start right navigation -->
<ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->
<?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>

<!-- Start notifications -->
<li class="dropdown navbar-notification">

    <a href="#" id="notify" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i>
    <span class="rounded count label label-info" id="countNoty"></span></a>

    <!-- Start dropdown menu -->
    <div class="dropdown-menu animated flipInX">
        <div class="dropdown-header">
            <span class="title">Notificationes <strong id="load13"></strong></span>
        </div>
        <div class="dropdown-body niceScroll">

            <!-- Start notification list -->
            <div id="load" class="media-list small">

                <!--/ End notification indicator -->
            </div>
            <!--/ End notification list -->

        </div>
        <div class="dropdown-footer">
            <a href="#">See All</a>
        </div>
    </div>
    <!--/ End dropdown menu -->

</li><!-- /.dropdown navbar-notification -->
<!--/ End notifications -->
<?php }?>
<!-- Start profile -->
<li class="dropdown navbar-profile">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                                    <span class="avatar"><img src="<?php echo base_url('assets/global/img/icon/64/contact.png');?>
" class="img-circle" alt="admin"></span>
                                    <span class="text hidden-xs hidden-sm text-muted">Perfil</span>
                                    <span class="caret"></span>
                                </span>
    </a>
    <!-- Start dropdown menu -->
    <ul class="dropdown-menu animated flipInX">
        <li><a href="#"><i class="fa fa-user"></i>View profile</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo base_url('account/logout');?>
"><i class="fa fa-sign-out"></i>Logout</a></li>
    </ul>
    <!--/ End dropdown menu -->
</li><!-- /.dropdown navbar-profile -->
<!--/ End profile -->

<!-- Start settings 
<li class="navbar-setting pull-right">
    <a href="javascript:void(0);"><i class="fa fa-cog fa-spin"></i></a>
</li>--><!-- /.navbar-setting pull-right -->
<!--/ End settings -->

</ul><!-- /.navbar-right -->
<!--/ End right navigation -->

</div><!-- /.navbar-toolbar -->
<!--/ End navbar toolbar -->
</div><!-- /.header-right -->
<!--/ End header left -->

</header> <!-- /#header --><?php }} ?>
