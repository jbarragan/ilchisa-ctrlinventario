<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-15 19:58:46
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\producer\newproducer.html" */ ?>
<?php /*%%SmartyHeaderCode:325125a330e13c67716-10205653%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '72b6750e0655535a6f0fc5dd1c549542192f35a9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\producer\\newproducer.html',
      1 => 1513362265,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '325125a330e13c67716-10205653',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a330e13d65124_93184439',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a330e13d65124_93184439')) {function content_5a330e13d65124_93184439($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> NUEVO PRODUCTOR</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Nuevo Productor</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
           
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="producer" action="<?php echo base_url('producer/add_producer');?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name_producer" placeholder="">
                            </div>
                        </div>
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
