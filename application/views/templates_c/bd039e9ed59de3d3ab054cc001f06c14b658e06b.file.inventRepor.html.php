<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-05 17:56:16
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/reports/inventRepor.html" */ ?>
<?php /*%%SmartyHeaderCode:9832381095a395ef8448159-82343750%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd039e9ed59de3d3ab054cc001f06c14b658e06b' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/reports/inventRepor.html',
      1 => 1515200130,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9832381095a395ef8448159-82343750',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a395ef84aed44_23248522',
  'variables' => 
  array (
    'employees' => 0,
    'datIn' => 0,
    'datFin' => 0,
    'brand' => 0,
    'con' => 0,
    'plant' => 0,
    'clasification' => 0,
    'item' => 0,
    'custom' => 0,
    'customer' => 0,
    'items' => 0,
    'whse' => 0,
    'whse_sel' => 0,
    'invList' => 0,
    'custoUser' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a395ef84aed44_23248522')) {function content_5a395ef84aed44_23248522($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-usd fa-4x"></i> REPORTE IINVENTARIO</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estaás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Reportes</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Inventario</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <div>
        <?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>
        	<form id="receiptReport" method="get">
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label class="control-label">Fecha Inicial</label>                            
                             <input type="text" class="form-control input-sm" name="dateIn" placeholder="dd/M/yyyy" id="date" value="<?php echo $_smarty_tpl->tpl_vars['datIn']->value;?>
">
                    </div><!-- /.form-group -->
                    <div class="form-group col-sm-2">
                        <label class="control-label">A la fecha:</label>                            
                             <input type="text" class="form-control input-sm" name="dateFin" placeholder="dd/M/yyyy" id="date2" value="<?php echo $_smarty_tpl->tpl_vars['datFin']->value;?>
">
                    </div><!-- /.form-group -->

                    <div class="form-group col-sm-2">
                        <label class="control-label">Marca</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="brand" name="brand" tabindex="2">
                            <option value="">Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['brand']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_brand'];?>
" <?php if ($_smarty_tpl->tpl_vars['brand']->value==$_smarty_tpl->tpl_vars['con']->value['id_brand']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['name_brand'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>

                    <div class="form-group col-sm-2">
                        <label class="control-label">Planta</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="plant" name="plant" tabindex="2">
                            <option value="">Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['plant']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_plant'];?>
" <?php if ($_smarty_tpl->tpl_vars['plant']->value==$_smarty_tpl->tpl_vars['con']->value['id_plant']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['name_plant'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>

                    <div class="form-group col-sm-2">
                        <label class="control-label">Clasificacion</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="clasification" name="clasification" tabindex="2">
                            <option value="">Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['clasification']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_clasification'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value==$_smarty_tpl->tpl_vars['con']->value['id_clasification']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['name_clasification'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                </div> 
        		<div class="row">
                    <div class="form-group col-sm-3">
                        <label class="control-label">Cliente</label>                            
                        <select data-placeholder="Choose a Country" class="form-control chosen-select mb-15" id="customer" name="customer" tabindex="2">
                            <option value="">Selecciona Cliente</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['customer']->value==$_smarty_tpl->tpl_vars['con']->value['customer_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Producto</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="item" name="item" tabindex="2">
                            <option value="">Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value==$_smarty_tpl->tpl_vars['con']->value['items_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Bodega</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="whse" name="whse" tabindex="2">
                            <option value="">Selecciona Bodega</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whse']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['whse_sel']->value==$_smarty_tpl->tpl_vars['con']->value['whse_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                     <div class="form-group col-sm-3">
                       <!-- <a <?php if (!empty($_smarty_tpl->tpl_vars['invList']->value)) {?>href="<?php echo base_url('reports/reporinvent/');?>
?dateIn=<?php echo $_smarty_tpl->tpl_vars['datIn']->value;?>
&dateFin=<?php echo $_smarty_tpl->tpl_vars['datFin']->value;?>
&customer=<?php echo $_smarty_tpl->tpl_vars['customer']->value;?>
&item=<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
&whse=<?php echo $_smarty_tpl->tpl_vars['whse_sel']->value;?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin-top:29px" target="_blank">Exportar PDF</a>-->
                        <input type="button" id="ToExcelInvent" class="btn btn-theme pull-right" value="Exportar Excel" style="margin-top:29px; margin-right: 10px"/>
                    </div>
                </div> 
            </form>
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcelInvent">
                <input type="hidden" id="tablaExcelInvent" name="table" />
                <input type="hidden" name="filename" value="Reporte_Inventario" />
            </form>
        </div>
        <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['employees']->value=='customer') {?>

            <input type="hidden" id="idUserCustom" name="idUserCustom" value="<?php echo $_smarty_tpl->tpl_vars['custoUser']->value;?>
" />
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcelInvent">
                <input type="hidden" id="tablaExcelInvent" name="table" />
                <input type="hidden" name="filename" value="Reporte_Inventario" />
            </form>
            <!-- <a <?php if (!empty($_smarty_tpl->tpl_vars['invList']->value)) {?> href="<?php echo base_url('report- {s/reporinvent/');?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin:15px 20px;"  target="_blank">Exportar PDF</a>-->

            <input type="button" id="ToExcelInvent" class="btn btn-theme pull-right" value="Exportar Excel" style="margin:15px 20px;"/>
            <?php }?>
        </div>
        <!-- Start datatable using ajax -->
                <div>
                    <!-- Start datatable -->
                    <span><?php echo $_smarty_tpl->tpl_vars['employees']->value;?>
</span>
                     <table id="datatableInvent" class="table table-striped table-theme text-center">
                        <thead>
                        <tr>
                            <th data-hide="phone" class="text-center">No. Ctrl</th> 
                            <th data-hide="phone" class="text-center">Fecha de Entrada</th> 
                            <th data-class="expand" class="text-center">Cliente</th>
                            <th data-hide="phone" class="text-center">Proveedor</th>
                            <th data-hide="phone" class="text-center">Producto</th>
                            <th data-hide="phone" class="text-center">Marca</th>
                            <th data-hide="phone" class="text-center">Lote</th> 
                            <th data-hide="phone" class="text-center">Fecha Produccion</th>
                            <th data-hide="phone" class="text-center">Fecha Expiración</th>   
                            <th data-hide="phone" class="text-center">KG/Bolsa</th>                         
                            <th data-hide="phone" class="text-center">Bags</th>
                            <?php if ($_smarty_tpl->tpl_vars['employees']->value!='customer') {?> 
                            <th data-hide="phone" class="text-center">Ubicación</th>
                            <th data-hide="phone" class="text-center">Almacenamiento</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
