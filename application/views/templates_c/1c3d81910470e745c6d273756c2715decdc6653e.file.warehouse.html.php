<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-18 19:56:45
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\warehouse\warehouse.html" */ ?>
<?php /*%%SmartyHeaderCode:182935a302d2f7719b9-95691945%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1c3d81910470e745c6d273756c2715decdc6653e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\warehouse\\warehouse.html',
      1 => 1513371670,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182935a302d2f7719b9-95691945',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a302d2f7d75f2_70733151',
  'variables' => 
  array (
    'list_whse' => 0,
    'con' => 0,
    'list_location' => 0,
    'con2' => 0,
    'list_lote_loc' => 0,
    'con3' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a302d2f7d75f2_70733151')) {function content_5a302d2f7d75f2_70733151($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-building fa-4x"></i> BODEGAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Bodega</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-6">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Bodegas</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="warehouse" class="table table-striped table-theme">
                        <thead>
                        <tr>
                            <th data-hide="phone" class="text-center" style="max-width: 20px">Codigo</th>      
                            <th data-class="expand">Nombre</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone" class="text-center">Acción</th>
                            <th style="display:none;"></th>
                            <th style="display:none;"></th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                         <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_whse']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td class="text-center" style="width: 1%"><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['address'];?>
</td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('warehouse/load_edit');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Editar</a>
                                    <a href="<?php echo base_url('warehouse/delete_whse');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                                <th style="display:none;"></th>
                                <th style="display:none;"></th>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
                <a class="btn btn-sm btn-success btn-xs btn-push float-right" data-toggle="modal" data-target=".location"></i> Agregar Location</a>
            <!--/ End datatable using ajax -->
        </div>
        <div class="col-md-6">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Locaciones</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="location" class="table table-striped table-theme">
                        <thead>
                        <tr>
                           <th data-class="expand" style="max-width: 20px">Codigo</th>       
                            <th data-class="phone" class="text-center">locación</th>
                            <th data-hide="phone" class="text-center">status</th>
                            <th data-hide="phone" class="text-center">Lote</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con2']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_location']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con2']->key => $_smarty_tpl->tpl_vars['con2']->value) {
$_smarty_tpl->tpl_vars['con2']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con2']->key;
?>
                            <tr>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con2']->value['id_location'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con2']->value['location'];?>
</td>
                                <td class="text-center">    
                                    <?php if ($_smarty_tpl->tpl_vars['con2']->value['status']==1) {?>
                                        Ocupado
                                    <?php } else { ?>Vacio<?php }?>
                                </td>
                                <td class="text-center"><?php  $_smarty_tpl->tpl_vars['con3'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con3']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_lote_loc']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con3']->key => $_smarty_tpl->tpl_vars['con3']->value) {
$_smarty_tpl->tpl_vars['con3']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con3']->key;
?>
                                                    <?php if ($_smarty_tpl->tpl_vars['con2']->value['id_location']==$_smarty_tpl->tpl_vars['con3']->value['id_location']) {?>
                                                    <?php echo $_smarty_tpl->tpl_vars['con3']->value['lot'];?>

                                                    <?php }?>
                                     <?php } ?>
                                </td>
                                <th style="display:none;"></th>
                                <th style="display:none;"></th>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
      <div class="row">
        
    </div>
</div>
<!-- Start inside form layout -->
            <div class="modal fade location" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-photo-viewer">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Locations</h4>
                        </div>
                        <div class="modal-body no-padding">
                            <form class="form-horizontal form-bordered" role="form" method="post" action="<?php echo base_url('warehouse/insert_location');?>
">
                                <div class="form-body">
                                    <div class="form-group">
                                        <label for="firstname-1" class="col-sm-4 control-label">Prefijo</label>
                                        <div class="col-sm-1">
                                            <input type="text" class="form-control input-sm" id="firstname-1" name="prefijo" style="text-transform:uppercase;">
                                        </div>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="password-1" class="col-sm-4 control-label">Rango</label>
                                        <div class="col-sm-1">
                                            <input type="number" name="rango_ini" class="form-control input-sm" id="password-1">
                                        </div>
                                        <label for="password-1" class="col-sm-1 control-label" style="padding-right:40px;">-</label>
                                        <div class="col-sm-1">
                                            <input type="number" name="rango_fin" class="form-control input-sm" id="password-1">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-4">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-success">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                        </div>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!--/ End inside form layout -->
<?php }} ?>
