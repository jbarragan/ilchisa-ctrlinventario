<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-12 14:04:34
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/reports/receiptCustomer.html" */ ?>
<?php /*%%SmartyHeaderCode:17319347355a4d23b5bd22f4-82334715%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cd0c853eb6b4a9cacb1df41875b5cfd28daa537f' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/reports/receiptCustomer.html',
      1 => 1515786822,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17319347355a4d23b5bd22f4-82334715',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a4d23b5ceab44_14016504',
  'variables' => 
  array (
    'receipt' => 0,
    'con2' => 0,
    'date' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'numlots' => 0,
    'lots' => 0,
    'i' => 0,
    'conLots' => 0,
    'role' => 0,
    'loc' => 0,
    'x' => 0,
    'images' => 0,
    'img' => 0,
    'c' => 0,
    'extencion' => 0,
    'submit' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a4d23b5ceab44_14016504')) {function content_5a4d23b5ceab44_14016504($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-right fa-4x"></i>ENTRADA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

                        <div id="success" class="alert alert-success animated fadeIn" style="display:none;">
                           
                        </div>
                        <div id="error" class="alert alert-danger animated fadeIn" style="display:none;">
                            <span class="alert-icon"><i class="fa fa-times"></i></span>
                            <strong>Error al enviar!</strong> verifique la dirección de correo electronico </a>.
                        </div>

        <!-- Start basic validation -->
            <div>
            	<?php  $_smarty_tpl->tpl_vars['con2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con2']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['receipt']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con2']->key => $_smarty_tpl->tpl_vars['con2']->value) {
$_smarty_tpl->tpl_vars['con2']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con2']->key;
?>
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <p class="h4"><b>No. Ctrl:</b><br>
                                     <?php echo $_smarty_tpl->tpl_vars['con2']->value['reny_ctrl'];?>
</p>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" >
                                <div class="row">
                                    <div class="col-sm-12 text-right" style="font-size:16px;">
                                        <?php echo $_smarty_tpl->tpl_vars['con2']->value['user'];?>

                                    </div> 
                                </div> 
                                <div class="row">
                                    <div class="col-sm-12text-right" style="font-size:20px;">
                                        <?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="nuevo actualizado") {?>
                                        	<span id="stat" class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                                        <?php } else { ?>
                                        	<span id="stat" class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                                        <?php }?>
                                    </div> 
                                </div> 
                            </div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <p class="h4"><b>Fecha:</b><br>      
                                    <?php echo $_smarty_tpl->tpl_vars['date']->value;?>

                                </p>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="form-control mb-15" name="whse" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['whse_code'];?>
" readonly="true" disabled>
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['whse_code']==$_smarty_tpl->tpl_vars['con2']->value['whse_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Po. No.<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="po" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['po_no'];?>
" readonly>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="customer" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['first_name'];?>
" readonly>             
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="item" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['item_name'];?>
" readonly>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class=" control-label">Proveedor<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="vendor" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['name'];?>
" readonly>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Productor<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="producer" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['name_producer'];?>
" readonly>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Marca<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="brand" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['name_brand'];?>
" readonly>
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Planta<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="plant" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['name_plant'];?>
" readonly>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Clasificacion<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="clasification" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['name_clasification'];?>
" readonly>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-4">
                                <label class="control-label">Recibido</label>
                                <input type="text" class="form-control input-sm" name="received" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['received'];?>
" readonly>
                            </div><!-- /.form-group -->
                        </div>
                         <?php } ?>  
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="<?php echo $_smarty_tpl->tpl_vars['numlots']->value;?>
">
                                <table id="datatable" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Lote</td>
                                        <td>KG/Bolsa</td>
                                        <td style="width:0px;"></td>
                                        <td>Bags</td>
                                        <td>Prod Date</td>
                                        <td>Exp Date</td>
                                        <td>Damage</td>
                                        <td style="width:110px;">AMT Damage</td>
                                        <td>Location</td>
                                    </tr>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                    <?php $_smarty_tpl->tpl_vars['x'] = new Smarty_variable(0, null, 0);?>
                                    <?php  $_smarty_tpl->tpl_vars['conLots'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['conLots']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['lots']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['conLots']->key => $_smarty_tpl->tpl_vars['conLots']->value) {
$_smarty_tpl->tpl_vars['conLots']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['conLots']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                    <tr id="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                    	<td style="display:none;">
                                            <?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>

                                        </td>
                                        <td>
                                            <input type="text" name="lots[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lot'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>'>
                                        </td>
                                        <td>
                                            <input type="number" name="pallets[]" id="pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['pallets'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>' min = '1'>
                                        </td> 
                                        <td style="width:0px">
                                            <input type="hidden" name="bags_pallets[]" id="bags_pallets" class="form-control bags_pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['bags_pallets'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>' >
                                        </td>
                                        <td>
                                            <input type="number" name="bags[]" id="bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['bags_total'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?> readonly <?php }?>' min = '1'>
                                        </td>
                                        <td>
                                            <input type="date" name="prod_date[]" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['prod_date'];?>
" id="prod" class="form-control" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?>  disabled <?php }?>' >
                                        </td>
                                        <td>
                                            <input type="date" name="exp_date[]" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['exp_date'];?>
" id="exp" class="form-control" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido"&&$_smarty_tpl->tpl_vars['role']->value!="Supervisor") {?>  disabled <?php }?>' >
                                        </td>  
                                        <td class="text-center">
                                            <select class="form-control" name="damage[]" tabindex="2" id="damage<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" disabled >
                                                <option value="0" '<?php if ($_smarty_tpl->tpl_vars['conLots']->value['damage']=="0") {?> selected="true" <?php }?>'>OK</option>
                                                <option value="1" '<?php if ($_smarty_tpl->tpl_vars['conLots']->value['damage']=="1") {?> selected="true" <?php }?>'>Roto</option>
                                                <option value="2" '<?php if ($_smarty_tpl->tpl_vars['conLots']->value['damage']=="2") {?> selected="true" <?php }?>'>Faltante</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" name="amtDamage[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['amtDamage'];?>
" readonly>
                                        </td>
                                        <td>
                                        <select class="form-control" name="whse_loc[]" tabindex="2" id="locat<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" style="width: 80px;" disabled>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['id_location'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['conLots']->value['location'];?>
</option>
                                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['loc']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_location'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_location']==$_smarty_tpl->tpl_vars['conLots']->value['id_location']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['location'];?>
</option>
                                            <?php } ?>                                   
                                        </select>
                                        </td>
                                    </tr>
                                     <?php $_smarty_tpl->tpl_vars['x'] = new Smarty_variable($_smarty_tpl->tpl_vars['x']->value+1, null, 0);?>
                                     <?php } ?>
                                     <input type="hidden" name="index" class="" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Notas:</label>  <br>            
                                    <?php if (isset($_smarty_tpl->tpl_vars['receipt']->value['0']['notes'])) {?>
                                        <?php echo $_smarty_tpl->tpl_vars['receipt']->value['0']['notes'];?>
 
                                    <?php }?>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total KG/Bags:</label>
                                     <input type="text" class="form-control input-sm" id="total" placeholder="" disabled>
                                <label class="control-label">Total Bags:</label>
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="" disabled>
                            </div><!-- /.form-group -->
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Imagenes:</label>
                                        <div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(0, null, 0);?>
                                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['img']->key;
?>
                                            <div class="col-sm-2" id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
">
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" target="_blank">
                                                <?php if ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='image') {?>
                                                    <img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='pdf') {?>
                                                    <img src="<?php echo base_url('assets/global/img/pdf.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='doc') {?>
                                                    <img src="<?php echo base_url('assets/global/img/doc.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='xls') {?>
                                                    <img src="<?php echo base_url('assets/global/img/xls.png');?>
" width="100%">
                                                <?php }?>
                                                </a>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" target="_blank"> <?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
</a>
                                            </div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div id="button" class="col-sm-offset-3">
                           <?php echo $_smarty_tpl->tpl_vars['submit']->value;?>

                        </div>
                    </div><!-- /.form-footer -->
            </div>
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
