<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-20 15:19:45
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/appointment/newAppointment.html" */ ?>
<?php /*%%SmartyHeaderCode:10180550365a3ae201315434-87169783%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2428fb79de0c2577d80339208a28c1fb78a59423' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/appointment/newAppointment.html',
      1 => 1513708385,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10180550365a3ae201315434-87169783',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'date' => 0,
    'custom' => 0,
    'con' => 0,
    'vendor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a3ae201358dc0_91249767',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3ae201358dc0_91249767')) {function content_5a3ae201358dc0_91249767($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-calendar fa-4x"></i> AGENDAR CITA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Agendar Cita</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Agendar Cita</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="appoint" action="<?php echo base_url('appointment/create');?>
" method="post">
                    <div class="form-body">
                        <div class="form-group">
                                <label class="col-sm-3 control-label">Fecha <span class="asterisk">*</span></label>
                            <div class="col-sm-7">                         
                                 <input type="text" class="form-control input-sm" name="date" id="date" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Po No<span class="asterisk">*</span></label>
                           	<div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="po" >
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cliente<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <select data-placeholder="Selecciona al Cliente" class="form-control mb-15" name="customer" tabindex="2">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Proveedor<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <select data-placeholder="Selcciona al proveedor" id="vendor" class="form-control mb-15" name="vendor" tabindex="2">
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div>
                        </div><!-- /.form-group -->
                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label">Enviar Correo</label>
                            <div class="col-sm-7 ckbox ckbox-theme">
                               <input id="sendMail" type="checkbox" name="sendMail" value="1">
                               <label for="sendMail" class=" control-label"></label>
                            </div>
                        </div> -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
