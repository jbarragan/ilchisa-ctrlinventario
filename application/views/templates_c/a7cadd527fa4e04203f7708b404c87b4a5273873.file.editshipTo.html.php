<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-04 10:06:05
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/customers/editshipTo.html" */ ?>
<?php /*%%SmartyHeaderCode:2317252105a4e5efd81df27-11926622%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7cadd527fa4e04203f7708b404c87b4a5273873' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/customers/editshipTo.html',
      1 => 1513708391,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2317252105a4e5efd81df27-11926622',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id' => 0,
    'name' => 0,
    'state' => 0,
    'city' => 0,
    'zip' => 0,
    'address' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a4e5efd85ca01_95855489',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a4e5efd85ca01_95855489')) {function content_5a4e5efd85ca01_95855489($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-users fa-4x"></i> Agregar Ship to</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Agregar Ship</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="item" action="<?php echo base_url('customer/edit_ship');?>
/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Estado<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="state" value="<?php echo $_smarty_tpl->tpl_vars['state']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ciudad<span class="asterisk">*</span></label>
                           <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="city" value="<?php echo $_smarty_tpl->tpl_vars['city']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Codigo Postal<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="zip" value="<?php echo $_smarty_tpl->tpl_vars['zip']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dirección<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="address" value="<?php echo $_smarty_tpl->tpl_vars['address']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefono<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="phone" value="<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
">
                            </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                        	<a href="<?php echo base_url('customer/search');?>
" class="btn btn-theme"><span class="icon"><i class="fa fa-arrow-left"></i></span> Regresar</a>
                            <button type="submit" class="btn btn-theme">Actualizar</button>
                            <a href="<?php echo base_url('customer/delete_ship');?>
/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" class="btn btn-theme" onclick="return confirm('Realmente deseas eliminar Este registro?')">Eliminar</a>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->

<?php echo '<script'; ?>
 language="JavaScript"> 
    function confirmar(){ 
		if(confirm("Realmente desea eliminar este registro?"))
	    {
	        return true;
	    }
	    return false;
	} 
<?php echo '</script'; ?>
><?php }} ?>
