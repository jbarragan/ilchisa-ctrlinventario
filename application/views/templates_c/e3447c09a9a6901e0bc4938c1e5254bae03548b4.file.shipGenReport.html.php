<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 11:43:42
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/reports/shipGenReport.html" */ ?>
<?php /*%%SmartyHeaderCode:1651969635570eeaad504765-22028087%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3447c09a9a6901e0bc4938c1e5254bae03548b4' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/reports/shipGenReport.html',
      1 => 1503277496,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1651969635570eeaad504765-22028087',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_570eeaad5a86b4_14632881',
  'variables' => 
  array (
    'employees' => 0,
    'datIn' => 0,
    'datFin' => 0,
    'status' => 0,
    'custom' => 0,
    'con' => 0,
    'customer' => 0,
    'items' => 0,
    'item' => 0,
    'shipList' => 0,
    'custoUser' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570eeaad5a86b4_14632881')) {function content_570eeaad5a86b4_14632881($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-left fa-4x"></i> REPORTE SALIDAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Reportes</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Salidas</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
         <?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>
        <div>
        	<form id="receiptReport" action="<?php echo base_url('reports/searchShipRep');?>
" method="get">
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label class="control-label">Fecha Inicial</label>                            
                             <input type="text" class="form-control input-sm" name="dateIn" placeholder="mm/dd/yyy" id="date" value="<?php echo $_smarty_tpl->tpl_vars['datIn']->value;?>
">
                    </div><!-- /.form-group -->
                    <div class="form-group col-sm-2">
                        <label class="control-label">A la fecha:</label>                            
                             <input type="text" class="form-control input-sm" name="dateFin" placeholder="mm/dd/yyy" id="date2" value="<?php echo $_smarty_tpl->tpl_vars['datFin']->value;?>
">
                    </div><!-- /.form-group -->
                </div> 
        		<div class="row">
                    <div class="form-group col-sm-2">
                        <label class="control-label">status</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="status" name="status" tabindex="2">
                            <option value=0 <?php if ($_smarty_tpl->tpl_vars['status']->value==0) {?> selected <?php }?>>Selecciona status</option>
                            <option value=1 <?php if ($_smarty_tpl->tpl_vars['status']->value==1) {?> selected <?php }?>>Nuevo</option>
                            <option value=2 <?php if ($_smarty_tpl->tpl_vars['status']->value==2) {?> selected <?php }?>>Listo</option>
                            <option value=3 <?php if ($_smarty_tpl->tpl_vars['status']->value==3) {?> selected <?php }?>>listo modificado</option>
                            <option value=4 <?php if ($_smarty_tpl->tpl_vars['status']->value==4) {?> selected <?php }?>>Terminado</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Cliente</label>                            
                        <select data-placeholder="Choose a Country" class="form-control chosen-select mb-15" id="customer" name="customer" tabindex="2">
                            <option value=0>Selecciona Cliente</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['customer']->value==$_smarty_tpl->tpl_vars['con']->value['customer_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Producto</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="item" name="item" tabindex="2">
                            <option value=0>Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value==$_smarty_tpl->tpl_vars['con']->value['items_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                       <!-- <a <?php if (!empty($_smarty_tpl->tpl_vars['shipList']->value)) {?>href="<?php echo base_url('reports/ReporShip/');?>
?dateIn=<?php echo $_smarty_tpl->tpl_vars['datIn']->value;?>
&dateFin=<?php echo $_smarty_tpl->tpl_vars['datFin']->value;?>
&customer=<?php echo $_smarty_tpl->tpl_vars['customer']->value;?>
&item=<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
&status=<?php echo $_smarty_tpl->tpl_vars['status']->value;?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin-top:29px" target="_blank">Exportar PDF</a>-->
                        <input type="button" id="ToExcelShip" class="btn btn-theme pull-right" value="Exportar Excel" style="margin-top:29px; margin-right: 10px"/>
                    </div>
                </div> 
            </form>
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcelShip">
                <input type="hidden" id="tablaExcelShip" name="table" />
                <input type="hidden" name="filename" value="Reporte_Salidas" />
            </form>
        </div>
        <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['employees']->value=='customer') {?>
            
                <input type="hidden" id="idUserCustom" name="idUserCustom" value="<?php echo $_smarty_tpl->tpl_vars['custoUser']->value;?>
" />
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcelShip">
                <input type="hidden" id="tablaExcelShip" name="table" />
                <input type="hidden" name="filename" value="Reporte_Salidas" />
            </form>
            <!--<a <?php if (!empty($_smarty_tpl->tpl_vars['shipList']->value)) {?> href="<?php echo base_url('reports/ReporShip/');?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin:15px 20px;"  target="_blank">Exportar PDF</a>-->

            <input type="button" id="ToExcelShip" class="btn btn-theme pull-right m-r-2" value="Exportar Excel" style="margin:15px 20px;"/>
            <?php }?>
                <div>
                <table id="datatableShip" class="table table-striped table-danger">
                        <thead>
                        <tr> 
                        	<th data-class="expand">No. de Salida</th>
		                    <th data-hide="phone">Estatus</th>     
		                    <th data-hide="phone">Fecha Registro</th>
		                    <th data-hide="phone">Fecha Salida</th>
		                    <th data-hide="phone">Carrier</th>
		                    <th data-hide="phone">Trailer</th>
		                    <th data-hide="phone">Factura</th>
		                    <th data-hide="phone">Vicar Ctrl</th>
		                    <th data-hide="phone">Cliente</th>
		                    <th data-hide="phone">Producto</th>
		                    <th data-hide="phone">Lote</th>
		                    <th data-hide="phone">Pallets</th>
		                    <th data-hide="phone">Bags</th>
                            <?php if ($_smarty_tpl->tpl_vars['employees']->value!='customer') {?> 
		                    <th data-hide="phone">storage</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody class="text-center">
                        </tbody>
                    </table>
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
