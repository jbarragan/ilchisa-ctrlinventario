<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-25 08:09:00
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/edit_users.html" */ ?>
<?php /*%%SmartyHeaderCode:96276391357338985930ff6-74386980%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8ebd12fee611b30f9e006edc42d1097d150334a3' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/edit_users.html',
      1 => 1503277208,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '96276391357338985930ff6-74386980',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57338985aa7b02_66307390',
  'variables' => 
  array (
    'val' => 0,
    'error_val' => 0,
    'Val_user' => 0,
    'user' => 0,
    'Val_email' => 0,
    'email' => 0,
    'Val_rol' => 0,
    'auth_level' => 0,
    'passwd' => 0,
    'passwd2' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57338985aa7b02_66307390')) {function content_57338985aa7b02_66307390($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-user fa-4x"></i> Editar Usuario</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Usuario a Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">
         <div class="panel rounded shadow">
            <div class="panel-body">
                <!-- Register form -->
                <form class="form-horizontal rounded shadow no-overflow" action="<?php echo base_url('account/edit_user');?>
/<?php echo $_smarty_tpl->tpl_vars['val']->value['user_id'];?>
" style="width:500px; margin: 0 auto;" method="post">
                    <div class="sign-body">
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $_smarty_tpl->tpl_vars['val']->value['username'];?>
">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['user']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_email']->value==null) {?> has-error has-feedback<?php }?>">
                            <div class="input-group input-group-lg rounded no-overflow">
                                <input type="email" class="form-control" placeholder="Your Email" name="email" value="<?php echo $_smarty_tpl->tpl_vars['val']->value['email'];?>
">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            </div>
                            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['email']->value;
}?>
                        </div>
                        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_rol']->value==null) {?>has-error has-feedback<?php }?>">
                        <div class="input-group input-group-lg rounded no-overflow " name="username">
                            <select type="email" class="form-control" name="auth_level">
                            <?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==1) {?>
                            <option value=1 '<?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==1) {?> selected="true" <?php }?>'>Cliente</option>
                            <?php } else { ?>
                            <option value=3 '<?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==3) {?> selected="true" <?php }?>'>Bodega</option>
                            <option value=3 '<?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==3) {?> selected="true" <?php }?>'>Bodega</option>
                            <option value=6 '<?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==6) {?> selected="true" <?php }?>'>Oficina</option>
                            <option value=9 '<?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==9) {?> selected="true" <?php }?>'>Administrador</option>
                            <option value=5 '<?php if ($_smarty_tpl->tpl_vars['val']->value["auth_level"]==5) {?> selected="true" <?php }?>'>Supervisor</option>
                            <?php }?>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-users"></i></span>
                        </div>
                        <?php if (isset($_smarty_tpl->tpl_vars['auth_level']->value)) {
echo $_smarty_tpl->tpl_vars['auth_level']->value;
}?>
                    </div>
                        <div class="form-group">
                            <button id="bpwd" type="button" class="btn btn-theme btn-lg btn-block no-margin rounded" onclick="$('#pwd').removeAttr('style');$('#bpwd').attr('style','display:none;')">Cambiar Contraseña</button>
                        </div>
                        <div id="pwd" style="display:none;">
                            <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>" style="">
                                <div class="input-group input-group-lg rounded no-overflow">
                                    <input type="password" class="form-control" placeholder="Password" name="passwd">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                </div>
                                <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd']->value;
}?>
                            </div>
                            <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
                                <div class="input-group input-group-lg rounded no-overflow">
                                    <input type="password" class="form-control " placeholder="Confirm Password" name="passwd2">
                                    <span class="input-group-addon"><i class="fa fa-check"></i></span>
                                </div>
                                <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd2']->value;
}?>
                            </div>
                        </div>
                    </div>
                    <div class="sign-footer">
                        <div class="form-group">
                            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Aceptar</button>
                        </div>
                    </div>
                </form>
                <!--/ Register form -->
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
         <?php echo '<script'; ?>
>
        $("#logout").fadeTo(1000, 500).slideUp(500, function(){
            $("#logout").alert('close');
        });
        <?php echo '</script'; ?>
>
    </div>
</div><!-- /.row --><?php }} ?>
