<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 08:49:51
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/partials/header.html" */ ?>
<?php /*%%SmartyHeaderCode:88775526957028c75631305-46434166%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6de09283c8f1f621c4f3e3d0e22b9798f8c9314f' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/partials/header.html',
      1 => 1503277171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '88775526957028c75631305-46434166',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57028c7565cd05_56600485',
  'variables' => 
  array (
    'employees' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57028c7565cd05_56600485')) {function content_57028c7565cd05_56600485($_smarty_tpl) {?><header id="header">

<!-- Start header left -->
<div class="header-left">
    <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
    <div class="navbar-minimize-mobile left">
        <i class="fa fa-bars"></i>
    </div>
    <!--/ End offcanvas left -->

    <!-- Start navbar header -->
    <div class="navbar-header">

        <!-- Start brand -->
        <a class="navbar-brand" href="">
            <img class="logo" src="<?php echo base_url('assets/global/img/vicarlogo.jpg');?>
" alt="brand logo" width="220" height="52" />
        </a><!-- /.navbar-brand -->
        <!--/ End brand -->

    </div><!-- /.navbar-header -->
    <!--/ End navbar header -->

    <div class="clearfix"></div>
</div><!-- /.header-left -->
<!--/ End header left -->

<!-- Start header right -->
<div class="header-right">
<!-- Start navbar toolbar -->
<div class="navbar navbar-toolbar navbar-danger">

<!-- Start left navigation -->
<ul class="nav navbar-nav navbar-left">

    <!-- Start sidebar shrink -->
    <li class="navbar-minimize">
        <a href="javascript:void(0);" title="Minimize sidebar">
            <i class="fa fa-bars"></i>
        </a>
    </li>
    <!--/ End sidebar shrink -->

    <!-- Start form search -->
    <li class="navbar-search">
        <!-- Just view on mobile screen-->
        <a href="#" class="trigger-search"><i class="fa fa-search"></i></a>
        <form class="navbar-form">
            <div class="form-group has-feedback">
                <input type="text" class="form-control typeahead rounded" placeholder="Search for people, places and things">
                <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
            </div>
        </form>
    </li>
    <!--/ End form search -->

</ul><!-- /.navbar-left -->
<!--/ End left navigation -->

<!-- Start right navigation -->
<ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->
<?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>

<!-- Start notifications -->
<li class="dropdown navbar-notification">

    <a href="#" id="notify" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i>
    <span class="rounded count label label-info" id="countNoty"></span></a>

    <!-- Start dropdown menu -->
    <div class="dropdown-menu animated flipInX">
        <div class="dropdown-header">
            <span class="title">Notificationes <strong id="load13"></strong></span>
        </div>
        <div class="dropdown-body niceScroll">

            <!-- Start notification list -->
            <div id="load" class="media-list small">

                <!--/ End notification indicator -->
            </div>
            <!--/ End notification list -->

        </div>
        <div class="dropdown-footer">
            <a href="#">See All</a>
        </div>
    </div>
    <!--/ End dropdown menu -->

</li><!-- /.dropdown navbar-notification -->
<!--/ End notifications -->
<?php }?>
<!-- Start profile -->
<li class="dropdown navbar-profile">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                                    <span class="avatar"><img src="<?php echo base_url('assets/global/img/icon/64/contact.png');?>
" class="img-circle" alt="admin"></span>
                                    <span class="text hidden-xs hidden-sm text-muted">Perfil</span>
                                    <span class="caret"></span>
                                </span>
    </a>
    <!-- Start dropdown menu -->
    <ul class="dropdown-menu animated flipInX">
        <li><a href="#"><i class="fa fa-user"></i>View profile</a></li>
        <li class="divider"></li>
        <li><a href="<?php echo base_url('account/logout');?>
"><i class="fa fa-sign-out"></i>Logout</a></li>
    </ul>
    <!--/ End dropdown menu -->
</li><!-- /.dropdown navbar-profile -->
<!--/ End profile -->

<!-- Start settings 
<li class="navbar-setting pull-right">
    <a href="javascript:void(0);"><i class="fa fa-cog fa-spin"></i></a>
</li>--><!-- /.navbar-setting pull-right -->
<!--/ End settings -->

</ul><!-- /.navbar-right -->
<!--/ End right navigation -->

</div><!-- /.navbar-toolbar -->
<!--/ End navbar toolbar -->
</div><!-- /.header-right -->
<!--/ End header left -->

</header> <!-- /#header --><?php }} ?>
