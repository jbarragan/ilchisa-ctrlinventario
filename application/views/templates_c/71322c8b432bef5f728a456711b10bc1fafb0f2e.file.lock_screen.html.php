<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-01 00:45:51
         compiled from "C:\xampp\htdocs\vic\application\views\templates\contents\sign\lock_screen.html" */ ?>
<?php /*%%SmartyHeaderCode:124456d4d82f7d9e23-23940836%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '71322c8b432bef5f728a456711b10bc1fafb0f2e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\vic\\application\\views\\templates\\contents\\sign\\lock_screen.html',
      1 => 1456775594,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '124456d4d82f7d9e23-23940836',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56d4d82f7e0f60_40133581',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d4d82f7e0f60_40133581')) {function content_56d4d82f7e0f60_40133581($_smarty_tpl) {?><!-- Lock screen form -->
<form class="lock form-horizontal rounded shadow" action="<?php echo base_url('dashboard');?>
">
    <div class="sign-header">
        <div class="form-group">
            <div class="sign-text">
                <img src="<?php echo base_url('assets/global/img/avatar/100/1.png');?>
" alt="admin" class="img-circle">
                <p>Welcome Back - <strong>Tol Lee</strong></p>
            </div>
        </div>
    </div>
    <div class="sign-body">
        <div class="form-group">
            <div class="input-group input-group-lg rounded">
                <input type="password" class="form-control" placeholder="Password">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
        </div>
    </div>
    <div class="sign-footer">
        <div class="form-group">
            <div class="row">
                <div class="col-xs-6">
                    <div class="ckbox ckbox-theme">
                        <input id="rememberme" value="1" type="checkbox">
                        <label for="rememberme" class="rounded">Remember me</label>
                    </div>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="<?php echo base_url('account/lost-password');?>
" title="lost password">Lost password?</a>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Unlock</button>
        </div>
    </div>
</form>
<!--/ Lock screen form --><?php }} ?>
