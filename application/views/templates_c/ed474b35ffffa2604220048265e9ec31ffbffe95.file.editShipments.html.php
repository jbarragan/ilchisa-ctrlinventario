<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-30 09:46:52
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/shipments/editShipments.html" */ ?>
<?php /*%%SmartyHeaderCode:6861121695a3974543a8667-09327448%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ed474b35ffffa2604220048265e9ec31ffbffe95' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/shipments/editShipments.html',
      1 => 1517330802,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6861121695a3974543a8667-09327448',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a397454503ba8_18464035',
  'variables' => 
  array (
    'no' => 0,
    'noShipm' => 0,
    'status' => 0,
    'date' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'whse' => 0,
    'cust_code' => 0,
    'first_name' => 0,
    'carrier' => 0,
    'item_code' => 0,
    'item_name' => 0,
    'trailer' => 0,
    'ship_to' => 0,
    'ship' => 0,
    'shipTo' => 0,
    'seal' => 0,
    'shipment' => 0,
    'shipLots' => 0,
    'i' => 0,
    'reny_ctr' => 0,
    'notes' => 0,
    'images' => 0,
    'img' => 0,
    'c' => 0,
    'extencion' => 0,
    'id_out' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a397454503ba8_18464035')) {function content_5a397454503ba8_18464035($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/ilchisa/public_html/ctrlinventario/application/third_party/smarty/libs/plugins/modifier.date_format.php';
?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-left fa-4x"></i>SALIDA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Salidas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Salida</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">
                        <div id="success" class="alert alert-success animated fadeIn" style="display:none;">
                           
                        </div>
                        <div id="error" class="alert alert-danger animated fadeIn" style="display:none;">
                            <span class="alert-icon"><i class="fa fa-times"></i></span>
                            <strong>Error al enviar!</strong> verifique la dirección de correo electronico </a>.
                        </div>

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Editar Salida</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
            
                <form class="form-horizontal" role="form" id="shipments" action="<?php echo base_url('shipments/upShipment');?>
/<?php echo $_smarty_tpl->tpl_vars['no']->value;?>
" method="post">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Salida No.<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm date_rec" name="receipt_reny" value="<?php echo $_smarty_tpl->tpl_vars['noShipm']->value;?>
" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;">
                            <?php if ($_smarty_tpl->tpl_vars['status']->value=="Nuevo") {?>
                                <span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
                            <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Listo"||$_smarty_tpl->tpl_vars['status']->value=="Listo Modificado") {?>
                                <span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
                            <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Terminado") {?>
                                <span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
                            <?php }?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" name="date" id="date" value="<?php echo $_smarty_tpl->tpl_vars['date']->value;?>
">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="form-control mb-15" name="whse" tabindex="2">
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['whse_code']==$_smarty_tpl->tpl_vars['whse']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?> 
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>
                                <select class="form-control mb-15" name="customer" tabindex="2" id="custShip">
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['cust_code']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['first_name']->value;?>
</option>
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Carrier<span class="asterisk">*</span></label>
                               <input type="text" class="form-control input-sm" name="carrier" id="" value="<?php echo $_smarty_tpl->tpl_vars['carrier']->value;?>
">
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <select class="form-control mb-15" name="item" tabindex="2" id="itemShip"> 
                                <option value="<?php echo $_smarty_tpl->tpl_vars['item_code']->value;?>
"selected><?php echo $_smarty_tpl->tpl_vars['item_name']->value;?>
</option>
                                </select>
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                                <label class="control-label">Trailer<span class="asterisk">*</span></label>
                               <input type="text" class="form-control input-sm" name="trailer" value="<?php echo $_smarty_tpl->tpl_vars['trailer']->value;?>
">
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                            <label class="control-label">Ship To<span class="asterisk">*</span></label>
                                <select class="form-control mb-15" name="shipTo" tabindex="2" id=shipto> 
                                <option value="">Selecciona Ship</option>
                                <?php  $_smarty_tpl->tpl_vars['ship'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ship']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ship_to']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ship']->key => $_smarty_tpl->tpl_vars['ship']->value) {
$_smarty_tpl->tpl_vars['ship']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['ship']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_code'];?>
" '<?php if ($_smarty_tpl->tpl_vars['ship']->value['ship_code']==$_smarty_tpl->tpl_vars['shipTo']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_state'];?>
,<?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_address'];?>
</option>
                                <?php } ?>                              
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Factura<span class="asterisk">*</span></label>
                               <input type="text" class="form-control input-sm" name="seal" value="<?php echo $_smarty_tpl->tpl_vars['seal']->value;?>
">
                            </div><!-- /.form-group -->
                        </div> 
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <table id="table-ship" class="table table-striped table-lilac">
                                    <tr>
                                        <td>reny Ctrl</td>
                                        <td>Lote</td>
                                        <td style="dispaly:none;"></td>
                                        <td>Bags</td>
                                        <td>Fecha Prod</td>
                                        <td>Fecha cad</td>
                                        <td>Location</td>
                                        <td>storage</td>
                                        <td>acción</td>
                                    </tr>
                                    <tbody id="addrow">
                                     <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                    <?php  $_smarty_tpl->tpl_vars['shipLots'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['shipLots']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['shipment']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['shipLots']->key => $_smarty_tpl->tpl_vars['shipLots']->value) {
$_smarty_tpl->tpl_vars['shipLots']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['shipLots']->key;
?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['shipLots']->value['reny_ctrl'])) {?>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                    <tr id="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lotShip_code'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                        <td style="display:none;">
                                            <input type="text" name="idLotShip[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lotShip_code'];?>
" readonly>
                                        </td>
                                    	<td>
                                           <select type="text" name="renyCtrl[]" class="form-control renyCtrl<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" placeholder="reny Ctrl" id="renyCtrl<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['reny_ctrl'];?>
">
                                           <option value="<?php echo $_smarty_tpl->tpl_vars['reny_ctr']->value[$_smarty_tpl->tpl_vars['i']->value-1];?>
" selected><?php echo $_smarty_tpl->tpl_vars['reny_ctr']->value[$_smarty_tpl->tpl_vars['i']->value-1];?>
</option>
                                           </select>
                                        </td>
                                        <td>
                                           <select type="text" name="lots[]" placeholder="Lote" id="lot<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control lots<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lot'];?>
">
                                           <option value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lote_code'];?>
" selected><?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lot'];?>
</option>
                                           </select>
                                        </td>
                                        <td style="dispaly:none;">
                                            <input type="hidden" name="bags_pallets[]" id="bags_pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="form-control bags_palletsl<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['bags_pallets'];?>
">
                                        </td>
                                        <td>
                                            <input type="number" name="bags[]" id="bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" placeholder="Bags" class="form-control bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['bags_totalShip'];?>
" max="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['bags_totalShip']+($_smarty_tpl->tpl_vars['shipLots']->value['stock']-$_smarty_tpl->tpl_vars['shipLots']->value['reserved']);?>
" min="1">
                                        </td>
                                        <td>
                                            <input type="text" name="prod_date[]" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shipLots']->value['prod_date'],"%e/%b/%Y");?>
" id="prod" class="form-control prod_date<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"  readonly="true">
                                        </td>
                                        <td>
                                            <input type="text" name="exp_date[]" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['shipLots']->value['exp_date'],"%e/%b/%Y");?>
" id="exp" class="form-control exp_date<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"  readonly="true">
                                        </td>   
                                        <td>
                                            <input type="text" name="location[]" class="form-control location<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" readonly="true" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['location'];?>
" style="width:60px;">
                                        </td> 
                                        <td>
                                       		<input type="text" name="storage[]" class="form-control storage<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" readonly="true" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['storage'];?>
" style="width:60px;">
                                        </td>
                                        <td class="text-center">
                                            <a id="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lotShip_code'];?>
" class="btn btn-danger btn-xs delLot" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <?php }?>
                                     <?php } ?>
                                    </tbody>
                                     <input type="hidden" id="numLots" name="index" class="" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                     <input type="hidden" id="lotShipNo" name="lotShipment" class="" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                </table>
                                <?php if ($_smarty_tpl->tpl_vars['status']->value=="Nuevo") {?>
                                <button type="button" class="btn btn-theme pull-right" id="addreny">Agregar Lote</button>
                                <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Listo"||$_smarty_tpl->tpl_vars['status']->value=="Listo Modificado") {?>
                                <button type="button" class="btn btn-theme pull-right" id="addreny">Agregar Lote</button>
                                <?php }?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <label class="control-label">Notas:</label>              
                                     <textarea class="form-control" rows="5" name="notes"><?php echo $_smarty_tpl->tpl_vars['notes']->value;?>
</textarea>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10">
                                <div class="row"> 
                                <?php if ($_smarty_tpl->tpl_vars['status']->value=="Nuevo") {?>
                                    <div class="col-sm-6">
                                        <label>Imagenes</label>
                                        <div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(0, null, 0);?>
                                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['img']->key;
?>
                                            <div class="col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
">
                                                <?php if ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='image') {?>
                                                    <img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='pdf') {?>
                                                    <img src="<?php echo base_url('assets/global/img/pdf.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='doc') {?>
                                                    <img src="<?php echo base_url('assets/global/img/doc.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='xls') {?>
                                                    <img src="<?php echo base_url('assets/global/img/xls.png');?>
" width="100%">
                                                <?php }?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" target="_blank"> <?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
</a>
                                                <a id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" class="btn btn-danger btn-xs delImg" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar" style="position:absolute; top:1px;"><i class="fa fa-times"></i></a>
                                            </div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Cargar Imagenes</label> 
                                        <div id="my-dropzone" action="<?php echo base_url('shipments/images');?>
/<?php echo $_smarty_tpl->tpl_vars['no']->value;?>
" class="col-sm-12 dropzone pull-right">
                                            <div class="fallback">
                                                <input name="img" type="file" multiple />
                                            </div>
                                        </div><!-- /.panel-body -->
                                    </div>
                                <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Listo"||$_smarty_tpl->tpl_vars['status']->value=="Listo Modificado") {?>
                                    <div class="col-sm-6">
                                        <label>Imagenes</label>
                                        <div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(0, null, 0);?>
                                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['img']->key;
?>
                                            <div class="col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
">
                                                <?php if ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='image') {?>
                                                    <img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='pdf') {?>
                                                    <img src="<?php echo base_url('assets/global/img/pdf.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='doc') {?>
                                                    <img src="<?php echo base_url('assets/global/img/doc.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='xls') {?>
                                                    <img src="<?php echo base_url('assets/global/img/xls.png');?>
" width="100%">
                                                <?php }?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" target="_blank"> <?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
</a>
                                                <a id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" class="btn btn-danger btn-xs delImg" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar" style="position:absolute; top:1px;"><i class="fa fa-times"></i></a>
                                            </div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Cargar Imagenes</label> 
                                        <div id="my-dropzone" action="<?php echo base_url('shipments/images');?>
/<?php echo $_smarty_tpl->tpl_vars['no']->value;?>
" class="col-sm-12 dropzone pull-right">
                                            <div class="fallback">
                                                <input name="img" type="file" multiple />
                                            </div>
                                        </div><!-- /.panel-body -->
                                    </div>

                                <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Terminado") {?>
                                    <div class="col-sm-12">
                                        <label>Imagenes</label>
                                        <div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(0, null, 0);?>
                                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['img']->key;
?>
                                            <div class="col-sm-3" id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
">
                                                <?php if ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='image') {?>
                                                    <img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='pdf') {?>
                                                    <img src="<?php echo base_url('assets/global/img/pdf.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='doc') {?>
                                                    <img src="<?php echo base_url('assets/global/img/doc.png');?>
" width="100%">
                                                <?php } elseif ($_smarty_tpl->tpl_vars['extencion']->value[$_smarty_tpl->tpl_vars['c']->value]=='xls') {?>
                                                    <img src="<?php echo base_url('assets/global/img/xls.png');?>
" width="100%">
                                                <?php }?>
                                                <a href="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" target="_blank"> <?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
</a>
                                            </div>
                                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['c']->value+1, null, 0);?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php }?>
                                </div>
                            </div>
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                        <?php if ($_smarty_tpl->tpl_vars['status']->value=="Nuevo") {?>
                        <button type="submit" class="btn btn-theme" formaction="<?php echo base_url('shipments/upShipmentnuevo');?>
/<?php echo $_smarty_tpl->tpl_vars['no']->value;?>
">Actualizar</button>
                        <button type="submit" class="btn btn-theme">Marcar Listo</button>
                         <a id="<?php echo $_smarty_tpl->tpl_vars['noShipm']->value;?>
" class="btn btn-theme delShip"><i class="fa fa-trash"></i> Eliminar</a>
                        <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Listo"||$_smarty_tpl->tpl_vars['status']->value=="Listo Modificado") {?>
                        <button type="submit" class="btn btn-theme">Actualizar</button>
                         <a id="<?php echo $_smarty_tpl->tpl_vars['noShipm']->value;?>
" class="btn btn-theme delShip"><i class="fa fa-trash"></i> Eliminar</a>
                        <a href="<?php echo base_url('shipments/searchShip');?>
" class="btn btn-theme">Cerrar</a>
                         <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Terminado") {?>
                        <a href="<?php echo base_url('shipments/billOfLading');?>
/<?php echo $_smarty_tpl->tpl_vars['id_out']->value;?>
" class="btn btn-theme" target="_blank">Bill of Lading</button>
                        <a id ="<?php echo $_smarty_tpl->tpl_vars['noShipm']->value;?>
" class="btn btn-sm btn-theme btn-xs mail" style="height:32px;width: 120px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Enviar Correo</a>
                        <a href="<?php echo base_url('shipments/searchShip');?>
" class="btn btn-theme">Cerrar</a>
                        <?php }?>
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
