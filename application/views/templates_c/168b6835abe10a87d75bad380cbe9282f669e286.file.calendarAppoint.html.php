<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-31 13:18:25
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/appointment/calendarAppoint.html" */ ?>
<?php /*%%SmartyHeaderCode:79002465056fc994b63fba7-98339578%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '168b6835abe10a87d75bad380cbe9282f669e286' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/appointment/calendarAppoint.html',
      1 => 1459455498,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '79002465056fc994b63fba7-98339578',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56fc994b6ba226_80031887',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56fc994b6ba226_80031887')) {function content_56fc994b6ba226_80031887($_smarty_tpl) {?><!-- Start page header -->
                <div class="header-content">
                    <h2><i class="fa fa-calendar"></i> Calendario de Citas</h2>
                    <div class="breadcrumb-wrapper hidden-xs">
                        <span class="label">You are here:</span>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="dashboard.html">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Transacciones</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Calendario de Citas</li>
                        </ol>
                    </div><!-- /.breadcrumb-wrapper -->
                </div><!-- /.header-content -->
                <!--/ End page header -->

                <!-- Start body content -->
                <div class="body-content animated fadeIn">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <div class="calendar-toolbar">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-2">
                                        <!-- Start offcanvas btn menu calendar: This menu will take position at the top of calendar (mobile only). -->
                                        <div class="btn-group hidden-lg hidden-md">
                                            <button type="button" class="btn btn-theme btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars fa-2x"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#" data-calendar-nav="prev">Anterior</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-nav="today">Hoy</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-nav="next">Siguiente</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="hidden-sm hidden-xs">
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-nav="prev"><i class="fa fa-angle-left"></i> Anterior</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-nav="today">Hoy</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-nav="next">Siguente <i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-8 col-xs-8">
                                        <div class="page-header no-border no-margin no-padding"><h4 class="no-border no-margin no-padding text-center text-capitalize">&nbsp;</h4></div>
                                    </div>
                                    <div class="col-md-5 col-sm-2 col-xs-2">
                                        <!-- Start offcanvas btn menu calendar: This menu will take position at the top of calendar (mobile only). -->
                                        <div class="btn-group calendar-menu-mobile pull-right hidden-lg hidden-md">
                                            <button type="button" class="btn btn-theme btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bars fa-2x"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#" data-calendar-view="year">Año</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-view="month">Mes</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-view="week">Semana</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-calendar-view="day">Día</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pull-right hidden-sm hidden-xs">
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-view="year">Año</button>
                                            <button class="btn btn-theme btn-sm active rounded" data-calendar-view="month">Mes</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-view="week">Semana</button>
                                            <button class="btn btn-theme btn-sm rounded" data-calendar-view="day">Día</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="calendar" class="rounded mb-20"></div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <h4>Citas</h4>
                            <ul id="eventlist" class="nav nav-pills nav-stacked niceScroll"></ul>

                        </div>
                    </div><!-- /.row -->

                </div><!-- /.body-content -->
                <!--/ End body content -->
<div class="modal fade" id="events-modal">
	<div class="modal-dialog">
    	<div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Event</h3>
            </div>
            <div class="modal-body" style="height: 400px">
            </div>
            <div class="modal-footer">
                <a href="#" data-dismiss="modal" class="btn">Close</a>
            </div>
        </div>
    </div>
</div><?php }} ?>
