<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-12 21:29:48
         compiled from "C:\xampp\htdocs\vic\application\views\templates\partials\sidebar_left.html" */ ?>
<?php /*%%SmartyHeaderCode:2280456d49bba5642a4-21182495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b5074cc36b6f9ce82179adcbe70a589b524eed2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\vic\\application\\views\\templates\\partials\\sidebar_left.html',
      1 => 1457814583,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2280456d49bba5642a4-21182495',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56d49bba7431b6_19781878',
  'variables' => 
  array (
    'active_dashboard' => 0,
    'active_blog' => 0,
    'active_item_search' => 0,
    'active_blog_list' => 0,
    'active_mail' => 0,
    'active_mail_inbox' => 0,
    'active_mail_compose' => 0,
    'active_pages' => 0,
    'active_gallery' => 0,
    'active_faq' => 0,
    'active_forms' => 0,
    'active_components' => 0,
    'active_component_grid_system' => 0,
    'active_component_button' => 0,
    'active_widget_overview' => 0,
    'active_widget_social' => 0,
    'active_widget_blog' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56d49bba7431b6_19781878')) {function content_56d49bba7431b6_19781878($_smarty_tpl) {?><aside id="sidebar-left" class="sidebar-circle">

<!-- Start left navigation - profile shortcut -->
<div class="sidebar-content">
    <div class="media">
        <a class="pull-left has-notif avatar" href="page-profile.html">
            <img src="http://img.djavaui.com/?create=50x50,4888E1?f=ffffff" alt="admin">
            <i class="online"></i>
        </a>
        <div class="media-body">
            <h4 class="media-heading">Hello, <span>Admin</span></h4>
            <small>Administrator</small>
        </div>
    </div>
</div><!-- /.sidebar-content -->
<!--/ End left navigation -  profile shortcut -->

<!-- Start left navigation - menu -->
<ul class="sidebar-menu">

<!-- Start navigation - dashboard -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_dashboard']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_dashboard']->value;?>
 <?php }?>">
<a href="<?php echo base_url('dashboard');?>
">
    <span class="icon"><i class="fa fa-home"></i></span>
    <span class="text">Dashboard</span>
    <?php if (isset($_smarty_tpl->tpl_vars['active_dashboard']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
</a>
</li>
<!--/ End navigation - dashboard -->

<!-- Start category catalogos -->
<li class="sidebar-category">
    <span>CATALOGOS</span>
    <span class="pull-right"><i class="fa fa-folder-open"></i></span>
</li>
<!--/ End category apps -->

<!-- Start navigation -Productos -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_blog']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_blog']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-shopping-cart"></i></span>
        <span class="text">Productos</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_blog']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_item_search']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_item_search']->value;?>
 <?php }?>"><a href="<?php echo base_url('item/search');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_blog_list']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_blog_list']->value;?>
 <?php }?>"><a href="<?php echo base_url('blog/blog-list');?>
">Agregar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - Productos -->

<!-- Start navigation - Proveedores -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_mail']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_mail']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-truck"></i></span>
        <span class="text">Proveedores</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_mail']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_mail_inbox']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_mail_inbox']->value;?>
 <?php }?>"><a href="<?php echo base_url('mail/mail-inbox');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_mail_compose']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_mail_compose']->value;?>
 <?php }?>"><a href="<?php echo base_url('mail/mail-compose');?>
">Registrar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - proveedores -->

<!-- Start navigation - clientes -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_pages']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_pages']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-users"></i></span>
        <span class="text">Clientes</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_pages']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_gallery']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_gallery']->value;?>
 <?php }?>"><a href="<?php echo base_url('pages/page-gallery');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_gallery']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_faq']->value;?>
 <?php }?>"><a href="<?php echo base_url('pages/page-faq');?>
">Registrar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - clientes -->

<!-- Start navigation - Bodegas -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_pages']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_pages']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-building"></i></span>
        <span class="text">Bodegas</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_pages']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_gallery']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_gallery']->value;?>
 <?php }?>"><a href="<?php echo base_url('pages/page-gallery');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_gallery']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_faq']->value;?>
 <?php }?>"><a href="<?php echo base_url('pages/page-faq');?>
">Registrar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - Bodegas -->

<!-- Start category ui citas-->
<li class="sidebar-category">
    <span>CITAS</span>
    <span class="pull-right"><i class="fa fa-clipboard"></i></span>
</li>
<!--/ End category ui citas-->

<!-- Start navigation - calendario -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_forms']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_forms']->value;?>
 <?php }?>">
    <a href="">
        <span class="icon"><i class="fa fa-calendar"></i></span>
        <span class="text">Calendario</span>
        <span class="label label-primary pull-right rounded">10</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_forms']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End navigation - calendario -->

<!-- Start navigation - agendar -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_forms']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_forms']->value;?>
 <?php }?>">
    <a href="">
        <span class="icon"><i class="fa fa-book"></i></span>
        <span class="text">Agendar Cita</span>
        <span class="label label-primary pull-right rounded">10</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_forms']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End navigation - agendar -->

<!-- Start category Transacciones -->
<li class="sidebar-category">
    <span>TRANSACCIONES</span>
    <span class="pull-right"><i class="fa fa-exchange"></i></span>
</li>
<!--/ End category Transacciones -->

<!-- Start development - Entradas -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_components']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_components']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-arrow-right"></i></span>
        <span class="text">Entradas</span>
        <span class="plus"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_components']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_component_grid_system']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_component_grid_system']->value;?>
 <?php }?>"><a href="<?php echo base_url('components/component-grid-system');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_component_button']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_component_button']->value;?>
 <?php }?>"><a href="<?php echo base_url('components/component-button');?>
">Nueva Entrada</a></li>
    </ul>
</li>
<!--/ End development - Entradas -->

<!-- Start development - Salidas -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_components']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_components']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-arrow-left"></i></span>
        <span class="text">Salidas</span>
        <span class="plus"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_components']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_component_grid_system']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_component_grid_system']->value;?>
 <?php }?>"><a href="<?php echo base_url('components/component-grid-system');?>
">Calendario</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_component_button']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_component_button']->value;?>
 <?php }?>"><a href="<?php echo base_url('components/component-button');?>
">Nueva Salida</a></li>
    </ul>
</li>
<!--/ End development - Salidas -->

<!-- Start category widget -->
<li class="sidebar-category">
    <span>REPORTES</span>
    <span class="pull-right"><i class="fa fa-file-text"></i></span>
</li>
<!--/ End category widget -->

<!-- Start widget - overview -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_widget_overview']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_widget_overview']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('widget/widget-overview');?>
">
        <span class="icon"><i class="fa fa-arrow-right"></i></span>
        <span class="text">Entradas</span>
        <span class="label label-primary pull-right rounded">10</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_widget_overview']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End widget - overview -->

<!-- Start widget - social -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_widget_social']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_widget_social']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('widget/widget-social');?>
">
        <span class="icon"><i class="fa fa-arrow-left"></i></span>
        <span class="text">Salidas</span>
        <span class="label label-success pull-right rounded">28</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_widget_social']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End widget - social -->

<!-- Start widget - blog -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_widget_blog']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_widget_blog']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('widget/widget-blog');?>
">
        <span class="icon"><i class="fa fa-usd"></i></span>
        <span class="text">Inventario</span>
        <span class="label label-info pull-right rounded">15</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_widget_blog']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End widget - blog -->

</ul><!-- /.sidebar-menu -->
<!--/ End left navigation - menu -->

<!-- Start left navigation - footer -->
<div class="sidebar-footer hidden-xs hidden-sm hidden-md">
    <a id="setting" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Setting"><i class="fa fa-cog"></i></a>
    <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
    <a id="lock-screen" data-url="account/lock-screen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a>
    <a id="logout" data-url="account/sign-in" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Logout"><i class="fa fa-power-off"></i></a>
</div><!-- /.sidebar-footer -->
<!--/ End left navigation - footer -->

</aside><!-- /#sidebar-left --><?php }} ?>
