<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-30 13:25:34
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/ajusteInv.html" */ ?>
<?php /*%%SmartyHeaderCode:1596494041572506aed76f81-44887117%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa7d35bb17cb8618247c37fad75b47f4c50faf6e' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/ajusteInv.html',
      1 => 1461958543,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1596494041572506aed76f81-44887117',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'custom' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_572506aee11ae7_52135093',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_572506aee11ae7_52135093')) {function content_572506aee11ae7_52135093($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-left fa-4x"></i>SALIDA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Salidas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Ajuste de inventario</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Ajustar inventario</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">
                <form class="form-horizontal" role="form" id="receipt" action="<?php echo base_url('shipments/adjustment');?>
" method="post">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Salida No.<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm date_rec" name="receipt_vicar" value="" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                     <div class="text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                            </div><!-- /.form-group -->
                        </div>   
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="whse" tabindex="2">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>                                 
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>
                                <select class="chosen-select mb-15" name="customer" tabindex="2" id="custShip">      
                                <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                  
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>                     
                                <select data-placeholder="Choose a Country" class="form-control input-sm" name="item" tabindex="2" id="itemShip">                                   
                                </select>
                            </div><!-- /.form-group -->
                            </div><!-- /.form-group -->
                        </div>    
                        <?php } ?>  
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="0">
                                <table id="table-ship" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Vicar Ctrl</td>
                                        <td>Lote</td>
                                        <td>pallets</td>
                                        <td style="dispaly:none;"></td>
                                        <td>Bags</td>
                                        <td>HT</td>
                                        <td>Etiquetas</td>
                                        <td>storage</td>
                                    </tr>
                                </table>
                                <button type="button" class="btn btn-theme pull-right" id="addVicar" disabled>Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Pallets:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="total" placeholder="">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                        <button type="submit" class="btn btn-theme">Aceptar</button>
                        <a href="<?php echo base_url('shipments/searchShip');?>
" class="btn btn-theme">Cancelar</a>
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
