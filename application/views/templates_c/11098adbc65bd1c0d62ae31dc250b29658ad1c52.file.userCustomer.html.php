<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-09-12 17:55:51
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/catalogs/customers/userCustomer.html" */ ?>
<?php /*%%SmartyHeaderCode:164129514757118ff3047ee2-61868358%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '11098adbc65bd1c0d62ae31dc250b29658ad1c52' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/catalogs/customers/userCustomer.html',
      1 => 1503277501,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164129514757118ff3047ee2-61868358',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57118ff309ac25_81491084',
  'variables' => 
  array (
    'customer_data' => 0,
    'error_val' => 0,
    'Val_user' => 0,
    'user' => 0,
    'passwd' => 0,
    'passwd2' => 0,
    'Val_email' => 0,
    'email' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57118ff309ac25_81491084')) {function content_57118ff309ac25_81491084($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-users fa-4x"></i> Crear Usuario</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Usuario a Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class=" text-center">
                    <h3 class="panel-title"><?php echo $_smarty_tpl->tpl_vars['customer_data']->value['first_name'];?>
</h3>
                </div><!-- /.pull-left -->
            </div>
            <div class="panel-body">

               <form class="form-horizontal rounded shadow no-overflow" action="<?php echo base_url('/customer/sign-up');?>
/<?php echo $_smarty_tpl->tpl_vars['customer_data']->value['customer_code'];?>
" method="post" style="width:500px; margin: 0 auto;">
			    <div class="sign-body">
			        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
			            <div class="input-group input-group-lg rounded no-overflow">
			                <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $_smarty_tpl->tpl_vars['Val_user']->value;?>
">
			                <span class="input-group-addon"><i class="fa fa-user"></i></span>
			            </div>
			            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['user']->value;
}?>
			        </div>
			        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
			            <div class="input-group input-group-lg rounded no-overflow">
			                <input type="password" class="form-control" placeholder="Password" name="passwd">
			                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
			            </div>
			             <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd']->value;
}?>
			        </div>
			        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
			            <div class="input-group input-group-lg rounded no-overflow">
			                <input type="password" class="form-control" placeholder="Confirm Password" name="passwd2">
			                <span class="input-group-addon"><i class="fa fa-check"></i></span>
			            </div>
			            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['passwd2']->value;
}?>
			        </div>
			        <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
			            <div class="input-group input-group-lg rounded no-overflow">
			                <input type="email" class="form-control" placeholder="Your Email" name="email" value="<?php if (isset($_smarty_tpl->tpl_vars['Val_email']->value)) {
echo $_smarty_tpl->tpl_vars['Val_email']->value;
} else {
echo $_smarty_tpl->tpl_vars['customer_data']->value[0]['email'];
}?>">
			                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
			            </div>
			            <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)) {
echo $_smarty_tpl->tpl_vars['email']->value;
}?>
			        </div>
			         <div class="form-group <?php if (isset($_smarty_tpl->tpl_vars['error_val']->value)&&$_smarty_tpl->tpl_vars['Val_user']->value==null) {?>has-error has-feedback<?php }?>">
			            <div class="input-group input-group-lg rounded no-overflow">
			                <input type="hidden" class="form-control" name="auth_level" value=1>
			                
			            </div>
			        </div>
			    </div>
			    <div class="sign-footer">
			        <div class="form-group">
			            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Sign Up</button>
			        </div>
			    </div>
				</form>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
