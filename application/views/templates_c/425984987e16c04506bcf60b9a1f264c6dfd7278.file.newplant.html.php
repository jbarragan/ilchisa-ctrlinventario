<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-20 14:24:22
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/plant/newplant.html" */ ?>
<?php /*%%SmartyHeaderCode:5286483395a3ad5061f39d3-74634146%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '425984987e16c04506bcf60b9a1f264c6dfd7278' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/catalogs/plant/newplant.html',
      1 => 1513708393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5286483395a3ad5061f39d3-74634146',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a3ad506220496_47912217',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3ad506220496_47912217')) {function content_5a3ad506220496_47912217($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> NUEVA PLANTA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Nueva Planta</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
           
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="plant" action="<?php echo base_url('plant/add_plant');?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name_plant" placeholder="">
                            </div>
                        </div>
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
