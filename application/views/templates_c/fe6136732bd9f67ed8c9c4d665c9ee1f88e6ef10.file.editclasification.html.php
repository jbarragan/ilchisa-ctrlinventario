<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-15 19:56:52
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\clasification\editclasification.html" */ ?>
<?php /*%%SmartyHeaderCode:54435a341af4704665-21823596%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fe6136732bd9f67ed8c9c4d665c9ee1f88e6ef10' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\clasification\\editclasification.html',
      1 => 1513362848,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '54435a341af4704665-21823596',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'editclasification' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a341af481a0b0_06712555',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a341af481a0b0_06712555')) {function content_5a341af481a0b0_06712555($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> CLASIFICACIÓN</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Clasificación</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
           
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="clasification" action="<?php echo base_url('clasification/edit_clasification');?>
/<?php echo $_smarty_tpl->tpl_vars['editclasification']->value->id_clasification;?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name_clasification" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['editclasification']->value->name_clasification;?>
">
                            </div>
                        </div>
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
