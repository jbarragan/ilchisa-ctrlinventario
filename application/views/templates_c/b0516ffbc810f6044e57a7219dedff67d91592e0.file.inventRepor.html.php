<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-13 18:08:02
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\appointment\inventRepor.html" */ ?>
<?php /*%%SmartyHeaderCode:84755a315e7255f6a9-60551782%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b0516ffbc810f6044e57a7219dedff67d91592e0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\appointment\\inventRepor.html',
      1 => 1513108023,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '84755a315e7255f6a9-60551782',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'employees' => 0,
    'datIn' => 0,
    'datFin' => 0,
    'custom' => 0,
    'con' => 0,
    'customer' => 0,
    'items' => 0,
    'item' => 0,
    'whse' => 0,
    'whse_sel' => 0,
    'invList' => 0,
    'i' => 0,
    'dateS' => 0,
    'prod_date' => 0,
    'exp_date' => 0,
    'resB' => 0,
    'sumBag' => 0,
    'res' => 0,
    'sumPal' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a315e72672630_17280017',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a315e72672630_17280017')) {function content_5a315e72672630_17280017($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-usd fa-4x"></i> REPORTE IINVENTARIO</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Reportes</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Inventario</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <div>
        <?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>
        	<form id="receiptReport" action="<?php echo base_url('reports/searchinvent');?>
" method="get">
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label class="control-label">Fecha Inicial</label>                            
                             <input type="text" class="form-control input-sm" name="dateIn" placeholder="mm/dd/yyyy" id="date" value="<?php echo $_smarty_tpl->tpl_vars['datIn']->value;?>
">
                    </div><!-- /.form-group -->
                    <div class="form-group col-sm-2">
                        <label class="control-label">A la fecha:</label>                            
                             <input type="text" class="form-control input-sm" name="dateFin" placeholder="mm/dd/yyyy" id="date2" value="<?php echo $_smarty_tpl->tpl_vars['datFin']->value;?>
">
                    </div><!-- /.form-group -->
                </div> 
        		<div class="row">
                    <div class="form-group col-sm-3">
                        <label class="control-label">Cliente</label>                            
                        <select data-placeholder="Choose a Country" class="form-control chosen-select mb-15" id="customer" name="customer" tabindex="2">
                            <option value="">Selecciona Cliente</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['customer']->value==$_smarty_tpl->tpl_vars['con']->value['customer_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Producto</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="item" name="item" tabindex="2">
                            <option value="">Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value==$_smarty_tpl->tpl_vars['con']->value['items_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Bodega</label>
                        <select data-placeholder="Choose a Country" class="form-control  chosen-select mb-15" id="whse" name="whse" tabindex="2">
                            <option value="">Selecciona Bodega</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whse']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['whse_sel']->value==$_smarty_tpl->tpl_vars['con']->value['whse_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                     <div class="form-group col-sm-3">
                        <input type="submit" class="btn btn-theme" id="generar" value="Generar Reporte" style="margin-top:29px">
                        <a <?php if (!empty($_smarty_tpl->tpl_vars['invList']->value)) {?>href="<?php echo base_url('reports/reporinvent/');?>
?dateIn=<?php echo $_smarty_tpl->tpl_vars['datIn']->value;?>
&dateFin=<?php echo $_smarty_tpl->tpl_vars['datFin']->value;?>
&customer=<?php echo $_smarty_tpl->tpl_vars['customer']->value;?>
&item=<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
&whse=<?php echo $_smarty_tpl->tpl_vars['whse_sel']->value;?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin-top:29px" target="_blank">Exportar PDF</a>
                        <input type="button" id="ToExcel" class="btn btn-theme pull-right" value="Exportar Excel" style="margin-top:29px; margin-right: 10px"/>
                    </div>
                </div> 
            </form>
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcel">
                <input type="hidden" id="tablaExcel" name="table" />
                <input type="hidden" name="filename" value="Reporte_Salidas" />
            </form>
        </div>
        <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['employees']->value=='customer') {?>
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcel">
                <input type="hidden" id="tablaExcel" name="table" />
                <input type="hidden" name="filename" value="Reporte_Inventario" />
            </form>
            <a <?php if (!empty($_smarty_tpl->tpl_vars['invList']->value)) {?> href="<?php echo base_url('report- {s/reporinvent/');?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin:15px 20px;"  target="_blank">Exportar PDF</a>

            <input type="button" id="ToExcel" class="btn btn-theme pull-right" value="Exportar Excel" style="margin:15px 20px;"/>
            <?php }?>
        </div>
        <!-- Start datatable using ajax -->
                <div>
                    <!-- Start datatable -->
                     <table id="datatableInvent" class="table table-striped table-theme">
                        <thead>
                        <tr>
                            <th data-hide="phone" class="text-center">reny Ctrl</th> 
                            <th data-hide="phone" class="text-center">Fecha de Entrada</th> 
                            <th data-class="expand" class="text-center">Cliente</th>
                            <th data-hide="phone" class="text-center">Proveedor</th>
                            <th data-hide="phone" class="text-center">Producto</th>
                            <th data-hide="phone" class="text-center">Lote</th> 
                            <th data-hide="phone" class="text-center">Fecha Produccion</th>
                            <th data-hide="phone" class="text-center">Fecha Expiración</th>   
                            <th data-hide="phone" class="text-center">Pallets</th>                           
                            <th data-hide="phone" class="text-center">Bags</th>
                            <?php if ($_smarty_tpl->tpl_vars['employees']->value!='customer') {?> 
                            <th data-hide="phone" class="text-center">Location</th>
                            <?php }?>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['invList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr class="text-center">
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['reny_ctrl'];?>
</td>   
                                <td><?php echo $_smarty_tpl->tpl_vars['dateS']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['lot'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['prod_date']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['exp_date']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['pallets'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['stock'];?>
</td>
                                <?php if ($_smarty_tpl->tpl_vars['employees']->value!='customer') {?> 
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['location'];?>
</td>
                                <?php }?>
                            </tr>
                            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                        <?php } ?>
                        	<tr class="text-center">
                                <td style="background-color:transparent;"></td>
                                <td style="background-color:transparent;"></td>
                                <td style="background-color:transparent;"></td>
                                <td style="background-color:transparent;"></td>
                                <td style="background-color:transparent;"></td>   
                                <td style="background-color:transparent;"></td>  
                                <td style="background-color:transparent;"></td>
                                <td>Totales:</td>
                                <td>
                                    <?php $_smarty_tpl->tpl_vars['resB'] = new Smarty_variable(0, null, 0);?>
                                    <?php  $_smarty_tpl->tpl_vars['sumBag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sumBag']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['invList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sumBag']->key => $_smarty_tpl->tpl_vars['sumBag']->value) {
$_smarty_tpl->tpl_vars['sumBag']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['sumBag']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['resB'] = new Smarty_variable($_smarty_tpl->tpl_vars['resB']->value+$_smarty_tpl->tpl_vars['sumBag']->value['pallets'], null, 0);?>
                                    <?php } ?>
                                    <?php echo $_smarty_tpl->tpl_vars['resB']->value;?>

                                </td>
                                <td>
                                	<?php $_smarty_tpl->tpl_vars['res'] = new Smarty_variable(0, null, 0);?>
                                	<?php  $_smarty_tpl->tpl_vars['sumPal'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sumPal']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['invList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sumPal']->key => $_smarty_tpl->tpl_vars['sumPal']->value) {
$_smarty_tpl->tpl_vars['sumPal']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['sumPal']->key;
?>
                                	<?php $_smarty_tpl->tpl_vars['res'] = new Smarty_variable($_smarty_tpl->tpl_vars['res']->value+$_smarty_tpl->tpl_vars['sumPal']->value['stock'], null, 0);?>
                                	<?php } ?>
                                	<?php echo $_smarty_tpl->tpl_vars['res']->value;?>

                                </td>
                                <td style="background-color:transparent;"></td>
                            </tr>
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
