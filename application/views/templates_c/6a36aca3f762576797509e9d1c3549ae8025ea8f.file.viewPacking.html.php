<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-21 08:51:51
         compiled from "/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/shipments/viewPacking.html" */ ?>
<?php /*%%SmartyHeaderCode:116218667957053a46f1b607-26476984%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6a36aca3f762576797509e9d1c3549ae8025ea8f' => 
    array (
      0 => '/home/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/transaction/shipments/viewPacking.html',
      1 => 1503277292,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '116218667957053a46f1b607-26476984',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_57053a470d0c43_06937884',
  'variables' => 
  array (
    'no' => 0,
    'status' => 0,
    'date' => 0,
    'dateSyst' => 0,
    'whse' => 0,
    'cust' => 0,
    'carrier' => 0,
    'item_name' => 0,
    'trailer' => 0,
    'ship_list' => 0,
    'ship' => 0,
    'shipTo' => 0,
    'seal' => 0,
    'packingList' => 0,
    'i' => 0,
    'shipLots' => 0,
    'vic_ctr' => 0,
    'totalbag' => 0,
    'email' => 0,
    'noShipment' => 0,
    'id_out' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57053a470d0c43_06937884')) {function content_57053a470d0c43_06937884($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-left fa-4x"></i>Packing List</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Salidas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Packing List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">
                <div id="success" class="alert alert-success animated fadeIn" style="display:none;">
                           
                </div>
                <div id="error" class="alert alert-danger animated fadeIn" style="display:none;">
                    <span class="alert-icon"><i class="fa fa-times"></i></span>
                    <strong>Error al enviar!</strong> verifique la dirección de correo electronico </a>.
                </div>
        <!-- Start basic validation -->
            <div class="">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Salida No.</label>
                                <?php echo $_smarty_tpl->tpl_vars['no']->value;?>

                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;">
                            <?php if ($_smarty_tpl->tpl_vars['status']->value=="Nuevo") {?>
                                <span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
                            <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Listo"||$_smarty_tpl->tpl_vars['status']->value=="Listo Modificado") {?>
                                <span id="stat" class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
                            <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="Terminado") {?>
                                <span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['status']->value;?>
</span>
                            <?php }?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha:</label>                     
                                     <?php echo $_smarty_tpl->tpl_vars['date']->value;?>

                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div> 
                        <div class="row">
                            <div class="form-group col-sm-6">

                                <label class="control-label">Bodega:</label>                     
                                     <?php echo $_smarty_tpl->tpl_vars['whse']->value;?>

                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"></div> 
                        </div> 
                        <hr />
                    <form class="form-horizontal" role="form" id="receipt"  method="post">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente</label>
                                 <?php echo $_smarty_tpl->tpl_vars['cust']->value;?>

                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Carrier</label>
                                <input type="text" class="form-control input-sm" name="carrier" id="" value="<?php echo $_smarty_tpl->tpl_vars['carrier']->value;?>
" >
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto</label>
                                <?php echo $_smarty_tpl->tpl_vars['item_name']->value;?>
 
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                                <label class="control-label">Trailer</label>
                                <input type="text" class="form-control input-sm" name="trailer" value="<?php echo $_smarty_tpl->tpl_vars['trailer']->value;?>
">
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                             <label class="control-label">Ship To<span class="asterisk">*</span></label>
                                <select class="form-control mb-15" name="shipTo" tabindex="2"> 
                                <option value="">Selecciona Ship</option>
                                <?php  $_smarty_tpl->tpl_vars['ship'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ship']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['ship_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ship']->key => $_smarty_tpl->tpl_vars['ship']->value) {
$_smarty_tpl->tpl_vars['ship']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['ship']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['ship']->value['ship_code']==$_smarty_tpl->tpl_vars['shipTo']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_state'];?>
,<?php echo $_smarty_tpl->tpl_vars['ship']->value['ship_address'];?>
</option>
                                <?php } ?>                              
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Factura</label>
                                <input type="text" class="form-control input-sm" name="seal" value="<?php echo $_smarty_tpl->tpl_vars['seal']->value;?>
">
                            </div><!-- /.form-group -->
                        </div> 
                        <hr />
                        <div class="row">
                            <div class="table-responsive mb-20">
                                <table id="table-ship" class="table">
                                 <thead>
                                    <tr>
                                        <td>Vicar Ctrl</td>
                                        <td>Lote</td>
                                        <td>Bags</td>
                                        <td>HT</td>
                                        <td>Etiquetas</td>
                                        <td>Location</td>
                                        <td>Storage</td>
                                        <td>Notas</td>
                                    </tr>
                                  </thead>
                                  <tbody>
                                     <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                    <?php  $_smarty_tpl->tpl_vars['shipLots'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['shipLots']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['packingList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['shipLots']->key => $_smarty_tpl->tpl_vars['shipLots']->value) {
$_smarty_tpl->tpl_vars['shipLots']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['shipLots']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                    <tr id="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lotShip_code'];?>
">
                                        <td style="display:none;">
                                            <input type="text" name="idLotShip[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lotShip_code'];?>
" readonly>
                                        </td>
                                    	<td>
                                           <?php echo $_smarty_tpl->tpl_vars['vic_ctr']->value[$_smarty_tpl->tpl_vars['i']->value-1];?>

                                        </td>
                                        <td>
                                           <?php echo $_smarty_tpl->tpl_vars['shipLots']->value['lot'];?>

                                        </td>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['shipLots']->value['bags_totalShip'];?>

                                        </td>
                                        <td>
                                            <div class="ckbox ckbox-theme">
                                            	<input id="ht<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" type="checkbox" name="ht[]" value="1"'<?php if ($_smarty_tpl->tpl_vars['shipLots']->value['ht']=="si") {?> checked <?php }?>' disabled>
                                            	<label for="ht<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="control-label"></label>
                                            </div>
                                        </td>
                                        <td>
                                           <div class="ckbox ckbox-theme">
	                                           <input id="tags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" type="checkbox" name="tags[]" value="1"'<?php if ($_smarty_tpl->tpl_vars['shipLots']->value['tags']=="si") {?> checked <?php }?>' disabled>
	                                           <label for="tags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="control-label"></label>
                                           </div>
                                        </td>    
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['shipLots']->value['location'];?>

                                        </td>
                                        <td>
                                       		<?php echo $_smarty_tpl->tpl_vars['shipLots']->value['storage'];?>

                                        </td>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['shipLots']->value['notes'];?>

                                        </td>
                                    </tr>
                                     <?php } ?>
                                   </tbody>
                                </table>
                                <input type="hidden" id="numLots" name="index" class="" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                             <div class="form-group col-sm-2">
                             <?php $_smarty_tpl->tpl_vars['totalbag'] = new Smarty_variable(0, null, 0);?>
                             <?php  $_smarty_tpl->tpl_vars['shipLots'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['shipLots']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['packingList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['shipLots']->key => $_smarty_tpl->tpl_vars['shipLots']->value) {
$_smarty_tpl->tpl_vars['shipLots']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['shipLots']->key;
?>
                             <?php $_smarty_tpl->tpl_vars['totalbag'] = new Smarty_variable($_smarty_tpl->tpl_vars['totalbag']->value+$_smarty_tpl->tpl_vars['shipLots']->value['bags_totalShip'], null, 0);?>
                             <?php } ?>
                                     <br>
                                <label class="control-label">Total Bags:</label>                     
                                     <?php echo $_smarty_tpl->tpl_vars['totalbag']->value;?>

                            </div><!-- /.form-group -->

                            <div class="col-sm-10 pull-right">
                                <label class="control-label">Cargar Imagenes</label> 
                                <div action="<?php echo base_url('shipments/images');?>
/<?php echo $_smarty_tpl->tpl_vars['no']->value;?>
" class="col-sm-12 dropzone pull-right">
                                    <div class="fallback">
                                        <input name="img" type="file" multiple />
                                    </div>
                                </div><!-- /.panel-body -->
                            </div>
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <?php if ($_smarty_tpl->tpl_vars['email']->value==null) {?>
                            <div class="callout callout-warning">
                                <strong>Aviso!</strong>Casilla <strong>Enviar Correo</strong> desabilitada, Cliente sin Email Asignado.
                            </div>
                         <?php }?>
                        <div class="form-group col-sm-4">
                            <label class="col-sm-6 control-label" style="margin-left:200px;">Enviar Correo</label>
                            <div class="col-sm-1 ckbox ckbox-theme">
                               <input id="sendMail" type="checkbox" name="sendMail" value="1" <?php if ($_smarty_tpl->tpl_vars['email']->value==null) {?> disabled <?php }?>>
                               <label for="sendMail" class=" control-label"></label>
                            </div>
                        </div>
                        <div id="button" class="col-sm-offset-3">
                        <a href="<?php echo base_url('packingList/impPacking');?>
/<?php echo $_smarty_tpl->tpl_vars['noShipment']->value;?>
" id="export" type="button" class="btn btn-theme" target="_blank">Exportar PDF</a>
                        <button type="button" id="embarcado" data-ship="<?php echo $_smarty_tpl->tpl_vars['no']->value;?>
" data-bill="<?php echo $_smarty_tpl->tpl_vars['id_out']->value;?>
" class="btn btn-theme" disabled>Marcar Embarcado</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div><!-- /.panel-body -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <?php }} ?>
