<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-04 09:16:21
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/transaction/receipts/receipts.html" */ ?>
<?php /*%%SmartyHeaderCode:25633735156f2de7de43572-21280270%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '84615cb72f5532c80aae091c445b980e8f1ff663' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/transaction/receipts/receipts.html',
      1 => 1459782962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '25633735156f2de7de43572-21280270',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f2de7ded2ab4_20191764',
  'variables' => 
  array (
    'list_receipts' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f2de7ded2ab4_20191764')) {function content_56f2de7ded2ab4_20191764($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> ENTRADAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Entrada</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
                <div>
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-danger">
                        <thead>
                        <tr> 
                            <th data-class="expand">Vicar Ctrl</th> 
                            <th data-hide="phone">Estatus</th>    
                            <th data-hide="phone">Fecha Registro</th>
                            <th data-hide="phone">Fecha Entrada</th>
                            <th data-hide="phone">Po. No.</th>
                            <th data-hide="phone">Cliente</th>
                            <th data-hide="phone">Producto</th>
                            <th data-hide="phone">Proveedor</th>
                            <th data-hide="phone">HT</th>
                            <th data-hide="phone">Etiquetas</th>
                            <th data-hide="phone">Opciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_receipts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
</td>
                                <?php if ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo") {?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo actualizado") {?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php } else { ?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php }?>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['system_date'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['receipt_date'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['po_no'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['ht'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['tags'];?>
</td>                                
                                <td class="text-center">
                                <?php if ($_smarty_tpl->tpl_vars['con']->value['status']=="Recibido") {?>
                                    <a href="select_lotsReceipts/<?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
" class="btn btn-sm btn-success btn-xs btn-push btn-view"><i class="fa fa-eye"></i> Ver Más</a>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo") {?>
                                    <a href="load_updateRecipt/<?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil">&nbsp;</i>&nbsp; Editar &nbsp;</a>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo actualizado") {?>
                                    <a href="select_lotsReceipts/<?php echo $_smarty_tpl->tpl_vars['con']->value['vicar_ctrl'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil">&nbsp;</i>&nbsp; Editar &nbsp;</a>
                                <?php }?>
                                    <a href="#" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<!--MODAL VER-->
        <div class="modal fade bs-example-modal-lg" id="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <iframe src=""></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<?php }} ?>
