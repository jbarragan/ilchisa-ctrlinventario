<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-08 12:04:27
         compiled from "/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/catalogs/items/items.html" */ ?>
<?php /*%%SmartyHeaderCode:6317259435989ef1ba73289-74402728%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b651af552299da0a42e2d383ff53c6d6768c2bf' => 
    array (
      0 => '/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/contents/catalogs/items/items.html',
      1 => 1461178179,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6317259435989ef1ba73289-74402728',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'list_items' => 0,
    'con' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5989ef1c015096_32809056',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5989ef1c015096_32809056')) {function content_5989ef1c015096_32809056($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> PRODUCTOS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Producto</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Productos</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-danger">
                        <thead>
                        <tr>
                            <th data-hide="phone" class="text-center">Codigo</th>      
                            <th data-class="expand" class="text-center">Nombre</th>
                            <th data-hide="phone,tablet" class="text-center">Descripción</th>
                            <th data-hide="phone" class="text-center">Acción</th>
                            <th style="display:none;"></th>
                            <th style="display:none;"></th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['desc'];?>
</td>
                                <td class="text-center">
                                    <a href="<?php echo base_url('item/load_edit');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Editar</a>
                                    <a href="<?php echo base_url('item/delete_item');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                                <td style="display:none;"></td>
                                <td style="display:none;"></td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                        <!--tfoot section is optional-->
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
