<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-12 12:54:25
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/receipts/receipts.html" */ ?>
<?php /*%%SmartyHeaderCode:12904929025a397605f2e475-98391509%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc5ad066b049a09ea01c97811354bc6fc63c27c0' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/transaction/receipts/receipts.html',
      1 => 1515786837,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12904929025a397605f2e475-98391509',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a39760608d041_32972222',
  'variables' => 
  array (
    'list_receipts' => 0,
    'con' => 0,
    'i' => 0,
    'renyC' => 0,
    'dateS' => 0,
    'date' => 0,
    'employees' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a39760608d041_32972222')) {function content_5a39760608d041_32972222($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-right fa-4x"></i> ENTRADAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Entrada</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
                <div>
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-theme" >
                        <thead>
                        <tr> 
                            <th data-class="expand" style="max-width: 20px">No. Ctrl</th> 
                            <th data-hide="phone,tablet" style="max-width: 20px">Estatus</th>    
                            <th data-hide="phone,tablet">Fecha Registro</th>
                            <th data-hide="phone,tablet">Fecha Entrada</th>
                            <th data-hide="phone,tablet">Po. No.</th>
                            <th data-hide="phone,tablet">Cliente</th>
                            <th data-hide="phone,tablet">Producto</th>
                            <th data-hide="phone,tablet">Proveedor</th>
                            <th data-hide="phone,tablet" style="max-width: 20px">HT</th>
                            <th data-hide="phone,tablet" style="max-width: 30px">Etiquetas</th>
                            <th data-hide="phone,tablet">Opciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_receipts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr id="<?php echo $_smarty_tpl->tpl_vars['con']->value['reny_ctrl'];?>
">
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['renyC']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <?php if ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo") {?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-success"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo actualizado") {?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php } else { ?>
                                <td style="font-size:16px; text-align:center;"><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</span></td>
                                <?php }?>
                                <td><?php echo $_smarty_tpl->tpl_vars['dateS']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['date']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['po_no'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['ht'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['tags'];?>
</td>                                
                                <td class="text-center">
                                <?php if ($_smarty_tpl->tpl_vars['con']->value['status']=="Recibido") {?>
                                    <a href="select_lotsReceipts/<?php echo $_smarty_tpl->tpl_vars['con']->value['reny_ctrl'];?>
" class="btn btn-sm btn-success btn-xs btn-push btn-view"><i class="fa fa-eye"></i> Ver Más</a>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo") {?>
                                    <a href="load_updateRecipt/<?php echo $_smarty_tpl->tpl_vars['con']->value['reny_ctrl'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil">&nbsp;</i>&nbsp; Editar &nbsp;</a>
                                <?php } elseif ($_smarty_tpl->tpl_vars['con']->value['status']=="nuevo actualizado") {?>
                                    <a href="select_lotsReceipts/<?php echo $_smarty_tpl->tpl_vars['con']->value['reny_ctrl'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil">&nbsp;</i>&nbsp; Editar &nbsp;</a>
                                <?php }?>
                                    <a id="<?php echo $_smarty_tpl->tpl_vars['con']->value['reny_ctrl'];?>
" class="btn btn-sm btn-danger btn-xs btn-push delRec"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                            </tr>
                            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
    <div class="row">
        <div class="row">
        <div class="col-md-12">
        <div class="callout callout-info mb-20">
            <br>
            <strong>Ingresa en el cuadro de busqueda el No. de control de la entrada que deseas consultar</strong>
        </div>
        
        <div class="row">
            <div class="col-sm-4 pull-left">
                <form id="receiptReport" class="navbar-form" action="<?php echo base_url('receipts/search_recreny');?>
" method="post" >
                    <div class="form-group has-feedback">
                        <label class="control-label">reny Ctrl.:</label>
                        <input placeholder="Buscar Entrada" name="buscarrenyCtrl" class="form-control" type="text">
                        <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
                    </div>
                </form>
            </div>
        </div>
            
             <?php if (isset($_smarty_tpl->tpl_vars['list_receipts']->value[0]["sigo_ctrl"])) {?>
        <!-- Start datatable using ajax -->
                <div id="table">
                    <!-- Start datatable -->
                     <table id="datatabletrx" class="table table-theme">
                        <thead>
                        <tr> 
                            <th class="text-center" data-class="expand">RT</th> 
                            <th class="text-center" data-hide="phone">PO</th>
                            <th class="text-center" data-hide="phone">Fecha</th>
                            <th class="text-center" data-hide="phone">Usuario</th>
                            <th class="text-center" data-hide="phone">Producto</th>
                            <th class="text-center" data-hide="phone">QTY</th> 
                            <th class="text-center" data-hide="phone">Lote</th>
                            <th class="text-center" data-hide="phone">KG/Bolsa</th>    
                            <th class="text-center" data-hide="phone">F.Caducidad</th>    
                            <th class="text-center" data-hide="phone">storage</th>    
                            <th class="text-center" data-hide="phone">Estatus</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody class="text-center">
                        <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_receipts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr id="<?php echo $_smarty_tpl->tpl_vars['con']->value['sigo_ctrl'];?>
">
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['sigo_ctrl'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['po_no'];?>
</span></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['date']->value[$_smarty_tpl->tpl_vars['i']->value];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['user'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['bags_total'];?>
</td> 
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['lot'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['pallets'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['exp_date'];?>
</td> 
                                <td></td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['status'];?>
</td>      
                            </tr>
                        <tr class="text-center">
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['no_shipment'];?>
</td>
                               <td style="background-color: #DFEEF8"></td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['system_dateShip'];?>
</td>
                               <td style="background-color: #DFEEF8"></td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['bags_totalShip'];?>
</td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['lot'];?>
</td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['palletsShip'];?>
</td>
                               <td style="background-color: #DFEEF8"></td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['storage'];?>
</td>
                               <td style="background-color: #DFEEF8"><?php echo $_smarty_tpl->tpl_vars['con']->value['statusShip'];?>
</td>
                         </tr> 
                            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                        <?php } ?> 
                        </tbody>
                    </table>
                    <?php }?>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>

         <?php if ($_smarty_tpl->tpl_vars['employees']->value=='customer') {?>
         <div class="form-group col-sm-3">
                        <a <?php if (!empty($_smarty_tpl->tpl_vars['list_receipts']->value)) {?>href="<?php echo base_url('reports/selectReceipt/');?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin-top:29px" target="_blank">Exportar Reporte</a>
                    </div>
         <?php }?>
    </div>
    </div>
</div>
<!--MODAL VER-->
        <div class="modal fade bs-example-modal-lg" id="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <iframe src=""></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
<?php }} ?>
