<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-04 09:16:24
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/transaction/receipts/receiptLots.html" */ ?>
<?php /*%%SmartyHeaderCode:27990993956f2f0c960f156-29132455%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '525a5a72d458ec98a80a05647a66001b9c38f5fc' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/transaction/receipts/receiptLots.html',
      1 => 1459782962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27990993956f2f0c960f156-29132455',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f2f0c97d03d8_92827096',
  'variables' => 
  array (
    'receipt' => 0,
    'con2' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'items' => 0,
    'custom' => 0,
    'ht' => 0,
    'vendor' => 0,
    'tags' => 0,
    'numlots' => 0,
    'lots' => 0,
    'i' => 0,
    'conLots' => 0,
    'loc' => 0,
    'images' => 0,
    'img' => 0,
    'vicarCtrl' => 0,
    'submit' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f2f0c97d03d8_92827096')) {function content_56f2f0c97d03d8_92827096($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i>ENTRADA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
            <div>
            	<?php  $_smarty_tpl->tpl_vars['con2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con2']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['receipt']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con2']->key => $_smarty_tpl->tpl_vars['con2']->value) {
$_smarty_tpl->tpl_vars['con2']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con2']->key;
?>
                <form class="form-horizontal" role="form" id="receipt" action="<?php echo base_url('receipts/ready');?>
/<?php echo $_smarty_tpl->tpl_vars['con2']->value['vicar_ctrl'];?>
" method="post">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Vicar Ctrl<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm" name="receipt_vicar" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['vicar_ctrl'];?>
" readonly="true" >
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;">
                            <?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="nuevo actualizado") {?>
                            	<span class="label label-warning"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                            <?php } else { ?>
                            	<span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['con2']->value['status'];?>
</span>
                            <?php }?>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                            
                                     <input type="text" class="form-control input-sm" name="date" id="date_rec" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['receipt_date'];?>
" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="whse" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['whse_code'];?>
">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['whse_code']==$_smarty_tpl->tpl_vars['con2']->value['whse_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['con2']->value['id_appointment'];?>
</div> 
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Po. No.<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="po" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['po_no'];?>
">
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="item" tabindex="2">
                                    <option value="">Selecciona Producto</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['items_code']==$_smarty_tpl->tpl_vars['con2']->value['items_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>                            
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="customer" tabindex="2" value="<?php echo $_smarty_tpl->tpl_vars['con2']->value['customer_code'];?>
">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['con2']->value['customer_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>

                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                            <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="ht" type="checkbox" name="ht" value="1" <?php echo $_smarty_tpl->tpl_vars['ht']->value;?>
>
                                    <label for="ht" class=" control-label">HT</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Proveedor<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="vendor" tabindex="2" >
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['vendor_code']==$_smarty_tpl->tpl_vars['con2']->value['vendor_code']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                             <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="tags" type="checkbox" name="tags" value="1" <?php echo $_smarty_tpl->tpl_vars['tags']->value;?>
>
                                    <label for="tags" class=" control-label">Etiquetas</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>
                         <?php } ?>  
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                            <input type="hidden" id="numLots" value="<?php echo $_smarty_tpl->tpl_vars['numlots']->value;?>
">
                                <table id="datatable" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Lote</td>
                                        <td>Pallets</td>
                                        <td style="width:0px;"></td>
                                        <td>Bags</td>
                                        <td>Prod Date</td>
                                        <td>Exp Date</td>
                                        <td>Damage</td>
                                        <td>Location</td>
                                        <td>acción</td>
                                    </tr>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>
                                    <?php  $_smarty_tpl->tpl_vars['conLots'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['conLots']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['lots']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['conLots']->key => $_smarty_tpl->tpl_vars['conLots']->value) {
$_smarty_tpl->tpl_vars['conLots']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['conLots']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
                                    <tr id="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
">
                                    	<td style="display:none;">
                                            <input type="text" name="idLots[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
" readonly>
                                        </td>
                                        <td>
                                            <input type="text" name="lots[]" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lot'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?> readonly <?php }?>' readonly>
                                        </td>
                                        <td>
                                            <input type="number" name="pallets[]" id="pallets" class="form-control pallets<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['pallets'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?> readonly <?php }?>' readonly>
                                        </td> 
                                        <td style="width:0px">
                                            <input type="hidden" name="bags_pallets[]" id="bags_pallets" class="form-control bags_palletsl<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['bags_pallets'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?> readonly <?php }?>' readonly>
                                        </td>
                                        <td>
                                            <input type="number" name="bags[]" id="bags" class="form-control bags<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['bags_total'];?>
" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?> readonly <?php }?>' readonly>
                                        </td>
                                        <td>
                                            <input type="text" name="prod_date[]" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['prod_date'];?>
" id="prod" class="form-control" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?>  disabled <?php }?>' readonly>
                                        </td>
                                        <td>
                                            <input type="text" name="exp_date[]" value="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['exp_date'];?>
" id="exp" class="form-control" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?>  disabled <?php }?>' readonly>
                                        </td>  
                                        <td class="text-center">
                                            <div class="ckbox ckbox-theme">
                            				    <input id="damage<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="checkbox" type="checkbox" name="damage[]" value="1"'<?php if ($_smarty_tpl->tpl_vars['conLots']->value['damage']=="si") {?> checked="true" <?php }?>'>
                                                <label for="damage<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="control-label">
                                            </div>
                                        </td>
                                        <td>
                                        <select class="form-control" name="whse_loc[]" tabindex="2" id="locat<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" >
                                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['loc']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['id_location'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['id_location']==$_smarty_tpl->tpl_vars['conLots']->value['id_location']) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['location'];?>
</option>
                                            <?php } ?>                                   
                                        </select>
                                        </td>
                                        <td class="text-center">
                                            <a id="<?php echo $_smarty_tpl->tpl_vars['conLots']->value['lote_code'];?>
" class="btn btn-danger btn-xs delLot" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                     <?php } ?>
                                     <input type="hidden" name="index" class="" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                                </table>
                                    <button type="button" class="btn btn-theme pull-right" id="addLots" '<?php if ($_smarty_tpl->tpl_vars['con2']->value['status']=="Recibido") {?> disabled <?php }?>'>Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Pallets:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="total" placeholder="">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Imagenes</label>
                                        <div>
                                            <?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value) {
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['img']->key;
?>
                                            <div class="col-sm-4" id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
">
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['img']->value['url'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" width="100%">
                                                <a id="<?php echo $_smarty_tpl->tpl_vars['img']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['img']->value['name'];?>
" class="btn btn-danger btn-xs delImg" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar" style="position:absolute; top:1px;"><i class="fa fa-times"></i></a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">Cargar Imagenes</label> 
                                        <div action="<?php echo base_url('receipts/images');?>
/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vicarCtrl']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
" class="col-sm-12 dropzone pull-right">
                                            <div class="fallback">
                                                <input name="img" type="file" multiple />
                                            </div>
                                        </div><!-- /.panel-body -->
                                    </div>
                                </div>
                            </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                           
                           <?php echo $_smarty_tpl->tpl_vars['submit']->value;?>

                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div>
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
