<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-15 20:35:04
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\brand\editbrand.html" */ ?>
<?php /*%%SmartyHeaderCode:134845a340d3d814356-96856280%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '26ca7cb095c096b6c4c30ec948a94ae445629286' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\brand\\editbrand.html',
      1 => 1513362115,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134845a340d3d814356-96856280',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a340d3d952618_57376057',
  'variables' => 
  array (
    'editbrand' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a340d3d952618_57376057')) {function content_5a340d3d952618_57376057($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i> EDITAR MARCA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Editar Marca</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
           
            <div class="panel-body">

                <form class="form-horizontal form-bordered" role="form" id="brand" action="<?php echo base_url('brand/edit_brand');?>
/<?php echo $_smarty_tpl->tpl_vars['editbrand']->value->id_brand;?>
" method="post">
                    <div class="form-body">
                        <div class="form-group has-feedback">
                            <label class="col-sm-3 control-label">Nombre<span class="asterisk">*</span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control input-sm" name="name_brand" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['editbrand']->value->name_brand;?>
">
                            </div>
                        </div>
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme">Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>

            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row --><?php }} ?>
