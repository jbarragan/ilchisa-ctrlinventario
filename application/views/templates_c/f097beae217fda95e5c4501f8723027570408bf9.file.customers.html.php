<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-12-16 20:18:25
         compiled from "C:\xampp\htdocs\renypicot\application\views\templates\contents\catalogs\customers\customers.html" */ ?>
<?php /*%%SmartyHeaderCode:175035a302d2a60c206-49333709%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f097beae217fda95e5c4501f8723027570408bf9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\renypicot\\application\\views\\templates\\contents\\catalogs\\customers\\customers.html',
      1 => 1513371595,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175035a302d2a60c206-49333709',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a302d2a671ba3_65845964',
  'variables' => 
  array (
    'list_customers' => 0,
    'con' => 0,
    'list_Ship' => 0,
    'sh' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a302d2a671ba3_65845964')) {function content_5a302d2a671ba3_65845964($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-users fa-4x"></i> CLIENTES</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Catalogos</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Buscar Cliente</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <!-- Start datatable using ajax -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h3 class="panel-title">Lista de Clientes</h3>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body">
                    <!-- Start datatable -->
                     <table id="datatable-dom" class="table table-striped table-theme">
                        <thead>
                        <tr>
                            <th data-class="expand" style="max-width: 20px">Codigo</th>      
                            <th data-hide="phone">Nombre</th>
                            <th data-hide="phone">Dirección</th>
                            <th data-hide="phone">Telefono</th>
                            <th data-hide="phone,tablet">Email</th>
                            <th data-hide="phone,tablet">Ship to</th>
                            <th data-hide="phone,tablet" style="min-width: 310px" class="text-center">Acciones</th>
                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody>
                        <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_customers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <tr id="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
">
                                <td class="text-center" style="width: 1%"><?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['address'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['con']->value['phone'];?>
</td>
                                <td class="text-center"><?php echo $_smarty_tpl->tpl_vars['con']->value['email'];?>
</span></td>
                                <td>
                                <?php  $_smarty_tpl->tpl_vars['sh'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sh']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['list_Ship']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sh']->key => $_smarty_tpl->tpl_vars['sh']->value) {
$_smarty_tpl->tpl_vars['sh']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['sh']->key;
?>
                                    <?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['sh']->value['customer_code']) {?>
                                    <a href="<?php echo base_url('customer/loadedit_ship');?>
/<?php echo $_smarty_tpl->tpl_vars['sh']->value['ship_code'];?>
"><b><?php echo $_smarty_tpl->tpl_vars['sh']->value['ship_state'];?>
</b>, <?php echo $_smarty_tpl->tpl_vars['sh']->value['ship_address'];
echo $_smarty_tpl->tpl_vars['sh']->value['customer_code'];?>
</a>
                                    </br>
                                    <?php }?>
                                <?php } ?> 
                                </td>
                                <td class="text-center">
                                     <a href="<?php echo base_url('customer/load_edit');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Editar</a>
                                    <a href="<?php echo base_url('customer/customerUser');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" class="btn btn-sm btn-success btn-xs btn-push"><i class="fa fa-eye"></i> Usuario</a>
                                    <a href="<?php echo base_url('customer/load_ship');?>
/<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" class="btn btn-sm btn-warning btn-xs btn-push"><i class="fa fa-pencil"></i> Crear Ship</a>
                                    <a id="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" data-customer="<?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
" class="btn btn-sm btn-danger btn-xs btn-push delCust"><i class="fa fa-trash"></i> Eliminar</a>
                                </td>
                            </tr>
                        <?php } ?>  
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            </div><!-- /.panel -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
