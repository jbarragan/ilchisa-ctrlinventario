<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-04-04 09:34:10
         compiled from "/home/vicarcor/public_html/prueba/application/views/templates/contents/transaction/receipts/NewReceipt.html" */ ?>
<?php /*%%SmartyHeaderCode:184838620356f2e0f1afd3b2-34395785%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc0ba542ab91b533d677f900fc3480498d2dce67' => 
    array (
      0 => '/home/vicarcor/public_html/prueba/application/views/templates/contents/transaction/receipts/NewReceipt.html',
      1 => 1459782962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '184838620356f2e0f1afd3b2-34395785',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56f2e0f1b87974_42304835',
  'variables' => 
  array (
    'vicarCtrl' => 0,
    'dat' => 0,
    'dateSyst' => 0,
    'whseOption' => 0,
    'con' => 0,
    'appointment' => 0,
    'po' => 0,
    'items' => 0,
    'custom' => 0,
    'customer' => 0,
    'vendor' => 0,
    'vend' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56f2e0f1b87974_42304835')) {function content_56f2e0f1b87974_42304835($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-shopping-cart fa-4x"></i>ENTRADA</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Transacciones</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Entradas</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Nuevo Entrada</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->
<div class="row">
    <div class="col-md-12">

        <!-- Start basic validation -->
        <div class="panel rounded shadow">
            <div class="panel-heading">
                <div class="pull-left">
                    <h3 class="panel-title">Registrar Nueva Entrada</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body">

                <form class="form-horizontal" role="form" id="receipt" action="<?php echo base_url('receipts/reg_receipts');?>
" method="post">
                            
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                    <label class="control-label">Vicar Ctrl<span class="asterisk">*</span></label>
                                    <input type="text" class="form-control input-sm date_rec" name="receipt_vicar" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['vicarCtrl']->value;?>
" readonly="true">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right" style="font-size:22px;"><span class="label label-success">NUEVO</span></div> 
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Fecha<span class="asterisk">*</span></label>                            
                                     <input type="text" class="form-control input-sm" name="date" id="date" value="<?php echo $_smarty_tpl->tpl_vars['dat']->value;?>
">
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['dateSyst']->value;?>
</span></div> 
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Bodega<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="form-control mb-15" name="whse" tabindex="2">
                                    <option value="">Selecciona Bodega</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['whseOption']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['whse_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['whse_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="col-sm-6 text-right"  style="font-size:22px;"><span><?php echo $_smarty_tpl->tpl_vars['appointment']->value;?>
</div> 
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Po. No.<span class="asterisk">*</span></label>
                                <input type="text" class="form-control input-sm" name="po" placeholder="" value="<?php echo $_smarty_tpl->tpl_vars['po']->value;?>
">
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                                <label class="control-label">Producto<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="item" tabindex="2" disabled="true">
                                    <option value="">Selecciona Producto</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
"><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class="control-label">Cliente<span class="asterisk">*</span></label>                            
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="customer" tabindex="2">
                                    <option value="">Selecciona Cliente</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['customer_code']==$_smarty_tpl->tpl_vars['customer']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                            <div class="form-group col-sm-6">
                            <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="ht" type="checkbox" name="ht" value="1">
                                    <label for="ht" class=" control-label">HT</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>    
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label class=" control-label">Proveedor<span class="asterisk">*</span></label>
                                <select data-placeholder="Choose a Country" class="chosen-select mb-15" name="vendor" tabindex="2">
                                    <option value="">Selecciona Proveedor</option>
                                    <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['vendor']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['vendor_code'];?>
"'<?php if ($_smarty_tpl->tpl_vars['con']->value['vendor_code']==$_smarty_tpl->tpl_vars['vend']->value) {?> selected <?php }?>'><?php echo $_smarty_tpl->tpl_vars['con']->value['name'];?>
</option>
                                    <?php } ?>                                   
                                </select>
                            </div><!-- /.form-group -->
                             <div class="form-group col-sm-6">
                             <br>
                                <div class="ckbox ckbox-theme">
                                    <input id="tags" type="checkbox" name="tags" value="1">
                                    <label for="tags" class=" control-label">Etiquetas</label>
                                </div>
                            </div><!-- /.form-group -->
                        </div>
                        <hr />
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <table id="datatable-dom" class="table table-striped table-lilac">
                                    <tr>
                                        <td>Lote</td>
                                        <td>Pallets</td>
                                        <td style="width:0px;"></td>
                                        <td>Bags</td>
                                        <td>Prod Date</td>
                                        <td>Exp Date</td>
                                        <td>Damage</td>
                                        <td>Location</td>
                                    </tr>
                                    <!-- /<tr>
                                        <td>
                                            <input type="text" name="lots[]" placeholder="Lote" class="form-control">
                                        </td>
                                        <td>
                                            <input type="number" name="pallets[]" placeholder="Pallets" id="pallets" class="form-control">
                                        </td> 
                                        <td style="width:0px">
                                            <input type="hidden" name="bags_pallets[]" id="bags_pallets" class="form-control">
                                        </td>
                                        <td>
                                            <input type="number" name="bags[]" placeholder="Bags" id="bags" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <input type="date" name="prod_date[]" placeholder="Prod. Date" id="prod" class="form-control">
                                        </td>
                                        <td>
                                            <input type="date" name="exp_date[]" placeholder="Exp. Date" id="exp" class="form-control">
                                        </td>  
                                        <td class="text-center">
                                            <div class="ckbox ckbox-theme">
                                                <input id="damage" class="checkbox" type="checkbox" name="damage[]" value="1">
                                                <label for="damage"></label>
                                            </div>
                                        </td>
                                        <td><input type="text" class="form-control" name="receipt_loc" placeholder="" class="form-control input-sm"></td>
                                    </tr> --> 
                                </table>
                                <button type="button" class="btn btn-theme pull-right" id="addLots" disabled="true">Agregar Lote</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                <label class="control-label">Total Pallets:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="total" placeholder="">
                                <label class="control-label">Total Bags:<span class="asterisk">*</span></label>                     
                                     <input type="text" class="form-control input-sm" id="totalBag" placeholder="">
                            </div><!-- /.form-group -->
                            <div class="col-sm-10 pull-right">
                                <label class="control-label">Cargar Imagenes</label> 
                                <div action="<?php echo base_url('receipts/images');?>
/<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['vicarCtrl']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo $_tmp1;?>
" class="col-sm-12 dropzone pull-right">
                                    <div class="fallback">
                                        <input name="img" type="file" multiple />
                                    </div>
                                </div><!-- /.panel-body -->
                              </div>
                        
                    </div><!-- /.form-body -->
                    <div class="form-footer">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-theme" >Guardar</button>
                        </div>
                    </div><!-- /.form-footer -->
                </form>
            </div><!-- /.panel-body -->
        </div><!-- /.panel -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
 <div class="modal fade bs-example-modal-lg" id="bagsPallet">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Bags Por Pallets</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal form-bordered" id="bagsP" role="form">
                                <div class="form-body">
                                    <div class="form-group">
                                    Introduce la cantidad de bolsas por Pallets
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control input-sm" name="bagsPall" id="bagPallets" value="0">
                                        </div>
                                    </div><!-- /.form-group -->
                                </div><!-- /.form-body -->
                                <div class="form-footer">
                                    <div class="col-sm-offset-3">
                                        <button type="button" class="btn btn-theme" id="acept">Aceptar</button>
                                    </div>
                                </div><!-- /.form-footer -->
                            </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal --><?php }} ?>
