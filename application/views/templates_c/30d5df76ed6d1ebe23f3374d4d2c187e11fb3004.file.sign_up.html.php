<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-12 21:56:20
         compiled from "C:\xampp\htdocs\vic\application\views\templates\contents\sign\sign_up.html" */ ?>
<?php /*%%SmartyHeaderCode:1608856e48274e57a13-16137728%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '30d5df76ed6d1ebe23f3374d4d2c187e11fb3004' => 
    array (
      0 => 'C:\\xampp\\htdocs\\vic\\application\\views\\templates\\contents\\sign\\sign_up.html',
      1 => 1456775592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1608856e48274e57a13-16137728',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e48274e82153_80198745',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e48274e82153_80198745')) {function content_56e48274e82153_80198745($_smarty_tpl) {?><!-- Register form -->
<form class="form-horizontal rounded shadow no-overflow" action="<?php echo base_url('account/sign-in');?>
">
    <div class="sign-header">
        <div class="form-group">
            <div class="sign-text">
                <span>Create a new account</span>
            </div>
        </div>
    </div>
    <div class="sign-body">
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="text" class="form-control" placeholder="Username">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="password" class="form-control" placeholder="Password">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="password" class="form-control" placeholder="Confirm Password">
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="email" class="form-control" placeholder="Your Email">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            </div>
        </div>
    </div>
    <div class="sign-footer">
        <div class="form-group">
            <div class="callout callout-info no-margin">
                <p class="text-muted">To confirm and activate your new account, we will need to send the activation code to your e-mail.</p>
            </div>
        </div>
        <div class="form-group">
            <div class="ckbox ckbox-theme">
                <input id="term-of-service" value="1" type="checkbox">
                <label for="term-of-service" class="rounded">I agree with <a href="#">Term Of Service</a></label>
            </div>
            <div class="ckbox ckbox-theme">
                <input id="newsletter" value="1" type="checkbox">
                <label for="newsletter" class="rounded">Send me newsletter</label>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Sign Up</button>
        </div>
    </div>
</form>
<!--/ Register form -->

<!-- Content text -->
<p class="text-muted text-center sign-link">Already have an account? <a href="<?php echo base_url('account/sign-in');?>
"> Sign in here</a></p>
<!--/ Content text --><?php }} ?>
