<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-05 17:56:21
         compiled from "/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/reports/reportReceipt.html" */ ?>
<?php /*%%SmartyHeaderCode:11519272655a39736b0267e1-61131945%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5066fbb7a099585967eed2886de389c7ce6110e5' => 
    array (
      0 => '/home/ilchisa/public_html/ctrlinventario/application/views/templates/contents/reports/reportReceipt.html',
      1 => 1515200131,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11519272655a39736b0267e1-61131945',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a39736b088db4_09870580',
  'variables' => 
  array (
    'employees' => 0,
    'status' => 0,
    'custom' => 0,
    'con' => 0,
    'customer' => 0,
    'items' => 0,
    'item' => 0,
    'custoUser' => 0,
    'list_receipts' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a39736b088db4_09870580')) {function content_5a39736b088db4_09870580($_smarty_tpl) {?><!-- Start page header -->
<div class="header-content">
    <h2><i class="fa fa-arrow-right fa-4x"></i> REPORTE ENTRADAS</h2>
    <div class="breadcrumb-wrapper hidden-xs">
        <span class="label">Estás aquí:</span>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="<?php echo base_url('dashboard');?>
">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Reportes</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Entrada</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

    <div class="row">
        <div class="col-md-12">
        <div>
         <?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>
        	<form id="receiptReport" action="<?php echo base_url('reports/searchReceipt');?>
" method="get" >
                <div class="row">
                    <div class="form-group col-sm-2">
                        <label class="control-label">Fecha Inicial:</label>                            
                             <input type="text" class="form-control input-sm" name="dateIn" placeholder="dd/M/yyyy" id="date" value="">
                    </div><!-- /.form-group -->
                    <div class="form-group col-sm-2">
                        <label class="control-label">A la fecha:</label>                            
                             <input type="text" class="form-control input-sm" name="dateFin" placeholder="dd/M/yyyy" id="date2" value="">
                    </div><!-- /.form-group -->
                </div>   
        		<div class="row">
                    <div class="form-group col-sm-2">
                        <label class="control-label">status</label>
                        <select data-placeholder="Choose a Country" class="form-control chosen-select mb-15" id="status" name="status" tabindex="2">
                            <option value=0 <?php if ($_smarty_tpl->tpl_vars['status']->value==0) {?> selected <?php }?>>Selecciona status</option>
                            <option value=1 <?php if ($_smarty_tpl->tpl_vars['status']->value==1) {?> selected <?php }?>>Nuevo</option>
                            <option value=2 <?php if ($_smarty_tpl->tpl_vars['status']->value==2) {?> selected <?php }?>>Nuevo Actualizado</option>
                            <option value=3 <?php if ($_smarty_tpl->tpl_vars['status']->value==3) {?> selected <?php }?>>Recibido</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Cliente</label>                            
                        <select data-placeholder="Choose a Country" class="form-control chosen-select mb-15" id="customer" name="customer" tabindex="2">
                            <option value=0>Selecciona Cliente</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['custom']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['customer_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['customer']->value==$_smarty_tpl->tpl_vars['con']->value['customer_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['first_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-3">
                        <label class="control-label">Producto</label>
                        <select data-placeholder="Choose a Country" class="form-control chosen-select mb-15" id="item" name="item" tabindex="2">
                            <option value=0>Selecciona Producto</option>
                            <?php  $_smarty_tpl->tpl_vars['con'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['con']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['con']->key => $_smarty_tpl->tpl_vars['con']->value) {
$_smarty_tpl->tpl_vars['con']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['con']->key;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['con']->value['items_code'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value==$_smarty_tpl->tpl_vars['con']->value['items_code']) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['con']->value['item_name'];?>
</option>
                            <?php } ?>                                   
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                       <!-- <a id="exportar" class="btn btn-theme pull-right" style="margin-top:29px"  target="_blank">Exportar Reporte</a>-->
                       <input type="button" id="ToExcelReceipt" class="btn btn-theme pull-right" value="Exportar Excel" style="margin-top:29px; margin-right: 10px"/>
                    </div>
                </div> 
            </form>
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcelReceipt">
                <input type="hidden" id="tablaExcelReceipt" name="table" />
                <input type="hidden" name="filename" value="Reporte_Entradas" />
            </form>
        </div>
        <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['employees']->value=='customer') {?>
            
                <input type="hidden" id="idUserCustom" name="idUserCustom" value="<?php echo $_smarty_tpl->tpl_vars['custoUser']->value;?>
" />
            <form action="<?php echo base_url('reports/exporExcel');?>
" method="post" target="_blank" id="exportExcelReceipt">
                <input type="hidden" id="tablaExcelReceipt" name="table" />
                <input type="hidden" name="filename" value="Reporte_Entradas" />
            </form>
            <!--<a <?php if (!empty($_smarty_tpl->tpl_vars['list_receipts']->value)) {?> href="<?php echo base_url('reports/selectReceipt/');?>
" <?php } else { ?>onclick="alert('No hay reporte que Generar')"<?php }?> class="btn btn-theme pull-right" style="margin:15px 20px;"  target="_blank" id="exportar">Exportar PDF</a>-->
            <input type="button" id="ToExcelReceipt" class="btn btn-theme pull-right" value="Exportar Excel" style="margin:15px 20px;"/>
            <?php }?>
        </div>
        <!-- Start datatable using ajax -->
                <div>
                    <!-- Start datatable -->
                     <table id="datatable" class="table table-striped table-theme">
                        <thead>
                        <tr> 
                            <th class="text-center" data-class="expand">No. Ctrl</th> 
                            <th class="text-center" data-hide="phone">Estatus</th>    
                            <th class="text-center" data-hide="phone">Fecha Registro</th>
                            <th class="text-center" data-hide="phone">Fecha Entrada</th>
                            <th class="text-center" data-hide="phone">Po. No.</th>
                            <th class="text-center" data-hide="phone">Cliente</th>
                            <th class="text-center" data-hide="phone">Producto</th>
                            <th class="text-center" data-hide="phone">Proveedor</th>
                            <th class="text-center" data-hide="phone">Lote</th>
                            <th class="text-center" data-hide="phone">KG/Bolsa</th>
                            <th class="text-center" data-hide="phone">bags</th>

                        </tr>
                        </thead>
                        <!--tbody section is required-->
                        <tbody class="text-center">
                        </tbody>
                    </table>
                    <!--/ End datatable -->
                </div><!-- /.panel-body -->
            <!--/ End datatable using ajax -->
        </div>
    </div>
</div>
<?php }} ?>
