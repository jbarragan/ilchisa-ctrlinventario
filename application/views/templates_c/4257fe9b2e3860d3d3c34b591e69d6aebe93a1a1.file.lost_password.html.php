<?php /* Smarty version Smarty-3.1.21-dev, created on 2016-03-12 21:56:12
         compiled from "C:\xampp\htdocs\vic\application\views\templates\contents\sign\lost_password.html" */ ?>
<?php /*%%SmartyHeaderCode:1477956e4826cec2fb0-61747675%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4257fe9b2e3860d3d3c34b591e69d6aebe93a1a1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\vic\\application\\views\\templates\\contents\\sign\\lost_password.html',
      1 => 1456775593,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1477956e4826cec2fb0-61747675',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_56e4826ceec737_15368062',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56e4826ceec737_15368062')) {function content_56e4826ceec737_15368062($_smarty_tpl) {?><!-- Lost password form -->
<form class="form-horizontal rounded shadow" action="#">
    <div class="sign-header">
        <div class="form-group">
            <div class="sign-text">
                <span>Reset your password</span>
            </div>
        </div>
    </div>
    <div class="sign-body">
        <div class="form-group">
            <div class="input-group input-group-lg rounded">
                <input type="text" class="form-control" placeholder="Username or email ">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            </div>
        </div>
    </div>
    <div class="sign-footer">
        <div class="form-group">
            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded">Send reset email</button>
        </div>
    </div>
</form>
<!--/ Lost password form -->

<!-- Content text -->
<p class="text-muted text-center sign-link">Back to <a href="<?php echo base_url('account/sign-in');?>
"> Sign in</a></p><?php }} ?>
