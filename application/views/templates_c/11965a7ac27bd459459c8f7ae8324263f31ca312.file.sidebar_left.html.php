<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-08-03 10:06:30
         compiled from "/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/partials/sidebar_left.html" */ ?>
<?php /*%%SmartyHeaderCode:122045604559833bf61a0063-88338395%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '11965a7ac27bd459459c8f7ae8324263f31ca312' => 
    array (
      0 => '/home2/vicarcor/public_html/ctrlinventarios/application/views/templates/partials/sidebar_left.html',
      1 => 1467656692,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '122045604559833bf61a0063-88338395',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'username' => 0,
    'role' => 0,
    'employees' => 0,
    'active_dashboard' => 0,
    'active_item' => 0,
    'active_item_search' => 0,
    'new_item' => 0,
    'active_vendor' => 0,
    'active_vendor_search' => 0,
    'new_vendor' => 0,
    'active_customers' => 0,
    'active_customers_search' => 0,
    'new_customers' => 0,
    'active_whse' => 0,
    'active_whseSearch' => 0,
    'new_whse' => 0,
    'active_users' => 0,
    'active_newUsers' => 0,
    'active_users_table' => 0,
    'active_calendar' => 0,
    'active_newAppointment' => 0,
    'active_receipt' => 0,
    'active_searchReceipt' => 0,
    'active_newReceipt' => 0,
    'active_shipments' => 0,
    'active_searchShip' => 0,
    'active_newShipment' => 0,
    'active_packing' => 0,
    'active_adjust' => 0,
    'active_reportreceipt' => 0,
    'active_reporShip' => 0,
    'active_reporInvent' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_59833bf62c8e63_22585535',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59833bf62c8e63_22585535')) {function content_59833bf62c8e63_22585535($_smarty_tpl) {?><aside id="sidebar-left" class="sidebar-circle sidebar-light">

<!-- Start left navigation - profile shortcut -->
<div class="sidebar-content">
    <div class="media">
        <a class="pull-left has-notif avatar">
            <img src="<?php echo base_url('assets/global/img/icon/64/contact.png');?>
" alt="admin">
            <i class="online"></i>
        </a>
        <div class="media-body">
            <h4 class="media-heading">Hello, <span><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</span></h4>
            <small><?php echo $_smarty_tpl->tpl_vars['role']->value;?>
</small>
        </div>
    </div>
</div><!-- /.sidebar-content -->
<!--/ End left navigation -  profile shortcut -->

<!-- Start left navigation - menu -->
<ul class="sidebar-menu">
<?php if ($_smarty_tpl->tpl_vars['employees']->value=='employees') {?>
    <?php if ($_smarty_tpl->tpl_vars['role']->value=='Administrador'||$_smarty_tpl->tpl_vars['role']->value=='Oficina') {?>
<!-- Start navigation - dashboard -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_dashboard']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_dashboard']->value;?>
 <?php }?>">
<a href="<?php echo base_url('dashboard');?>
">
    <span class="icon"><i class="fa fa-home"></i></span>
    <span class="text">Dashboard</span>
    <?php if (isset($_smarty_tpl->tpl_vars['active_dashboard']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
</a>
</li>
<!--/ End navigation - dashboard -->

<!-- Start category catalogos -->
<li class="sidebar-category">
    <span>CATALOGOS</span>
    <span class="pull-right"><i class="fa fa-folder-open"></i></span>
</li>
<!--/ End category apps -->

<!-- Start navigation -Productos -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_item']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_item']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-shopping-cart"></i></span>
        <span class="text">Productos</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_item']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_item_search']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_item_search']->value;?>
 <?php }?>"><a href="<?php echo base_url('item/search');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['new_item']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['new_item']->value;?>
 <?php }?>"><a href="<?php echo base_url('item/load_additem');?>
">Agregar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - Productos -->

<!-- Start navigation - Proveedores -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_vendor']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_vendor']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-truck"></i></span>
        <span class="text">Proveedores</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_vendor']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_vendor_search']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_vendor_search']->value;?>
 <?php }?>"><a href="<?php echo base_url('vendors/search');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['new_vendor']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['new_vendor']->value;?>
 <?php }?>"><a href="<?php echo base_url('vendors/load_addvendor');?>
">Registrar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - proveedores -->

<!-- Start navigation - clientes -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_customers']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_customers']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-users"></i></span>
        <span class="text">Clientes</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_customers']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_customers_search']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_customers_search']->value;?>
 <?php }?>"><a href="<?php echo base_url('customer/search');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['new_customers']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['new_customers']->value;?>
 <?php }?>"><a href="<?php echo base_url('customer/load_addCust');?>
">Registrar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - clientes -->

<!-- Start navigation - Bodegas -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_whse']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_whse']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-building"></i></span>
        <span class="text">Bodegas</span>
        <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_whse']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_whseSearch']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_whseSearch']->value;?>
 <?php }?>"><a href="<?php echo base_url('warehouse/search');?>
">Buscar</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['new_whse']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['new_whse']->value;?>
 <?php }?>"><a href="<?php echo base_url('warehouse/load_add_whse');?>
">Registrar Nuevo</a></li>
    </ul>
</li>
<!--/ End navigation - Bodegas -->
<?php if ($_smarty_tpl->tpl_vars['role']->value=='Administrador') {?>
<!--/ start navigation - usuarios -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_users']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_users']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-user"></i></span>
        <span class="text">Usuarios</span>
         <span class="arrow"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_users']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_newUsers']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_newUsers']->value;?>
 <?php }?>"><a href="<?php echo base_url('account/users_table');?>
">Usuarios</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_users_table']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_users_table']->value;?>
 <?php }?>"><a href="<?php echo base_url('account/load_signup');?>
">Nuevo Usuario</a></li>
    </ul>
</li>
<!--/ End navigation - usuarios -->
<?php }?>
<!-- Start category ui citas-->
<li class="sidebar-category">
    <span>CITAS</span>
    <span class="pull-right"><i class="fa fa-clipboard"></i></span>
</li>
<!--/ End category ui citas-->

<!-- Start navigation - calendario -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_calendar']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_calendar']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('appointment/calendarAppoint');?>
">
        <span class="icon"><i class="fa fa-calendar"></i></span>
        <span class="text">Calendario</span>
        <span class="label label-primary pull-right rounded"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_calendar']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End navigation - calendario -->

<!-- Start navigation - agendar -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_newAppointment']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_newAppointment']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('appointment/add');?>
">
        <span class="icon"><i class="fa fa-book"></i></span>
        <span class="text">Agendar Cita</span>
        <span class="label label-primary pull-right rounded"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_newAppointment']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End navigation - agendar -->
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['role']->value=='Administrador'||$_smarty_tpl->tpl_vars['role']->value=='Oficina'||$_smarty_tpl->tpl_vars['role']->value=='Bodega'||$_smarty_tpl->tpl_vars['role']->value=='Supervisor') {?>
<!-- Start category Transacciones -->
<li class="sidebar-category">
    <span>TRANSACCIONES</span>
    <span class="pull-right"><i class="fa fa-exchange"></i></span>
</li>
<!--/ End category Transacciones -->

<!-- Start development - Entradas -->
<li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_receipt']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_receipt']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-arrow-right"></i></span>
        <span class="text">Entradas</span>
        <span class="plus"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_receipt']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_searchReceipt']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_searchReceipt']->value;?>
 <?php }?>"><a href="<?php echo base_url('receipts/searchReceipt');?>
">Buscar</a></li>
        <?php if ($_smarty_tpl->tpl_vars['role']->value!='Supervisor') {?><li class="<?php if (isset($_smarty_tpl->tpl_vars['active_newReceipt']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_newReceipt']->value;?>
 <?php }?>"><a href="<?php echo base_url('receipts/load_addReceipts');?>
">Nueva Entrada</a></li><?php }?>
    </ul>
</li>
<!--/ End development - Entradas -->

<!-- Start development - Salidas -->
<?php if ($_smarty_tpl->tpl_vars['role']->value!='Supervisor') {?><li class="submenu <?php if (isset($_smarty_tpl->tpl_vars['active_shipments']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_shipments']->value;?>
 <?php }?>">
    <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-arrow-left"></i></span>
        <span class="text">Salidas</span>
        <span class="plus"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_shipments']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
    <ul>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_searchShip']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_searchShip']->value;?>
 <?php }?>"><a href="<?php echo base_url('shipments/searchShip');?>
">Calendario</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_newShipment']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_newShipment']->value;?>
 <?php }?>"><a href="<?php echo base_url('shipments/load_addShipments');?>
">Nueva Salida</a></li>
        <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_packing']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_packing']->value;?>
 <?php }?>"><a href="<?php echo base_url('packingList/load_packingList');?>
">Packing List</a></li>
        <!--/ <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_adjust']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_adjust']->value;?>
 <?php }?>"><a href="<?php echo base_url('shipments/load_adjustment');?>
">Ajuste Inventario</a></li>-->
    </ul>
</li><?php }?>
<!--/ End development - Salidas -->
<?php }?>
<?php }?>
<!-- Start category widget -->
<li class="sidebar-category">
    <span>REPORTES</span>
    <span class="pull-right"><i class="fa fa-file-text"></i></span>
</li>
<!--/ End category widget -->

<!-- Start widget - overview -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_reportreceipt']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_reportreceipt']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('reports/searchReceipt');?>
">
        <span class="icon"><i class="fa fa-arrow-right"></i></span>
        <span class="text">Entradas</span>
        <span class="label label-primary pull-right rounded"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_reportreceipt']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End widget - overview -->

<!-- Start widget - social -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_reporShip']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_reporShip']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('reports/searchShipRep');?>
">
        <span class="icon"><i class="fa fa-arrow-left"></i></span>
        <span class="text">Salidas</span>
        <span class="label label-success pull-right rounded"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_reporShip']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End widget - social -->

<!-- Start widget - blog -->
<li class="<?php if (isset($_smarty_tpl->tpl_vars['active_reporInvent']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_reporInvent']->value;?>
 <?php }?>">
    <a href="<?php echo base_url('reports/searchinvent');?>
">
        <span class="icon"><i class="fa fa-usd"></i></span>
        <span class="text">Inventario</span>
        <span class="label label-info pull-right rounded"></span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_reporInvent']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
    </a>
</li>
<!--/ End widget - blog -->


</ul><!-- /.sidebar-menu -->
<!--/ End left navigation - menu -->

<!-- Start left navigation - footer -->
<!-- /.sidebar-footer -->
<!--/ End left navigation - footer -->

</aside><!-- /#sidebar-left --><?php }} ?>
