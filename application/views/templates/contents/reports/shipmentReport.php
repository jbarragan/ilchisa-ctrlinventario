<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
        <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
        <meta name="author" content="Djava UI">

        <link href="<?php echo base_url('/assets/admin/css/reports.css')?>" rel="stylesheet">
     
    </head>
    <body>
        <div id="header">
                <div class="logo">
                    <img class="logo" src="<?php echo base_url('/assets/global/img/renylogo.png')?>" alt="brand logo" width="250">
                </div>
                <div class="info">
                    <p>9650-A Railroad Dr El Paso, TX 79924</p>
                    <p><strong>Phone Number:</strong> +1 (915) 751.1612</p>
                    <p><strong>Fax Number:</strong> +1 (915) 751.1613</p>
                    <p><strong>E-mail:</strong> info@renycorp.com</p>
                </div>
           <h2><?php echo $title;?></h2> 
        </div>
        <div id="footer">
            <p class="page">page: </p>
        </div>
        <table class="table table-striped table-theme">
            <thead>
                <tr>
                    <th>No. de Salida</th>
                    <th>Estatus</th>     
                    <th>Fecha Registro</th>
                    <th>Fecha Salida</th>
                    <th>Carrier</th>
                    <th>Trailer</th>
                    <th>Factura</th>
                    <th>Lote</th>
                    <th>KG/Bolsa</th>
                    <th>Bags</th>
                    <th>No. Ctrl</th>
                    <th>Cliente</th>
                    <th>Producto</th>
                <?php if ($employees != 'customer') {?> 
                    <th>storage</th>
                <?php }?> 
                </tr> 
            </thead>
        <?php $i = 0; foreach ($list as $rec):?>   
            <tr>
                <td><?php echo $rec['no_shipment'];?></td>
                <td><?php echo $rec['statusShip'];?></td>
                <td><?php echo $dateS[$i];?></td>
                <td><?php echo $date[$i];?></td>
                <td><?php echo $rec['carrier'];?></td>
                <td><?php echo $rec['trailer'];?></td>
                <td><?php echo $rec['seal'];?></td>
                <td><?php echo $rec['lot'];?></td>
                <td><?php echo $rec['palletsShip'];?></td>
                <td><?php echo $rec['bags_totalShip'];?></td>
                <td><?php echo $rec['reny_ctrl'];?></td>
                <td><?php echo $rec['first_name'];?></td>
                <td><?php echo $rec['item_name'];?></td>
                <?php if ($employees != 'customer') {?> 
                <td><?php echo $rec['storage'];?></td>
                <?php }?> 
            </tr>
        <?php $i=$i + 1; endforeach;?>
        <tr class="text-center">
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
                <td>Totales:</td>
                <td>
                    <?php $res = 0; foreach ($list as $rec): 
                    $res = $res +  $rec['palletsShip'];
                    endforeach;
                    echo $res;?>
                </td>
                <td>
                    <?php $resb = 0; foreach ($list as $rec): 
                    $resb = $resb +  $rec['bags_totalShip'];
                    endforeach;
                    echo $resb;?>
                </td>
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>   
                <td style="background-color:transparent;"></td>
                <td style="background-color:transparent;"></td>
            </tr>                   
        </table>
    <style>
    </style>
    </body>
</html>