<html lang="en"><!--<![endif]--><!-- START @HEAD -->
<head>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
        <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
        <meta name="author" content="Djava UI">

        
        <link href="<?php echo base_url('/assets/admin/css/reports.css')?>" rel="stylesheet">
        
</head>
<body>
    <div id="header">
                <div class="logo">
                    <img class="logo" src="<?php echo base_url('/assets/global/img/logo-reny-picot.png')?>" alt="brand logo" width="250">
                </div>
                <div class="info">
                    <p>Industrias Lacteas Chihuahuenses, S.A. de C.V.</p>
                    <p>7158 Merchant Ave, El Paso, Tx 79915</p>
                    <p><strong>Phone Number:</strong> +1 (915) 778-3933</p>
                    <p><strong>Fax Number:</strong> +1 (915) 778-3009</p>
                </div>
                <br>
                <br>
           <h2><?php echo $title;?></h2> 
    </div>
    <br>
    <br>
        <div id="footer">
            <p class="page">page: </p>
        </div><div class="row">
    <div>
        <!-- Start basic validation -->
            <div>
                    <div>
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left;" width="400px">
                                    <p style="font-size:12px;font-weight: 900">No Control:</p>
                                    <?php echo $reny_ctrl; ?>
                                    <br>
                                </td>
                                <td style="text-align: right;">
                                    <span style="font-size:16px;"><?php echo $status; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <p style="font-size:12px; font-weight: 900">Fecha:</p>                  
                                    <?php echo date_format(date_create($date), 'd/M/Y'); ?>
                                </td>
                                <td style="text-align: right;"> 
                                    <span style="font-size:16px;"><?php echo date_format(date_create($dateSyst), 'd/M/Y'); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <p style="font-size:12px; font-weight: 900">Bodega:</p>                  
                                    <?php echo $whse; ?>
                                </td>
                                <td style="text-align: right;"> 
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Cliente:</p>
                                    <?php echo $cust; ?>
                                    <br>
                                </td>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Po. No.: </p>
                                    <?php echo $po_no; ?>

                                </td>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Producto: </p>
                                    <?php echo $item_name; ?>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Proveedor:</p>
                                    <?php echo $vendor; ?>
                                    <br>
                                </td>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Productor:</p>
                                    <?php echo $producer; ?>
                                    <br>
                                </td>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Marca:</p>
                                    <?php echo $brand; ?>
                                    <br>
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Planta:</p>
                                    <?php echo $plant; ?>
                                    <br>
                                </td>
                                <td>
                                    <p style="font-size:14px; font-weight: 900">Clasificaci&oacute;n:</p>
                                    <?php echo $clasification; ?>
                                    <br>
                                </td>
                            </tr> 
                        </table> 
                        <div class="row">
                            <div class="table-responsive mb-20">
                                <table id="table-ship" class="table table-theme">
                                 <thead>
                                    <tr class="text-center">
                                        <th>Lote</th>
                                        <th>KG/Bolsa</th>
                                        <th>Bags</th>
                                        <th>Prod Date</th>
                                        <th>Esp Date</th>
                                        <th>Damage</th>
                                        <th>AMT Damage</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $i = 0; foreach ($list as $lots):?>
                                    <tr class="text-center">
                                        <td>
                                           <?php echo $lots['lot']; ?>
                                        </td>
                                        <td>
                                           <?php echo $lots['pallets']; ?>
                                        </td>
                                        <td>
                                            <?php echo $lots['bags_total']; ?>
                                        </td>
                                        <td>
                                            <?php if ($lots['prod_date'] == '0000-00-00') {
                                                echo '--';
                                             }else{
                                                echo date_format(date_create($lots['prod_date']), 'd/M/Y');
                                            }?>
                                        </td>
                                        <td>
                                           <?php if ($lots['exp_date'] == '0000-00-00') {
                                                echo '--';
                                             }else{
                                                echo date_format(date_create($lots['exp_date']), 'd/M/Y');
                                            }?>
                                        </td>    
                                        <td>
                                            <?php
                                            if ($lots['damage'] == 0) {
                                                echo 'ok';
                                             } elseif ($lots['damage'] == 1) {
                                                echo 'Roto';
                                             }elseif ($lots['damage'] == 2) {
                                                echo 'Faltante';
                                             } ?>
                                        </td>  
                                        <td>
                                            <?php echo $lots['amtDamage']; ?>
                                        </td>
                                    </tr>
                                         <?php $i=$i + 1; endforeach;?>
                                   </tbody>
                                </table>
                                <input type="hidden" id="numLots" name="index" class="" value="{$i}">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                 <?php 
                                 $totalKGbag = 0;

                                 foreach ($list as $kgtotals){
                                 $totalKGbag = $totalKGbag + ($kgtotals['pallets'] * $kgtotals['bags_total']);

                                 } ;?>  
                                    <label class="control-label">Total KG/Bags:</label>                     
                                         <?php echo$totalKGbag;?>
                            </div><!-- /.form-group -->
                         </div><!-- /.form-body -->
                        <div class="row">
                             <div class="form-group col-sm-2">
                                 <?php 
                                 $totalbag = 0;

                                 foreach ($list as $totals){
                                 $totalbag = $totalbag + $totals['bags_total'];
                                 } ;?>  
                                    <label class="control-label">Total Bags:</label>                     
                                         <?php echo$totalbag;?>
                            </div><!-- /.form-group -->
                         </div><!-- /.form-body -->
                         <hr>
                        <label>Imagenes</label>
                         <div class="row">
                                <?php foreach ($images as $img): ?>
                                    <?php if (strrpos($img['name'] , 'jpg') || strrpos($img['name'] , 'JPG') ||strrpos($img['name'] , 'png') || strrpos($img['name'] , 'PNG')|| strrpos($img['name'] , 'jpeg') || strrpos($img['name'] , 'JPEG')): ?>
                                        
                                        <img src="<?php echo $img['url']; ?>/<?php echo str_replace(' ','%20',$img['name']);?>" class="div-img">
                                    <?php endif ?>
                                <?php endforeach;?>
                         </div>
            </div><!-- /.panel-body -->
        <!--/ End basic validation -->
    </div>
</div><!-- /.row -->
</body>
</html>