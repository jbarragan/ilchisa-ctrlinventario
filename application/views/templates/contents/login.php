<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?> 
<html lang="en"><!--<![endif]--><!-- START @HEAD --><head>
        <!-- START @META SECTION -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
        <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
        <meta name="author" content="Djava UI">
        <title> SIGN IN  | Reny Picot</title>
        <!--/ END META SECTION -->

        <!-- START @FAVICONS -->
        <link href="<?php echo base_url('/img/ico/codeigniter/apple-touch-icon-144x144-precomposed.png');?>" rel="apple-touch-icon-precomposed" sizes="144x144">
        <link href="<?php echo base_url('/img/ico/codeigniter/apple-touch-icon-114x114-precomposed.png');?>" rel="apple-touch-icon-precomposed" sizes="114x114">
        <link href="<?php echo base_url('/img/ico/codeigniter/apple-touch-icon-72x72-precomposed.png');?>" rel="apple-touch-icon-precomposed" sizes="72x72">
        <link href="<?php echo base_url('/img/ico/codeigniter/apple-touch-icon-57x57-precomposed.png');?>" rel="apple-touch-icon-precomposed">
        <link href="<?php echo base_url('../img/favicon.png');?>" rel="shortcut icon">
        <!--/ END FAVICONS -->

        <!-- START @FONT STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <!--/ END FONT STYLES -->

        <!-- START @GLOBAL MANDATORY STYLES -->
        <link href="<?php echo base_url('/assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('/assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('/assets/global/plugins/bower_components/animate.css/animate.min.css');?>" rel="stylesheet">
        <!--/ END GLOBAL MANDATORY STYLES -->

        <!-- START @THEME STYLES -->
        <link href="<?php echo base_url('/assets/admin/css/reset.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('/assets/admin/css/layout.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('/assets/admin/css/components.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('/assets/admin/css/plugins.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('/assets/admin/css/themes/black-and-white.theme.css');?>" rel="stylesheet" id="theme">
        <link href="<?php echo base_url('/assets/admin/css/pages/sign.css');?>" rel="stylesheet">
        <!--/ END THEME STYLES -->

        <!-- START @IE SUPPORT -->
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/respond-minmax/dest/respond.min.js');?>"></script>
        <![endif]-->
        <!--/ END IE SUPPORT -->
    </head>
    <!--/ END HEAD -->

    <body class="page-sound">
    <?php 
    if( ! isset( $on_hold_message ) )
    {
       ?>


        <!--[if lt IE 9]>
        <p class="upgrade-browser">Upps!! You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @SIGN WRAPPER -->
        <div id="sign-wrapper" style="min-height: 511px;">
         <?php   
         if( $this->input->get('logout') )
        {
            echo '
                <div class="alert-success" id="logout">
                    <p>
                        You have successfully logged out.
                    </p>
                </div>
            ';
        }
        if( isset( $login_error_mesg ) )
        {
            echo '
                <div class="callout callout-danger animated fadeIn">
                    <p>
                        Login Error #' . $this->authentication->login_errors_count . ': Invalid Username, or Password.
                    </p>
                </div>
            ';
        }
        ?>
            <!-- Brand -->
            <div class="brand">
                <img src="<?php echo base_url('/assets/global/img/logo-reny-picot.png');?>" alt="Reny Picot logo">
            </div>
            <!--/ Brand -->

            <!-- View files to load -->
            <!-- Login form -->
           <?php echo form_open( $login_url, array( 'class' => 'sign-in form-horizontal shadow rounded no-overflow','novalidate'=>'novalidate', 'id'=>'sign-in' ) ); ?>
    <div class="sign-header">
        <div class="form-group">
            <div class="sign-text">
                <span>ACCESO A USUARIO</span>
            </div>
        </div><!-- /.form-group -->
    </div><!-- /.sign-header -->
    <div class="sign-body">
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="text" class="form-control input-sm" placeholder="Username or email " name="login_string">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
        </div><!-- /.form-group -->
        <div class="form-group">
            <div class="input-group input-group-lg rounded no-overflow">
                <input type="password" class="form-control input-sm" placeholder="Password" name="login_pass">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
        </div><!-- /.form-group -->
    </div><!-- /.sign-body -->
    <div class="sign-footer">
        <div class="form-group">
            <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Aceptar</button>
        </div><!-- /.form-group -->
    </div><!-- /.sign-footer -->
</form><!-- /.form-horizontal -->
<!--/ Login form -->
<?php
    
}
    else
    {
        // EXCESSIVE LOGIN ATTEMPTS ERROR MESSAGE
        echo '
            <div style="border:1px solid red;">
                <p>
                    Excessive Login Attempts
                </p>
                <p>
                    You have exceeded the maximum number of failed login<br />
                    attempts that this website will allow.
                <p>
                <p>
                    Your access to login and account recovery has been blocked for ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes.
                </p>
                <p>
                    Please use the <a href="/examples/recover">Account Recovery</a> after ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' minutes has passed,<br />
                    or contact us if you require assistance gaining access to your account.
                </p>
            </div>
        ';
    }?>
        </div><!-- /#sign-wrapper -->
        <!--/ END SIGN WRAPPER -->

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- START @CORE PLUGINS -->
        <script async="" src="//www.google-analytics.com/analytics.js"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/jquery/dist/jquery.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js');?>"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js');?>"></script>
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js');?>"></script>
        <!--/ END CORE PLUGINS -->

        <!-- START @PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url('/assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js');?>"></script>
        <!--/ END PAGE LEVEL PLUGINS -->

        <!-- START @PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url('/assets/admin/js/pages/blankon.sign.js');?>"></script>
        <!--/ END PAGE LEVEL SCRIPTS -->
        <!--/ END JAVASCRIPT SECTION -->

        
        <!-- START GOOGLE ANALYTICS -->
        <script>
        $("#logout").fadeTo(1000, 500).slideUp(500, function(){
            $("#logout").alert('close');
        });
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-55892530-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!--/ END GOOGLE ANALYTICS -->
        

    
    <!-- END BODY -->

</body></html>