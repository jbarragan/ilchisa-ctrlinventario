<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>
<meta charset="utf-8">
<link href="<?php echo base_url('/assets/admin/css/reports.css')?>" rel="stylesheet">
        
</head>
<body>
    <div id="header">
                <div class="logo">
                    <img class="logo" src="<?php echo base_url('/assets/global/img/logo-reny-picot.png')?>" alt="brand logo" width="250">
                </div>
                <div class="info">
                    <p>Industrias Lacteas Chihuahuenses, S.A. de C.V.</p>
                    <p>7158 Merchant Ave, El Paso, Tx 79915</p>
                    <p><strong>Phone Number:</strong> +1 (915) 778-3933</p>
                    <p><strong>Fax Number:</strong> +1 (915) 778-3009</p>
                </div>
           <h2><?php echo $title;?></h2> 
    </div>
    <br>
    <br>
        <div id="footer">
            <p class="page">page: </p>
        </div>
<div class="row">
    <div>
        <!-- Start basic validation -->
            <div>
                    <div >
                        <table id="table-ship" class="table">
                            <tr>
                                <td style="text-align: left;">
                                    <p style="font-size:12px;">Salida No:</p>
                                    <?php echo $no; ?>
                                    <br>
                                </td>
                                <td style="text-align: right;">
                                    <span style="font-size:16px;"><?php echo $status; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <p style="font-size:12px;">Fecha:</p>                  
                                    <?php echo $date; ?>
                                </td>
                                <td style="text-align: right;"> 
                                    <span style="font-size:16px;"><?php echo $dateSyst; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <p style="font-size:12px;">Bodega:</p>                  
                                    <?php echo $whse; ?>
                                </td>
                                <td style="text-align: right;"> 
                                </td>
                            </tr>
                        </table>
                        <hr />
                        <table>
                            <tr>
                                <td width="400px">
                                    <p>Cliente:</p>
                                    <?php echo $cust; ?>
                                    <br>
                                </td>
                                <td>
                                    <p>Carrier: <?php echo $carrier; ?></p>

                                </td>
                            </tr>
                            <tr>
                                <td style="width:50%;">
                                    <p>Producto:</p>
                                    <?php echo $item_name; ?>
                                    <br>
                                </td>
                                <td>
                                    <p>Trailer: <?php echo $trailer; ?></p>
                                     
                                </td>
                            </tr> 
                            <tr>
                                <td style="min-width:50%;">
                                </td>
                                <td>
                                    <p>Factura: <?php echo $seal; ?></p>
                                     
                                </td>  
                            </tr> 
                        </table> 
                        <div class="row">
                            <div class="table-responsive mb-20">
                                <table id="table-ship" class="table table-theme">
                                 <thead>
                                    <tr class="text-center">
                                        <th>reny Ctrl</th>
                                        <th>Lote</th>
                                        <th>Bags</th>
                                        <th>Fecha Prod</th>
                                        <th>Fecha Cad</th>
                                        <th>Location</th>
                                        <th>Sotage</th>
                                        <th>Notas</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $i = 0; foreach ($list as $shipLots):?>
                                    <tr class="text-center">
                                        <td>
                                           <?php echo $vic[$i]; ?>
                                        </td>
                                        <td>
                                           <?php echo $shipLots['lot']; ?>
                                        </td>
                                        <td>
                                            <?php echo $shipLots['bags_totalShip']; ?>
                                        </td>
                                        <td>
                                            <?php echo date_format(date_create($shipLots['prod_date']), 'd/M/Y'); ?>
                                        </td>
                                        <td>
                                           <?php echo date_format(date_create($shipLots['exp_date']), 'd/M/Y'); ?>
                                        </td>    
                                        <td>
                                            <?php echo $shipLots['location']; ?>
                                        </td>
                                        <td>
                                            <?php echo $shipLots['storage']; ?>
                                        </td>    
                                        <td>
                                            <?php echo $shipLots['notes']; ?>
                                        </td>
                                    </tr>
                                         <?php $i=$i + 1; endforeach;?>
                                   </tbody>
                                </table>
                                <input type="hidden" id="numLots" name="index" class="" value="{$i}">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                             <div class="form-group col-sm-2">
                                 <?php 
                                 $totalbag = 0;

                                 foreach ($list as $packingList){
                                 $totalbag =$totalbag + $packingList['bags_totalShip'];
                                 } ;?>  
                                    <label class="control-label">Total Bags:</label>                     
                                         <?php echo$totalbag;?>
                            </div><!-- /.form-group -->
                         </div><!-- /.form-body -->
                         <hr>
                        <label>Imagenes</label>
                         <div class="row">
                                <?php foreach ($images as $img):?>
                                        <img src="<?php echo $img['url']; ?>/<?php echo $img['name'];?>" class="div-img">
                                <?php endforeach;?>
                         </div>
            </div><!-- /.panel-body -->


        <!--/ End basic validation -->
    </div>

</div><!-- /.row -->

     <p class="pos-bootom">Nombre y firma del chofer</p>
</body>
</html>
 