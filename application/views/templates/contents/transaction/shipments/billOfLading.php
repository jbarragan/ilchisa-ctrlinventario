<html lang="en"><!--<![endif]--><!-- START @HEAD -->
<head>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
        <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
        <meta name="author" content="Djava UI">

        <link href="<?php echo base_url('/assets/admin/css/reports.css')?>" rel="stylesheet">
</head>
<body>
		<div id="header">
                <div class="logo">
                    <img class="logo" src="<?php echo base_url('/assets/global/img/logo-reny-picot.png')?>" alt="brand logo" width="250">
                </div>
                <div class="info">
                    <p>300 Revere St El Paso, TX 79905</p>
                    <p><strong>Phone Number:</strong> +1 (915) 751.1612</p>
                    <p><strong>Phone Number:</strong> +1 (915) 751.1613</p>
                    <p><strong>E-mail:</strong> info@renycorp.com</p>
                </div>
           <h2><?php echo $title;?></h2> 
        </div>
        <div id="footer">
            <p class="page">page: </p>
        </div>
   					<div class="ta">
                        <table class=" table table-primary">
                            <tr>
                                <td style="text-align: left;">
                                    <p style="font-size:12px; font-weight: bolder;">Date:                
                                    <?php echo $date; ?></p>  
                                </td>
                                <td style="text-align: right;">
                                    <p style="font-size:12px; font-weight: bolder;">Salida No:
                                    <?php echo $no; ?></p>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" class="table tablebill">
		                        		<tr>
		                        			<th width="54%">SHIP FROM</th>
		                        			<td style="text-align:center; vertical-align: top;">
			                        			Bill Number:<?php echo $billNum; ?>                     			
			                        		</td>
		                        		</tr>
		                        		<tr>
		                        			<td>
		                        				<br>
			                                    <p><strong>Name:</strong>
			                                    <?php echo $whse_name; ?></p>
			                                     <p><strong>Address:</strong>
			                                    <?php echo $whse_address; ?></p>
			                                     <p><strong>City</strong>
			                                    </p>
			                                    <br>
			                                </td>
                                            <td style="text-align:center; vertical-align: top;border-top:none;">
                                                <img width="200" height="40" src="data:image/png;base64,<?php echo $barcode; ?>">                               
                                            </td>
		                        		</tr>
		                        		<tr>
		                        			<th>SHIP TO</th>
		                        			<td rowspan="2">
		                                    	<p><strong>CARRIER:</strong> <?php echo $carrier; ?></p>
		                                    	<p><strong>TRAILER:</strong> <?php echo $trailer; ?></p>
		                                    	<p><strong>FACTURA:</strong> <?php echo $seal; ?></p>
		                                	</td>
		                        		</tr>
		                        		<tr height="0px">
                                            <td>
                                                <p><strong>Name:</strong>
                                                <?php echo $cust; ?></p>
                                            </td>
		                        		</tr>
		                        		<tr>
		                        			<td style="border-top:none;">
			                                    <p><strong>Address:</strong>
			                                    <?php echo $ship_address;?></p>
			                                     <p><strong>City</strong>
			                                    <?php echo $ship_state;?></p>
			                                    <p><strong>Phone</strong>
			                                    <?php echo $ship_phone;?></p>
			                                </td>
			                                <td>
		                                    	<p><strong>SCAC:</strong></p>
		                                    	<p><strong>PRO NUMBER:</strong></p>
			                                </td>
		                        		</tr>
                                        <tr>
                                            <th>THIRD PARTY FREIGHT BILL CHARGES BILL TO</th>
                                            <td rowspan="2">
                                                <p>Freight Charges:(Freight charges are prepaid unless marked otherwise)</p>
                                                <p><strong>Prepaid:____&emsp;  Collect:____&emsp; 3rd Party:____&emsp;</strong></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Name:</strong></p>
                                                <p><strong>Address:</strong></p>
                                                <p><strong>City / State/ Zip:</strong></p>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td style="vertical-align: top;height:40px">
                                                <p><strong>SPECIAL INSTRUCTIONS:</strong></p>
                                            </td>
                                            <td>
                                                <p>____Master Bill of Lading whit attached underlying Bills of Lading.</p>
                                            </td>
                                        </tr>  
                        </table> 

                        			<?php 
		                            $totalPallet = 0;
		                            $totalbag = 0;

		                            foreach ($list as $packingList){
		                            //$totalPallet = $totalPallet + $packingList['palletsShip'];
		                            $totalbag =$totalbag + $packingList['bags_totalShip'];
		                            } ;?> 
                        <table width="100%" class="table tablebill" >
                        	<tr>
                        		<th colspan="6">CUSTOMER ORDER INFORMATION</th>
                        	</tr>
                        	<tr>
                        		<td>CUSTOMER ORDER NUMBER</td>
                        		<td style="text-align: center;width:80px">#PKGCS</td>
                        		<td style="text-align: center;width:80px">WEIGHT</td>
                        		<td colspan="2" style="text-align: center;width:40px">PALLET/SLIP<br><span style="font-size:10px;;">(CIRCLE ONE)</span></td>
                        		<td>ADDITIONAL SIPPER INFO</td>
                        	</tr>
                        	<tr style="text-align: center;">
                        		<td>
                        			<?php echo $po_no;?>
                        		</td>
                        		<td>
                                    <?php echo$totalbag;?>
                        		</td>
                        		<td></td>
                        		<td>Y</td>
                        		<td>N</td>
                        		<td></td>
                        	</tr>
                        	<tr class="tr" style="text-align: center;">
                        		<td></td>
                        		<td></td>
                        		<td></td>
                        		<td>Y</td>
                        		<td>N</td>
                        		<td></td>
                        	</tr>
                            <tr class="tr" style="text-align: center;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Y</td>
                                <td>N</td>
                                <td></td>
                            </tr>
                            <tr class="tr" style="text-align: center;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Y</td>
                                <td>N</td>
                                <td></td>
                            </tr>
                            <tr class="tr" style="text-align: center;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Y</td>
                                <td>N</td>
                                <td></td>
                            </tr>
                            <tr class="tr" style="text-align: center;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Y</td>
                                <td>N</td>
                                <td></td>
                            </tr>
                            <tr class="tr" style="text-align: center;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Y</td>
                                <td>N</td>
                                <td></td>
                            </tr>
                        	<tr>
                        		<td>GRAND TOTAL</td>
                        		<td>
		                            <?php echo $totalbag;?>
                        		</td>
                        		<td></td>
                        		<td colspan="3"></td>
                        	</tr>
                        </table>
                        <table width="100%" class="table tablebill">
                            <tr>
                                <th colspan="9">CARRIER INFORMATION</th>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:center;">HANDLING UNIT</td>
                                <td colspan="2" style="text-align:center;">PACKAGE</td>
                                <td style="text-align:center;" rowspan="2">WEIGHT</td>
                                <td style="text-align:center;" rowspan="2">H.M.<br>(X)</td>
                                <td style="text-align:center;" rowspan="2">COMMODITY DESCRIPTION <br>
                                <span style="font-size:8px; text-align:center;">Commodities requiring special or additional care or attention in handling or stowing must be so marked and packaged as to ensure safe transportation with ordinary care.<br>
                                    <strong>See Section 2(e) of NMFC Item 360</strong></span></td>
                                <td colspan="2" style="text-align:center;">LTL ONLY</td>
                            </tr>
                            <tr>
                                <td style="text-align:center;">QTY</td>
                                <td style="text-align:center;">TYPE</td>
                                <td style="text-align:center;">QTY</td>
                                <td style="text-align:center;">TYPE</td>
                                <td style="text-align:center;">NMFC#</td>
                                <td style="text-align:center;">CLASS</td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="text-align:center;">GRAND TOTAL</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                        <table width="100%" class="table tablebill">
                        <tr>
                                <td style="font-size:8px;">
                                   Where the rate is dependent on value, shippers are required to state specifically in writing the agreed or
                                    declared value of the property as follows: <br>
                                    "The agreed or declared value of the property is specifically stated by the shipper to be not exceeding<br>
                                    __________________ per ___________________."" 
                                </td>
                                <td style="text-align:center; font-weight:bold;">
                                    COD Amount: $ ______________________ <br>
                                     Fee Terms: Collect:____  Prepaid:____
                                     Customer check acceptable:____

                                </td>
                            </tr>
                        </table>
                        <table width="100%" class="table tablebill">
                            <tr>
                                <td colspan="2">
NOTE Liability Limitation for loss or damage in this shipment may be applicable. See 49 U.S.C. - 14706(c)(1)(A) and (B).
                                </td>
                            </tr>
                             <tr>
                                <td  width="55%"  style="font-size:9px;">
                                    RECEIVED, subject to individually determined rates or contracts that have been agreed upon in writing between the carrier and shipper, if applicable, otherwise to the rates, classifications and rules that have been established by the carrier and are available to the shipper, on request, and to all applicable state and federal regulations
                                </td>
                                <td style="font-size:10px;">
                                    The carrier shall not make delivery of this shipment without payment of freight
and all other lawful charges.
_______________________________________Shipper Signature
                                </td>
                            </tr>
                        </table>
                        <table width="100%" class="table tablebill">
                            <tr>
                                <td width="30%" style="font-weight:bold;">
                                SHIPPER SIGNATURE / DATE
                                </td>
                                <td width="15%" style="font-weight:bold;">
                                    Trailer Loaded: 
                                </td>
                                <td  width="25%" style="font-weight:bold;">
                                    Freight Counted:
                                </td>
                                <td width="35%" style="font-weight:bold;">
                                    CARRIER SIGNATURE / PICKUP DATE 
                                </td>
                            </tr>
                        <tr>
                                <td>
                                <span style="font-size:6px;">This is to certify that the above named materials are properly classified,
                                packaged, marked and labeled, and are in proper condition for
                                transportation according to the applicable regulations of the DOT.</span>
                                </td>
                                <td style="font-size:9px;">
                                ___ By Shipper<br>
                                ___ By Driver<br>
                                <td style="font-size:9px;">
                                ___ By Shipper<br>
                                ___ By Driver/pallets said to contain<br>
                                ___ By Driver/Pieces<br>
                                </td>
                                <td>
                                <span style="font-size:6px;">Carrier acknowledges receipt of packages and required placards. Carrier certifies
                                emergency response information was made available and/or carrier has the DOT
                                emergency response guidebook or equivalent documentation in the vehicle.
                                Property described above is received in good order, except as noted.</span>
                                </td>
                            </tr>
                        </table>
           			</div><!-- /.panel-body -->
        <!--/ End basic validation -->
</body>
</html>