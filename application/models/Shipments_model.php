<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipments_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();//cargar Base de datos
        $this->load->helper('date');
        $this->load->helper('email');
		
	}

    public function getShipments()
    {
        $query = $this->db->select('*')->from('shipments')->order_by('system_dateShip', 'DESC')->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return object();
    }

    public function getShipmentsList()
    {
        
        $query = $this->db->select('*')->from('shipments')->order_by('system_dateShip', 'DESC')->get();
        if($query->num_rows() > 0)
        {
            
            return $query->result();
        }else{
            return $query->result();
        }
    }

    public function select_shipments($noShipment=NULL)
	{
        if (isset($noShipment)) 
        {
            $this->db->select('*');
            $this->db->from('shipments');
            $this->db->join('lotsship', 'lotsship.no_shipment = shipments.no_shipment');
            $this->db->join('lots', 'lots.lote_code = lotsship.lote_code');
            $this->db->join('locations', 'locations.id_location = lots.id_location');
            $this->db->join('inventory', 'inventory.lote_code = lots.lote_code');
            $this->db->join('receipts', 'receipts.reny_ctrl = lots.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = shipments.customer_code');
            $this->db->join('items', 'items.items_code = shipments.items_code');
            $this->db->join('warehouse', 'warehouse.whse_code = receipts.whse_code');
            $this->db->where('shipments.no_shipment', $noShipment);
            
            $query = $this->db->get();
            $shp = $query->result_array();

            if ($shp != null) {

                return  $shp;

            }elseif (!isset($shp)){

                $this->db->select('*');
                $this->db->from('shipments');
                $this->db->join('lotsship', 'lotsship.no_shipment = shipments.no_shipment');
                $this->db->join('receipts', 'receipts.reny_ctrl = lotsship.reny_ctrl');
                $this->db->join('customers', 'customers.customer_code = shipments.customer_code');
                $this->db->join('items', 'items.items_code = shipments.items_code');
                $this->db->join('warehouse', 'warehouse.whse_code = shipments.whse_code');
                $this->db->where('shipments.no_shipment', $noShipment);
                $query = $this->db->get();
                $shp = $query->result_array();

                return  $shp;
            }else{

                $this->db->select('*');
                $this->db->from('shipments');
                $this->db->join('customers', 'customers.customer_code = shipments.customer_code');
                $this->db->join('items', 'items.items_code = shipments.items_code');
                $this->db->join('warehouse', 'warehouse.whse_code = shipments.whse_code');
                $this->db->where('shipments.no_shipment', $noShipment);
                $query = $this->db->get();
                $shp = $query->result_array();
                
                return  $shp;
            }
            
        }       
	}
     public function showShip($noShipment)
    { 
        if (isset($noShipment)) 
        {
            $this->db->select('ship_code');
            $this->db->from('shipments');
            $this->db->where('no_shipment',$noShipment);
            $query = $this->db->get();
            $res = $query->row_array();

            if ($res['ship_code'] != 0) {

                return $res['ship_code'];

            }else{
                return 0;
            }


        }
    }          
    public function noShipment()
    {
        $datereny = date('y');
        $query=$this->db->get('shipments');
        
        $row= $query->last_row();
        
        if ($query->num_rows()>0) 
        {
            $numreny= $row->no_shipment;
             
            $year =substr($numreny, -7, 2);
            $num = substr($numreny, -5, 5);
            //list($vic, $year, $num) = explode("-",  $numreny);

            if ($year ==  $datereny) {
                $nextNum=str_pad($num + 1,5,'0', STR_PAD_LEFT);
            
                $nrenyCtrl = 'SHP'.$year.$nextNum;
                return  $nrenyCtrl;
            }else{
                $nrenyCtrl = 'SHP'.$datereny.'00001';
                return  $nrenyCtrl;
            }
        }else{
            $nrenyCtrl = 'SHP'.$datereny.'00001';
            return  $nrenyCtrl;
        }
    }
    
     public function select_cust()
	{
            $this->db->select('customers.customer_code, first_name');
            $this->db->distinct();
            $this->db->from('customers');
            $this->db->where('receipts.status', 'recibido');
            $this->db->join('receipts', 'receipts.customer_code = customers.customer_code','INNER');
            $query = $this->db->get();

        if ($query->num_rows()>0)
        {
            return  $query->result_array();
        }else{

            $arrayName = array('0'=>array('customer_code' =>'',
                               'first_name' =>'sin inventario'));
            return  $arrayName;
        }
	}
    
    
    public function selectShipEdit($noShipment)
    { 
        if (isset($noShipment)) 
        {
            $this->db->select('customer_code');
            $this->db->from('shipments');
            $this->db->where('no_shipment',$noShipment);
            $query = $this->db->get();
            $res = $query->row_array();

            $customer_code = $res['customer_code'];
 
            $this->db->select('*');
            $this->db->from('ship_to');
            $this->db->where('customer_code', $customer_code);
            
            $query = $this->db->get();
            return  $query->result_array();
        }
    }
    public function select_itemReceipt($id=NULL)
	{
            $this->db->select('*');
            $this->db->from('items');
            $this->db->where('receipts.customer_code', $id);
            $this->db->where('receipts.status', 'recibido');
            $this->db->join('receipts', 'receipts.items_code = items.items_code');
            $query = $this->db->get();
            
            foreach($query->result_array() as $row)
            {
                $data[0]='Seleccionar Producto';
                $data[$row['items_code']]=$row['item_name'];
            }
                echo json_encode($data);  
	}

    public function selectShip($id=NULL)
    { 
            $this->db->select('*');
            $this->db->from('ship_to');
            $this->db->where('customer_code', $id);
            $query = $this->db->get();
            
           foreach($query->result_array() as $row)
            {
                $data[0]='Seleccionar Ship';
                $data[$row['ship_code']]=$row['ship_state'].', '.$row['ship_address'];
            }
                echo json_encode($data);  
    }
    
    public function select_renyReceipt($idc=NULL, $idi=NULL)
	{
        $this->db->select('*');
        $this->db->from('lots')->order_by('lots.reny_ctrl');
        $this->db->join('receipts', 'receipts.reny_ctrl = lots.reny_ctrl');
        $this->db->join('inventory', 'inventory.reny_ctrl = lots.reny_ctrl'); 
        $this->db->where('receipts.items_code', $idi);
        $this->db->where('receipts.customer_code', $idc);
        $this->db->where_not_in('receipts.items_code', '0');
        $this->db->where('inventory.stock >', '0');
        $query = $this->db->get();
            
        foreach($query->result_array() as $row)
        {
            $data[0]='No. reny';
            $data[$row['reny_ctrl']]=$row['reny_ctrl'];
        }
            echo json_encode($data);  
	}
    public function select_renyLots($reny)
	{
        $this->db->select('lots.lote_code, lot');
        $this->db->join('inventory', 'inventory.lote_code = lots.lote_code'); 
        $this->db->from('lots');
        $this->db->where('lots.reny_ctrl', $reny);
        $this->db->where('inventory.stock >', '0');
        $this->db->where('inventory.chekShip',0);       
        $query = $this->db->get();
            
        foreach($query->result_array() as $row)
        {
            $data[0]='Lote';
            $data[$row['lote_code']]=$row['lot'];
        }
            echo json_encode($data);  
	}
    
    public function select_lots()
	{
        $id = $this->input->get('id');
        $id2 = $this->input->get('array');

        $this->db->select('*');
        $this->db->from('lots');
        $this->db->where('lots.lote_code',$id);
        $this->db->join('receipts', 'receipts.reny_ctrl = lots.reny_ctrl');
        $this->db->join('locations', 'locations.id_location = lots.id_location');
        $this->db->join('inventory', 'inventory.lote_code = lots.lote_code');  
        $query = $this->db->get();
            
        foreach($query->result_array() as $row)
        {
             echo json_encode($row);
        }
        
        $this->db->set('chekShip', '1');
        $this->db->where_in('lote_code',$id2);
        $this->db->update("inventory");

        $this->db->set('chekShip', '0');
        $this->db->where_not_in('lote_code',$id2);
        $this->db->update("inventory");
	}

    public function select_whse()
    {
        //consultar Bodega
        $this->db->from('warehouse')->order_by('whse_name');
        $query = $this->db->get();
        if ($query->num_rows()>0) 
        {
        return $query->result_array();
        }
         
    }
    
    public function createShipment()
	{

           $noShipment= $this->input->post('no_ship');
           $lot= $this->input->post('lots');
           $bag=$this->input->post('bags');
           $storage=$this->input->post('storage');
           $renyCtrl=$this->input->post('renyCtrl');
           $notes= $this->input->post("notes");

        $dateF = $this->input->post("date");
        list($day, $month, $year) = explode("/", $dateF);

        switch ($month) {
                    case 'Jan':
                        $monthi = 1;
                        break;
                    case 'Feb':
                        $monthi = 2;
                        break;
                    case 'Mar':
                        $monthi = 3;
                        break;
                    case 'Apr':
                        $monthi = 4;
                        break;
                    case 'May':
                        $monthi = 5;
                        break;
                    case 'Jun':
                        $monthi = 6;
                        break;
                    case 'Jul':
                        $monthi = 7;
                        break;
                    case 'Aug':
                        $monthi = 8;
                        break;
                    case 'Sep':
                        $monthi = 9;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }
        
        //list($year, $hour) = explode(" ", $yearh);

        $dateShort = $year.'-'.$monthi.'-'.$day.' '.mdate('%H:%i', time());

        $dateUnix = human_to_unix($dateShort)*1000;
        $dateEnd=$dateUnix + 3600000;

        $this->db->set("no_shipment", $noShipment);
        $this->db->set("start",$dateUnix);
        $this->db->set("end",$dateEnd);
        $this->db->set("system_dateShip", mdate('%d/%M/%Y',time()));
        $this->db->set("whse_code", $this->input->post("whse"));
        $this->db->set("customer_code",$this->input->post("customer"));
        $this->db->set("items_code", $this->input->post("item"));
        $this->db->set("carrier", $this->input->post("carrier"));
        $this->db->set("trailer",$this->input->post("trailer"));
        $this->db->set("seal", $this->input->post("seal"));
        $this->db->set("statusShip", 'Nuevo');
        $this->db->set("class", 'event-success');
        $this->db->set("url", base_url().'shipments/showEdit/');        
        $this->db->set("notesShip",$notes);
        $this->db->set("ship_code",$this->input->post("shipTo"));

        $this->db->insert("shipments");

        $this->put_notifications($noShipment);

        for ($i = 0; $i < count($lot); $i++) 
        {
			$this->db->set('lote_code',$lot[$i]);
            $this->db->set('bags_totalShip',$bag[$i]);
            $this->db->set('storage',$storage[$i]);
            $this->db->set("reny_ctrl",$renyCtrl[$i]);
            $this->db->set("no_shipment", $noShipment);
            //Grabamos los datos en la tabla lotes
            $this->db->insert("lotsship");

            $this->db->select("reserved");
            $this->db->from("inventory");
            $this->db->where("lote_code", $lot[$i]);
            $query = $this->db->get();
            $reserv = $query->row();

            $resp = $reserv->reserved + $bag[$i];

            $this->db->set('reserved',$resp);
            $this->db->where("lote_code", $lot[$i]);
            $this->db->update("inventory");
       }
       
            $this->db->set('chekShip', 0);
            $this->db->update("inventory");
        //$this->create_lots($renyCtrl);
	}

    public function upShipmentnuevo($noShipment)
    {
        $c= $this->input->post('lotShipment');

           $lot= $this->input->post('lots');
           $bag=$this->input->post('bags');
           $storage=$this->input->post('storage');
           $renyCtrl=$this->input->post('renyCtrl');
           $idlotShip=$this->input->post('idLotShip');
           $notes= $this->input->post("notes");
           


        $dateF = $this->input->post("date");
        list($day, $month, $year) = explode("/", $dateF);

        switch ($month) {
                    case 'Jan':
                        $monthi = 1;
                        break;
                    case 'Feb':
                        $monthi = 2;
                        break;
                    case 'Mar':
                        $monthi = 3;
                        break;
                    case 'Apr':
                        $monthi = 4;
                        break;
                    case 'May':
                        $monthi = 5;
                        break;
                    case 'Jun':
                        $monthi = 6;
                        break;
                    case 'Jul':
                        $monthi = 7;
                        break;
                    case 'Aug':
                        $monthi = 8;
                        break;
                    case 'Sep':
                        $monthi = 9;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }
        

        $dateShort = $year.'-'.$monthi.'-'.$day.' '.mdate('%H:%i', time());

        $dateStorage = $year.'-'.$monthi.'-'.$day;

        $dateUnix = human_to_unix($dateShort)*1000;
        $dateEnd=$dateUnix + 3600000;

        $this->db->set("start",$dateUnix);
        $this->db->set("end",$dateEnd);
        $this->db->set("system_dateShip",  mdate('%d/%M/%Y',time()));
        $this->db->set("whse_code", $this->input->post("whse"));
        $this->db->set("customer_code",$this->input->post("customer"));
        $this->db->set("items_code", $this->input->post("item"));
        $this->db->set("carrier", $this->input->post("carrier"));
        $this->db->set("trailer",$this->input->post("trailer"));
        $this->db->set("seal", $this->input->post("seal"));
        $this->db->set("statusShip", "Nuevo");
        $this->db->set("class", 'event-success');
        $this->db->set("url", base_url().'shipments/showEdit/');
        $this->db->set("notesShip",$notes);                 
        $this->db->set("ship_code",$this->input->post("shipTo"));
        $this->db->where("no_shipment", $noShipment);  

        $this->db->update("shipments");


        for ($i = 0; $i < $c; $i++) 
       {
            $this->db->select("bags_totalShip");
            $this->db->from("lotsship");
            $this->db->where("lote_code", $lot[$i]);
            $this->db->where("no_shipment", $noShipment);
            $query = $this->db->get();
            $bagSh = $query->row();

            if (isset($bagSh))
            {
                $shipPall = $bagSh->bags_totalShip - $bag[$i]; 

                $this->db->select("reserved");
                $this->db->from("inventory");
                $this->db->where("lote_code", $lot[$i]);
                $query = $this->db->get();
                $reserv = $query->row();

                $resp = $reserv->reserved - $shipPall;
            }else{
                $this->db->select("reserved");
                $this->db->from("inventory");
                $this->db->where("lote_code", $lot[$i]);
                $query = $this->db->get();
                $reserv = $query->row();

                $resp = $reserv->reserved + $bag[$i];
            }

            $this->db->select("receipt_date");
            $this->db->from("receipts");
            $this->db->where("reny_ctrl", $renyCtrl[$i]);
            $query = $this->db->get();
            $stordate = $query->row();

            $datetime1 = new DateTime($stordate->receipt_date);

            $datetime2 = new DateTime($dateShort);

            $storage2 = date_diff($datetime1 , $datetime2);

            $interval =  $storage2->format('%a');

            $this->db->set('lote_code',$lot[$i]);
            $this->db->set('bags_totalShip',$bag[$i]);
            $this->db->set('storage',$interval);
            $this->db->set("reny_ctrl",$renyCtrl[$i]);
            $this->db->where("lotShip_code", $idlotShip[$i]);            
            //Grabamos los datos en la tabla lotes
            $this->db->update("lotsship");
            
            $this->db->set('reserved',$resp);
            $this->db->where("lote_code", $lot[$i]);
            $this->db->update("inventory");
       }
        //$this->create_lots($renyCtrl);
        if ($c < count($lot)) 
            {
                for ($x = $c; $x < count($lot); $x++) 
                {
                    $this->db->set('lote_code',$lot[$x]);
                    $this->db->set('bags_totalShip',$bag[$x]);
                    $this->db->set('storage',$storage[$x]);
                    $this->db->set("reny_ctrl",$renyCtrl[$x]);
                    $this->db->set("no_shipment", $noShipment);
                    //Grabamos los datos en la tabla personal
                    $this->db->insert("lotsship");

                    $this->db->select("reserved");
                    $this->db->from("inventory");
                    $this->db->where("lote_code", $lot[$x]);
                    $query = $this->db->get();
                    $reserv = $query->row();

                    $resp = $reserv->reserved + $bag[$x];

                    $this->db->set('reserved',$resp);
                    $this->db->where("lote_code", $lot[$x]);
                    $this->db->update("inventory");
                }
            } 

            $this->db->set('chekShip', 0);
            $this->db->update("inventory");
    
    }
    
     public function upShipment($noShipment)
    {
        $this->db->select("statusShip");
        $this->db->from("shipments");
        $this->db->where('shipments.no_shipment', $noShipment);
        $query = $this->db->get();
        $status = $query->row();

        if ($status->statusShip == "Nuevo") {
            $inStatus = "Listo";
        }elseif ($status->statusShip == "Listo") {
            $inStatus = "Listo Modificado";
        }else{
            $inStatus = "Listo Modificado";
        }
        $c= $this->input->post('lotShipment');

           $lot= $this->input->post('lots');
           $bag=$this->input->post('bags');
           $storage=$this->input->post('storage');
           $renyCtrl=$this->input->post('renyCtrl');
           $idlotShip=$this->input->post('idLotShip');
           $notes= $this->input->post("notes");
           


        $dateF = $this->input->post("date");
        list($day, $month, $year) = explode("/", $dateF);
   
        switch ($month) {
                    case 'Jan':
                        $monthi = 1;
                        break;
                    case 'Feb':
                        $monthi = 2;
                        break;
                    case 'Mar':
                        $monthi = 3;
                        break;
                    case 'Apr':
                        $monthi = 4;
                        break;
                    case 'May':
                        $monthi = 5;
                        break;
                    case 'Jun':
                        $monthi = 6;
                        break;
                    case 'Jul':
                        $monthi = 7;
                        break;
                    case 'Aug':
                        $monthi = 8;
                        break;
                    case 'Sep':
                        $monthi = 9;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }
        //list($year, $hour) = explode(" ", $yearh);

        $dateShort = $year.'-'.$monthi.'-'.$day.' '.mdate('%H:%i', time());
        $dateStorage = $year.'-'.$monthi.'-'.$day;

        $dateUnix = human_to_unix($dateShort)*1000;
        $dateEnd=$dateUnix + 3600000;

        $this->db->set("start",$dateUnix);
        $this->db->set("end",$dateEnd);
        $this->db->set("system_dateShip",  mdate('%d/%M/%Y',time()));
        $this->db->set("whse_code", $this->input->post("whse"));
        $this->db->set("customer_code",$this->input->post("customer"));
        $this->db->set("items_code", $this->input->post("item"));
        $this->db->set("carrier", $this->input->post("carrier"));
        $this->db->set("trailer",$this->input->post("trailer"));
        $this->db->set("seal", $this->input->post("seal"));
        $this->db->set("statusShip", $inStatus);
        $this->db->set("class", 'event-warning');
        $this->db->set("url", base_url().'shipments/showEdit/');
        $this->db->set("notesShip",$notes);
        $this->db->set("ship_code",$this->input->post("shipTo"));
        $this->db->where("no_shipment", $noShipment);  

        $this->db->update("shipments");


        for ($i = 0; $i < $c; $i++) 
       {
            $this->db->select("bags_totalShip");
            $this->db->from("lotsship");
            $this->db->where("lote_code", $lot[$i]);
            $this->db->where("no_shipment", $noShipment);
            $query = $this->db->get();
            $bagSh = $query->row();

            if (isset($bagSh))
            {
                $shipPall = $bagSh->bags_totalShip - $bag[$i]; 

                $this->db->select("reserved");
                $this->db->from("inventory");
                $this->db->where("lote_code", $lot[$i]);
                $query = $this->db->get();
                $reserv = $query->row();

                $resp = $reserv->reserved - $shipPall;
            }else{
                $this->db->select("reserved");
                $this->db->from("inventory");
                $this->db->where("lote_code", $lot[$i]);
                $query = $this->db->get();
                $reserv = $query->row();

                $resp = $reserv->reserved + $bag[$i];
            }

            $this->db->select("receipt_date");
            $this->db->from("receipts");
            $this->db->where("reny_ctrl", $renyCtrl[$i]);
            $query = $this->db->get();
            $stordate = $query->row();

            $datetime1 = new DateTime($stordate->receipt_date);

            $datetime2 = new DateTime($dateShort);

            $storage2 = date_diff($datetime1 , $datetime2);

            $interval =  $storage2->format('%a');

            $this->db->set('lote_code',$lot[$i]);
            $this->db->set('bags_totalShip',$bag[$i]);
            $this->db->set('storage',$interval);
            $this->db->set("reny_ctrl",$renyCtrl[$i]);
            $this->db->where("lotShip_code", $idlotShip[$i]);            
            //Grabamos los datos en la tabla lotes
            $this->db->update("lotsship");
            
            $this->db->set('reserved',$resp);
            $this->db->where("lote_code", $lot[$i]);
            $this->db->update("inventory");
       }
        //$this->create_lots($renyCtrl);
        if ($c < count($lot)) 
            {
                for ($x = $c; $x < count($lot); $x++) 
                {
                    $this->db->set('lote_code',$lot[$x]);
                    $this->db->set('bags_totalShip',$bag[$x]);
                    $this->db->set('storage',$storage[$x]);
                    $this->db->set("reny_ctrl",$renyCtrl[$x]);
                    $this->db->set("no_shipment", $noShipment);
                    //Grabamos los datos en la tabla personal
                    $this->db->insert("lotsship");

                    $this->db->select("reserved");
                    $this->db->from("inventory");
                    $this->db->where("lote_code", $lot[$x]);
                    $query = $this->db->get();
                    $reserv = $query->row();

                    $resp = $reserv->reserved + $bag[$x];

                    $this->db->set('reserved',$resp);
                    $this->db->where("lote_code", $lot[$x]);
                    $this->db->update("inventory");
                }
            } 
    
    }

    public function is_ship($noShipment)
    {
            
            $shipTo = $this->input->post('shipTo');
            $carrier = $this->input->post('carrier');
            $trailer = $this->input->post('trailer');
            $seal = $this->input->post('seal');
            $index = $this->input->post('index');;
            
            $this->db->select("lote_code, bags_totalShip");
            $this->db->from("lotsship");
            $this->db->where("no_shipment", $noShipment);
            $query = $this->db->get();
            $pallSh = $query->result_array();

            //$data = array();
        for ($i = 0; $i < $index; $i++) 
        {
            $this->db->select("*");
            $this->db->from("inventory");
            $this->db->where("lote_code", $pallSh[$i]['lote_code']);
            $query = $this->db->get();
            $lotsRes = $query->row_array();

            $operStock = $lotsRes['stock'] - $pallSh[$i]['bags_totalShip'];
            $operReserv = $lotsRes['reserved'] - $pallSh[$i]['bags_totalShip'];

			$this->db->set('stock', $operStock);
            $this->db->set('reserved', $operReserv);
            $this->db->where('lote_code',$pallSh[$i]['lote_code']);
            //Grabamos los datos en la tabla personal
            $this->db->update("inventory");

            $this->db->set('statusShip', 'Terminado');
            $this->db->set('class', 'event-important');
            $this->db->set('carrier',  $carrier);
            $this->db->set('trailer', $trailer);
            $this->db->set('seal',  $seal);
            $this->db->set('ship_code', $shipTo);
            $this->db->where('no_shipment', $noShipment);
            $this->db->update("shipments");
        } 
        if ($this->input->post("sendMail")==1) {


            $date=$this->input->post("date");
            $po=$this->input->post("po");
            $carrier=$this->input->post("carrier");
            $trailer=$this->input->post("trailer");
            $seal=$this->input->post("seal");

            $this->db->select('*');
            $this->db->from('shipments');
            $this->db->join('customers', 'customers.customer_code = shipments.customer_code');
            $this->db->join('items', 'items.items_code = shipments.items_code');
            $this->db->where('shipments.no_shipment', $noShipment);
            $query = $this->db->get();
            
            $date= $query->row()->start/1000;
            $datestring = '%m/%d/%Y';
            $date2=mdate($datestring, $date); 

            $item=$query->row()->item_name;
            $email=$query->row()->email;
            $customer = $query->row()->first_name;

            $this->sendMail($noShipment);
          
        }

    } 
    public function deleteImg($id, $name){
        if(file_exists('./uploadsShip/'.$name))
        {
            $this->db->delete('imgship', array('id' => $id));
            unlink('./uploadsShip/'.$name);
            echo json_encode(array('res' => true,
                                        'id'=>$id));
        }
        else
        {
            echo json_encode(array('res' => false));
        }
    }

    public function images($noShipment)
    {
        if (isset($_FILES["file"]["name"])) 
        {
            $file = $_FILES["file"]["name"];
            $type = $_FILES["file"]["type"];
            
            list($name, $extension) = explode('.', $file);

            if(!is_dir("uploadsShip/"))
                mkdir("uploadsShip/", 0777);

             if (file_exists("uploadsShip/".$file)) {

                $newname =  $name.rand(000,999).'.'.$extension;

                if($file && move_uploaded_file($_FILES["file"]["tmp_name"], "uploadsShip/".$newname))
                {
                    $this->db->set('name', $newname);
                    $this->db->set('url',base_url('uploadsShip'));
                    $this->db->set('no_shipment',$noShipment);
                    $this->db->insert("imgship");
                }


            } else {

                if($file && move_uploaded_file($_FILES["file"]["tmp_name"], "uploadsShip/".$file))
                {
                    $this->db->set('name', $file);
                    $this->db->set('url',base_url('uploadsShip'));
                    $this->db->set('no_shipment',$noShipment);
                    $this->db->insert("imgship");
                }
            }
        }     
    }

    public function select_images($noShipment= NULL)
    {
         if(isset($noShipment))
        { 
            $this->db->select('*');
            $this->db->from('imgship');
            $this->db->where('imgship.no_shipment', $noShipment);
            $query = $this->db->get();
         
            return $query->result_array();
        }
        
    }
     public function deleteLots($id=NULL){
            if(isset($id))
            {
                $this->db->select("lote_code, bags_totalShip, no_shipment");
                $this->db->from("lotsship");
                $this->db->where("lotShip_code", $id);
                $query = $this->db->get();
                $bagSh = $query->row_array();


                $this->db->select('stock, reserved');
                $this->db->from("inventory");
                $this->db->where("lote_code", $bagSh['lote_code']);
                $query = $this->db->get();
                $reserv = $query->row_array();

                $this->db->select('statusShip');
                $this->db->from('shipments');
                $this->db->where('no_shipment', $bagSh['no_shipment']);
                $query = $this->db->get();
                $resp = $query->row();

                if($resp->statusShip == 'Terminado')
                {

                    $resStock =  $reserv['stock'] + $bagSh['bags_totalShip'];

                    $this->db->set('stock', $resStock);
                    $this->db->set('reserved', 0);
                    $this->db->where("lote_code", $bagSh['lote_code']);
                    $this->db->update("inventory");

                    $this->db->delete('lotsship', array('lotShip_code' => $id));

                    echo json_encode(array('res' => true,
                                        'id'=>$id));
                }else{

                    $resp =  $reserv['reserved'] - $bagSh['bags_totalShip']; 

                    $this->db->set('reserved',$resp);
                    $this->db->where("lote_code", $bagSh['lote_code']);
                    $this->db->update("inventory");


                    $this->db->delete('lotsship', array('lotShip_code' => $id));

                    echo json_encode(array('res' => true,
                                        'id'=>$id));
                }
                
            }
            else
            {
                echo json_encode(array('res' => false));
            }
    }

    public function select_lotsShip($id)
    {
        $this->db->select('*');
        $this->db->from('lotsship');
        $this->db->where('lotsship.lote_code',$id);
        $this->db->join('lots', 'lots.lote_code = lotsship.lote_code');
        $this->db->join('inventory', 'inventory.lote_code = lots.lote_code');         
        $query = $this->db->get();
            
        foreach($query->result_array() as $row)
        {
             echo json_encode($row);
        }
    }

    public function sendMail($noShipment)
    {
        $shipm = $this->select_shipments($noShipment);
        $images=$this->select_images($noShipment);

        $this->load->library('email');

        $configGmail = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'aws.digital-webhosting.com',
            'smtp_user' => 'info@ilchisa.com',
            'smtp_pass' => 'Ilchisa17',
            'smtp_port' => '465',
            'smtp_crypto'=> 'ssl',
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        );     
 
        //cargamos la configuración para enviar con gmail
        $this->email->initialize($configGmail);

        $tot_pal=0;
        $tot_bag=0;

         foreach ($shipm as $rec) {
            $tot_bag=$tot_bag + $rec['bags_totalShip'];
        }

        $logo=base_url('/assets/global/img/logo-reny-picot.png');
        $date =mdate('%m/%d/%Y', $shipm[0]['start']/1000);
        $customer =$shipm[0]['first_name'];
        $email = $shipm[0]['email'];
        $carrier = $shipm[0]['carrier'];
        $trailer = $shipm[0]['trailer'];
        $item = $shipm[0]['item_name'];
        $seal = $shipm[0]['seal'];
        $notes = $shipm[0]['notesShip'];
        $whse = $shipm[0]['whse_name'];
        $status = $shipm[0]['statusShip'];
       // Varios destinatarios
        $mensaje= '<html lang="en"><!--<![endif]--><!-- START @HEAD -->
        <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

                    <!-- START @THEME STYLES -->
                    <link href="'.base_url('/assets/admin/css/reset.css').'" rel="stylesheet">
        </head>
        <body style="background:#fff">
            <div style="width:90%; margin:auto;">
                <table width="100%">
                    <tr>
                        <td class="logo">
                            <img class="logo" src="'.$logo.'" alt="brand logo" width="250">
                        </td>
                        <td>
                        <h1 style="text-align:center;">Industrias Lacteas Chihuahuenses, S.A. de C.V.</h1>
                        </td>
                        <td style="text-align:right;">
                            7158 Merchant Ave,El Paso, Tx 79915<br>
                            <strong>Phone Number:</strong> +1 (915) 778-3933<br>
                            <strong>Fax Number:</strong> +1 (915) 778-3009<br>
                            <strong>E-mail:</strong> info@ilchisa.com <br>
                        </td>
                    </tr>
                </table>
                <section>
                    <div class="row">
                        <div class="col-md-12">

                        <!-- Start basic validation -->

                            <hr> 
                            <table width="100%">
                                <tr>
                                    <td>
                                        <strong>Salida No.:</strong> '.$noShipment.'
                                    </td>   
                                    <td>
                                        <strong>Bodega:</strong> '. $whse
                                    .'</td>
                                    <td style="text-align:right;">
                                        <strong>Fecha:</strong>'.$date
                                    .'</td> 
                                </tr>
                                <tr class="row">
                                    <td>
                                        <strong>Cliente:</strong>
                                        '.$customer.'
                                    </td>
                                    <td >
                                        <strong>Carrier:</strong> '.$carrier
                                        .'</select>
                                    </td>
                                </tr>
                                <tr class="row">
                                    <td>
                                        <strong>Producto:</strong>                            
                                        '.$item.'
                                    </td>
                                    <td>
                                        <strong>Trailer:</strong> '.$trailer
                                    .'</td>
                                </tr>    
                                <tr class="row">
                                    <td>
                                    </td>
                                    <td>
                                        <strong>Factura:</strong> '.$seal
                                    .'</td>
                                </tr>
                            </table

                                <hr>   
                                <hr> <table id="datatable" class="table table-striped" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td style="font-weight:bold">No. Ctrl.</td>
                                                    <td style="font-weight:bold">Lote</td>
                                                    <td style="font-weight:bold">Bags</td>
                                                    <td style="font-weight:bold">Fecha Prod</td>
                                                    <td style="font-weight:bold">Fecha Cad</td>
                                                    <td style="font-weight:bold">Storage</td>
                                                    <td style="font-weight:bold">Notas</td>
                                                </tr>';
                                            foreach ($shipm as $rec) {
                                    $mensaje .='<tr>
                                                    <td>'.$rec['reny_ctrl']
                                                    .'</td>
                                                    <td>'.$rec['lot']
                                                    .'</td>
                                                    <td>'.$rec['bags_totalShip']
                                                    .'</td> 
                                                    <td>';
                                                    if ($rec['prod_date'] != "0000-00-00") {
                                                            $mensaje .= date_format(date_create($rec['prod_date']), 'd/M/Y');# code...
                                                            }else{ $mensaje .="---"; }
                                                    $mensaje .='</td>
                                                    <td>';if ($rec['prod_date'] != "0000-00-00") {
                                                                $mensaje .=date_format(date_create($rec['exp_date']), 'd/M/Y');# code...
                                                            }else{$mensaje .="---";}
                                                    $mensaje .='</td>
                                                    <td>'.$rec['storage']
                                                    .'</td>
                                                    <td>'.$rec['notes']
                                                    .'</td>
                                                </tr>';
                                            }
                                    $mensaje .='</tbody>
                                        </table>
                                <hr>
                                <div>
                                    <div style="font-size:16px;">'
                                    .' <strong>Total Bags:</strong> '.$tot_bag
                                    .'</div
                                </div>
                                <div>
                                    <div style="font-size:16px;">
                                        <strong>Notas:</strong>
                                        <br>'.$notes     
                                    .'</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </body>
    </html>';

        $this->email->from('info@ilchisa.com ', 'Industrias Lacteas Chihuahuenses');
        $this->email->to($email);
        $this->email->subject('Salida de Mercancia '.$noShipment);
        $this->email->message($mensaje);
        foreach ($images as $img) {
        $image= $img['url'].'/'.$img['name'];  
        $this->email->attach($image); 
        }
        //$this->email->send();
        if ($this->email->send())
        {
            echo json_encode(array('res' => true,
                                        'mail'=>$email,
                                        'status'=>$status));
        }else
            {
               echo json_encode(array('res' => false,
                                        'mail'=>$email,
                                        'status'=>$status));
            }
    }
   public function put_notifications($noShipment){
        //Status : nuevo, visto, leido

        $this->db->set('code_trx', $noShipment);
        $this->db->set('trx','Salida');
        $this->db->set('status','Nuevo');
        $this->db->set('notify',1);
        $this->db->insert('notifications');
    }
    public function delete_notifications($noShipment){
        //Status : nuevo, visto, leido

        $this->db->where('code_trx',$noShipment);
        $this->db->delete('notifications');
    }
    public function billOfLading($id){

        if (isset($id)) 
        {
            $this->db->select('id_out');
            $this->db->from('bill_lading');
            $this->db->where('id_out', $id);
            $query = $this->db->get();

            if ($query->num_rows() == 0) 
            {
                $this->db->set('bill_number', $this->billNumber());
                $this->db->set('id_out', $id);
                $this->db->insert('bill_lading');
            }

            $this->db->select('*');
            $this->db->from('shipments');
            $this->db->join('lotsship', 'lotsship.no_shipment = shipments.no_shipment');
            $this->db->join('receipts', 'receipts.reny_ctrl = lotsship.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('ship_to', 'ship_to.ship_code = shipments.ship_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->join('warehouse', 'warehouse.whse_code = shipments.whse_code');
            $this->db->join('bill_lading', 'bill_lading.id_out = shipments.id_out');
            $this->db->where('shipments.id_out',$id);
            $this->db->where('shipments.statusShip','Terminado');
            $query = $this->db->get();
            $res = $query->result_array();

            return  $res;
        }
    }
     public function billNumber()
    {
        $datereny = date('y');
        $query=$this->db->get('bill_lading');
        
        $row= $query->last_row();
        
        if ($query->num_rows()>0) 
        {
            $billNumber= $row->bill_number;

                $nextNum=str_pad($billNumber + 1,17,'0', STR_PAD_LEFT);
                return  $nextNum;
        }else{
            $nextNum = '00000000000000001';
            return  $nextNum;
        }
    }
    public function adjustment()
    {
            $lot= $this->input->post('lots');
            $bag= $this->input->post('bags');
            $renyCtrl=$this->input->post('renyCtrl');

            //$data = array();
        for ($i = 0; $i < count($lot); $i++) 
        {
            $this->db->select("*");
            $this->db->from("inventory");
            $this->db->where("lote_code", $lot[$i]);
            $query = $this->db->get();
            $lotsRes = $query->row_array();

            $operStock = $lotsRes['stock'] - $bag[$i];

            $this->db->set('stock', $operStock);
            $this->db->where('lote_code',$lot[$i]);
            //Grabamos los datos en la tabla personal
            $this->db->update("inventory");
        } 

    } 

    public function deleteshipment($id){
            $this->db->select('statusShip');
            $this->db->from('shipments');
            $this->db->where('no_shipment', $id);
            $query = $this->db->get();
            $resp = $query->row();

            if($resp->statusShip != 'Terminado')
            {
                $this->db->select("lote_code, bags_totalShip");
                $this->db->from("lotsship");
                $this->db->where("no_shipment", $id);
                $query = $this->db->get();
                $bagSh = $query->result_array();

                foreach ($bagSh as $resBag) {


                    $this->db->select('reserved');
                    $this->db->from("inventory");
                    $this->db->where("lote_code", $resBag['lote_code']);
                    $query = $this->db->get();
                    $reserv = $query->row_array();

                    $resp =  $reserv['reserved'] - $resBag['bags_totalShip']; 

                    $this->db->set('reserved',$resp);
                    $this->db->where("lote_code", $resBag['lote_code']);
                    $this->db->update("inventory");
                }

                $this->db->delete('shipments', array('no_shipment' => $id));
                $this->db->delete('lotsship', array('no_shipment' => $id));

                $this->db->select('name');
                $this->db->from('imgship');
                $this->db->where('no_shipment', $id);
                $img = $this->db->get();

                if ($img->num_rows() > 0) {
                    $delImg = $img->result_array();

                    for ($i=0; $i < $img->num_rows(); $i++) { 
                        $this->db->delete('imgship', array('no_shipment' => $id));
                        unlink('./uploadsShip/'.$delImg[$i]['name']);
                    }
                }

                $this->db->delete('lotsship', array('lotShip_code' => $id));
                $this->db->delete('notifications', array('code_trx' => $id));
                echo json_encode(array('res' => true,
                                        'id'=>$id));

            }
            else
            {
                echo json_encode(array('res' => 'No puede eliminar Salidas con status Terminado'));
            }
    }

    public function buscar_duplicado()
    {
        
      $shipCtrl = $this->input->post("valorBusqueda");

        $this->db->select('no_shipment');
        $this->db->from('shipments');
        $this->db->where('no_shipment', $shipCtrl);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            echo 'true';
        }else echo 'false';
        //$this->create_lots($renyCtrl);
    
    }

    public function chekShip()
    {
        $renyCtrl=$this->input->post('renyCtrl');
        $loteCode = $this->input->post("lotCode");
        $this->db->set('chekShip', '1');
        $this->db->where('lote_code',$renyCtrl);
        $this->db->update("inventory");
    }

}
 