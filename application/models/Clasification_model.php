<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clasification_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	 //Metodo para Registrar
	public function insert($data)
    {
		$this->db->insert('clasification',$data);		
	}
    
    //Editar
    public function edit($id)
	{
        $this->db->set("name_clasification", $this->input->post("name_clasification"));
        $this->db->where('id_clasification', $id);
        $this->db->update('clasification');
        
    }
    
    //consultar todos
    public function select($id=NULL)
	{
        //consultar si esta definido por un id.
        if(isset($id) != NULL)
        {
            $this->db->select('*');
            $this->db->from('clasification');
            $this->db->where('id_clasification',$id);
            $query = $this->db->get();

            return $query->row();
        }
        else
        {
            $this->db->select('*');//si no se definio id consulta todos
            $this->db->from('clasification');
            $query = $this->db->get();
            
            if($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                $query = $this->db->get('clasification');
                 return $query->result_array();
            }
        }
	}   
    public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('id_clasification'=>$id));
        if ($query->num_rows() == 0) {
            $this->db->delete('clasification', array('id_clasification' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar clasificación, tiene transacciones '));
        }
    }

    public function id_clasification(){
        $query=$this->db->get('clasification');
        $row= $query->last_row();

        return $row->id_clasification;
    }
    
}