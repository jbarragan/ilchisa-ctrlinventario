<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Whse_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    
	 //Metodo para Registrar producto
	public function insert($data)
    {
		$this->db->insert('warehouse',$data);		
	}
    
    //Editar producto
    public function edit($id, $data)
	{
		$this->db->where('whse_code', $id);
        $this->db->update('warehouse', $data);
    }
    
    //consultar todos los productos
    public function select($id=NULL)
	{
        //consultar producto si esta definido por un id.
        if(isset($id))
        {
            $query = $this->db->get_where('warehouse',array('whse_code'=>$id));
            return $query->result_array();
        }
            $query = $this->db->get('warehouse');//si no se definio id consulta todos los productos
            return $query->result_array();
	}   
    public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('whse_code'=>$id));
        if ($query->num_rows() == 0) {

            $this->db->delete('warehouse', array('whse_code' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar Bodega, tiene transacciones '));
        }
    }
    //Metodo que muestra los proveedores que no estan relacionados al producto.
    public function select_vendor($id=NULL)
	{
        $query1 = $this->db->query('SELECT id_location FROM warehouse_locations WHERE id_location ='.$id);
        if ($query1->num_rows() > 0)
        {
            $i=0;
            foreach($query1->result() as $show)
            {
                $array[$i] =$show->vendor_code;
                $i++;
            }
            $this->db->select('*');
            $this->db->from('locations');
            $this->db->where_not_in('id_location',$array);
            $query = $this->db->get();
            return $query->result_array();
        }
        elseif($query1->num_rows() === 0)
        {
            $this->db->select('*');
            $this->db->from('locations');
            $query = $this->db->get();
            return $query->result_array();
        }           
	}
    //consulta proveedores por producto.
     public function select_vendor_item($id=NULL)
	{
            $this->db->select('*');
            $this->db->from('locations');
            $this->db->where('warehouse_locations.whse_code', $id);
            $this->db->join('warehouse_locations', 'warehouse_locations.id_location = locations.id_location');
            $this->db->join('warehouse', 'warehouse.whse_code = warehouse_locations.whse_code');

            $query = $this->db->get();
            return $query->result_array();     
	}
    //consulta proveedores por producto.
     public function select_location()
    {
            $this->db->select('*');
            $this->db->from('locations');
            $this->db->order_by('location');

            $query = $this->db->get();
            return $query->result_array();     
    }
    public function select_location_lote()
    {
            $this->db->select('*');
            $this->db->from('locations');
            $this->db->join('lots', 'lots.id_location = locations.id_location');
            $this->db->where('status',1);
            $this->db->order_by('location');

            $query = $this->db->get();
            return $query->result_array();     
    }
    public function insert_item_vendor($idv,$idc)
	{
        $this->db->insert('items_vendors',array('items_code ' => $idc, 'vendor_code' => $idv));      
    }
    
    public function delete_item_vendor($idv, $idc)
	{
        $this->db->where('items_code', $idc)->where('vendor_code', $idv)->delete('items_vendors');
    }
    public function insert_location()
    {
        $prefijo=$this->input->post('prefijo');
        $ini=$this->input->post('rango_ini');
        $fin=$this->input->post('rango_fin');

        for ($i = $ini; $i <= $fin; $i++) 
       {
           $num=(string)$i;
            $this->db->set('location',$prefijo.$num);
            $this->db->set('status',0);
            //Grabamos los datos en la tabla personal
           $this->db->insert("locations");
       }  
    }
}