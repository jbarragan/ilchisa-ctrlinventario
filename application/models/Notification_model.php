<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();//cargar Base de datos
        $this->load->helper('email');
		
	}
     public function load_notifications(){
        //Status : nuevo, visto, leido

        $this->db->select('*');
        $this->db->from('notifications');
        $query = $this->db->get();

        $x = 0;

        foreach ($query->result_array() as $key) {
            switch ($key['trx']) {
                case 'Entrada':
                    $icon = 'fa-arrow-right';
                    $baseRel =base_url('receipts/load_updateRecipt/');
                    break;
                case 'Salida':
                   $icon = 'fa-arrow-left';
                   $baseRel =base_url('shipments/showEdit/');
                    break;
            }
            if ($key['status'] == 'Nuevo') {
            	$st = '<span class="rounded count label label-success pull-right" id="countNoty">Nuevo</span>';
            }else{
            	$st = ''; 
            }
            echo '<a id="'.$key['id_notify'].'" href="'.$baseRel.'/'.$key['code_trx'] .'" class="media count'.$x.'" data-status="'.$key['status'].'" onmouseover="showNotification(this);" onclick="delNotification(this);" data-trx="'.$key['trx'].'" data-notify="'.$key['notify'].'">'.
                    $st
                    .'<div class="media-object pull-left">
                    <i class="fa '.$icon.' fg-success"></i>
                    </div>
                    <div class="media-body">
                        <span class="media-text"> Una <strong>Nueva </strong><strong>'.$key['trx'].',</strong> fue registrada </span>
                        <span class="media-meta">'.$key['code_trx'].'</span>
                    </div>
                </a>';
                $x = $x + 1;
        }
       
    }
    public function update_notifications(){
        //Status : nuevo, visto, leido
		$stat=$this->input->get('status');
		$id=$this->input->get('id');
		$notify=$this->input->get('notify');

        $this->db->set('status', $stat);
        $this->db->set('notify', $notify);
        $this->db->where('status','Nuevo');
        $this->db->where('id_notify',$id);
        $this->db->update('notifications');
    }
     public function delete_notifications(){
        //Status : nuevo, visto, leido
		$id=$this->input->get('id');

        $this->db->where('id_notify',$id);
        $this->db->delete('notifications');
    }
}