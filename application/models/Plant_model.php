<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plant_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	 //Metodo para Registrar
	public function insert($data)
    {
		$this->db->insert('plant',$data);		
	}
    
    //Editar
    public function edit($id)
	{
        $this->db->set("name_plant", $this->input->post("name_plant"));
        $this->db->where('id_plant', $id);
        $this->db->update('plant');
        
    }
    
    //consultar todos
    public function select($id=NULL)
	{
        //consultar si esta definido por un id.
        if(isset($id) != NULL)
        {
            $this->db->select('*');
            $this->db->from('plant');
            $this->db->where('id_plant',$id);
            $query = $this->db->get();

            return $query->row();
        }
        else
        {
            $this->db->select('*');//si no se definio id consulta todos
            $this->db->from('plant');
            $query = $this->db->get();
            
            if($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                $query = $this->db->get('plant');
                 return $query->result_array();
            }
        }
	}   
    public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('id_plant'=>$id));
        if ($query->num_rows() == 0) {

            $this->db->delete('plant', array('id_plant' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar esta Planta, tiene transacciones '));
        }
    }

    public function id_plant(){
        $query=$this->db->get('plant');
        $row= $query->last_row();

        return $row->id_plant;
    }
    
}