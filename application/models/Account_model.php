<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();//cargar Base de datos
		
	}

	//consulta de tabla usuarios
	public function select_user($id = null){

		if (isset($id)) {
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_id',$id);
			$query = $this->db->get();

			return $query->row_array();
		}else{
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('auth_level !=', 1);
			$query = $this->db->get();

			return $query->result_array();
		}
	}
	//consulta de tabla usuarios
	public function select_userClient($id = null){

		if (isset($id)) {
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_id',$id);
			$this->db->where('auth_level =', 1);
			$query = $this->db->get();

			return $query->row_array();
		}else{
			$this->db->select('customers.user_id, customers.permisoDetalle, users.username, users.email, users.auth_level');
			$this->db->from('customers');
			$this->db->join('users', 'users.user_id = customers.user_id');
			$query = $this->db->get();

			return $query->result_array();
		}
	}
	//Actualiza tabla users
	public function edit_user($id = null,$data)
	{
		if (isset($id)) 
		{
			$this->db->set($data);
			$this->db->where('user_id',$id);
			$this->db->update('users');
		}
	}
	//Elimina Registro del usuario definido por su id.
	public function delete_user($id = null)
	{
		if (isset($id)) 
		{
			$this->db->where('user_id',$id);
			$this->db->delete('users');
		}
	}

	//Elimina Registro del usuario definido por su id.
	public function permitirDetalle()
	{
		$id = $this->input->post("id");
		if (isset($id)) 
		{
	        $this->db->set("permisoDetalle", $this->input->post("valor"));
	        $this->db->where('user_id',$id);
	        $this->db->update('customers');
	        if ($this->db->affected_rows() == 1) {
	        	echo json_encode(array('res' => true));
	        }else{
	        	echo json_encode(array('res' => false));
	        }
		}
	}
}