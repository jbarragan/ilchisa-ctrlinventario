<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appointment_model extends CI_Model
{
    
        const ALL_DAY_REGEX = '/^\d{4}-\d\d-\d\d$/'; // matches strings like "2013-12-29"

        public $title;
        public $className;
        public $allDay; // a boolean
        public $start; // a DateTime
        public $end; // a DateTime, or null
        public $properties = array(); // an array of other misc properties
        
	public function __construct()
	{        
		parent::__construct();
		$this->load->database();//cargar Base de datos
		date_default_timezone_set("US/Mountain");
        $this->load->helper('email');
	}
    public function edit($id)
	{
        $dateF = $this->input->post("date");
        list($day, $month, $yearh) = explode("/", $dateF);
        
        list($year, $hour) = explode(" ", $yearh);

        switch ($month) {
                    case 'Jan':
                        $monthi = 01;
                        break;
                    case 'Feb':
                        $monthi = 02;
                        break;
                    case 'Mar':
                        $monthi = 03;
                        break;
                    case 'Apr':
                        $monthi = 04;
                        break;
                    case 'May':
                        $monthi = 05;
                        break;
                    case 'Jun':
                        $monthi = 06;
                        break;
                    case 'Jul':
                        $monthi = 07;
                        break;
                    case 'Aug':
                        $monthi = 08;
                        break;
                    case 'Sep':
                        $monthi = 09;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }

        $dateShort = $year.'-'.$monthi.'-'.$day.' '.$hour;

        $dateUnix = human_to_unix($dateShort)*1000;
        $dateEnd=$dateUnix + 3600000;
        //Se insertan en la tabla los datos almacenados en $customer
		$this->db->where('id', $id);
        $this->db->set("start", $dateUnix);
        $this->db->set("end", $dateEnd);
        $this->db->set("status", 'reagendada');
        $this->db->set("class", 'event-info');
        $this->db->update('appointment');		
	}
    
    public function select($id=NULL)
	{
       
        if (isset($id)) 
        {
            $this->db->select('customer_code');
            $this->db->from('appointment');
            $this->db->where('id',$id);  
            $query = $this->db->get();
            if($query->row()->customer_code == 0)
            {
                $this->db->select('*');
                $this->db->from('appointment'); 
                $this->db->where('id',$id);   
                $this->db->join('vendors', 'vendors.vendor_code = appointment.vendor_code');
                $query = $this->db->get();
                return $query->result_array(); 
            }else{
                $this->db->select('*');
                $this->db->from('appointment'); 
                $this->db->where('id',$id); 
                $this->db->join('customers', 'customers.customer_code = appointment.customer_code');  
                $this->db->join('vendors', 'vendors.vendor_code = appointment.vendor_code');
                $query = $this->db->get();
                return $query->result_array();   
            }
        }
        
            $query = $this->db->get('appointment');//si no se definio id consulta todos los clientes
            return $query->result_array();
            
	}
    
    public function select_customer($id=NULL)
	{
        if(isset($id))
        {
            $query = $this->db->get_where('customers',array('customer_code'=>$id));
            return $query->result_array();
        }else{
            $this->db->from('customers')->order_by('first_name');
            $query = $this->db->get();
            return $query->result_array();
        }
	}
    
    public function select_vendor($id=NULL)
	{
        if(isset($id))
        {
            if($id == 0 )
            {
                $this->db->from('vendors')->order_by('name');
                $query = $this->db->get();
                $data[0]='Seleccionar Proveedor';
                foreach($query->result_array() as $row)
                {
                    $data[$row['vendor_code']]=$row['name'];
                }
                     echo json_encode($data);    
            }
            else
            {
                /*/$this->db->select('*');
                $this->db->from('vendors')->order_by('name');
                $this->db->where('customers_vendors.customer_code', $id);
                $this->db->join('customers_vendors', 'customers_vendors.vendor_code = vendors.vendor_code');
                $query = $this->db->get();*/
                $this->db->select('vendor_email,name');
                $this->db->from('vendors');
                $this->db->where('vendor_code', $id);
                $query = $this->db->get();
                
                echo json_encode($query->row_array()); 
           
            }
        }else
            {
                $this->db->from('vendors')->order_by('name');
                $query = $this->db->get();
                return $query->result_array();
        }  
	}
     
    public function buscador($s)
	{ 
		//usamos after para decir que empiece a buscar por
		//el principio de la cadena
		//ej SELECT localidad from localidades_es 
		//WHERE localidad LIKE '%$abuscar' limit 12
		$this->db->select('*');
		$this->db->like('first_name',$s,'after');
		$query = $this->db->get('customers', 12);
		
		//si existe algún resultado lo devolvemos
		if($query->num_rows() > 0)
		{
			foreach($query->result_array() as $row)
            {
            $data[]=$row['first_name'];
            $id=$row['customer_code'];
            }
            echo json_encode($data); 
            return  $id;
        	
		//en otro caso devolvemos false
		}else{
			
			return FALSE;
			
		}
		
	}	
    
    /**
	*añade un nuevo evento
	*/
	public function create()
	{
       if ($this->input->post("sendMail")==1) {
            $this->db->select('vendor_email, name');
            $this->db->from('vendors'); 
            $this->db->where('vendors.vendor_code', $this->input->post("vendor"));
            $query = $this->db->get();
            foreach($query->result_array() as $row)
            {
                $vendor=$row['name'];
                $email=$row['vendor_email'];
            }
           
            $this->db->select('first_name');
            $this->db->from('customers'); 
            $this->db->where('customers.customer_code', $this->input->post("customer"));
            $query2 = $this->db->get();
     
            foreach($query2->result_array() as $row)
            {
                $customer=$row['first_name'];
                
            }
                $date=$this->input->post("date");
                $po=$this->input->post("po");

          $this->sendMail($date,$customer,$email,$po,$vendor);
          
        }

        $dateF = $this->input->post("date");
        list($day, $month, $yearh) = explode("/", $dateF);

        switch ($month) {
                    case 'Jan':
                        $monthi = 01;
                        break;
                    case 'Feb':
                        $monthi = 02;
                        break;
                    case 'Mar':
                        $monthi = 03;
                        break;
                    case 'Apr':
                        $monthi = 04;
                        break;
                    case 'May':
                        $monthi = 05;
                        break;
                    case 'Jun':
                        $monthi = 06;
                        break;
                    case 'Jul':
                        $monthi = 07;
                        break;
                    case 'Aug':
                        $monthi = 08;
                        break;
                    case 'Sep':
                        $monthi = 09;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }
        
        list($year, $hour) = explode(" ", $yearh);

        $dateShort = $year.'-'.$monthi.'-'.$day.' '.$hour;

        $dateUnix = human_to_unix($dateShort)*1000;
        $dateEnd=$dateUnix + 3600000;
        
        $this->db->set("start", $dateUnix);
        $this->db->set("end", $dateEnd);
		$this->db->set("po_no", $this->input->post("po"));
		$this->db->set("status", 'agendada');
		$this->db->set("customer_code", $this->input->post("customer"));
		$this->db->set("vendor_code", $this->input->post("vendor"));
        $this->db->set("url", base_url().'appointment/showEdit/');
        $this->db->set("class", 'event-success');
		if($this->db->insert("appointment"))
		{
			return TRUE;
		}
		return FALSE;
	}
    
    public function sendMail()
    {
        $date ='13/05/2016';
        $customer ='Core';
        $email = 'web@meridiano109.com';
        $po = 'frte785';
        $vendor = 'Nootfooed';
       // Varios destinatarios
        $mensaje= '';
     
        $this->load->library('email');

        $this->email->from('info@renycorp.com', 'renyCORP');
        $this->email->to($email);

        $this->email->subject('Entrada De Mercancia Recibida');
        $this->email->message($mensaje);
        $this->email->attach(base_url('/assets/global/img/renylogo.jpg'));
        
        if ($this->email->send())
        {
            echo "<script> alert('Se envio Correo de confirmacion a: $email');</script>";
        }
    }

	/**
	*/
	public function getAll()
	{
        $this->db->select('*');
        $this->db->from('appointment'); 
        $this->db->join('vendors', 'vendors.vendor_code = appointment.vendor_code');     
        $query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
        return object();
	}
    public function getAppointmentList()
    {
        $query = $this->db->select('*')->from('appointment')->join('vendors', 'vendors.vendor_code = appointment.vendor_code')->order_by('start','DESC')->get();
        if($query->num_rows() > 0)
        {
            
            return $query->result();
        }else{
            return $query->result();
        }
    }
    // Returns whether the date range of our event intersects with the given all-day range.
	// $rangeStart and $rangeEnd are assumed to be dates in UTC with 00:00:00 time.
	public function isWithinDayRange($rangeStart, $rangeEnd) 
    {

		// Normalize our event's dates for comparison with the all-day range.
		$eventStart = $this->stripTime($this->start);
		$eventEnd = isset($this->end) ? $this->stripTime($this->end) : null;

		if (!$eventEnd) {
			// No end time? Only check if the start is within range.
			return $eventStart < $rangeEnd && $eventStart >= $rangeStart;
		}
		else {
			// Check if the two ranges intersect.
			return $eventStart < $rangeEnd && $eventEnd > $rangeStart;
		}
	}
	// Converts this Event object back to a plain data array, to be used for generating JSON
	public function toArray() 
    {

		// Start with the misc properties (don't worry, PHP won't affect the original array)
		$array = $this->properties;

		$array['title'] = $this->title;
        $array['className'] = $this->className;

		// Figure out the date format. This essentially encodes allDay into the date string.
		if ($this->allDay) {
			$format = 'Y-m-d'; // output like "2013-12-29"
		}
		else {
			$format = 'c'; // full ISO8601 output, like "2013-12-29T09:00:00+08:00"
		}

		// Serialize dates into strings
		$array['start'] = $this->start->format($format);
		if (isset($this->end)) {
			$array['end'] = $this->end->format($format);
		}

		return $array;
	}
        // Date Utilities
        //----------------------------------------------------------------------------------------------


        // Parses a string into a DateTime object, optionally forced into the given timezone.
        function parseDateTime($string, $timezone=null) {
            $date = new DateTime(
                $string,
                $timezone ? $timezone : new DateTimeZone('UTC')
                    // Used only when the string is ambiguous.
                    // Ignored if string has a timezone offset in it.
            );
            if ($timezone) {
                // If our timezone was ignored above, force it.
                $date->setTimezone($timezone);
            }
            return $date;
        }


        // Takes the year/month/date values of the given DateTime and converts them to a new DateTime,
        // but in UTC.
        function stripTime($datetime) {
            return new DateTime($datetime->format('Y-m-d'));
        }
    
    public function delete_appointment($id)
	{
        $this->db->delete('appointment', array('id' => $id));
    }
    public 
     
    function cancel_appointment($id)
	{
        //Se insertan en la tabla los datos almacenados en $customer
		$this->db->where('id', $id);
        $this->db->set("status", 'cancelada');
        $this->db->set("class", 'event-important');
        $this->db->update('appointment');		
	}
}
/* End of file events_model.php */
/* Location: ./application/models/events_model.php */

	
