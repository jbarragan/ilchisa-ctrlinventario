<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();//cargar Base de datos
		
	}

    public function select_item($id=NULL)
    {
       if($id == 0 )
        {
            $this->db->from('items')->order_by('item_name');
            $query = $this->db->get();
             
            return $query->result_array();  
        }
        else
        {
            $this->db->select('*');
            $this->db->from('items')->order_by('item_name');
            $this->db->where('items_vendors.vendor_code', $id);
            $this->db->join('items_vendors', 'items_vendors.items_code = items.items_code');
            $query = $this->db->get();
      
            return $query->result_array(); 
        }  
    }
     public function select_customer($id=NULL)
    {
         if($id == 0 )
        {
            $this->db->from('customers')->order_by('first_name');
            $query = $this->db->get();
            
            return $query->result_array(); 
        }
        else
        {
            $this->db->select('*');
            $this->db->from('customers')->order_by('first_name');
            $this->db->where('customers_vendors.vendor_code', $id);
            $this->db->join('customers_vendors', 'customers_vendors.customer_code = customers.customer_code');
            $query = $this->db->get();
            
            return $query->result_array(); 
        } 
    }
     public function select_whse()
    {
        $this->db->from('warehouse')->order_by('whse_name');
        $query = $this->db->get();
            
        return $query->result_array(); 
    }
    public function select_vendor($id=NULL)
    {
        if($id == 0 )
        {
            $this->db->from('vendors')->order_by('name');
            $query = $this->db->get();
            
            return $query->result_array();     
        }
        else
        {
            $this->db->select('*');
            $this->db->from('vendors')->order_by('name');
            $this->db->where('customers_vendors.customer_code', $id);
            $this->db->join('customers_vendors', 'customers_vendors.vendor_code = vendors.vendor_code');
            $query = $this->db->get();
            
            return $query->result_array(); 
        }  
    }

    public function select_brand($id=NULL)
    {
        $this->db->from('brand')->order_by('name_brand');
        $query = $this->db->get();
             
        return $query->result_array();  
    }

    public function select_plant($id=NULL)
    {
        $this->db->from('plant')->order_by('name_plant');
        $query = $this->db->get();
             
        return $query->result_array();  
    }

     public function select_clasification($id=NULL)
    {
        $this->db->from('clasification')->order_by('name_clasification');
        $query = $this->db->get();
             
        return $query->result_array();  
    }

    public function select_receipts($idUser = NULL)
    {
        
        if (isset($idUser)) 
        {
            $this->db->select('*');
            $this->db->from('receipts');
            $this->db->join('lots', 'lots.reny_ctrl = receipts.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->where('user_id',$idUser);
            $this->db->where('status','Recibido');
            $this->db->order_by('receipts.reny_ctrl','status');
            
            $query = $this->db->get();
            if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;
                    foreach ($data as $key ) {

                        if ($key["permisoDetalle"] == 1) {

                        $arrayName[$i] = array( 'reny_ctrl' => '<a href="'.base_url("reports/report_lotsReceipts").'/'.$key["reny_ctrl"].'" id="export"  class="">'.$key["reny_ctrl"].'</a>',
                                            'status'         => $key["status"],
                                            'system_date'    => date_format(date_create($key['system_date']), 'd/M/Y'),
                                            'receipt_date'   => date_format(date_create($key['receipt_date']), 'd/M/Y'),
                                            'po_no'          => $key["po_no"],
                                            'first_name'     => $key["first_name"],
                                            'item_name'      => $key["item_name"],
                                            'name'           => $key["name"],
                                            'lot'            => $key["lot"],
                                            'pallets'        => $key["pallets"],
                                            'bags_total'     => $key["bags_total"]);
                    }else{
                        $arrayName[$i] = array( 'reny_ctrl' => $key["reny_ctrl"],
                                            'status'         => $key["status"],
                                            'system_date'    => date_format(date_create($key['system_date']), 'd/M/Y'),
                                            'receipt_date'   => date_format(date_create($key['receipt_date']), 'd/M/Y'),
                                            'po_no'          => $key["po_no"],
                                            'first_name'     => $key["first_name"],
                                            'item_name'      => $key["item_name"],
                                            'name'           => $key["name"],
                                            'lot'            => $key["lot"],
                                            'pallets'        => $key["pallets"],
                                            'bags_total'     => $key["bags_total"]);
                    }

                        $i = $i + 1;
                    
                    }
                       
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }

        }else 
        {

            $status = $this->input->get('status');
            $customer = $this->input->get('customer');
            $item = $this->input->get('item');

            $dateIn = $this->input->get('dateIn');
            $dateFin = $this->input->get('dateFin');

            if (isset($dateIn) && isset($dateFin) && $dateIn != null && $dateFin != null) {

                list( $dayIn, $monthIn, $yearIn) = explode("/", $dateIn);

                list( $dayFin, $monthFin, $yearFin) = explode("/", $dateFin);

                switch ($monthIn) {
                    case 'Jan':
                        $monthi = 01;
                        break;
                    case 'Feb':
                        $monthi = 02;
                        break;
                    case 'Mar':
                        $monthi = 03;
                        break;
                    case 'Apr':
                        $monthi = 04;
                        break;
                    case 'May':
                        $monthi = 05;
                        break;
                    case 'Jun':
                        $monthi = 06;
                        break;
                    case 'Jul':
                        $monthi = 07;
                        break;
                    case 'Aug':
                        $monthi = 08;
                        break;
                    case 'Sep':
                        $monthi = 09;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }

                switch ($monthFin) {
                    case 'Jan':
                        $monthf = 01;
                        break;
                    case 'Feb':
                        $monthf = 02;
                        break;
                    case 'Mar':
                        $monthf = 03;
                        break;
                    case 'Apr':
                        $monthf = 04;
                        break;
                    case 'May':
                        $monthf = 05;
                        break;
                    case 'Jun':
                        $monthf = 06;
                        break;
                    case 'Jul':
                        $monthf = 07;
                        break;
                    case 'Aug':
                        $monthf = 08;
                        break;
                    case 'Sep':
                        $monthf = 09;
                        break;
                    case 'Oct':
                        $monthf = 10;
                        break;
                    case 'Nov':
                        $monthf = 11;
                        break;
                    case 'Dec':
                        $monthf = 12;
                        break;
                    default:
                        break;
                }

                $fechaIn = $yearIn."/".$monthi."/".$dayIn;
                $fechaFin = $yearFin."/".$monthf."/".$dayFin;
            }

            switch ($status) {
                case '3':
                    $stat = 'Recibido';
                    break;
                case '2':
                    $stat = 'nuevo actualizado';
                    break;
                case '1':
                    $stat = 'nuevo';
                    break;
                default:
                    $stat = 0;
                    break;
            }

            if ( $status != 0 || $customer != 0  || $item != 0 || !isset($fechaIn )|| !isset($fechaFin) ) {
                $this->db->select('*');
                $this->db->from('receipts');
                $this->db->join('lots', 'lots.reny_ctrl = receipts.reny_ctrl');
                $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
                $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
                $this->db->join('items', 'items.items_code = receipts.items_code');
                if($status != 0 )$this->db->where('status',$stat);
                if($customer != 0)$this->db->where('receipts.customer_code',$customer);
                if($item != 0)$this->db->where('receipts.items_code',$item);
                if(isset($fechaIn )) $this->db->where('receipts.receipt_date >=',$fechaIn);
                if(isset($fechaFin))$this->db->where('receipts.receipt_date <=',$fechaFin);
                
                $query = $this->db->get();
                if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;
                    foreach ($data as $key ) {

                        $arrayName[$i] = array( 'reny_ctrl'     => $key["reny_ctrl"],
                                            'status'         => $key["status"],
                                            'system_date'    => date_format(date_create($key['system_date']), 'd/M/Y'),
                                            'receipt_date'   => date_format(date_create($key['receipt_date']), 'd/M/Y'),
                                            'po_no'          => $key["po_no"],
                                            'first_name'     => $key["first_name"],
                                            'item_name'      => $key["item_name"],
                                            'name'           => $key["name"],
                                            'lot'            => $key["lot"],
                                            'pallets'        => $key["pallets"],
                                            'bags_total'     => $key["bags_total"]);
                        $i = $i + 1;
                    }
                       
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }
            }else
            {
                $this->db->select('*');
                $this->db->from('receipts');
                $this->db->join('lots', 'lots.reny_ctrl = receipts.reny_ctrl');
                $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
                $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
                $this->db->join('items', 'items.items_code = receipts.items_code');
                if(isset($fechaIn))$this->db->where('receipts.receipt_date >=',$fechaIn);
                if(isset($fechaFin))$this->db->where('receipts.receipt_date <=',$fechaFin);
                
                $query = $this->db->get();
               if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;
                    foreach ($data as $key ) {

                        $arrayName[$i] = array( 'reny_ctrl' => $key["reny_ctrl"],
                                            'status'         => $key["status"],
                                            'system_date'    => date_format(date_create($key['system_date']), 'd/M/Y'),
                                            'receipt_date'   => date_format(date_create($key['receipt_date']), 'd/M/Y'),
                                            'po_no'          => $key["po_no"],
                                            'first_name'     => $key["first_name"],
                                            'item_name'      => $key["item_name"],
                                            'name'           => $key["name"],
                                            'lot'            => $key["lot"],
                                            'pallets'        => $key["pallets"],
                                            'bags_total'     => $key["bags_total"]);
                        $i = $i + 1;
                    }
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }
            }
        }
    }
    public function select_receiptsPdf($idUser = NULL)
    {
        
        if (isset($idUser)) 
        {
            $this->db->select('*');
            $this->db->from('receipts');
            $this->db->join('lots', 'lots.reny_ctrl = receipts.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->where('user_id',$idUser);
            $this->db->where('status','Recibido');
            $this->db->order_by('receipts.reny_ctrl','status');
            
            $query = $this->db->get();
            if ($query->num_rows()>0)
            {
                   return  json_encode($query->result_array());
            }

        }else 
        {

            $status = $this->input->get('status');
            $customer = $this->input->get('customer');
            $item = $this->input->get('item');

            $dateIn = $this->input->get('dateIn');
            $dateFin = $this->input->get('dateFin');

            if (isset($dateIn) && isset($dateFin) && $dateIn != null && $dateFin != null) {
               
                list($monthIn, $dayIn, $yearIn) = explode("/", $dateIn);

                list($monthFin, $dayFin, $yearFin) = explode("/", $dateFin);

                $fechaIn = $yearIn."/".$monthIn."/".$dayIn;
                $fechaFin = $yearFin."/".$monthFin."/".$dayFin;
            }

            switch ($status) {
                case '3':
                    $stat = 'Recibido';
                    break;
                case '2':
                    $stat = 'nuevo actualizado';
                    break;
                case '1':
                    $stat = 'nuevo';
                    break;
                default:
                    $stat = 0;
                    break;
            }
            if ( $status != 0 || $customer != 0  || $item != 0 || !isset($fechaIn )|| !isset($fechaFin) ) {
                $this->db->select('*');
                $this->db->from('receipts');
                $this->db->join('lots', 'lots.reny_ctrl = receipts.reny_ctrl');
                $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
                $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
                $this->db->join('items', 'items.items_code = receipts.items_code');
                if($status != 0 )$this->db->where('status',$stat);
                if($customer != 0)$this->db->where('receipts.customer_code',$customer);
                if($item != 0)$this->db->where('receipts.items_code',$item);
                if(isset($fechaIn )) $this->db->where('receipts.receipt_date >=',$fechaIn);
                if(isset($fechaFin))$this->db->where('receipts.receipt_date <=',$fechaFin);
                $this->db->order_by('receipts.reny_ctrl','status');
                
                $query = $this->db->get();
                if ($query->num_rows()>0)
                {
                   return  $query->result_array();
                }else
                {
               
                    $this->db->select('*');
                    $this->db->from('receipts');
                    $this->db->join('lots', 'lots.reny_ctrl = receipts.reny_ctrl');
                    $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
                    $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
                    $this->db->join('items', 'items.items_code = receipts.items_code');
                    if(isset($fechaIn))$this->db->where('receipts.receipt_date >=',$fechaIn);
                    if(isset($fechaFin))$this->db->where('receipts.receipt_date <=',$fechaFin);
                    $this->db->order_by('receipts.reny_ctrl','status');
                    
                    $query = $this->db->get();
                    if ($query->num_rows()>0)
                    {
                       return  $query->result_array();
                    }
                }
            }           
        }
    }
     
    public function shipmentJson($idUser = NULL)
    {
        if (isset($idUser)) 
        {
            $this->db->select('*');
            $this->db->from('shipments');
            $this->db->join('lotsship', 'lotsship.no_shipment = shipments.no_shipment');
            $this->db->join('lots', 'lots.lote_code = lotsship.lote_code');
            $this->db->join('inventory', 'inventory.lote_code = lots.lote_code');
            $this->db->join('receipts', 'receipts.reny_ctrl = lotsship.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->where('user_id',$idUser);
            $this->db->where('statusShip','Terminado');
            $this->db->order_by('shipments.no_shipment','statusShip');
                        
            $query = $this->db->get();
           
            if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;
                    foreach ($data as $key ) {
                        if ($key["permisoDetalle"] == 1) {
                            
                        $arrayName[$i] = array( 'no_shipment'    => $key["no_shipment"],
                                                'status'         => $key["statusShip"],
                                                'system_dateShip'=> $key["system_dateShip"],
                                                'start'          => mdate('%d/%M/%Y', ($key["start"]/1000)),
                                                'carrier'        => $key["carrier"],
                                                'trailer'        => $key["trailer"],
                                                'seal'           => $key["seal"],
                                                'reny_contrl'   => $key["reny_ctrl"],
                                                'first_name'     => $key["first_name"],
                                                'item_name'      => $key["item_name"],
                                                'lot'            => $key["lot"],
                                                'pallets'        => $key["pallets"],
                                                'bags_total'     => $key["bags_totalShip"]);
                        }else{
                            $arrayName[$i] = array( 'no_shipment'=> $key["no_shipment"],
                                                'status'         => $key["statusShip"],
                                                'system_dateShip'=> $key["system_dateShip"],
                                                'start'          => mdate('%d/%M/%Y', ($key["start"]/1000)),
                                                'carrier'        => $key["carrier"],
                                                'trailer'        => $key["trailer"],
                                                'seal'           => $key["seal"],
                                                'reny_contrl'   => $key["reny_ctrl"],
                                                'first_name'     => $key["first_name"],
                                                'item_name'      => $key["item_name"],
                                                'lot'            => $key["lot"],
                                                'pallets'        => $key["pallets"],
                                                'bags_total'     => $key["bags_totalShip"]);
                        }
                        $i = $i + 1;
                    }
                       
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }
        }else{

            $status = $this->input->get('status');
            $customer = $this->input->get('customer');
            $item = $this->input->get('item');
            $dateIn = $this->input->get('dateIn');
            $dateFin = $this->input->get('dateFin');

            if (isset($dateIn) && isset($dateFin) && $dateIn != null && $dateFin != null) {
               
                list($dayIn, $monthIn, $yearIn) = explode("/", $dateIn);

                list($dayFin, $monthFin, $yearFin) = explode("/", $dateFin);

                switch ($monthIn) {
                    case 'Jan':
                        $monthi = 01;
                        break;
                    case 'Feb':
                        $monthi = 02;
                        break;
                    case 'Mar':
                        $monthi = 03;
                        break;
                    case 'Apr':
                        $monthi = 04;
                        break;
                    case 'May':
                        $monthi = 05;
                        break;
                    case 'Jun':
                        $monthi = 06;
                        break;
                    case 'Jul':
                        $monthi = 07;
                        break;
                    case 'Aug':
                        $monthi = 08;
                        break;
                    case 'Sep':
                        $monthi = 09;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }

                switch ($monthFin) {
                    case 'Jan':
                        $monthf = 01;
                        break;
                    case 'Feb':
                        $monthf = 02;
                        break;
                    case 'Mar':
                        $monthf = 03;
                        break;
                    case 'Apr':
                        $monthf = 04;
                        break;
                    case 'May':
                        $monthf = 05;
                        break;
                    case 'Jun':
                        $monthf = 06;
                        break;
                    case 'Jul':
                        $monthf = 07;
                        break;
                    case 'Aug':
                        $monthf = 08;
                        break;
                    case 'Sep':
                        $monthf = 09;
                        break;
                    case 'Oct':
                        $monthf = 10;
                        break;
                    case 'Nov':
                        $monthf = 11;
                        break;
                    case 'Dec':
                        $monthf = 12;
                        break;
                    default:
                        break;
                }

                $fechaIn = $monthi."/".$dayIn."/".$yearIn;
                $fechaFin = $monthf."/".$dayFin."/".$yearFin;
            }


            switch ($status) {
                 case '4':
                    $stat = 'Terminado';
                    break;
                case '3':
                    $stat = 'Listo Modificado';
                    break;
                case '2':
                    $stat = 'Listo';
                    break;
                case '1':
                    $stat = 'Nuevo';
                    break;
                default:
                    $stat = 0;
                    break;
            }

                $this->db->select('*');
                $this->db->from('shipments');
                $this->db->join('lotsship', 'lotsship.no_shipment = shipments.no_shipment');
                $this->db->join('lots', 'lots.lote_code = lotsship.lote_code');
                $this->db->join('inventory', 'inventory.lote_code = lots.lote_code');
                $this->db->join('receipts', 'receipts.reny_ctrl = lotsship.reny_ctrl');
                $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
                $this->db->join('items', 'items.items_code = receipts.items_code');
                if($status != 0)$this->db->where('statusShip',$stat);
                if($customer != 0)$this->db->where('shipments.customer_code',$customer);
                if($item != 0)$this->db->where('shipments.items_code',$item);
                if(isset($fechaIn )) $this->db->where('shipments.system_dateShip >=',$fechaIn);
                if(isset($fechaFin))$this->db->where('shipments.system_dateShip <=',$fechaFin);
                $this->db->where('statusShip','Terminado');
                $this->db->order_by('shipments.no_shipment','statusShip');
                $query = $this->db->get();
                if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;
                    foreach ($data as $key ) {

                        $arrayName[$i] = array( 'no_shipment'    => $key["no_shipment"],
                                                'status'         => $key["statusShip"],
                                                'system_dateShip'=> $key["system_dateShip"],
                                                'start'          => mdate('%d/%M/%Y', ($key["start"]/1000)),
                                                'carrier'        => $key["carrier"],
                                                'trailer'        => $key["trailer"],
                                                'seal'           => $key["seal"],
                                                'reny_contrl'   => $key["reny_ctrl"],
                                                'first_name'     => $key["first_name"],
                                                'item_name'      => $key["item_name"],
                                                'lot'            => $key["lot"],
                                                'pallets'        => $key["pallets"],
                                                'bags_total'     => $key["bags_totalShip"],
                                                'storage'        => $key["storage"]);
                        $i = $i + 1;
                    }
                       
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }
        }

    }

    public function inventoryReport($idUser = NULL)
    {
        if (isset($idUser)) 
        {     
            $this->db->select('*');
            $this->db->from('inventory');
            $this->db->join('lots', 'lots.lote_code = inventory.lote_code');
            $this->db->join('receipts', 'lots.reny_ctrl = receipts.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->join('brand', 'brand.id_brand = receipts.id_brand');
            $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
            $this->db->join('locations', 'lots.id_location = locations.id_location');
            $this->db->where('user_id',$idUser);
            $this->db->where('stock !=','0');
            $this->db->order_by('receipts.reny_ctrl','status');
            $query = $this->db->get();
            
            if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;

                    foreach ($data as $key ) {

                        if($key['prod_date'] != '0000-00-00'){

                            $prod_date = date_format(date_create($key['prod_date']), 'd/M/Y');

                        }else{
                            $prod_date = '--';
                        }

                        if($key['exp_date'] != '0000-00-00'){

                            $exp_date = date_format(date_create($key['exp_date']), 'd/M/Y');

                        }else{
                            $exp_date = '--';
                        }


                        $arrayName[$i] = array( 'reny_ctrl'     => $key["reny_ctrl"],
                                                'receipt_date'   => date_format(date_create($key['receipt_date']), 'd/M/Y'),
                                                'first_name'     => $key["first_name"],
                                                'name'           => $key["name"],
                                                'item_name'      => $key["item_name"],
                                                'name_brand'      => $key["name_brand"],
                                                'lot'            => $key["lot"],
                                                'prod_date'      => $prod_date,
                                                'exp_date'       => $exp_date,
                                                'pallets'        => $key["pallets"],
                                                'stock'          => $key["stock"]);
                        $i = $i + 1;
                    }
                       
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }
        }else{
            $status = $this->input->get('status');
            $customer = $this->input->get('customer');
            $item = $this->input->get('item');
            $whse = $this->input->get('whse');
            $brand = $this->input->get('brand');
            $plant = $this->input->get('plant');
            $clasification = $this->input->get('clasification');
            $dateIn = $this->input->get('dateIn');
            $dateFin = $this->input->get('dateFin');

            if (isset($dateIn) && isset($dateFin) &&  $dateIn != null && $dateFin != null) {
               
                list($dayIn, $monthIn, $yearIn) = explode("/", $dateIn);

                list($dayFin, $monthFin, $yearFin) = explode("/", $dateFin);

                switch ($monthIn) {
                    case 'Jan':
                        $monthi = 01;
                        break;
                    case 'Feb':
                        $monthi = 02;
                        break;
                    case 'Mar':
                        $monthi = 03;
                        break;
                    case 'Apr':
                        $monthi = 04;
                        break;
                    case 'May':
                        $monthi = 05;
                        break;
                    case 'Jun':
                        $monthi = 06;
                        break;
                    case 'Jul':
                        $monthi = 07;
                        break;
                    case 'Aug':
                        $monthi = 08;
                        break;
                    case 'Sep':
                        $monthi = 09;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }

                switch ($monthFin) {
                    case 'Jan':
                        $monthf = 01;
                        break;
                    case 'Feb':
                        $monthf = 02;
                        break;
                    case 'Mar':
                        $monthf = 03;
                        break;
                    case 'Apr':
                        $monthf = 04;
                        break;
                    case 'May':
                        $monthf = 05;
                        break;
                    case 'Jun':
                        $monthf = 06;
                        break;
                    case 'Jul':
                        $monthf = 07;
                        break;
                    case 'Aug':
                        $monthf = 08;
                        break;
                    case 'Sep':
                        $monthf = 09;
                        break;
                    case 'Oct':
                        $monthf = 10;
                        break;
                    case 'Nov':
                        $monthf = 11;
                        break;
                    case 'Dec':
                        $monthf = 12;
                        break;
                    default:
                        break;
                }

                $fechaIn = $yearIn."/".$monthi."/".$dayIn;
                $fechaFin = $yearFin."/".$monthf."/".$dayFin;
            }

            $this->db->select('*');
            $this->db->from('inventory');
            $this->db->join('lots', 'lots.lote_code = inventory.lote_code');
            $this->db->join('receipts', 'lots.reny_ctrl = receipts.reny_ctrl');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->join('brand', 'brand.id_brand = receipts.id_brand');
            $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
            $this->db->join('locations', 'lots.id_location = locations.id_location');
            if($customer != 0)$this->db->where('receipts.customer_code',$customer);
            if($item != 0)$this->db->where('receipts.items_code',$item);
            if($whse != 0)$this->db->where('receipts.whse_code',$whse);
            if($brand != 0)$this->db->where('receipts.id_brand',$brand);
            if($plant != 0)$this->db->where('receipts.id_plant',$plant);
            if($clasification != 0)$this->db->where('receipts.id_clasification',$clasification);
            if(isset($fechaIn )) $this->db->where('receipts.receipt_date >=',$fechaIn);
            if(isset($fechaFin))$this->db->where('receipts.receipt_date <=',$fechaFin);
            $this->db->where('stock !=','0');
            $this->db->order_by('receipts.reny_ctrl');
            $query = $this->db->get();
                if ($query->num_rows()>0)
                {
                    $data = $query->result_array();
                    $i = 0;
                    foreach ($data as $key ) {

                        $start_ts = strtotime($key['receipt_date']); 

                        $end_ts = time(); 

                        $diff = $end_ts - $start_ts; 

                        if (round($diff / 86400) == 1) {

                            $d = "día";

                        }else{
                             $d = "días";
                        }

                        if($key['prod_date'] != '0000-00-00'){

                            $prod_date = date_format(date_create($key['prod_date']), 'd/M/Y');

                        }else{
                            $prod_date = '--';
                        }

                        if($key['exp_date'] != '0000-00-00'){

                            $exp_date = date_format(date_create($key['exp_date']), 'd/M/Y');

                        }else{
                            $exp_date = '--';
                        }

                        $arrayName[$i] = array( 'reny_ctrl'     => $key["reny_ctrl"],
                                                'receipt_date'   => date_format(date_create($key['receipt_date']), 'd/M/Y'),
                                                'first_name'     => $key["first_name"],
                                                'name'           => $key["name"],
                                                'item_name'      => $key["item_name"],
                                                'name_brand'      => $key["name_brand"],
                                                'lot'            => $key["lot"],
                                                'prod_date'      => $prod_date,
                                                'exp_date'       => $exp_date,
                                                'pallets'        => $key["pallets"],
                                                'stock'          => $key["stock"],
                                                'location'       => $key["location"],
                                                'storage'       => (round($diff / 86400))." ".$d);
                        $i = $i + 1;
                    }
                       
                }
                if (isset($arrayName)) {
                    
                    echo  json_encode(array('data'  => $arrayName));

                }else{
                    echo json_encode(array('data'  => ''));
                }
        }
    } 
    public function exporExcel(){

        $data = $this->input->post('table');
        $filname = $this->input->post('filename');

        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Content-Disposition: filename=$filname.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo  $data;
    }
}