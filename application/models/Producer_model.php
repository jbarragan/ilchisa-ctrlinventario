<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Producer_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	 //Metodo para Registrar
	public function insert($data)
    {
		$this->db->insert('producers',$data);		
	}
    
    //Editar
    public function edit($id)
	{
        $this->db->set("name_producer", $this->input->post("name_producer"));
        $this->db->where('id_producer', $id);
        $this->db->update('producers');
        
    }
    
    //consultar todos
    public function select($id=NULL)
	{
        //consultar si esta definido por un id.
        if(isset($id) != NULL)
        {
            $this->db->select('*');
            $this->db->from('producers');
            $this->db->where('id_producer',$id);
            $query = $this->db->get();

            return $query->row();
        }
        else
        {
            $this->db->select('*');//si no se definio id consulta todos
            $this->db->from('producers');
            $query = $this->db->get();
            
            if($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                $query = $this->db->get('producers');
                 return $query->result_array();
            }
        }
	}   
    public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('id_producer'=>$id));
        if ($query->num_rows() == 0) {

            $this->db->delete('producers', array('id_producer' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar Productor, tiene transacciones '));
        }
    }

    public function id_producer(){
        $query=$this->db->get('producers');
        $row= $query->last_row();

        return $row->id_producer;
    }
    
}