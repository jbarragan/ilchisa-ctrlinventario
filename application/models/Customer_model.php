<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();//cargar Base de datos
		
	}
    
    //Metodo para Registrar cliente
	public function reg_customer($data)
    {
		$this->db->insert('customers',$data);		
	}
    
    //Editar cliente
    public function edit_customer($id, $data)
	{
		$this->db->where('customer_code', $id);
        $this->db->update('customers', $data);
    }
     
    //Metodo para Registrar ship 	
    public function reg_ship($id)
	{
        $customer_code = $this->db->select('customers.customer_code');	

        $shipName = $this->input->post("name");
        $address = $this->input->post("address");
        $citystate= $this->input->post("city").', '.$this->input->post("state").','.$this->input->post("zip");
        $phone = $this->input->post('phone');

		$ship = array(
			'ship_name' => $shipName,
			'ship_state' => $citystate,
            'ship_address' => $address,
            'ship_phone' => $phone,
			'customer_code' => $id
		);
			
		$this->db->insert('ship_to',$ship);		
			
	}
     public function edit_ship($id)
    {
        $shipName = $this->input->post("name");
        $address = $this->input->post("address");
        $citystate= $this->input->post("city").', '.$this->input->post("state").','.$this->input->post("zip");
        $phone = $this->input->post('phone');

        $this->db->set('ship_name',$shipName);
        $this->db->set('ship_state',$citystate);
        $this->db->set('ship_address',$address);
        $this->db->set('ship_phone', $phone);
        $this->db->where('ship_code', $id);    
        $this->db->update('ship_to');     
            
    }
    
    //consultar todos los clientes
    public function select_cust($id=NULL)
	{
        //consultar cliente si esta definido por un id.
        if(isset($id) || !is_null($id))
        {
            $query = $this->db->get_where('customers',array('customer_code'=>$id));
            return $query->row_array();
        }
            $query = $this->db->get('customers');//si no se definio id consulta todos los clientes
            return $query->result_array();
	}
    
    //Consultar ship por clientes
    public function select_ship($id=NULL)
	{
        if (isset($id) || !is_null($id)) {
            $query = $this->db->get_where('ship_to',array('ship_code'=>$id));
            return $query->result_array(); 
        }else{

            $query=$this->db->get('ship_to');
            return $query->result_array(); 

        }        
	}
    
    public function delete_ship($id)
	{
        $this->db->delete('ship_to', array('ship_code' => $id));
    }
    
    public function delete_customer($id)
	{
        if(isset($id))
            {
                $query = $this->db->get_where('receipts',array('customer_code'=>$id));
                if ($query->num_rows() == 0) {

                    $this->db->delete('customers', array('customer_code' => $id));
                    echo json_encode(array('res' => true,
                                            'id'=>$id));
                }
                else
                {
                    echo json_encode(array('res' => false,
                                           'alert' => 'Imposible eliminar este cliente, tiene transacciones '));
                }
            }
        
    }
    //Metodo que muestra los proveedores que no estan relacionados al cliente.
    public function select_vendor($id=NULL)
	{
        $query1 = $this->db->query('SELECT vendor_code FROM customers_vendors WHERE customer_code ='.$id);
        if ($query1->num_rows() > 0)
        {
            $i=0;
            foreach($query1->result() as $show)
            {
                $array[$i] =$show->vendor_code;
                $i++;
            }
            $this->db->select('*');
            $this->db->from('vendors');
            $this->db->where_not_in('vendor_code',$array);
            $query = $this->db->get();
            return $query->result_array();
        }
        elseif($query1->num_rows() === 0)
        {
            $this->db->select('*');
            $this->db->from('vendors');
            $query = $this->db->get();
            return $query->result_array();
        }           
	}
    //consulta proveedores por cliente.
     public function select_vendor_customer($id=NULL)
	{
            $this->db->select('*');
            $this->db->from('vendors');
            $this->db->where('customers_vendors.customer_code', $id);
            $this->db->join('customers_vendors', 'customers_vendors.vendor_code = vendors.vendor_code');
            $this->db->join('customers', 'customers.customer_code = customers_vendors.customer_code');

            $query = $this->db->get();
            return $query->result_array();     
	}
    
    public function customer_vendor($idv,$idc)
	{
        $this->db->insert('customers_vendors',array('customer_code ' => $idc, 'vendor_code' => $idv));      
    }
    
    public function delete_vendor($idv, $idc)
	{
        $this->db->where('customer_code', $idc)->where('vendor_code', $idv)->delete('customers_vendors');
    }

    public function createUserCust($user, $id)
    {
            $this->db->select('user_id');
            $this->db->from('users');
            $this->db->where('username', $user);
            $query = $this->db->get();
            $id_user = $query->row();

        $this->db->where('customer_code', $id);
        $this->db->set("user_id", $id_user->user_id);
        $this->db->update('customers');
    }

}