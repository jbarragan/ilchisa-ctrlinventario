<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class PackingList_model extends CI_Model

{

	public function __construct()

	{

		parent::__construct();

		$this->load->database();//cargar Base de datos

        $this->load->helper('date');

	}



	public function selectPacking($noShipment=NULL)

	{

		if (isset($noShipment)) 

        {

            $this->db->select('*');

            $this->db->from('shipments');

            $this->db->join('lotsship', 'lotsship.no_shipment = shipments.no_shipment');

            $this->db->join('lots', 'lots.lote_code = lotsship.lote_code');

            $this->db->join('locations', 'locations.id_location = lots.id_location');

            $this->db->join('receipts', 'receipts.reny_ctrl = lotsship.reny_ctrl');

            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');

            $this->db->join('items', 'items.items_code = receipts.items_code');

            $this->db->join('warehouse', 'warehouse.whse_code = shipments.whse_code');

            $this->db->where('shipments.no_shipment',$noShipment);

            $query = $this->db->get();

            $res = $query->result_array();



            return  $res;



        } else{

        	$this->db->select('*');

            $this->db->from('shipments');

            $this->db->where('statusShip', 'Listo');

            $this->db->or_where('statusShip','Listo Modificado');

            

            $query = $this->db->get();

            return  $query->result();

        }    

	}

    public function selectShip($noShipment)

    { 

        if (isset($noShipment)) 

        {

            $this->db->select('customer_code');

            $this->db->from('shipments');

            $this->db->where('no_shipment',$noShipment);

            $query = $this->db->get();

            $res = $query->row_array();



            $customer_code = $res['customer_code'];

 

            $this->db->select('*');

            $this->db->from('ship_to');

            $this->db->where('customer_code', $customer_code);

            

            $query = $this->db->get();

            return  $query->result_array();

        }

    }

    public function showShip($noShipment)

    { 

        if (isset($noShipment)) 

        {

            $this->db->select('ship_code');

            $this->db->from('shipments');

            $this->db->where('no_shipment',$noShipment);

            $query = $this->db->get();

            $res = $query->row_array();



            if ($res['ship_code'] != 0) {



                return $res['ship_code'];



            }else{

                return 0;

            }





        }

    } 



    public function images($noShipment)

    {

        if (isset($_FILES["file"]["name"])) 

        {

            $file = $_FILES["file"]["name"];

            $type = $_FILES["file"]["type"];



            if(!is_dir("uploadsShip/"))

                mkdir("uploadsShip/", 0777);



            if($file && move_uploaded_file($_FILES["file"]["tmp_name"], "uploadsShip/".$file))

            {

                $this->db->set('name', $file);

                $this->db->set('url',base_url('uploadsShip'));

                $this->db->set('no_shipment',$noShipment);

                $this->db->insert("imgship");

            }

        }     

    }  



    public function select_images($noShipment= NULL)

    {

         if(isset($noShipment))

        { 

            $this->db->select('*');

            $this->db->from('imgship');

            $this->db->where('imgship.no_shipment', $noShipment);

            $query = $this->db->get();

         

            return $query->result_array();

        }

        

    }       



}