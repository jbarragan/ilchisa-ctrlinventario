<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    
	//Insertar en la base de datos
	public function register($data)
	{
        $this->db->insert('vendors',$data);
	}
   
    public function select($id=NULL)
	{
        if(isset($id))
        {
            $query = $this->db->get_where('vendors',array('vendor_code'=>$id));
            return $query->row_array();
        }
            $query = $this->db->get('vendors');//si no se definio id consulta todos los proveedores
            return $query->result_array();           
	}
     //Editar preoveedor
    public function edit_vendor($id, $data)
	{
		$this->db->where('vendor_code', $id);
        $this->db->update('vendors', $data);
    }
    
     public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('vendor_code'=>$id));
        if ($query->num_rows() == 0) {
            $this->db->delete('vendors', array('vendor_code' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar este Proveedor, tiene transacciones '));
        }
    }
    
}