<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receipts_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();//cargar Base de datos
        $this->load->helper('email');
        $this->load->library('email');
		
	}
     public function select_appointment($id=NULL)
	{
        //consultar cliente si esta definido por un id.
        if(isset($id))
        {
            $this->db->select('customer_code');
            $this->db->from('appointment');
            $this->db->where('id',$id);  
            $query = $this->db->get();
            if($query->row()->customer_code == 0)
            {
                $this->db->select('*');
                $this->db->from('appointment'); 
                $this->db->where('id',$id);   
                $this->db->join('vendors', 'vendors.vendor_code = appointment.vendor_code');
                $query = $this->db->get();
                 $data=array();
            
                foreach ($query->result() as $appoint =>$v) 
                {
                    
                    $data[$appoint]=$v;
                }
                return $data;
            }else{
                $this->db->select('*');
                $this->db->from('appointment'); 
                $this->db->join('customers', 'customers.customer_code = appointment.customer_code');
                $this->db->join('vendors', 'vendors.vendor_code = appointment.vendor_code');
                $this->db->where('id',$id);
                $query = $this->db->get();
            
                $data=array();
            
                foreach ($query->result() as $appoint =>$v) 
                {
                    
                    $data[$appoint]=$v;
                }
                return $data;
            }
        }else
        {
            return 'sin cita';
        }
	}
    
    public function select_receipts($id=NULL)
	{
        
        if(isset($id))
        { 
        //consultar cliente si esta definido por un id.
            $this->db->select('*');
            $this->db->from('receipts');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code');
            $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
            $this->db->join('items', 'items.items_code = receipts.items_code');
            $this->db->join('warehouse', 'warehouse.whse_code = receipts.whse_code');
            $this->db->join('producers', 'producers.id_producer = receipts.id_producer');
            $this->db->join('brand', 'brand.id_brand = receipts.id_brand');
            $this->db->join('plant', 'plant.id_plant = receipts.id_plant');
            $this->db->join('clasification', 'clasification.id_clasification = receipts.id_clasification');
            $this->db->where('receipts.reny_ctrl', $id);
            $query = $this->db->get();
            
            if ($query->num_rows()>0)
            {
             return  $query->result_array();
            }
            else 
            {
                $this->db->select('*');
                $this->db->from('receipts');
                $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code');
                $this->db->where('reny_ctrl', $id);
                 $query = $this->db->get();
                 
                 if ($query->num_rows() > 0) {

                    return  $query->result_array();

                 }
            }
        }else{
            $this->db->select('*');
            $this->db->from('receipts');
            $this->db->join('customers', 'customers.customer_code = receipts.customer_code','left');
            $this->db->join('vendors', 'vendors.vendor_code = receipts.vendor_code','left');
            $this->db->join('items', 'items.items_code = receipts.items_code','left');
            $this->db->where_not_in('status', 'Recibido');
            $this->db->order_by('system_date','DESC');

            
            $query = $this->db->get();
            return  $query->result_array();
        }
	}
    
     public function select_whse()
	{
        //consultar Bodega
        $this->db->from('warehouse')->order_by('whse_name');
        $query = $this->db->get();
        if ($query->num_rows()>0) {
        return $query->result_array();
        }
         
	}
    public function select_item($id=NULL)
	{
       if($id == 0 )
        {
            $this->db->from('items')->order_by('item_name');
            $query = $this->db->get();
             
            return $query->result_array();  
        }
        else
        {
            $this->db->select('*');
            $this->db->from('items')->order_by('item_name');
            $this->db->where('items_vendors.vendor_code', $id);
            $this->db->join('items_vendors', 'items_vendors.items_code = items.items_code');
            $query = $this->db->get();
      
            return $query->result_array(); 
        }  
	}
    public function select_producer($id=NULL)
    {

        $this->db->from('producers')->order_by('name_producer');
        $query = $this->db->get();

        return $query->result_array();  
 
    }

    public function select_brand($id=NULL)
    {

        $this->db->from('brand')->order_by('name_brand');
        $query = $this->db->get();

        return $query->result_array();  
 
    }

    public function select_plant($id=NULL)
    {

        $this->db->from('plant')->order_by('name_plant');
        $query = $this->db->get();

        return $query->result_array();  
 
    }

    public function select_clasification($id=NULL)
    {

        $this->db->from('clasification')->order_by('name_clasification');
        $query = $this->db->get();

        return $query->result_array();  
 
    }

     public function select_customer($id=NULL)
	{
         if($id == 0 )
        {
            $this->db->from('customers')->order_by('first_name');
            $query = $this->db->get();
            
            return $query->result_array(); 
        }
        else
        {
            $this->db->select('*');
            $this->db->from('customers')->order_by('first_name');
            $this->db->where('customers_vendors.vendor_code', $id);
            $this->db->join('customers_vendors', 'customers_vendors.customer_code = customers.customer_code');
            $query = $this->db->get();
            
            return $query->result_array(); 
        } 
	}

    public function select_vendor($id=NULL)
	{
        if($id == 0 )
        {
            $this->db->from('vendors')->order_by('name');
            $query = $this->db->get();
            
            return $query->result_array();     
        }
        else
        {
            $this->db->select('*');
            $this->db->from('vendors')->order_by('name');
            $this->db->where('customers_vendors.customer_code', $id);
            $this->db->join('customers_vendors', 'customers_vendors.vendor_code = vendors.vendor_code');
            $query = $this->db->get();
            
            return $query->result_array(); 
        }  
	}
    public function create($renyCtrl)
	{

         $date = $this->input->post('date');
         list( $day, $month, $year) = explode("/", $date);

                switch ($month) {
                    case 'Jan':
                        $monthi = 1;
                        break;
                    case 'Feb':
                        $monthi = 2;
                        break;
                    case 'Mar':
                        $monthi = 3;
                        break;
                    case 'Apr':
                        $monthi = 4;
                        break;
                    case 'May':
                        $monthi = 5;
                        break;
                    case 'Jun':
                        $monthi = 6;
                        break;
                    case 'Jul':
                        $monthi = 7;
                        break;
                    case 'Aug':
                        $monthi = 8;
                        break;
                    case 'Sep':
                        $monthi = 9;
                        break;
                    case 'Oct':
                        $monthi = 10;
                        break;
                    case 'Nnov':
                        $monthi = 11;
                        break;
                    case 'Dec':
                        $monthi = 12;
                        break;
                    default:
                        break;
                }

                $datef = $year."/".$monthi."/".$day;

        $this->db->select('reny_ctrl');
        $this->db->from('receipts');
        $this->db->where('reny_ctrl', $renyCtrl);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            
           //list($d,$m,$y) = explode("/",$this->input->post('date'));
           // $dat=$y.$m.$d;
            $this->db->set("receipt_date", $datef);
            $this->db->set("system_date", date('y/m/d'));
            $this->db->set("reny_ctrl", $renyCtrl);
            $this->db->set("po_no", $this->input->post("po"));
            $this->db->set("status","nuevo");
            $this->db->set("images", $this->input->post("img"));
            $this->db->set("whse_code", $this->input->post("whse"));
            $this->db->set("items_code", $this->input->post("item"));
            $this->db->set("id_appointment", $this->input->post("appoint"));
            $this->db->set("customer_code", $this->input->post("customer"));
            $this->db->set("vendor_code", $this->input->post("vendor"));
            $this->db->set("id_producer", $this->input->post("producer"));
            $this->db->set("id_brand", $this->input->post("brand"));
            $this->db->set("id_plant", $this->input->post("plant"));
            $this->db->set("id_clasification", $this->input->post("clasification"));
            $this->db->set("notes", $this->input->post("notes"));
            $this->db->set("received", $this->input->post("received"));
            $this->db->set("user", $this->auth_username);
            $this->db->insert("receipts");

            $this->put_notifications($renyCtrl);
            return true;
        }else return false;
        //$this->create_lots($renyCtrl);
	
	}

    public function buscar_duplicado()
    {
        
      $renyCtrl = $this->input->post("valorBusqueda");

        $this->db->select('reny_ctrl');
        $this->db->from('receipts');
        $this->db->where('reny_ctrl', $renyCtrl);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            echo 'true';
        }else echo 'false';
        //$this->create_lots($renyCtrl);
    
    }
    public function create_lots($renyCtrl)
	{
        if ($this->input->post("ht")==1) {
            $ht='si';
        } else 
         {$ht='no';}
        if ($this->input->post("tags")==1) {
            $tg='si';
        } else 
          {$tg='no';}
        $this->db->set("notes", $this->input->post("notes"));
        $this->db->set("status","nuevo actualizado");
        $this->db->set("po_no", $this->input->post("po"));
        $this->db->set("ht", $ht);
        $this->db->set("tags", $tg);
        $this->db->set("whse_code", $this->input->post("whse"));
        $this->db->set("items_code", $this->input->post("item"));
            $this->db->set("id_producer", $this->input->post("producer"));
            $this->db->set("id_brand", $this->input->post("brand"));
            $this->db->set("id_plant", $this->input->post("plant"));
            $this->db->set("id_clasification", $this->input->post("clasification"));
            $this->db->set("received", $this->input->post("received"));
        $this->db->where('reny_ctrl', $this->input->post("receipt_reny"));
        $this->db->update('receipts');
       
        $lot= $this->input->post('lots');
        $pall= $this->input->post('pallets');
        $bag_pall=$this->input->post('bags_pallets');
        $bag=$this->input->post('bags');
        $pDate=$this->input->post('prod_date');
        $eDate=$this->input->post('exp_date');
        $dmge=$this->input->post('damage');
        $amtDamage=$this->input->post('amtDamage');
        $loc= $this->input->post("whse_loc");

        for ($i = 0; $i < count($lot); $i++) 
        {
           
			$this->db->set('lot',$lot[$i]);
            $this->db->set('pallets',$pall[$i]);
            $this->db->set("bags_pallets",null);
            $this->db->set('bags_total',$bag[$i]);
            $this->db->set('prod_date', $pDate[$i]);
            $this->db->set('exp_date',$eDate[$i]);
            $this->db->set('damage',$dmge[$i]);
            $this->db->set('amtDamage',$amtDamage[$i]);
            $this->db->set('id_location',$loc[$i]);
            $this->db->set("reny_ctrl",$this->input->post("receipt_reny"));
            //Grabamos los datos en la tabla personal
           $this->db->insert("lots");

           if ($loc[$i] != 0) {
                    $this->db->set('status',1);
                    $this->db->where('id_location',$loc[$i]);
                    $this->db->update("locations");
            }
       }
	}

    public function updateReceipt($renyCtrl)
    {
        $c= $this->input->post('index');
        $notes= $this->input->post("notes");

        if ($this->input->post("ht")==1) {
            $ht='si';
        } else 
         {$ht='no';}
                
        if ($this->input->post("tags")==1) {
            $tg='si';
        } else 
          {$tg='no';}
        $this->db->set("status","nuevo actualizado");
        $this->db->set("po_no", $this->input->post("po"));
        $this->db->set("customer_code", $this->input->post("customer"));
        $this->db->set("vendor_code", $this->input->post("vendor"));
        $this->db->set("notes", $notes);
        $this->db->set("ht", $ht);
        $this->db->set("tags", $tg);
        $this->db->set("whse_code", $this->input->post("whse"));
        $this->db->set("items_code", $this->input->post("item"));
        $this->db->set("user", $this->auth_username);
            $this->db->set("id_producer", $this->input->post("producer"));
            $this->db->set("id_brand", $this->input->post("brand"));
            $this->db->set("id_plant", $this->input->post("plant"));
            $this->db->set("id_clasification", $this->input->post("clasification"));
            $this->db->set("received", $this->input->post("received"));
        $this->db->where('reny_ctrl', $this->input->post("receipt_reny"));
        $this->db->update('receipts');
       
        $idlot= $this->input->post('idLots');
        $lot= $this->input->post('lots');
        $pall= $this->input->post('pallets');
        $bag_pall=null;
        $bag=$this->input->post('bags');
        $pDate=$this->input->post('prod_date');
        $eDate=$this->input->post('exp_date');
        $dmge=$this->input->post('damage');
        $amtDamage=$this->input->post('amtDamage');
        $loc= $this->input->post("whse_loc");

       for ($i = 0; $i < $c; $i++) 
       {
            $this->db->select('id_location');
            $this->db->from('lots');
            $this->db->where('lot', $lot[$i]);
            $query = $this->db->get();
            $idLoc= $query->row()->id_location;

            if ( $idLoc != $idlot[$i])  {
                $this->db->set('status', 0);
                $this->db->where('id_location',$idLoc);
                $this->db->update("locations");
            }
            
                $this->db->set('lot',$lot[$i]);
                $this->db->set('pallets',$pall[$i]);
                $this->db->set("bags_pallets",$bag_pall[$i]);
                $this->db->set('bags_total',$bag[$i]);
                $this->db->set('prod_date', $pDate[$i]);
                $this->db->set('exp_date',$eDate[$i]);
                $this->db->set('damage', $dmge[$i]);
                $this->db->set('amtDamage',$amtDamage[$i]);
                $this->db->set('id_location',$loc[$i]);
                $this->db->where('lote_code',$idlot[$i]);
                //Grabamos los datos en la tabla personal
                $this->db->update("lots");

                if ($loc[$i] != 0) {
                    $this->db->set('status',1);
                    $this->db->where('id_location',$loc[$i]);
                    $this->db->update("locations");
                }

                
        }
            if ($c < count($lot)) 
            {
                for ($x = $c; $x < count($lot); $x++) 
                {

                    {$is='no';}
                    $this->db->set('lot',$lot[$x]);
                    $this->db->set('pallets',$pall[$x]);
                    $this->db->set("bags_pallets",$bag_pall[$x]);
                    $this->db->set('bags_total',$bag[$x]);
                    $this->db->set('prod_date', $pDate[$x]);
                    $this->db->set('exp_date',$eDate[$x]);
                    $this->db->set('damage',$dmge[$x]);
                    $this->db->set('amtDamage',$amtDamage[$i]);
                    $this->db->set('id_location',$loc[$x]);
                    $this->db->set('reny_ctrl', $this->input->post("receipt_reny"));
                    //Grabamos los datos en la tabla personal
                    $this->db->insert("lots");

                    if ($loc[$x] != 0) {
                        $this->db->set('status',1);
                        $this->db->where('id_location',$loc[$i]);
                        $this->db->update("locations");
                    }
                }
            } 
    }
    public function ready($renyCtrl)
    {
        $this->db->set("status", "Recibido");
        $this->db->set("user", $this->auth_username);
        $this->db->where('reny_ctrl', $renyCtrl);
        $this->db->update('receipts');
        
       $stock= $this->input->post('bags');
       $lote_code=$this->input->post('idLots');
       
       for ($i = 0; $i < count($lote_code); $i++) 
       {
			$this->db->set('stock', $stock[$i]);
            $this->db->set('lote_code',$lote_code[$i]);
            $this->db->set('reny_ctrl',$renyCtrl);
            //Grabamos los datos en la tabla personal
           $this->db->insert("inventory");
       }
            $this->db->select('id_appointment');
            $this->db->from('receipts');
            $this->db->where('reny_ctrl', $renyCtrl);
            $query = $this->db->get();
            $idAppoint= $query->row()->id_appointment;
       if(isset($idAppoint))
        { 
            $this->db->set("status", "cerrada");
            $this->db->set("class", "event-warning");
            $this->db->where('id', $idAppoint);
            $this->db->update('appointment');
        }



        if ($this->input->post("sendMail")==1) {


            $date=$this->input->post("date");
            $po=$this->input->post("po");

            $this->db->select('name');
            $this->db->from('vendors'); 
            $this->db->where('vendors.vendor_code', $this->input->post("vendor"));
            $query = $this->db->get();
            
            $vendor=$query->row()->name;

            $this->db->select('item_name');
            $this->db->from('items'); 
            $this->db->where('items_code', $this->input->post("item"));
            $query = $this->db->get();
            
            $item=$query->row()->item_name;

            $this->db->select('first_name, email');
            $this->db->from('customers'); 
            $this->db->where('customer_code', $this->input->post("customer"));
            $query = $this->db->get();
           
            $email=$query->row()->email;
            $customer = $query->row()->first_name;

            $this->sendMail($renyCtrl);
          
        }

        
    }  
    public function MoifyRec($renyCtrl,$user)
    {
        if ($this->input->post("ht")==1) {
            $ht='si';
        } else 
         {$ht='no';}
                
        if ($this->input->post("tags")==1) {
            $tg='si';
        } else 
          {$tg='no';}

        $notes= $this->input->post("notes");

        $this->db->set("customer_code", $this->input->post("customer"));
        $this->db->set("whse_code", $this->input->post("whse"));
        $this->db->set("po_no", $this->input->post("po"));
        $this->db->set("vendor_code", $this->input->post("vendor"));
        $this->db->set("notes", $notes);
        $this->db->set("ht", $ht);
        $this->db->set("tags", $tg);
        $this->db->set("items_code", $this->input->post("item"));
            $this->db->set("id_producer", $this->input->post("producer"));
            $this->db->set("id_brand", $this->input->post("brand"));
            $this->db->set("id_plant", $this->input->post("plant"));
            $this->db->set("id_clasification", $this->input->post("clasification"));
            $this->db->set("received", $this->input->post("received"));
        $this->db->set("user", $user);
        $this->db->where('reny_ctrl', $renyCtrl);
        $this->db->update('receipts');

        $lot= $this->input->post('lots');
        $pall= $this->input->post('pallets');
        $bag=$this->input->post('bags');
        $pDate=$this->input->post('prod_date');
        $eDate=$this->input->post('exp_date');
        $dmge=$this->input->post('damage');
        $loc= $this->input->post("whse_loc");
        
       $stock= $this->input->post('bags');
       $lote_code=$this->input->post('idLots');
       
       for ($i = 0; $i < count($lote_code); $i++) 
       {
            $this->db->set('lot',$lot[$i]);
            $this->db->set('pallets',$pall[$i]);
            $this->db->set('bags_total',$bag[$i]);
            $this->db->set('prod_date', $pDate[$i]);
            $this->db->set('exp_date',$eDate[$i]);
            $this->db->set('damage',$dmge[$i]);
            $this->db->set('id_location',$loc[$i]);
            $this->db->where('lote_code',$lote_code[$i]);
            $this->db->update("lots");

            $this->db->set('stock', $stock[$i]);
            $this->db->where('lote_code',$lote_code[$i]);
            //Grabamos los datos en la tabla personal
           $this->db->update("inventory");
       }
    } 
    public function renyCtrl()
    {
        $datereny = date('y');
        $query=$this->db->get('receipts');
        
        $row= $query->last_row();
        
        if ($query->num_rows() > 0) 
        {
            $numreny= $row->reny_ctrl;

            $num = substr($numreny, -5, 5);
            //list($vic, $year, $num) = explode("-",  $numreny);

                $nextNum=str_pad($num + 1,5,'0', STR_PAD_LEFT);
            
                $nrenyCtrl = 'ILC'. $nextNum;
                return  $nrenyCtrl;
        }else{
                $nrenyCtrl = 'ILC'.'00001';
                return  $nrenyCtrl;
            }
    }
    
    public function select_lotsReceipts($renyCtrl = NULL)
    {
        if(isset($renyCtrl))
        { 
            $this->db->select('*');
            $this->db->from('lots');
            $this->db->join('locations', 'locations.id_location = lots.id_location');
            $this->db->where('lots.reny_ctrl', $renyCtrl);
            $query = $this->db->get();
         
            return $query->result_array(); 
        }else{

            $this->db->select('*');
            $this->db->from('lots');
            $this->db->join('locations', 'locations.id_location = lots.id_location');
            $this->db->where('lots.reny_ctrl', $renyCtrl);
            $query = $this->db->get();

            echo json_encode($query->result_array());
        } 
    }

    public function deleteImg($id, $name){
            if(file_exists('./uploads/'.$name))
            {
                $this->db->delete('images', array('id' => $id));
                unlink('./uploads/'.$name);
                echo json_encode(array('res' => true,
                                        'id'=>$id));
            }
            else
            {
                $this->db->delete('images', array('id' => $id));
                echo json_encode(array('res' => true,
                                        'id'=>$id));
            }
    }

    public function images($renyCtrl){
        
            $file = $_FILES["file"]["name"];
            $type = $_FILES["file"]["type"];
            $ext = $_FILES["file"]["type"];
            list($name, $extension) = explode('.', $file);

            if(!is_dir("uploads/"))
                mkdir("uploads/", 0777);

            if (file_exists("uploads/".$file)) {

                $newname =  $name.rand(000,999).'.'.$extension;

                if($file && move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/".$newname))
                {
                    $this->db->set('name', $newname);
                    $this->db->set('url',base_url('uploads'));
                    $this->db->set('reny_ctrl',$renyCtrl);
                    $this->db->insert("images");
                }


            } else {

                if($file && move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/".$file))
                {
                    $this->db->set('name', $file);
                    $this->db->set('url',base_url('uploads'));
                    $this->db->set('reny_ctrl',$renyCtrl);
                    $this->db->insert("images");
                }
            }
    }

    public function select_images($renyCtrl= NULL)
    {
         if(isset($renyCtrl))
        { 
            $this->db->select('*');
            $this->db->from('images');
            $this->db->where('images.reny_ctrl', $renyCtrl);
            $query = $this->db->get();
         
            return $query->result_array();
        }
        
    }
    public function select_location(){
        $this->db->select('*');
        $this->db->from('locations');
        $query = $this->db->get();
         
            if ($query->num_rows()>0)
            {
                echo json_encode($query->result_array());
            }
    }  
    public function select_locationArra(){
        $this->db->select('*');
        $this->db->from('locations');
        $query = $this->db->get();
         
        return $query->result_array();
    } 
    public function num_lotsReceipts($renyCtrl)
    {
            $this->db->select('*');
            $this->db->from('lots');
            $this->db->join('locations', 'locations.id_location = lots.id_location');
            $this->db->where('lots.reny_ctrl', $renyCtrl);
            $query = $this->db->get();
         
            return $query->num_rows();
    } 
     public function deleteLots($id=NULL){
            if(isset($id))
            {     
                    $this->db->select('id_location');
                    $this->db->from('lots');
                    $this->db->where('lote_code', $id);
                    $query = $this->db->get();
                    $idLoc = $query->row();

                    $this->db->set('status', 0);
                    $this->db->where('id_location',$idLoc->id_location);
                    $this->db->update("locations");

                $this->db->delete('lots', array('lote_code' => $id));
                echo json_encode(array('res' => true,
                                        'id'=>$id));

            }
            else
            {
                echo json_encode(array('res' => false));
            }

    }
    public function deleteReceipt($id){
            $this->db->select('status');
            $this->db->from('receipts');
            $this->db->where('reny_ctrl', $id);
            $query = $this->db->get();
            $resp = $query->row();

            if($resp->status != 'Recibido')
            {
                $this->db->delete('receipts', array('reny_ctrl' => $id));
                $this->db->delete('lots', array('reny_ctrl' => $id));

                $this->db->select('name');
                $this->db->from('images');
                $this->db->where('reny_ctrl', $id);
                $img = $this->db->get();

                if ($img->num_rows() > 0) {
                    $delImg = $img->result_array();

                    for ($i=0; $i < $img->num_rows(); $i++) { 
                        $this->db->delete('images', array('reny_ctrl' => $id));
                        unlink('./uploads/'.$delImg[$i]['name']);
                    }
                }
                $this->db->delete('notifications', array('code_trx' => $id));
                echo json_encode(array('res' => true,
                                        'id'=>$id));

            }
            else
            {
                echo json_encode(array('res' => 'No puede eliminar Entradas con status Recibido'));
            }
    }

    public function sendMail($renyCtrl)
    {
        $receipt = $this->select_receipts($renyCtrl);
        $lots=$this->receipts_model->select_lotsReceipts($renyCtrl);
        $images=$this->receipts_model->select_images($renyCtrl);

        $this->load->library('email');

        $configGmail = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'aws.digital-webhosting.com',
            'smtp_user' => 'info@ilchisa.com',
            'smtp_pass' => 'Ilchisa17',
            'smtp_port' => '465',
            'smtp_crypto'=> 'ssl',
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'newline'   => "\r\n"
        ); 
 
        //cargamos la configuración para enviar con gmail
        $this->email->initialize($configGmail);

        $tot_pal=0;
        $tot_bag=0;
        foreach ($lots as $rec) {
            $tot_pal=$tot_pal + ($rec['pallets'] * $rec['bags_total']);
            $tot_bag=$tot_bag + $rec['bags_total'];
        }

        $logo=base_url('/assets/global/img/logo-reny-picot.png');
        $date =$receipt[0]['receipt_date'];
        $customer =$receipt[0]['first_name'];
        $email = $receipt[0]['email'];
        $po = $receipt[0]['po_no'];
        $vendor = $receipt[0]['name'];
        $item = $receipt[0]['item_name'];
        $ht = $receipt[0]['ht'];
        $tags = $receipt[0]['tags'];
        $notes = $receipt[0]['notes'];
        $whse = $receipt[0]['whse_name'];
        $status = $receipt[0]['status'];
        $producer = $receipt[0]['name_producer'];
        $brand = $receipt[0]['name_brand'];
        $plant = $receipt[0]['name_plant'];
        $clasification = $receipt[0]['name_clasification'];
       // Varios destinatarios
        
        

        $mensaje= '<html lang="en"><!--<![endif]--><!-- START @HEAD -->
        <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">

                    <!-- START @THEME STYLES -->
                    <link href="'.base_url('/assets/admin/css/reset.css').'" rel="stylesheet">
        </head>
        <body style="background:#fff">
            <div style="width:90%; margin:auto;">
                <table width="100%">
                    <tr>
                        <td class="logo">
                            <img class="logo" src="'.$logo.'" alt="brand logo" width="250">
                            
                        </td>
                        <td>
                        <h1 style="text-align:center;">Industrias Lacteas Chihuahuenses, S.A. de C.V.</h1>
                        </td>
                        <td style="text-align:right;">
                            7158 Merchant Ave,El Paso, Tx 79915<br>
                            <strong>Phone Number:</strong> +1 (915) 778-3933<br>
                            <strong>Fax Number:</strong> +1 (915) 778-3009<br>
                            <strong>E-mail:</strong> info@ilchisa.com <br>
                        </td>
                    </tr>
                </table>
                <section>
                    <div class="row">
                        <div class="col-md-12">

                        <!-- Start basic validation -->

                            <hr> 
                            <table width="100%">
                                <tr>
                                    <td>
                                        <strong>No. Ctrl:</strong> '.$renyCtrl.'
                                    </td>   
                                    <td>
                                        <strong>Bodega:</strong> '. $whse
                                    .'</td>
                                    <td style="text-align:right;">
                                        <strong>Fecha:</strong>'.$date
                                    .'</td> 
                                </tr>
                                <tr class="row">
                                    <td>
                                        <strong>Po. No.:</strong>
                                        '.$po.'
                                    </td>
                                    <td >
                                        <strong>Producto:</strong> '.$item
                                        .'</select>
                                    </td>
                                </tr>
                                <tr class="row">
                                    <td>
                                        <strong>Cliente:</strong>                            
                                        '.$customer.'
                                    </td>
                                    <td>
                                        <strong>Productor:</strong> '.$producer
                                    .'</td>
                                </tr>    
                                <tr class="row">
                                    <td>
                                        <strong>Proveedor:</strong> '.$vendor.'
                                    </td>
                                    <td>
                                        <strong>Marca:</strong> '.$brand
                                    .'</td>
                                </tr>   
                                <tr class="row">
                                    <td>
                                        <strong>Planta:</strong> '.$plant.'
                                    </td>
                                    <td>
                                        <strong>Clasificación:</strong> '.$clasification
                                    .'</td>
                                </tr>
                            </table

                                <hr>   
                                <hr> <table id="datatable" class="table table-striped" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td style="font-weight:bold">Lote</td>
                                                    <td style="font-weight:bold">Pallets</td>
                                                    <td style="font-weight:bold">Bags</td>
                                                    <td style="font-weight:bold">Prod Date</td>
                                                    <td style="font-weight:bold">Exp Date</td>
                                                    <td style="font-weight:bold">Damage</td>
                                                    <td style="font-weight:bold">AMT Damage</td>
                                                    <td style="font-weight:bold">Location</td>
                                                </tr>';
                                            foreach ($lots as $rec) {
                                    $mensaje .='<tr>
                                                    <td>'.$rec['lot']
                                                    .'</td>
                                                    <td>'.$rec['pallets']
                                                    .'</td>
                                                    <td>'.$rec['bags_total']
                                                    .'</td> 
                                                    <td>'; 
                                                    if ($rec['prod_date'] != "0000-00-00") {
                                                            $mensaje .= date_format(date_create($rec['prod_date']), 'd/M/Y');# code...
                                                            }else{ $mensaje .="---"; }
                                                    $mensaje .='</td>
                                                    <td>';if ($rec['prod_date'] != "0000-00-00") {
                                                                $mensaje .=date_format(date_create($rec['exp_date']), 'd/M/Y');# code...
                                                            }else{$mensaje .="---";}
                                                    $mensaje .='</td>
                                                    <td>'.$rec['damage']
                                                    .'</td>
                                                    <td>'.$rec['amtDamage']
                                                    .'</td>
                                                    <td>'.$rec['location']
                                                    .'</td>
                                                </tr>';
                                            }
                                    $mensaje .='</tbody>
                                        </table>
                                <hr>
                                <div>
                                    <div style="font-size:16px;">
                                        <strong>Total KG/Bags:</strong> '.$tot_pal
                                    .' <strong>Total Bags:</strong> '.$tot_bag
                                    .'</div
                                </div>
                                <div>
                                    <div style="font-size:16px;">
                                        <strong>Notas:</strong>
                                        <br>'.$notes     
                                    .'</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </body>
    </html>';
        $this->email->from('info@ilchisa.com ', 'Industrias Lacteas Chihuahuenses');
        $this->email->to($email);
        $this->email->subject('Mercancia Recibida, No. Ctrl: '.$renyCtrl);
        $this->email->message($mensaje);
        foreach ($images as $img) {
        $image= $img['url'].'/'.$img['name'];  
        $this->email->attach($image); 
        }
        //$this->email->send();       
            if ($this->email->send())
            {
                echo json_encode(array('res' => true,
                                        'mail'=>$email,
                                        'status'=>$status));
            }else
                {
                    echo json_encode(array('res' => false,
                                        'mail'=>$email,
                                        'status'=>$status));
                }
    }
    public function delete_notifications($renyCtrl){
        //Status : nuevo, visto, leido

        $this->db->where('code_trx',$renyCtrl);
        $this->db->delete('notifications');
    }
    public function put_notifications($renyCtrl){
        //Status : nuevo, visto, leido

        $this->db->set('code_trx', $renyCtrl);
        $this->db->set('trx','Entrada');
        $this->db->set('status','Nuevo');
        $this->db->set('notify',1);
        $this->db->insert('notifications');
    }

    public function search_receipt($renyCtrl)
    {
        //consultar Bodega
        $this->db->from('receipts')->where('reny_ctrl', $renyCtrl)->where('status', 'Recibido');
        $query = $this->db->get();

        if ($query->num_rows()>0) {

            return true;

        }else{

            return false;
         
        }
    }
}