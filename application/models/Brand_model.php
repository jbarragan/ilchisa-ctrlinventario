<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	 //Metodo para Registrar
	public function insert($data)
    {
		$this->db->insert('brand',$data);		
	}
    
    //Editar
    public function edit($id)
	{
        $this->db->set("name_brand", $this->input->post("name_brand"));
        $this->db->where('id_brand', $id);
        $this->db->update('brand');
        
    }
    
    //consultar todos
    public function select($id=NULL)
	{
        //consultar si esta definido por un id.
        if(isset($id) != NULL)
        {
            $this->db->select('*');
            $this->db->from('brand');
            $this->db->where('id_brand',$id);
            $query = $this->db->get();

            return $query->row();
        }
        else
        {
            $this->db->select('*');//si no se definio id consulta todos
            $this->db->from('brand');
            $query = $this->db->get();
            
            if($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                $query = $this->db->get('brand');
                 return $query->result_array();
            }
        }
	}   
    public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('id_brand'=>$id));
        if ($query->num_rows() == 0) {
            $this->db->delete('brand', array('id_brand' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar la Marca, tiene transacciones '));
        }
    }

    public function id_brand(){
        $query=$this->db->get('brand');
        $row= $query->last_row();

        return $row->id_brand;
    }
    
}