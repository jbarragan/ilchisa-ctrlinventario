<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	 //Metodo para Registrar producto
	public function insert($data)
    {
		$this->db->insert('items',$data);		
	}
    
    //Editar producto
    public function edit($id)
	{
        $this->db->set("item_name", $this->input->post("item_name"));
        $this->db->set("desc", $this->input->post("item_desc"));
        $this->db->where('items_code', $id);
        $this->db->update('items');

        /*$this->db->set("items_code", $id);
        $this->db->set("vendor_code", $this->input->post("item_vendor"));
        $this->db->where('items_code', $id);
        $this->db->update('items_vendors');*/

        
    }
    
    //consultar todos los productos
    public function select($id=NULL)
	{
        //consultar producto si esta definido por un id.
        if(isset($id) != NULL)
        {
            $this->db->select('*');
            $this->db->from('items');
            //$this->db->join('items_vendors','items_vendors.items_code = items.items_code');
            $this->db->where('items.items_code',$id);
            $query = $this->db->get();

            return $query->row();
        }
        else
        {
            $this->db->select('*');//si no se definio id consulta todos los productos
            $this->db->from('items');
            $query = $this->db->get();
            
            if($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                $query = $this->db->get('items');
                 return $query->result_array();
            }
        }
	}   
    public function delete($id)
	{
        $query = $this->db->get_where('receipts',array('items_code'=>$id));
        if ($query->num_rows() == 0) {

            $this->db->delete('items', array('items_code' => $id));
             echo json_encode(array('res' => true,
                                            'id'=>$id));

        }else{
            echo json_encode(array('res' => false,
                                   'alert' => 'Imposible eliminar este producto, tiene transacciones '));
        }

    }
    public function id_item(){
        $query=$this->db->get('items');
        $row= $query->last_row();

        return $row->items_code;
    }
    //Metodo que muestra los proveedores que no estan relacionados al producto.
    public function select_vendor($id=NULL)
	{if (isset($id)) {
          $query1 = $this->db->query('SELECT vendor_code FROM items_vendors WHERE items_code ='.$id);
        if ($query1->num_rows() > 0)
        {
            $i=0;
            foreach($query1->result() as $show)
            {
                $array[$i] =$show->vendor_code;
                $i++;
            }
            $this->db->select('*');
            $this->db->from('vendors');
            $this->db->where_not_in('vendor_code',$array);
            $query = $this->db->get();
            return $query->result_array();
        }elseif($query1->num_rows() === 0)
        {
            $this->db->select('*');
            $this->db->from('vendors');
            $query = $this->db->get();
            return $query->result_array();
        }      
    }
        else
        {
            $this->db->select('*');
            $this->db->from('vendors');
            $query = $this->db->get();
            return $query->result_array();
        }           
	}
    //consulta proveedores por producto.
     public function select_vendor_item($id=NULL)
	{
            $this->db->select('*');
            $this->db->from('vendors');
            $this->db->where('items_vendors.items_code', $id);
            $this->db->join('items_vendors', 'items_vendors.vendor_code = vendors.vendor_code');
            $this->db->join('items', 'items.items_code = items_vendors.items_code');

            $query = $this->db->get();
            return $query->result_array();     
	}
    
    public function insert_item_vendor($idv,$idc)
	{
        $this->db->insert('items_vendors',array('items_code ' => $idc, 'vendor_code' => $idv));      
    }
    
    public function delete_item_vendor($idv, $idc)
	{
        $this->db->where('items_code', $idc)->where('vendor_code', $idv)->delete('items_vendors');
    }
}