<?php defined("BASEPATH") or die("El acceso al script no está permitido");
	
include('barcodeGenerator/src/BarcodeGenerator.php');
include('barcodeGenerator/src/BarcodeGeneratorPNG.php');
include('barcodeGenerator/src/BarcodeGeneratorSVG.php');
include('barcodeGenerator/src/BarcodeGeneratorHTML.php');

class Barcode
{

	public function __construct()
    {
    	$this->CI =& get_instance();
        
    }
 
	public function barcodeHTML($code)
	{
		//echo $this->$generatorHTML->getBarcode($cod, $this->$generatorHTML::$type);
	}
	public function barcodePNG($code)
	{
		$generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
		return base64_encode($generatorPNG->getBarcode($code, $generatorPNG::TYPE_CODE_128));	
    }
	public function barcodeSVG($code)
	{
		//echo $this->$generatorSVG->getBarcode($code, $this->generatorSVG::$type);
	}
}
