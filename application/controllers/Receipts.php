<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Receipts extends MY_Controller {

	function __construct()

	{

		parent::__construct();

        //carga de librerias y helpers
        $this->load->model('receipts_model');
		    $this->load->helper(array('url','language'));
		    $this->load->helper('form');
        $this->load->helper('date');
        date_default_timezone_set("America/Chihuahua");

	}
  public function searchReceipt() 
  {

        if( $this->require_min_level(3) )
        { 

           $this->smartyci->assign('username', $this->auth_username);
           $this->smartyci->assign('role', $this->auth_role);
           $this->smartyci->assign('employees', 'employees');
           $no_ctrl=$this->receipts_model->select_receipts();

          $data=array();

          $i=0;

          foreach ($no_ctrl as $pck) {

              if (isset($no_ctrl)) {

                  $renyCtrl =str_replace('-','',$pck['reny_ctrl']);
                  $data[$i]= $renyCtrl;
                  $date[$i]= date_format(date_create($pck['receipt_date']), 'd/M/Y');
                  $dateS[$i]= date_format(date_create($pck['system_date']), 'd/M/Y');
                  $this->smartyci->assign('date', $date);

                $this->smartyci->assign('dateS', $dateS);

                $i=$i+1;
              }else{

                $date = '';

              }

            }
            $this->smartyci->assign('renyC', $data);

          // Set title page
          $this->smartyci->assign('title', 'ENTRADAS');
          // Set CSS plugins

          $css_plugin = array(
              'datatables/css/dataTables.bootstrap.css',
              'datatables/css/datatables.responsive.css',
              'fuelux/dist/css/fuelux.min.css'
          );

          $this->smartyci->assign('list_css_plugin',$css_plugin);

          // Set JS plugins
          $js_plugin = array(
              'datatables/js/jquery.dataTables.js',
              'datatables/js/dataTables.bootstrap.js',
              'datatables/js/datatables.responsive.js',
              'fuelux/dist/js/fuelux.min.js'
          );

          $this->smartyci->assign('list_js_plugin',$js_plugin);

          // Set JS page
          $js_page = array(

              'reny.receiptTable.js'
          );

          $this->smartyci->assign('list_js_page',$js_page);

          $this->smartyci->assign('list_receipts',$this->receipts_model->select_receipts());

          // Set content page

          $this->smartyci->assign('body', 'contents/transaction/receipts/receipts.html');

          // Set active menu
          $this->smartyci->assign('active_receipt', 'active');

          $this->smartyci->assign('active_searchReceipt', 'active');

          // Render view on main layout
          $this->smartyci->display('contents/layout.html');

        }
  }
	//cargar vista

	public function load_addReceipts($id=NULL)
	{
        if( $this->require_min_level(3) )
        { 

           $this->smartyci->assign('username', $this->auth_username);
           $this->smartyci->assign('role', $this->auth_role);
           $this->smartyci->assign('employees', 'employees');
           $renyCtrl =str_replace('-','',$this->receipts_model->renyCtrl());
           $this->smartyci->assign('renyCtrl', $renyCtrl);

           $this->smartyci->assign('dateSyst', mdate('%d/%M/%Y',time()));

           if (isset($id)) {

               $appoint= $this->receipts_model->select_appointment($id);

                foreach ($appoint as $ap) 
                {
                    $date= $ap->start/1000;
                    $datestring = '%d/%M/%Y';
                    $date2=mdate($datestring, $date);
                    $this->smartyci->assign('dat', $date2);

                     $this->smartyci->assign('appointment', $ap->id);

                     $customer=$ap->customer_code;

                     $vendor=$ap->vendor_code;

                     $po=$ap->po_no;  

                }
                $this->smartyci->assign('customer', $customer);
                $this->smartyci->assign('vend', $vendor);
                $this->smartyci->assign('po', $po);

           }else 
           {

                $appointment= $this->receipts_model->select_appointment();
                $this->smartyci->assign('appointment', $appointment);
                $this->smartyci->assign('dat', mdate('%d/%M/%Y',time())); 
                $this->smartyci->assign('customer', '');
                $this->smartyci->assign('vend', '');
                $this->smartyci->assign('po', '');   
            }

           

            $whseOption=$this->receipts_model->select_whse();
            $this->smartyci->assign('whseOption',  $whseOption);

            $custom=$this->receipts_model->select_customer();
            $this->smartyci->assign('custom',  $custom);

            $vendor=$this->receipts_model->select_vendor();
            $this->smartyci->assign('vendor',  $vendor);

            $items=$this->receipts_model->select_item();
            $this->smartyci->assign('items', $items);

            $producer=$this->receipts_model->select_producer();
            $this->smartyci->assign('producer', $producer);

            $brand=$this->receipts_model->select_brand();
            $this->smartyci->assign('brand', $brand);

            $plant=$this->receipts_model->select_plant();
            $this->smartyci->assign('plant', $plant);

            $clasification=$this->receipts_model->select_clasification();
            $this->smartyci->assign('clasification', $clasification);

            $this->smartyci->assign('title', 'Nueva Entrada');

            $css_plugin = array(
                'dropzone/downloads/css/dropzone.css',
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'chosen_v1.2.0/chosen.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );

            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js',
                'dropzone/downloads/dropzone.min.js',
                'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
            );

            $this->smartyci->assign('list_js_plugin',$js_plugin);
            // Set JS page

            $js_page = array(
                'auto.js',
                'reny.receipt.js'
            );

            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/transaction/receipts/NewReceipt.html');

            // Set active menu
            $this->smartyci->assign('active_receipt', 'active');

            $this->smartyci->assign('active_newReceipt', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');

        }      

	}

  public function buscar_duplicado()
  {

      $this->receipts_model->buscar_duplicado();

  }

	//metodo para el registro de la entrada
	public function reg_receipts()
	{

    if( $this->require_min_level(3)  )
    { 

      if( $this->is_role('Administrador') || $this->is_role('Bodega') || $this->is_role('Oficina'))

      {

        $renyCtrl = $this->input->post("receipt_reny");

        if ($this->receipts_model->create($renyCtrl) == true) {

          redirect('receipts/load_updateRecipt/'.$renyCtrl,'refresh');

        }else
        {
          redirect('receipts/load_updateRecipt/'.$renyCtrl,'refresh');
        }

      }else{ echo "<script> alert('Accion no autorizada');</script>";

           redirect('receipts/load_addReceipts','refresh');}
    }
  
	}

  public function select_vendors()
	{
        $id = $this->input->get('id');
		    $this->receipts_model->select_vendor($id);
	}

  public function select_item()
	{
    $id = $this->input->get('id');

		$this->receipts_model->select_item($id);
	}

  public function select_producer()
  {
    $id = $this->input->get('id');

    $this->receipts_model->select_producer($id);
  }

  public function select_customer()
	{
    $id = $this->input->get('id');

		$this->receipts_model->select_customer($id);
	}

  public function renyCtrl()
  {
      $this->receipts_model->renyCtrl();
  }

  public function load_updateRecipt($renyCtrl)
  {
    if($this->require_min_level(3) )
    { 

        $this->smartyci->assign('username', $this->auth_username);
        $this->smartyci->assign('role', $this->auth_role);
        $this->smartyci->assign('employees', 'employees');
        
        $renyCtrl=str_replace('_','-',$renyCtrl);
        $receipt=$this->receipts_model->select_receipts($renyCtrl);

        
        $this->smartyci->assign('receipt', $receipt);

        $this->smartyci->assign('renyCtrl',  $renyCtrl);

       foreach ($receipt as $rt) 
        {
          if (isset($receipt)) {

            $date= date_format(date_create($rt['receipt_date']), 'd/M/Y');
            $this->smartyci->assign('date', $date);

            }else{

                $date = '';

            }
            if ($rt['ht']=='si') 
            {

              $ht = 'checked';
              $this->smartyci->assign('ht', $ht);

            }else{

              $ht = '';
              $this->smartyci->assign('ht', $ht);
            }
            if ($rt['tags']=='si')
              {
                $tags = 'checked';
                $this->smartyci->assign('tags', $tags);
              }
              else
                {
                  $tags = '';

                  $this->smartyci->assign('tags', $tags);
                } 

               //------------------------------------------------------+
        }

         $this->receipts_model->delete_notifications($renyCtrl);

         $this->smartyci->assign('dateSyst', mdate('%d/%M/%Y',time()));

          // Set title page

          $whseOption=$this->receipts_model->select_whse();
          $this->smartyci->assign('whseOption',  $whseOption);

          $custom=$this->receipts_model->select_customer();
          $this->smartyci->assign('custom',  $custom);

          $vendor=$this->receipts_model->select_vendor();
          $this->smartyci->assign('vendor',  $vendor);

          $items=$this->receipts_model->select_item();
          $this->smartyci->assign('items', $items);

            $producer=$this->receipts_model->select_producer();
            $this->smartyci->assign('producer', $producer);

            $brand=$this->receipts_model->select_brand();
            $this->smartyci->assign('brand', $brand);

            $plant=$this->receipts_model->select_plant();
            $this->smartyci->assign('plant', $plant);

            $clasification=$this->receipts_model->select_clasification();
            $this->smartyci->assign('clasification', $clasification);



          $this->smartyci->assign('title', 'Editar Entrada');

          // Set CSS plugins

          $css_plugin = array(

              'dropzone/downloads/css/dropzone.css',
              'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
              'chosen_v1.2.0/chosen.min.css',
              'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

          );

          $this->smartyci->assign('list_css_plugin',$css_plugin);

          // Set JS plugins

          $js_plugin = array(

              'chosen_v1.2.0/chosen.jquery.min.js',
              'jquery-mockjax/jquery.mockjax.js',
              'jquery-validation/dist/jquery.validate.min.js',
              'dropzone/downloads/dropzone.min.js',
              'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js',

          );

          $this->smartyci->assign('list_js_plugin',$js_plugin);

          // Set JS page

          $js_page = array(

              'auto.js',
              'reny.receipt.js'

          );

          $this->smartyci->assign('list_js_page',$js_page);

          // Set content page

          $this->smartyci->assign('body', 'contents/transaction/receipts/editReceipt.html');

          // Set active menu

          $this->smartyci->assign('active_receipt', 'active');

          // Render view on main layout

          $this->smartyci->display('contents/layout.html');

      }     

  }

  public function up_receipts($renyCtrl)
  { 

    if( $this->require_min_level(3) )

    { 

      if( $this->is_role('Administrador') || $this->is_role('Bodega') || $this->is_role('Oficina'))

      {

        $this->receipts_model->create_lots($renyCtrl);

        redirect('receipts/select_lotsReceipts/'.$renyCtrl,'refresh');


      }else echo "<script> alert('Accion no autorizada');</script>";

           redirect('receipts/load_updateRecipt/'.$renyCtrl,'refresh');

    }

  }

  public function upreceipts($renyCtrl)

  { 

    if( $this->require_min_level(3) )

    { 

      if( $this->is_role('Administrador') || $this->is_role('Bodega') || $this->is_role('Oficina'))

      {

         $this->receipts_model->updateReceipt($renyCtrl);

         redirect('receipts/select_lotsReceipts/'.$renyCtrl,'refresh');



      }else echo "<script> alert('Accion no autorizada');</script>";

           redirect('receipts/select_lotsReceipts/'.$renyCtrl,'refresh');

    }

  }

    public function modifyReceipt($renyCtrl)

  { 

    if( $this->require_min_level(3) )

    { 

        if( $this->is_role('Supervisor'))

        {

          $user = $this->auth_username;

          $this->receipts_model->MoifyRec($renyCtrl,$user);

          redirect('receipts/select_lotsReceipts/'.$renyCtrl,'refresh');

        } else echo "<script> alert('Accion no autorizada');</script>";

         redirect('receipts/select_lotsReceipts/'.$renyCtrl,'refresh');

    }

  }



  public function search_recreny()

  { 

    $renyCtrl = $this->input->post('buscarrenyCtrl');

    if ($this->receipts_model->search_receipt($renyCtrl)) {

       redirect('receipts/select_lotsReceipts/'.$renyCtrl,'refresh');

    }else{

      echo "<script language='JavaScript'>alert('El reny Ctrl ".$renyCtrl." no existe o no se ha marcado como Recibido');location.href='".base_url('receipts/searchReceipt')."';</script>";

      // redirect('receipts/searchReceipt/','refresh');

      

    }



  }



  public function select_lotsReceipts($renyCtrl)

  { 

    if( $this->require_min_level(3) )

    { 

        $this->smartyci->assign('username', $this->auth_username);

        $this->smartyci->assign('role', $this->auth_role);

        $this->smartyci->assign('employees', 'employees');

        $renyCtrl=str_replace('_','-',$renyCtrl);

        $receipt=$this->receipts_model->select_receipts($renyCtrl);

        $this->smartyci->assign('receipt',  $receipt);

        

        $lots=$this->receipts_model->select_lotsReceipts($renyCtrl);

        $this->smartyci->assign('lots',  $lots);

        if (isset($lots)) {



            $i=0;

            foreach ($lots as $lt) 

            {
          
                  $date_exp[$i] = date_format(date_create($lt['exp_date']), 'd/M/Y');

                  

                  $date_prod[$i] = date_format(date_create($lt['prod_date']), 'd/M/Y');

                  $i = $i + 1;

            }

        } 

        if (isset($date_exp)) {

           $this->smartyci->assign('exp_date', $date_exp);

        }

        else

        {

          $this->smartyci->assign('exp_date', '');

        }

        if (isset($date_prod)) {

          $this->smartyci->assign('prod_date', $date_prod);

        }

        else

        {

          $this->smartyci->assign('prod_date','');

        }

        $numlots=$this->receipts_model->num_lotsReceipts($renyCtrl);

        $this->smartyci->assign('numlots', $numlots);  

        $this->smartyci->assign('renyCtrl',  $renyCtrl);

        $images=$this->receipts_model->select_images($renyCtrl);

        $this->smartyci->assign('images', $images);

        $exten = array();

        $c = 0;

        foreach ($images as $ext) {

          if (strrpos($ext['name'] , 'jpg') || strrpos($ext['name'] , 'JPG') || strrpos($ext['name'] , 'png') || strrpos($ext['name'] , 'PNG') || strrpos($ext['name'] , 'jpeg') || strrpos($ext['name'] , 'JPEG')) {

            $exten[$c] = 'image'; 

          }elseif (strrpos($ext['name'] , 'pdf') || strrpos($ext['name'] , 'PDF')) {

            $exten[$c] = 'pdf';

          }elseif (strrpos($ext['name'] , 'doc') || strrpos($ext['name'] , 'DOC') || strrpos($ext['name'] , 'docx') || strrpos($ext['name'] , 'DOCX')) {


            $exten[$c] = 'doc';

          }elseif (strrpos($ext['name'] , 'xls') || strrpos($ext['name'] , 'XLS') || strrpos($ext['name'] , 'xlsx') || strrpos($ext['name'] , 'XLSX')) {


            $exten[$c] = 'xls';

          }

          $c = $c + 1;   

        }

        $this->smartyci->assign('extencion',  $exten);

        $appointment= $this->receipts_model->select_appointment();

        $this->smartyci->assign('appointment',  $appointment);

        $close=base_url('receipts/searchReceipt');

        $sendmail=base_url('receipts/email/'.$renyCtrl);

        $uprec=base_url('receipts/modifyReceipt');

        foreach ($receipt as $rt) 

        {

          if (isset($receipt)) {



                  $date = date_format(date_create($rt['receipt_date']), 'd/M/Y');

                  $this->smartyci->assign('date', $date);

                  $dateR = date_format(date_create($rt['system_date']), 'd/M/Y');

                  $this->smartyci->assign('dateSyst', $dateR);

                  

              }

              else{

                $date = '';

                $dateR = '';

                $this->smartyci->assign('dateSyst', $dateR);

              }

           

            if($rt['status'] == 'Recibido')

            {

                if ($this->auth_role == 'Supervisor') {

                  $up='<input type="submit" formaction="'.$uprec.'/'.$renyCtrl.'" value="Modificar" class="btn btn-sm btn-primary">';

                  

                  $this->smartyci->assign('up', $up);

                }

                $submit='<a href="'.$close.'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 79px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Cerrar</a> <a id ="'.$renyCtrl.'" class="btn btn-sm btn-theme btn-xs mail" style="height:32px;width: 120px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Enviar Correo</a>';

                $this->smartyci->assign('submit', $submit);



                if ($rt['ht']=='si') 

                {

                    $ht = 'checked';

                    $this->smartyci->assign('ht', $ht);

                }

                else

                {

                    $ht = '';

                    $this->smartyci->assign('ht', $ht);

                }

                if ($rt['tags']=='si')

                {

                    $tags = 'checked';

                    $this->smartyci->assign('tags', $tags);

                }

                else

                {

                    $tags = '';

                    $this->smartyci->assign('tags', $tags);

                } 

            }           

            else

            {               

                $submit='<button type="submit" id="SaveBtn" class="btn btn-theme" formaction="'.base_url('receipts/upreceipts').'/'.$renyCtrl.'" value="Actualizar"/>Actualizar</button>&nbsp;&nbsp;<button type="button" id ="'.$renyCtrl.'" class="btn btn-theme check " value="Marcar Recibido">Marcar Recibido</button> ';

                $this->smartyci->assign('submit', $submit);



                $this->smartyci->assign('up', '');



                if ($rt['ht']=='si') 

                {

                    $ht = 'checked';

                    $this->smartyci->assign('ht', $ht);

                }

                else

                {

                    $ht = '';

                    $this->smartyci->assign('ht', $ht);

                }

                if ($rt['tags']=='si')

                {

                    $tags = 'checked';

                    $this->smartyci->assign('tags', $tags);

                }

                else

                {

                    $tags = '';

                    $this->smartyci->assign('tags', $tags);

                } 

           

            }

      } 

         

          $whseOption=$this->receipts_model->select_whse();

          $this->smartyci->assign('whseOption',  $whseOption);



          $custom=$this->receipts_model->select_customer();

          $this->smartyci->assign('custom',  $custom);



          $vendor=$this->receipts_model->select_vendor();

          $this->smartyci->assign('vendor',  $vendor);



          $items=$this->receipts_model->select_item();

          $this->smartyci->assign('items', $items);



          $loc=$this->receipts_model->select_locationArra();

          $this->smartyci->assign('loc',  $loc);  

            $producer=$this->receipts_model->select_producer();
            $this->smartyci->assign('producer', $producer);

            $brand=$this->receipts_model->select_brand();
            $this->smartyci->assign('brand', $brand);

            $plant=$this->receipts_model->select_plant();
            $this->smartyci->assign('plant', $plant);

            $clasification=$this->receipts_model->select_clasification();
            $this->smartyci->assign('clasification', $clasification);



          $this->smartyci->assign('title', 'Editar Entrada');



          // Set CSS plugins

          $css_plugin = array(

              'dropzone/downloads/css/dropzone.css',

              'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',

              'chosen_v1.2.0/chosen.min.css',

              'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'

          );

          $this->smartyci->assign('list_css_plugin',$css_plugin);



          // Set JS plugins

          $js_plugin = array(

              'chosen_v1.2.0/chosen.jquery.min.js',

              'jquery-mockjax/jquery.mockjax.js',

              'jquery-validation/dist/jquery.validate.min.js',

              'dropzone/downloads/dropzone.min.js',

              'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js'

          );

          $this->smartyci->assign('list_js_plugin',$js_plugin);



          // Set JS page

          $js_page = array(

              'auto.js',

              'reny.receipt.js'

          );

          $this->smartyci->assign('list_js_page',$js_page);



          // Set content page

          $this->smartyci->assign('body', 'contents/transaction/receipts/receiptLots.html');



          // Set active menu

          $this->smartyci->assign('active_receipt', 'active');



          // Render view on main layout

          $this->smartyci->display('contents/layout.html');

      // redirect('receipts/load_updateRecipt/'.$renyCtrl,'refresh');

      }

  }

  

   public function ready($renyCtrl)

  { 

    if( $this->require_min_level(3) )

    { 

       $this->receipts_model->ready($renyCtrl);

    }

  }

  public function bags()

  {

      $bagsPall = $this->input->post('bagsPall');

      echo $bagsPall;

  }



  public function lotsReceipt($id){

    $this->receipts_model->select_receipts($id);

  }



  public function images($renyCtrl){

    $renyCtrl=str_replace('_','-',$renyCtrl);

    $this->receipts_model->images($renyCtrl);

  }



  public function selectImg(){

    $this->receipts_model->select_images();

  }

  public function deleteImg($id, $name){



    if (strrpos($name , '_')) {



        $codename = str_replace('_','-',$name);



    }elseif (strrpos($name , '%20')) {

      

        $codename = str_replace('%20',' ',$name);

    }else{

        $codename = $name;

    }



    $this->receipts_model->deleteImg($id, $codename);



  }

  public function select_location()

  {

    $this->receipts_model->select_location();

  }



  public function deleteLots($id)

  {

    $this->receipts_model->deleteLots($id);

  }

  public function deleteReceipt($id)

  {

    $this->receipts_model->deleteReceipt($id);

  }

  public function email($renyCtrl)

  {

    $this->receipts_model->sendMail($renyCtrl);

  }

}



