<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        
        //carga de de librerias y helpers
		$this->load->helper(array('url','language'));
		$this->load->model('vendor_model');
		$this->load->helper('form');
	}
    
	//carga index
	public function search()
	{

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'PROVEEDORES');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.vendor.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            $this->smartyci->assign('list_vendor',$this->vendor_model->select());

             

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/vendors/vendors.html');

            // Set active menu
            $this->smartyci->assign('active_vendor', 'active');
            $this->smartyci->assign('active_vendor_search', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    
    public function load_addvendor()
    {

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
          // Set title page
            $this->smartyci->assign('title', 'Nuevo Proveedor');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.vendor.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/vendors/newVendor.html');

            // Set active menu
            $this->smartyci->assign('active_vendor', 'active');
            $this->smartyci->assign('new_vendor', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
	//funcion registra Provedor
	public function reg_vendor()
	{
        //guarda en array los datos enviados por post del formulario
		$data = array(
			'name' => $this->input->post('name'),
			'vendor_address' => $this->input->post('address'),
            'vendor_phone' => $this->input->post('phone'),
			'vendor_email' => $this->input->post('email')     
		);
            //llamado al metodo register de vendor_model
			$this->vendor_model->register($data);
			redirect('vendors/search','refresh');
	}
   
    public function load_edit($id)
	{
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');

            $vendor = $this->vendor_model->select($id);
            $this->smartyci->assign('vendor',$vendor);
          // Set title page
            $this->smartyci->assign('title', 'Nuevo Proveedor');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.vendor.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/vendors/editVendor.html');

            // Set active menu
            $this->smartyci->assign('active_vendor', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    //Editar proveedor
    public function edit_vendor($id)
	{
     
        //guarda en array los datos enviados del formulario
		$datas = array(
			'name' => $this->input->post('name'),
			'vendor_address' => $this->input->post('address'),
			'vendor_phone' => $this->input->post('phone'),
			'vendor_email' => $this->input->post('email')
		);
        
            //llamada al metodo reg_customer de customer_model para insertar datos
		$this->vendor_model->edit_vendor($id, $datas);		
		redirect('vendors/search','refresh');	
    }
    
     public function delete_vendor($id)
    {
        $this->vendor_model->delete($id); 
    }
 
}