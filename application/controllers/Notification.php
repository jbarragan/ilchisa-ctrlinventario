<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        //carga de librerias y helpers
        $this->load->model('notification_model');
		$this->load->helper(array('url','language'));
		$this->load->helper('form');
        $this->load->helper('date');
	}

	public function notification()
	{
	   $this->notification_model->load_notifications();
	}
	public function update_notification()
	{
	   $this->notification_model->update_notifications();
	}
	public function delete_notification()
	{
	   $this->notification_model->delete_notifications();
	}
}