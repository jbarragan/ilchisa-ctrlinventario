<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url','language'));
		$this->load->model('appointment_model');
        $this->load->helper('form');
        $this->load->helper('date');
	}
    public function calendarAppoint() 
    {
        if( $this->require_group('employees') )
        {  
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'CITAS');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-calendar/css/calendar.min.css',
                'chosen_v1.2.0/chosen.min.css',
                'fuelux/dist/css/fuelux.min.css',
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'underscore/underscore.js',
                'jsTimezoneDetect/jstz.min.js',
                'bootstrap-calendar/js/calendar.js',
                'bootstrap-calendar/js/language/es-ES.js',
                'chosen_v1.2.0/chosen.jquery.min.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.calendAppoint.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/appointment/calendarAppoint.html');

            // Set active menu
            $this->smartyci->assign('active_calendar', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }         
    }
	public function add()
	{
        if( $this->require_group('employees') )
        {   
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');
            $date= time();
            $datestring = '%d/%M/%Y %H:%i';
            $date2=mdate($datestring, $date);
            $this->smartyci->assign('date', $date2);

            $custom=$this->appointment_model->select_customer();
            $this->smartyci->assign('custom', $custom);

            $vendor=$this->appointment_model->select_vendor();
            $this->smartyci->assign('vendor', $vendor);


            // Set title page
            $this->smartyci->assign('title', 'Agendar Cita');

            // Set CSS plugins
            $css_plugin = array(
                'dropzone/downloads/css/dropzone.css',
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'chosen_v1.2.0/chosen.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js',
                'dropzone/downloads/dropzone.min.js',
                'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.appointment.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/appointment/newAppointment.html');

            // Set active menu
            $this->smartyci->assign('active_newAppointment', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }    
	}
    //Seleccionar proveedor
    public function select_vendors()
	{
        $id = $this->input->get('id');
		$this->appointment_model->select_vendor($id);
	}
    //Crear Cita
    public function create()
    {
            $this->appointment_model->create();	
            redirect('appointment/calendarAppoint','refresh');	
    }
    //Muestra las citas en calendario
    public function getAppointment()
	{
			$events = $this->appointment_model->getAll();
			echo json_encode(
                array(
                    "success" => 1,
                    "result" => $events
                )
            );  
	}
    public function getAppointmentList()
    {
        $events = $this->appointment_model->getAppointmentList();

            $i = 0;
            foreach ($events as $ev) {

                $listShp[$i] = array('id'=>$ev->id,'appointment'=>'<a style="margin-left:20%; text-decoration: none;" href="'.$ev->url.$ev->id .'">'.$ev->name.' <span class="label '.$ev->class .'">'.$ev->status.'</span>&nbsp;</a>');

                $i = $i + 1;
            }
        if (isset($listShp)) {
                echo json_encode(array(
                            "data" => $listShp
                        ));
            }else{
                echo json_encode(array(
                            "data" => ''
                        ));
            }
    }

	public function showEdit($id)
    {
        if( $this->require_group('employees') )
        {
            //asigna el valor a la variable username
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('employees', 'employees');
            //asigna el valor a la variable role
            $this->smartyci->assign('role', $this->auth_role);

            //Mostrar datos de cita 
            $appo = $this->appointment_model->select($id);
            foreach( $appo as $a){
                
                $date= $a['start']/1000;//restando segundos a fecha unix
                $datestring = '%d/%M/%Y %H:%i';//convierte fecha unix a formato 
                $date2=mdate($datestring, $date);
                $this->smartyci->assign('id', $id);
                $this->smartyci->assign('date', $date2);
                $this->smartyci->assign('po_no', $a['po_no']);
                $this->smartyci->assign('customer', $a['customer_code']);
                $this->smartyci->assign('vend', $a['vendor_code']);
                $this->smartyci->assign('status', $a['status']);
                $this->smartyci->assign('vendor_email', $a['vendor_email']);
            }
            $this->smartyci->assign('appo', $appo);
            

            $custom=$this->appointment_model->select_customer();
            $this->smartyci->assign('custom', $custom);

            $vendor=$this->appointment_model->select_vendor();
            $this->smartyci->assign('vendor', $vendor);


            // Set title page
            $this->smartyci->assign('title', 'Agendar Cita');

            // Set CSS plugins
            $css_plugin = array(
                'dropzone/downloads/css/dropzone.css',
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'chosen_v1.2.0/chosen.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js',
                'dropzone/downloads/dropzone.min.js',
                'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.appointment.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/appointment/editAppointment.html');

            // Set active menu
            $this->smartyci->assign('active_newAppointment', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html'); 
        }
    }

    public function edit($id)
	{
        //llamada al metodo edit de appointment_model para insertar datos
		$this->appointment_model->edit($id);
        redirect('appointment/calendarAppoint','refresh');
    }
    public function delete_appointment($id)
    {
        $this->appointment_model->delete_appointment($id); 
        redirect('appointment/calendarAppoint','refresh');
    }
    public function cancel_appointment($id)
    {
        $this->appointment_model->cancel_appointment($id); 
        redirect('appointment/calendarAppoint','refresh');
    }
    public function email()
    {
        $this->appointment_model->sendMail(); 
        $this->calendarAppoint();
    }
}