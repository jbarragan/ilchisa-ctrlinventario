<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        //cargamos la libreria html2pdf
        $this->load->library('html2pdf');
        //cargamos el modelo pdf_model
        $this->load->model('reports_model');
        $this->load->model('receipts_model');
        $this->load->helper('date');
    }

    private function createFolder()
    {
        if(!is_dir("./files"))
        {
            mkdir("./files", 0777);
            mkdir("./files/reports", 0777);
        }elseif (is_dir("./files")) {
          if(!is_dir("./files/reports"))
            {
                mkdir("./files/reports", 0777);
            }
        }
    }

    public function getReceipt() 
    {
          $idUser = $this->input->get('iduserCustom');
        if (isset($idUser)){

            $this->reports_model->select_receipts($idUser);

        }else{
            $this->reports_model->select_receipts();

        }
    }
    public function searchReceipt() 
    {
        if( $this->require_min_level(1) )
        {
                if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                    $dateIn = $this->input->get('dateIn');
                    $dateFin = $this->input->get('dateFin');

                    if (isset($dateIn) && isset($dateFin) && $dateIn != null && $dateFin != null) {
               
                        $this->smartyci->assign('datIn', $dateIn);
                        $this->smartyci->assign('datFin', $dateFin);
                    }else{

                        $this->smartyci->assign('datIn', date('01/M/Y'));
                        $this->smartyci->assign('datFin',date('d/M/Y'));
                    }

                    $this->smartyci->assign('employees', 'employees');
                    $this->smartyci->assign('role', $this->auth_role);

                    $this->smartyci->assign('status', $this->input->get('status'));
                    $this->smartyci->assign('customer', $this->input->get('customer'));
                    $this->smartyci->assign('item', $this->input->get('item'));

                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                    $idUser = $this->auth_user_id;
                     $this->smartyci->assign('custoUser', $idUser);
                   // $ctrlVic=$this->reports_model->select_receipts($idUser);

                }
                $this->smartyci->assign('username', $this->auth_username);
                $this->smartyci->assign('role', $this->auth_role);

                    $custom=$this->reports_model->select_customer();
                    $this->smartyci->assign('custom',  $custom);

                    $vendor=$this->reports_model->select_vendor();
                    $this->smartyci->assign('vendor',  $vendor);

                    $items=$this->reports_model->select_item();
                    $this->smartyci->assign('items', $items);

                    //$this->smartyci->assign('list_receipts', $ctrlVic);

                    $status = $this->input->get('status');
                    $customer = $this->input->get('customer');
                    $item = $this->input->get('item');

                    if (isset($ctrlVic)) {
                        $i=0;
                        foreach ($ctrlVic as $pck) {
                           
                                    $date[$i]= date_format(date_create($pck['receipt_date']), 'd/M/Y');
                                    $dateS[$i]= date_format(date_create($pck['system_date']), 'd/M/Y');
                                    $i=$i + 1;
                            }
                            
                    }
                    if (isset($date)) {
                       $this->smartyci->assign('date', $date);
                       $this->smartyci->assign('dateS', $dateS);
                    }else{
                         $this->smartyci->assign('date', '');
                         $this->smartyci->assign('dateS', '');
                    }
                    // Set title page
                    $this->smartyci->assign('title', 'ENTRADAS');

                    // Set CSS plugins
                    $css_plugin = array(
                        'datatables/css/dataTables.bootstrap.css',
                        'datatables/css/datatables.responsive.css',
                        'fuelux/dist/css/fuelux.min.css',
                        'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
                    );
                    $this->smartyci->assign('list_css_plugin',$css_plugin);

                    // Set JS plugins
                    $js_plugin = array(
                        'datatables/js/jquery.dataTables.js',
                        'datatables/js/dataTables.bootstrap.js',
                        'datatables/js/datatables.responsive.js',
                        'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                        'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
                    );
                    $this->smartyci->assign('list_js_plugin',$js_plugin);

                    // Set JS page
                    $js_page = array(
                        //'auto.js',
                        'reny.report.js'
                    );
                    $this->smartyci->assign('list_js_page',$js_page);

                    // Set content page
                    $this->smartyci->assign('body', 'contents/reports/reportReceipt.html');

                    // Set active menu
                    $this->smartyci->assign('active_report', 'active');
                    $this->smartyci->assign('active_reportreceipt', 'active');

                    // Render view on main layout
                    $this->smartyci->display('contents/layout.html'); 
            }     
    }
    public function selectReceipt()
    {
        if( $this->require_min_level(1) )
        {
            if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                     $this->smartyci->assign('employees', 'employees');
                     $receipt=$this->reports_model->select_receiptsPdf();

                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                    $idUser = $this->auth_user_id;

                    $receipt=$this->reports_model->select_receiptsPdf($idUser);

                }
                $this->smartyci->assign('username', $this->auth_username);
                $this->smartyci->assign('role', $this->auth_role);
            //establecemos la carpeta en la que queremos guardar los pdfs,
            //si no existen las creamos y damos permisos
            $this->createFolder();

            //importante el slash del final o no funcionará correctamente
            $this->html2pdf->folder('./files/reports/');

            //establecemos el nombre del archivo
            $filename='entradas.pdf';
            $this->html2pdf->filename($filename);

            //establecemos el tipo de papel
            $this->html2pdf->paper('a4', 'landscape');

            //datos que queremos enviar a la vista, lo mismo de siempre



            $data=array();

            $i=0;
            foreach ($receipt as $pck) {
                if (isset($receipt)) {

                    $renyCtrl =str_replace('-','',$pck['reny_ctrl']);
                    $data[$i]= $renyCtrl;
                    $date[$i]= date_format(date_create($pck['receipt_date']), 'd/M/Y');
                    $dateS[$i]= date_format(date_create($pck['system_date']), 'd/M/Y');
                    $i=$i+1;
                }
            }
            
            if (isset($date)) {
               $data['date'] = $date;
               $data['dateS'] = $dateS;
            }else{
                 $data['date'] = '';
                 $data['dateS'] = '';
            }

            $data['title'] = 'Reporte de Entradas';
            $data['vic'] = $data;

            $data['list'] = $receipt;
            //$this->load->view('templates/contents/transaction/receipts/receipts', $data);
           
            //hacemos que coja la vista como datos a imprimir
            //importante utf8_decode para mostrar bien las tildes, ñ y demás
            $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/reports/receipts', $data, true)));

            //si el pdf se guarda correctamente lo mostramos en pantalla
            if($this->html2pdf->create('save'))
            {
                $this->show($filename);
            }
        }
    }
    public function getshipment() 
    {
        $idUser = $this->input->get('iduserCustom');
        if (isset($idUser)){

            $this->reports_model->shipmentJson($idUser);

        }else{
            $this->reports_model->shipmentJson();
        }
    }
    public function searchShipRep() 
    {
         if( $this->require_min_level(1) )
        {
            if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                     $this->smartyci->assign('employees', 'employees');
                      //$shipList=$this->reports_model->shipmentJson();

                    $this->smartyci->assign('status', $this->input->get('status'));
                    $this->smartyci->assign('customer', $this->input->get('customer'));
                    $this->smartyci->assign('item', $this->input->get('item'));

                    $dateIn = $this->input->get('dateIn');
                    $dateFin = $this->input->get('dateFin');

                    if (isset($dateIn) && isset($dateFin) && $dateIn != null && $dateFin != null) {
               
                        $this->smartyci->assign('datIn', $dateIn);
                        $this->smartyci->assign('datFin', $dateFin);
                    }else{

                        $this->smartyci->assign('datIn', '');
                        $this->smartyci->assign('datFin','');
                    }


                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                     $idUser = $this->auth_user_id;
                     $this->smartyci->assign('custoUser', $idUser);
                    //$shipList=$this->reports_model->shipmentJson($idUser);

                }
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $custom=$this->reports_model->select_customer();
            $this->smartyci->assign('custom',  $custom);

            $items=$this->reports_model->select_item();
            $this->smartyci->assign('items', $items);

           // $this->smartyci->assign('shipList',$shipList);
         
            
             if (isset($shipList)) {
                $i=0;
                foreach ($shipList as $pck) {

                            $date= $pck['start']/1000;
                            $datestring = '%m/%d/%Y';
                            $date2[$i]=mdate($datestring, $date); 
                            $dateS[$i]= date_format(date_create($pck['system_date']), 'd/M/Y');
                            $i=$i + 1;
                    }
            }
            if (isset($date2)) {
               $this->smartyci->assign('date', $date2);
               $this->smartyci->assign('dateS', $dateS);
            }else{
                 $this->smartyci->assign('date', '');
                 $this->smartyci->assign('dateS', '');
            }
           
            // Set title page
            $this->smartyci->assign('title', 'Reporte Salidas');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.report.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/reports/shipGenReport.html');

            // Set active menu
            $this->smartyci->assign('active_report', 'active');
            $this->smartyci->assign('active_reporShip', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }

    public function ReporShip()
    {
        if( $this->require_min_level(1) )
        {
             if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                     $this->smartyci->assign('employees', 'employees');
                       $shipm=$this->reports_model->shipmentJson();
                       $d = 'employees';

                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                     $idUser = $this->auth_user_id;
                   $shipm=$this->reports_model->shipmentJson($idUser);
                   $d = 'customer';

                }  
        $this->smartyci->assign('username', $this->auth_username);
        $this->smartyci->assign('role', $this->auth_role);
            //establecemos la carpeta en la que queremos guardar los pdfs,
            //si no existen las creamos y damos permisos
            $this->createFolder();

            //importante el slash del final o no funcionará correctamente
            $this->html2pdf->folder('./files/reports/');

            //establecemos el nombre del archivo
            $filename='Salidas.pdf';
            $this->html2pdf->filename($filename);

            //establecemos el tipo de papel
            $this->html2pdf->paper('a4', 'landscape');

            //datos que queremos enviar a la vista, lo mismo de siempre


            $data=array();

            $i=0;
            foreach ($shipm as $pck) {
                if (isset($shipm)) {

                        $date= $pck['start']/1000;
                        $datestring = '%m/%d/%Y';
                        $date2[$i]=mdate($datestring, $date);
                        $dateS[$i]= date_format(date_create($pck['system_date']), 'd/M/Y'); 
                }
                $i=$i + 1;
            }
            $data['title'] = 'Reporte de Salidas';
            
            if (isset($date2)) {
               $data['date'] = $date2;
               $data['dateS'] = $dateS;
            }else{
                 $data['date'] = '';
                 $data['dateS'] = '';
            }

            $data['employees'] = $d; 

            $data['list'] = $shipm;
            //$this->load->view('templates/contents/transaction/receipts/receipts', $data);
           
            //hacemos que coja la vista como datos a imprimir
            //importante utf8_decode para mostrar bien las tildes, ñ y demás
            $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/reports/shipmentReport', $data, true)));

            //si el pdf se guarda correctamente lo mostramos en pantalla
            if($this->html2pdf->create('save'))
            {
                $this->show($filename);
            }
        }
    }

    public function getInvent() 
    {

        $idUser = $this->input->get('iduserCustom');
        if (isset($idUser)){

            $this->reports_model->inventoryReport($idUser);

        }else{
            $this->reports_model->inventoryReport();
        }
    }
    public function searchinvent() 
    {
        if( $this->require_min_level(1) )
        {
             if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                    $this->smartyci->assign('employees', 'employees');
                    //$invList=$this->reports_model->inventoryReport();

                    $this->smartyci->assign('customer', $this->input->get('customer'));
                    $this->smartyci->assign('item', $this->input->get('item'));
                    $this->smartyci->assign('whse_sel', $this->input->get('whse'));

                    $dateIn = $this->input->get('dateIn');
                    $dateFin = $this->input->get('dateFin');
               
                        $this->smartyci->assign('datIn', $dateIn);
                        $this->smartyci->assign('datFin', $dateFin);


                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                    $idUser = $this->auth_user_id;
                     $this->smartyci->assign('custoUser', $idUser);
                    //$invList=$this->reports_model->inventoryReport($idUser);

                }
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $custom=$this->reports_model->select_customer();
            $this->smartyci->assign('custom',  $custom);

            $items=$this->reports_model->select_item();
            $this->smartyci->assign('items', $items);

            $whse=$this->reports_model->select_whse();
            $this->smartyci->assign('whse', $whse);

                    $brand=$this->reports_model->select_brand();
                    $this->smartyci->assign('brand', $brand);

                    $plant=$this->reports_model->select_plant();
                    $this->smartyci->assign('plant', $plant);


                    $clasification=$this->reports_model->select_clasification();
                    $this->smartyci->assign('clasification', $clasification);


            //$this->smartyci->assign('invList',$invList);

            if (isset($invList)) {
                $i=0;
                foreach ($invList as $pck) {

                            $date_exp[$i] = date_format(date_create($pck['exp_date']), 'd/M/Y');
                           
                            $date_prod[$i] = date_format(date_create($pck['prod_date']), 'd/M/Y');
                          
                   
                            $dateS[$i]= date_format(date_create($pck['receipt_date']), 'd/M/Y');
                            $i=$i + 1;
                    }
                    
            }
            if (isset($dateS)) {
                $this->smartyci->assign('dateS', $dateS);
                $this->smartyci->assign('exp_date', $date_exp);
                $this->smartyci->assign('prod_date', $date_prod);
            }else{
                 $this->smartyci->assign('dateS', '');
                 $this->smartyci->assign('exp_date','');
                 $this->smartyci->assign('prod_date', '');
             }

            // Set title page
            $this->smartyci->assign('title', 'Reporte Salidas');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                        'datatables/js/jquery.dataTables.js',
                        'datatables/js/dataTables.bootstrap.js',
                        'datatables/js/datatables.responsive.js',
                        'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                        'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.report.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/reports/inventRepor.html');

            // Set active menu
            $this->smartyci->assign('active_reporInvent', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }     
    }

     public function reporinvent()
    {
        if( $this->require_min_level(1) )
        {
            if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                     $this->smartyci->assign('employees', 'employees');
                      $invList=$this->reports_model->inventoryReport();
                  $data['employees'] = 'employees';

                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                    $idUser = $this->auth_user_id;
                    $invList=$this->reports_model->inventoryReport($idUser);
                    $data['employees'] = 'customer';

                }

                if (isset($invList)) {
                $i=0;
                foreach ($invList as $pck) {

                            $date_exp[$i] = date_format(date_create($pck['exp_date']), 'd/M/Y');
                           
                            $date_prod[$i] = date_format(date_create($pck['prod_date']), 'd/M/Y');
                   
                            $dateS[$i]= date_format(date_create($pck['receipt_date']), 'd/M/Y');
                            $i=$i + 1;
                    }
                    
                }
                if (isset($dateS)) {
                  $data['dateS'] = $dateS;
                }else{
                    $data['dateS'] = '';
                }

                if (isset($date_exp)) {
                  $data['exp_date'] = $date_exp;
                }else{
                    $data['exp_date'] = '';
                }

                 if (isset($date_prod)) {
                  $data['prod_date'] = $date_prod;
                }else{
                    $data['prod_date'] = '';
                }
                //establecemos la carpeta en la que queremos guardar los pdfs,
                //si no existen las creamos y damos permisos
                $this->createFolder();

                //importante el slash del final o no funcionará correctamente
                $this->html2pdf->folder('./files/reports/');

                //establecemos el nombre del archivo
                $filename='Inventario.pdf';
                $this->html2pdf->filename($filename);

                //establecemos el tipo de papel
                $this->html2pdf->paper('a4', 'landscape');

                //datos que queremos enviar a la vista, lo mismo de siempre
                $data['title'] = 'Reporte de Inventario';

                $data['list'] = $invList;
                //$this->load->view('templates/contents/transaction/receipts/receipts', $data);
               
                //hacemos que coja la vista como datos a imprimir
                //importante utf8_decode para mostrar bien las tildes, ñ y demás
                $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/reports/repGeneralInventario', $data, true)));

                //si el pdf se guarda correctamente lo mostramos en pantalla
                if($this->html2pdf->create('save'))
                {
                    $this->show($filename);
                }
        }
    }

    //funcion que ejecuta la descarga del pdf
    public function downloadPdf($filename)
    {
        //si existe el directorio
        if(is_dir("./files/reports"))
        {
            //ruta completa al archivo
            $route = base_url("files/reports/".$filename);
            //nombre del archivo
            
            //$filename = "test.pdf";
            //si existe el archivo empezamos la descarga del pdf
            if(file_exists("./files/reports/".$filename))
            {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header('Content-disposition: attachment; filename='.basename($route));
                header("Content-Type: application/pdf");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length: '. filesize($route));
                readfile($route);
            }
        }
    }


    //esta función muestra el pdf en el navegador siempre que existan
    //tanto la carpeta como el archivo pdf
    public function show($filename)
    {
        if(is_dir("./files/reports"))
        {
            $route = base_url("files/reports/".$filename);
            if(file_exists("./files/reports/".$filename))
            {
                header('Content-type: application/pdf');
                readfile($route);
            }
        }
    }

    //función para crear y enviar el pdf por email
    //ejemplo de la libreria sin modificar
    public function mail_pdf()
    {

        //establecemos la carpeta en la que queremos guardar los pdfs,
        //si no existen las creamos y damos permisos
        $this->createFolder();

        //importante el slash del final o no funcionará correctamente
        $this->html2pdf->folder('./files/reports/');

        //establecemos el nombre del archivo
        $this->html2pdf->filename('test.pdf');

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');

        //datos que queremos enviar a la vista, lo mismo de siempre
        $data = array(
            'title' => 'Listado de las provincias españolas en pdf',
            'provincias' => $this->pdf_model->getProvincias()
        );

        //hacemos que coja la vista como datos a imprimir
        //importante utf8_decode para mostrar bien las tildes, ñ y demás
        $this->html2pdf->html(utf8_decode($this->load->view('pdf', $data, true)));


        //Check that the PDF was created before we send it
        if($path = $this->html2pdf->create('save'))
        {

            $this->load->library('email');

            $this->email->from('your@example.com', 'Your Name');
            $this->email->to('israel965@yahoo.es');

            $this->email->subject('Email PDF Test');
            $this->email->message('Testing the email a freshly created PDF');

            $this->email->attach($path);

            $this->email->send();

            echo "El email ha sido enviado correctamente";

        }

    }

    public function exporExcel(){
       $this->reports_model->exporExcel();
    }

    public function report_receiptCustomer($renyCtrl)
    { 
        $DetailReceipt= $this->receipts_model->select_receipts($renyCtrl);
        $images = $this->receipts_model->select_images($renyCtrl);
        $data=array();
        $i=0;
        foreach ($DetailReceipt as $pck) {
            if (isset($DetailReceipt)) {
                $renyCtrl =str_replace('-','',$pck['reny_ctrl']);
                $data[$i]= $renyCtrl;
                $i=$i+1;
            }
            

            $date= $pck['receipt_date'];

            $data['reny_ctrl']=str_replace('-','',$pck['reny_ctrl']);

            $data['date']=$date;

            $data['dateSyst']=$pck['system_date'];

            $data['status']=$pck['status'];
            $data['po_no']=$pck['po_no'];

            $data['cust']=$pck['first_name'];

            $data['item_name']=$pck['item_name'];

            $data['vendor']=$pck['name'];

            $data['whse']=$pck['whse_name'];
            $data['producer']=$pck['name_producer'];
            $data['brand']=$pck['name_brand'];
            $data['plant']=$pck['name_plant'];
            $data['clasification']=$pck['name_clasification'];
        }
        $data['list'] = $this->receipts_model->select_lotsReceipts($renyCtrl);
        $data['title']='Entrada de Mercancia';
        $data['images'] = $images;


      //$this->load->view('templates/contents/transaction/shipments/impPacking', $data);

       $this->createFolder();

        //importante el slash del final o no funcionará correctamente

        $this->html2pdf->folder('./files/reports/');

        //establecemos el nombre del archivo

        $filename = 'Entrada'.$renyCtrl.'.pdf';

        $this->html2pdf->filename($filename);



        //establecemos el tipo de papel

        $this->html2pdf->paper('a4', 'portrait');

       

      //load the view and saved it into $html variabl

 

       $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/reports/receipt_reportCustomer', $data,true)));



        //si el pdf se guarda correctamente lo mostramos en pantalla

        if($this->html2pdf->create('save'))

        {

            $this->show($filename);

        }
      
    }
    public function report_lotsReceipts($renyCtrl)

    { 
        if( $this->require_min_level(1) )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');
            $renyCtrl=str_replace('_','-',$renyCtrl);
            $receipt=$this->receipts_model->select_receipts($renyCtrl);
            $this->smartyci->assign('receipt',  $receipt);

            $lots=$this->receipts_model->select_lotsReceipts($renyCtrl);
            $this->smartyci->assign('lots',  $lots);
            
            if (isset($lots)) {
                $i=0;
                foreach ($lots as $lt) 
                {
                      $date_exp[$i] = date_format(date_create($lt['exp_date']), 'd/M/Y');
                      $date_prod[$i] = date_format(date_create($lt['prod_date']), 'd/M/Y');
                      $i = $i + 1;
                }
            } 
            if (isset($date_exp)) {
               $this->smartyci->assign('exp_date', $date_exp);
            }
            else
            {
              $this->smartyci->assign('exp_date', '');
            }
            if (isset($date_prod)) {     
              $this->smartyci->assign('prod_date', $date_prod);
            }
            else
            {
              $this->smartyci->assign('prod_date','');
            }

            $numlots=$this->receipts_model->num_lotsReceipts($renyCtrl);

            $this->smartyci->assign('numlots', $numlots);  

            $this->smartyci->assign('renyCtrl',  $renyCtrl);

            $images=$this->receipts_model->select_images($renyCtrl);

            $this->smartyci->assign('images', $images);

            $exten = array();

            $c = 0;

            foreach ($images as $ext) {

              if (strrpos($ext['name'] , 'jpg') || strrpos($ext['name'] , 'JPG') || strrpos($ext['name'] , 'png') || strrpos($ext['name'] , 'PNG') || strrpos($ext['name'] , 'jpeg') || strrpos($ext['name'] , 'JPEG')) {

                $exten[$c] = 'image'; 

              }elseif (strrpos($ext['name'] , 'pdf') || strrpos($ext['name'] , 'PDF')) {

                $exten[$c] = 'pdf';

              }elseif (strrpos($ext['name'] , 'doc') || strrpos($ext['name'] , 'DOC') || strrpos($ext['name'] , 'docx') || strrpos($ext['name'] , 'DOCX')) {

                $exten[$c] = 'doc';

              }elseif (strrpos($ext['name'] , 'xls') || strrpos($ext['name'] , 'XLS') || strrpos($ext['name'] , 'xlsx') || strrpos($ext['name'] , 'XLSX')) {

                $exten[$c] = 'xls';

              }
              $c = $c + 1;          
        }

        $this->smartyci->assign('extencion',  $exten);
        $close=base_url('reports/searchReceipt');
        $exportatPDF=base_url('reports/report_receiptCustomer/'.$renyCtrl);
        foreach ($receipt as $rt) 
        {

          if (isset($receipt)) {
                  $date = date_format(date_create($rt['receipt_date']), 'd/M/Y');
                  $this->smartyci->assign('date', $date);
                  $dateR = date_format(date_create($rt['system_date']), 'd/M/Y');
                  $this->smartyci->assign('dateSyst', $dateR);
              }
              else{
                $date = '';
                $dateR = '';
                $this->smartyci->assign('dateSyst', $dateR);

              }

            if($rt['status'] == 'Recibido')
            {
                $submit='<a href="'.$close.'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 79px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Cerrar</a> 
                    <a href="'.$exportatPDF.'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 120px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;" target="_blank">Exportar PDF</a>';
                $this->smartyci->assign('submit', $submit);

                if ($rt['ht']=='si') 
                {
                    $ht = 'checked';
                    $this->smartyci->assign('ht', $ht);
                }
                else
                {
                    $ht = '';
                    $this->smartyci->assign('ht', $ht);
                }
                if ($rt['tags']=='si')
                {
                    $tags = 'checked';
                    $this->smartyci->assign('tags', $tags);
                }
                else
                {
                    $tags = '';
                    $this->smartyci->assign('tags', $tags);
                } 
            }           
            else
            {               
                $this->smartyci->assign('up', '');
                if ($rt['ht']=='si') 
                {
                    $ht = 'checked';
                    $this->smartyci->assign('ht', $ht);
                }
                else
                {
                    $ht = '';
                    $this->smartyci->assign('ht', $ht);
                }
                if ($rt['tags']=='si')
                {
                    $tags = 'checked';
                    $this->smartyci->assign('tags', $tags);
                }
                else
                {
                    $tags = '';
                    $this->smartyci->assign('tags', $tags);

                } 
            }
        } 

          $whseOption=$this->receipts_model->select_whse();
          $this->smartyci->assign('whseOption',  $whseOption);

          $custom=$this->receipts_model->select_customer();
          $this->smartyci->assign('custom',  $custom);

          $vendor=$this->receipts_model->select_vendor();
          $this->smartyci->assign('vendor',  $vendor);

          $items=$this->receipts_model->select_item();
          $this->smartyci->assign('items', $items);

          $loc=$this->receipts_model->select_locationArra();
          $this->smartyci->assign('loc',  $loc);  
          $this->smartyci->assign('title', 'Editar Entrada');

          // Set CSS plugins
          $css_plugin = array(
              'dropzone/downloads/css/dropzone.css',
              'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
              'chosen_v1.2.0/chosen.min.css',
              'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
          );
          $this->smartyci->assign('list_css_plugin',$css_plugin);

          // Set JS plugins
          $js_plugin = array(
              'chosen_v1.2.0/chosen.jquery.min.js',
              'jquery-mockjax/jquery.mockjax.js',
              'jquery-validation/dist/jquery.validate.min.js',
              'dropzone/downloads/dropzone.min.js',
              'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js'
          );
          $this->smartyci->assign('list_js_plugin',$js_plugin);

          // Set JS page
          $js_page = array(
              'auto.js',
              'reny.receipt.js'
          );
          $this->smartyci->assign('list_js_page',$js_page);

          // Set content page
          $this->smartyci->assign('body', 'contents/reports/receiptCustomer.html');

          // Set active menu
          $this->smartyci->assign('active_receipt', 'active');
          // Render view on main layout
          $this->smartyci->display('contents/layout.html');
      // redirect('receipts/load_updateRecipt/'.$renyCtrl,'refresh');
      }
    }
}
/* End of file pdf_ci.php */
/* Location: ./application/controllers/pdf_ci.php */