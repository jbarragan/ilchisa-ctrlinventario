<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        //carga de librerias y helpers
        $this->load->model('customer_model');
		$this->load->helper(array('url','language'));
		$this->load->helper('form');
	}

	//cargar vista

	public function search()
  {
    if( $this->require_group('employees') )
    {   
        $this->smartyci->assign('username', $this->auth_username);
        $this->smartyci->assign('role', $this->auth_role);
        $this->smartyci->assign('employees', 'employees');
        // Set title page
        $this->smartyci->assign('title', 'CLIENTES');

        // Set CSS plugins
        $css_plugin = array(
            'datatables/css/dataTables.bootstrap.css',
            'datatables/css/datatables.responsive.css',
            'fuelux/dist/css/fuelux.min.css'
        );
        $this->smartyci->assign('list_css_plugin',$css_plugin);

        // Set JS plugins
        $js_plugin = array(
            'datatables/js/jquery.dataTables.js',
            'datatables/js/dataTables.bootstrap.js',
            'datatables/js/datatables.responsive.js',
            'fuelux/dist/js/fuelux.min.js'
        );
        $this->smartyci->assign('list_js_plugin',$js_plugin);

        // Set JS page
        $js_page = array(
            'reny.customer.js'
        );
        $this->smartyci->assign('list_js_page',$js_page);

        $this->smartyci->assign('list_customers',$this->customer_model->select_cust());
        $this->smartyci->assign('list_Ship',$this->customer_model->select_ship());

         

        // Set content page
        $this->smartyci->assign('body', 'contents/catalogs/customers/customers.html');

        // Set active menu
        $this->smartyci->assign('active_customers', 'active');
        $this->smartyci->assign('active_customers_search', 'active');

        // Render view on main layout
        $this->smartyci->display('contents/layout.html');
    }
  }

	//metodo para el registro de cliente
	public function reg_customer()
	{  
        //guarda en array los datos enviados del formulario
    		$datas = array(
    			'first_name' => $this->input->post('name'),
    			'email' => $this->input->post('email'),
    			'phone' => $this->input->post('phone'),
    			'address' => $this->input->post('address')
    		);        
                //llamada al metodo reg_customer de customer_model para insertar datos
    			$this->customer_model->reg_customer($datas);		
    			redirect('customer/search','refresh');	
	}
    //Cargar vista agregar cliente.
    public function load_addcust()
    {
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
        // Set title page
            $this->smartyci->assign('title', 'Registrar Cliente');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.customer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/customers/newCustomer.html');

            // Set active menu
            $this->smartyci->assign('active_customers', 'active');
            $this->smartyci->assign('new_customers', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
    //carga vista editar cliente
    public function load_edit($id)
	{
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);

            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');

            $cust = $this->customer_model->select_cust($id);
            $this->smartyci->assign('cust',$cust);
        // Set title page
            $this->smartyci->assign('title', 'Editar Cliente');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.customer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/customers/editCustomer.html');

            // Set active menu
            $this->smartyci->assign('active_customers', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    
    //Editar cliente
    public function edit_customer($id)
	{
        //accede a los datos del cliente para mostrarlos
        $data['custom_list'] = $this->customer_model->select_cust($id);
     
        //guarda en array los datos enviados del formulario
		$datas = array(
			'first_name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address')
		);
         //llamada al metodo reg_customer de customer_model para insertar datos
			$this->customer_model->edit_customer($id, $datas);		
			redirect('customer/search','refresh');	
    }
    
    //metodo para el registro de ship
	public function reg_ship($id)
	{
        $this->customer_model->reg_ship($id);
        redirect('customer/search/');
         
	}
    //listar ship por cliente
    public function load_ship($id)
    {
        if( $this->require_group('employees') )
        {    
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'CLIENTES');
            $this->smartyci->assign('id', $id);

            // Set CSS plugins
            $css_plugin = array(
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.customer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            $this->smartyci->assign('list_customers',$this->customer_model->select_cust());

             

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/customers/shipTo.html');

            // Set active menu
            $this->smartyci->assign('active_customers', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    public function loadedit_ship($id)
    {
        if( $this->require_group('employees') )
        {    
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'CLIENTES');
            $this->smartyci->assign('id', $id);

            // Set CSS plugins
            $css_plugin = array(
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.customer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            //accedientdo a datos del ship
            $list_ship=$this->customer_model->select_ship($id);

            foreach ($list_ship as $ship) {

                 $name=$ship['ship_name'];
                 list($city,$state,$zip) = explode(',', $ship['ship_state']);
                 $address=$ship['ship_address'];
                 $phone=$ship['ship_phone'];
             } 
             $this->smartyci->assign('name',$name);
             $this->smartyci->assign('state',$state);
             $this->smartyci->assign('city',$city);
             $this->smartyci->assign('zip',$zip);
             $this->smartyci->assign('address',$address);
             $this->smartyci->assign('phone',$phone);             
             $this->smartyci->assign('id',$id);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/customers/editshipTo.html');

            // Set active menu
            $this->smartyci->assign('active_customers', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    public function edit_ship($id)
    {
        $this->customer_model->edit_ship($id);
        redirect('customer/search/');
         
    }
    public function delete_customer($id)
    {
        $this->customer_model->delete_customer($id); 
        //redirect('customer/search/');
    }
    
    public function delete_ship($ids)
    {
        $this->customer_model->delete_ship($ids); 
        redirect('customer/search/');
    }

    public function customer_vendor($idv, $idc)
    {
        $this->customer_model->customer_vendor($idv, $idc); 
        redirect('customer/select_vendor/'.$idc,'refresh');
    }
       public function delete_vendor($idv,$idc)
    {
        $this->customer_model->delete_vendor($idv,$idc); 
        redirect('customer/select_vendor/'.$idc,'refresh');
    }
    //Vista Crear Usuario a Cliente
    public function customerUser($idCustomer)
    {
        if( $this->require_group('employees') )
        { 
          
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            
            $customer_data = $this->customer_model->select_cust($idCustomer);

            $this->smartyci->assign('customer_data', $customer_data);

            $this->smartyci->assign('employees', 'employees');
        // Set title page
            $this->smartyci->assign('title', 'Registrar Cliente');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.customer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            $this->smartyci->assign('Val_user', set_value('username'));
            $this->smartyci->assign('Val_email', set_value('email'));
            $this->smartyci->assign('Val_rol', set_select('auth_level'));


            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/customers/userCustomer.html');

            // Set active menu
            $this->smartyci->assign('active_customers', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    //Crear Usuario a cliente
    public function sign_up($idCust)
    {
         $user_data = array(
            'username'   => $this->input->post('username'),
            'passwd'     => $this->input->post('passwd'),
            'email'      => $this->input->post('email'),
            'auth_level' => $this->input->post('auth_level'), // 9 if you want to login @ examples/index.
        );
        
        $this->is_logged_in();

        // Load resources
        $this->load->model('examples_model');
        $this->load->model('validation_callables');
        $this->load->library('form_validation');

        $this->form_validation->set_data( $user_data );
        $this->form_validation->set_error_delimiters('<label id="username-error" class="error" for="username" style="display: inline-block;">', '</label>');

        $validation_rules = array(
            array(
                'field' => 'username',
                'label' => 'username',
                'rules' => 'max_length[12]|is_unique[' . config_item('user_table') . '.username]',
                'errors' => array(
                    'is_unique' => 'Username ya esta en uso.'
                )
            ),
            array(
                'field' => 'passwd',
                'label' => 'passwd',
                'rules' => array(
                            'trim',
                            'required',
                            'max_length[12]',
                            'min_length[8]'
                        ),
                'errors' => array(
                    'required' => 'Este campo es requerido.',
                    'min_length' =>'La conteseña debe contar con 8 minimo caracteres',
                    'max_length' =>'La conteseña debe contar con 12 maximo caracteres'
                )
            ),
            array(
                'field' => 'passwd2',
                'label' => 'passwd2',
                'rules' => 'trim|matches[passwd]',
                'errors' => array(
                    'matches' => 'Las contraseñas no coinciden'
                )
            ),
            array(
                'field'  => 'email',
                'label'  => 'email',
                'rules'  => 'trim|required|valid_email|is_unique[' . config_item('user_table') . '.email]',
                'errors' => array(
                    'is_unique' => 'Email address ya existe.',
                    'required' => 'Este Campo es requerido'
                )
            )
        );
        //validacion de reglas
        $this->form_validation->set_rules( $validation_rules );

        if( $this->form_validation->run() )
        {
            $user_data['passwd']     = $this->authentication->hash_passwd($user_data['passwd']);
            $user_data['user_id']    = $this->examples_model->get_unused_id();
            $user_data['created_at'] = date('Y-m-d H:i:s');

            // If username is not used, it must be entered into the record as NULL
            if( empty( $user_data['username'] ) )
            {
                $user_data['username'] = NULL;
            }

            $this->db->set($user_data)
                ->insert(config_item('user_table'));

            if( $this->db->affected_rows() == 1 ){
             $this->customer_model->createUserCust($user_data['username'], $idCust);
             redirect('customer/search','refresh');}
        }
        else
        {
            $this->smartyci->assign('user',form_error('username'));
            $this->smartyci->assign('passwd',form_error('passwd'));
            $this->smartyci->assign('passwd2',form_error('passwd2'));
            $this->smartyci->assign('email',form_error('email'));
            
            $this->smartyci->assign('error_val', 'Error de'.validation_errors());
            //echo '<h1>User Creation Error(s)</h1>' . ;
             $this->customerUser($idCust);
        }

       //$this->load->view('templates/contents/sign/sign_up.php');
    }
}

