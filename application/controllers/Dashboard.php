<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        // Force SSL
        //$this->force_ssl();

        // Form and URL helpers always loaded (just for convenience)
        $this->load->helper('url');
        $this->load->model('reports_model');
        $this->load->model('receipts_model');
    }
    public function index()
    {
        if( $this->require_min_level(1) )
        {
             if( $this->is_role('Administrador') || $this->is_role('Oficina') || $this->is_role('Bodega') || $this->is_role('Supervisor'))
                {
                    $this->smartyci->assign('employees', 'employees');
                    //$invList=$this->reports_model->inventoryReport();

                    $this->smartyci->assign('customer', $this->input->get('customer'));
                    $this->smartyci->assign('item', $this->input->get('item'));
                    $this->smartyci->assign('whse_sel', $this->input->get('whse'));

                    $dateIn = $this->input->get('dateIn');
                    $dateFin = $this->input->get('dateFin');
               
                        $this->smartyci->assign('datIn', $dateIn);
                        $this->smartyci->assign('datFin', $dateFin);


                }elseif ( $this->is_role('Cliente')) {

                   $this->smartyci->assign('employees', 'customer');
                    $idUser = $this->auth_user_id;
                     $this->smartyci->assign('custoUser', $idUser);
                    //$invList=$this->reports_model->inventoryReport($idUser);

                }
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $custom=$this->reports_model->select_customer();
            $this->smartyci->assign('custom',  $custom);

            $items=$this->reports_model->select_item();
            $this->smartyci->assign('items', $items);

            $whse=$this->reports_model->select_whse();
            $this->smartyci->assign('whse', $whse);
            
                    $brand=$this->reports_model->select_brand();
                    $this->smartyci->assign('brand', $brand);

                    $plant=$this->reports_model->select_plant();
                    $this->smartyci->assign('plant', $plant);


                    $clasification=$this->reports_model->select_clasification();
                    $this->smartyci->assign('clasification', $clasification);

            //$this->smartyci->assign('invList',$invList);

            if (isset($invList)) {
                $i=0;
                foreach ($invList as $pck) {

                            $date_exp[$i] = date_format(date_create($pck['exp_date']), 'm/d/Y');
                           
                            $date_prod[$i] = date_format(date_create($pck['prod_date']), 'm/d/Y');
                          
                   
                            $dateS[$i]= date_format(date_create($pck['receipt_date']), 'm/d/Y');
                            $i=$i + 1;
                    }
                    
            }
            if (isset($dateS)) {
                $this->smartyci->assign('dateS', $dateS);
                $this->smartyci->assign('exp_date', $date_exp);
                $this->smartyci->assign('prod_date', $date_prod);
            }else{
                 $this->smartyci->assign('dateS', '');
                 $this->smartyci->assign('exp_date','');
                 $this->smartyci->assign('prod_date', '');
             }

            // Set title page
            $this->smartyci->assign('title', 'Reporte Salidas');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                        'datatables/js/jquery.dataTables.js',
                        'datatables/js/dataTables.bootstrap.js',
                        'datatables/js/datatables.responsive.js',
                        'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                        'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.report.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/reports/inventRepor.html');

            // Set active menu
            $this->smartyci->assign('active_reporInvent', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
}
