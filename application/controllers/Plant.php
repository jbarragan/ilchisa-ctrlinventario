<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Plant extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url'));
		$this->load->model('plant_model');
	}
	
	//cargar vista 
	public function search()
	{

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'PLANTA');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.plant.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);


            $this->smartyci->assign('list_plant',$this->plant_model->select());

             

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/plant/plant.html');

            // Set active menu
            $this->smartyci->assign('active_plant', 'active');
            $this->smartyci->assign('active_plant_search', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}

    function select()
    {
        $this->plant_model->select();
    }
    
	//metodo para el registro
	public function add_plant()
	{
     
    //guarda en array los datos enviados del formulario
		$datas = array(
			'name_plant' => $this->input->post('name_plant')
		);
    //llamada al metodo reg_customer de customer_model para insertar datos
    $this->plant_model->insert($datas);	

			redirect('plant/search','refresh');		
	}
    //Cargar vista agregar
    public function load_addplant()
    {

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');
          //cargar lista proveedores
          // Set title page
            $this->smartyci->assign('title', 'Nuevo Planta');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.plant.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/plant/newplant.html');

            // Set active menu
            $this->smartyci->assign('active_plant', 'active');
            $this->smartyci->assign('new_plant', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
    //carga vista editar 
    public function load_edit($id)
	{
       if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');


            $editplant = $this->plant_model->select($id);
            $this->smartyci->assign('editplant', $editplant);
            print_r( $editplant);
      
          // Set title page
            $this->smartyci->assign('title', 'Editar Planta');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.plant.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/plant/editplant.html');

            // Set active menu
            $this->smartyci->assign('active_plant', 'active');
            $this->smartyci->assign('new_plant', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    
    //Editar
    public function edit_plant($id)
	{

        //llamada al metodo reg_customer de customer_model para insertar datos
		$this->plant_model->edit($id);		
		redirect('plant/search','refresh');		
    }
    
    public function delete_plant($id)
    {
        $this->plant_model->delete($id); 
    }

}