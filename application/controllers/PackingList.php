<?php defined('BASEPATH') OR exit('No direct script access allowed');



class PackingList extends MY_Controller

{

	function __construct()

    {

        parent::__construct();

        $this->load->model('packingList_model');

		$this->load->helper(array('url','language'));

		$this->load->helper('form');

        date_default_timezone_set("America/Chihuahua");

        $this->load->helper('date');

        $this->load->library('html2pdf');

    }



    public function load_packingList()

    {



        if( $this->require_group('employees') )

        { 

            $this->smartyci->assign('username', $this->auth_username);

            $this->smartyci->assign('role', $this->auth_role);



            $this->smartyci->assign('employees', 'employees');

            $packing=$this->packingList_model->selectPacking();



            $data=array();

            $i=0;

            foreach ($packing as $pck => $val) {

                if (isset($packing)) {

                    $date= $val->start/1000;

                    $datestring = '%d/%M/%Y';



                    $noShip =str_replace('-','',$val->no_shipment);



                    $data[$i]=array('no_shipment'=>$noShip,

                                    'no_ship'=>$val->no_shipment,

                                    'statusShip'=>$val->statusShip,

                                    'system_date'=>$val->system_dateShip,

                                    'start'=>mdate($datestring, $date),

                                    'carrier'=>$val->carrier,

                                    'trailer'=>$val->trailer,

                                    'seal'=>$val->seal);

                $i=$i+1;

                }



            }

            $this->smartyci->assign('list_packing',$data);

        	$this->smartyci->assign('title', 'Packing List');



            // Set CSS plugins

            $css_plugin = array(

                'datatables/css/dataTables.bootstrap.css',

                'datatables/css/datatables.responsive.css',

                'fuelux/dist/css/fuelux.min.css'

            );

            $this->smartyci->assign('list_css_plugin',$css_plugin);



            // Set JS plugins

            $js_plugin = array(

                'datatables/js/jquery.dataTables.js',

                'datatables/js/dataTables.bootstrap.js',

                'datatables/js/datatables.responsive.js',

                'fuelux/dist/js/fuelux.min.js'

            );

            $this->smartyci->assign('list_js_plugin',$js_plugin);



            // Set JS page

            $js_page = array(

                'reny.ship.js'

            );

            $this->smartyci->assign('list_js_page',$js_page);



            //$this->smartyci->assign('list_packing',$this->packingList_model->selectPacking());





            // Set content page

            $this->smartyci->assign('body', 'contents/transaction/shipments/packingList.html');



            // Set active menu

            $this->smartyci->assign('active_shipments', 'active');

            $this->smartyci->assign('active_packing', 'active');



            // Render view on main layout

            $this->smartyci->display('contents/layout.html');

        }  

    }



    public function select_Packing($noShipment)

	{  

        if( $this->require_group('employees') )

        {  

            $this->smartyci->assign('username', $this->auth_username);

            $this->smartyci->assign('role', $this->auth_role);

            

            $this->smartyci->assign('employees', 'employees');  

            $images = $this->packingList_model->select_images($noShipment);

         $packingList = $this->packingList_model->selectPacking($noShipment);

         $this->smartyci->assign('packingList', $packingList);

         $this->smartyci->assign('noShipment', $noShipment);

         $data=array();

            $i=0;

          foreach ($packingList as $rt) 

          {

                $date= $rt['start']/1000;

                $datestring = '%d/%M/%Y';

                $date2=mdate($datestring, $date); 


                $id=$rt['id_out'];

                $no=$rt['no_shipment'];

                $dateSyst = $rt['system_dateShip'];

                $carrier=$rt['carrier'];

                $trailer=$rt['trailer'];

                $seal=$rt['seal'];

                $status=$rt['statusShip'];

                $cust=$rt['first_name'];

                $item_name=$rt['item_name'];

                $whse=$rt['whse_code'];

                $email=$rt['email'];

                $whse=$rt['whse_name'];

                

                $renyCtrl =str_replace('-','',$rt['reny_ctrl']);

                $data[$i]= $renyCtrl;

                $i=$i+1;

          }

            $this->smartyci->assign('date', $date2);

            $this->smartyci->assign('dateSyst', $dateSyst);

            $this->smartyci->assign('id_out', $id);

            $this->smartyci->assign('no', $no);

            $this->smartyci->assign('carrier', $carrier);

            $this->smartyci->assign('trailer', $trailer);

            $this->smartyci->assign('seal', $seal);

            $this->smartyci->assign('status', $status);

            $this->smartyci->assign('cust', $cust);

            $this->smartyci->assign('item_name', $item_name);

            $this->smartyci->assign('whse', $whse);

            $this->smartyci->assign('vic_ctr', $data);

            $this->smartyci->assign('email', $email);

            $this->smartyci->assign('whse', $whse);
            
            $this->smartyci->assign('images', $images);

            // Set title page



            $shipTo = $this->packingList_model->showShip($noShipment);

                $this->smartyci->assign('shipTo', $shipTo);



            $this->smartyci->assign('ship_list', $this->packingList_model->selectShip($noShipment));



            $this->smartyci->assign('title', 'Editar Entrada');



            // Set CSS plugins

            $css_plugin = array(

                'dropzone/downloads/css/dropzone.css',

                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',

                'chosen_v1.2.0/chosen.min.css',

                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'

            );

            $this->smartyci->assign('list_css_plugin',$css_plugin);



            // Set JS plugins

            $js_plugin = array(

               'chosen_v1.2.0/chosen.jquery.min.js',

                'jquery-mockjax/jquery.mockjax.js',

                'jquery-validation/dist/jquery.validate.min.js',

                'dropzone/downloads/dropzone.min.js',

                'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'

            );

            $this->smartyci->assign('list_js_plugin',$js_plugin);



            // Set JS page

            $js_page = array(

                'lotsShip.js',

                'reny.ship.js',

                'reny.packing.js'

            );

            $this->smartyci->assign('list_js_page',$js_page);



            // Set content page

            $this->smartyci->assign('body', 'contents/transaction/shipments/viewPacking.html');



            // Set active menu

            $this->smartyci->assign('active_shipments', 'active');



            // Render view on main layout

            $this->smartyci->display('contents/layout.html'); 

        }  

	}



    public function impPacking($noShipment)

    {



        $packingList= $this->packingList_model->selectPacking($noShipment);



        $images = $this->packingList_model->select_images($noShipment);



        $data=array();



        $i=0;

        foreach ($packingList as $pck) {

            if (isset($packingList)) {



                $renyCtrl =str_replace('-','',$pck['reny_ctrl']);

                $data[$i]= $renyCtrl;

                $i=$i+1;

            }

        }

           $data['vic'] = $data;



        $data['list'] = $packingList;



        $data['title']='Salida de Producto';





        $data['images'] = $images;



        foreach ($packingList as $rt) 

      {

            if ($rt['ht']=='si') 

            {

                 $data['ht'] = 'checked';

            }

            else

            {

                 $data['ht']  = '';

            }

            if ($rt['tags']=='si')

            {

                 $data['tags'] = 'checked';

            }

            else

            {

                 $data['tags'] = '';

             }

            $date= $rt['start']/1000;

            $datestring = '%d/%M/%Y %H:%i';

            $date2=mdate($datestring, $date); 





            $data['no']=str_replace('-','',$rt['no_shipment']);

            $data['date']=$date2;

            $data['dateSyst']=$rt['system_dateShip'];

            $data['carrier']=$rt['carrier'];

            $data['trailer']=$rt['trailer'];

            $data['seal']=$rt['seal'];

            $data['status']=$rt['statusShip'];

            $data['cust']=$rt['first_name'];

            $data['item_name']=$rt['item_name'];

            $data['whse']=$rt['whse_name'];

      }

      //$this->load->view('templates/contents/transaction/shipments/impPacking', $data);



       $this->createFolder();



        //importante el slash del final o no funcionará correctamente

        $this->html2pdf->folder('./files/packing/');



        //establecemos el nombre del archivo

        $filename = 'Packing'.$data['no'].'.pdf';

        $this->html2pdf->filename($filename);



        //establecemos el tipo de papel

        $this->html2pdf->paper('a4', 'portrait');

       

      //load the view and saved it into $html variabl

 

       $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/transaction/shipments/impPacking', $data,true)));



        //si el pdf se guarda correctamente lo mostramos en pantalla

        if($this->html2pdf->create('save'))

        {

            $this->show($filename);

        }

    }

    public function billOfLading($noShipment)

    {

        $packingList= $this->packingList_model->selectPacking($noShipment);





        $data=array();



        $i=0;

        foreach ($packingList as $pck) {

            if (isset($packingList)) {



                $renyCtrl =str_replace('-','',$pck['reny_ctrl']);

                $data[$i]= $renyCtrl;

                $i=$i+1;

            }

        }

           $data['vic'] = $data;



        $data['list'] = $packingList;



        $data['title']='Bill Of Lading';





        foreach ($packingList as $rt) 

      {

            if ($rt['ht']=='si') 

            {

                 $data['ht'] = 'checked';

            }

            else

            {

                 $data['ht']  = '';

            }

            if ($rt['tags']=='si')

            {

                 $data['tags'] = 'checked';

            }

            else

            {

                 $data['tags'] = '';

             }

            $date= $rt['start']/1000;

            $datestring = '%d/%M/%Y';

            $date2=mdate($datestring, $date); 





            $data['no']=str_replace('-','',$rt['no_shipment']);

            $data['date']=$date2;

            $data['dateSyst']=$rt['system_dateShip'];

            $data['carrier']=$rt['carrier'];

            $data['trailer']=$rt['trailer'];

            $data['seal']=$rt['seal'];

            $data['status']=$rt['statusShip'];

            $data['cust']=$rt['first_name'];

            $data['item_name']=$rt['item_name'];

            $data['whse']=$rt['whse_code'];

      }



       $this->createFolder();



        //importante el slash del final o no funcionará correctamente

        $this->html2pdf->folder('./files/packing/');



        //establecemos el nombre del archivo

        $filename = 'billOfLading'.$data['no'].'.pdf';

        $this->html2pdf->filename($filename);



        //establecemos el tipo de papel

        $this->html2pdf->paper('a4', 'portrait');

       

      //load the view and saved it into $html variabl

 

       $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/transaction/shipments/billOfLading', $data, true)));



        //si el pdf se guarda correctamente lo mostramos en pantalla

        if($this->html2pdf->create('save'))

        {

            $this->show($filename);

        }

    }

     private function createFolder()

    {

        if(!is_dir("./files"))

        {

            mkdir("./files", 0777);

            mkdir("./files/packing", 0777);

        }

    }

     public function show($filename)

    {

        if(is_dir("./files/packing"))

        {

            $route = base_url("files/packing/".$filename);

            if(file_exists("./files/packing/".$filename))

            {

                header('Content-type: application/pdf');

                readfile($route);

            }

        }

    }

    public function images($noShipment){

        $this->PackingList_model->images($noShipment);

    }

}