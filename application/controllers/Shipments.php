<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shipments extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('shipments_model');
		$this->load->helper(array('url','language'));
		$this->load->helper('form');
        $this->load->helper('date');
        date_default_timezone_set("America/Chihuahua");
        $this->lang->load('calendar_lang');
        $this->load->helper('date');
        $this->load->library('html2pdf');
        $this->load->library('barcode');
    }

    public function getshipment(){
        $events = $this->shipments_model->getShipments();
            echo json_encode(
                array(
                    "success" => 1,
                    "result" => $events
                )
            );
    }
    public function getshipmentList(){
        $events = $this->shipments_model->getShipmentsList();

            $i = 0;
            foreach ($events as $ev) {

                $listShp[$i] = array('no_shipment'=>'<a style="margin-left:20%; text-decoration: none;" href="'.$ev->url.$ev->no_shipment .'">'.$ev->no_shipment.' <span class="label '.$ev->class .'">'.$ev->statusShip.'</span>&nbsp;</a>');

                $i = $i + 1;
            }

            if (isset($listShp)) {
                echo json_encode(array(
                            "data" => $listShp
                        ));
            }else{
                echo json_encode(array(
                            "data" => ''
                        ));
            }

    }

   public function searchShip($id=NULL) 
    {
        
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'SALIDAS');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-calendar/css/calendar.min.css',
                'chosen_v1.2.0/chosen.min.css',
                'fuelux/dist/css/fuelux.min.css',
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'underscore/underscore.js',
                'jsTimezoneDetect/jstz.min.js',
                'bootstrap-calendar/js/calendar.js',
                'bootstrap-calendar/js/language/es-ES.js',
                'chosen_v1.2.0/chosen.jquery.min.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);
            // Set JS page
            $js_page = array(
                'reny.calendar.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/transaction/shipments/shipments.html');

            // Set active menu
            $this->smartyci->assign('active_shipments', 'active');
            $this->smartyci->assign('active_searchShip', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    public function load_addShipments($id=NULL)
	{      
    //-----------------------------------------------------------------

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            $this->smartyci->assign('noShipm', $this->shipments_model->noShipment());

            $this->smartyci->assign('imgShip',$this->shipments_model->noShipment());


            $this->smartyci->assign('dateSyst', mdate('%d/%M/%Y',time()));

            $this->smartyci->assign('dateRec', mdate('%d/%M/%Y',time()));

            $whseOption=$this->shipments_model->select_whse();
            $this->smartyci->assign('whseOption',  $whseOption);

            $custom=$this->shipments_model->select_cust();
            $this->smartyci->assign('custom', $custom);

            // Set title page
            $this->smartyci->assign('title', 'Nueva Salida');

            // Set CSS plugins
            $css_plugin = array(
                'dropzone/downloads/css/dropzone.css',
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'chosen_v1.2.0/chosen.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js',
                'dropzone/downloads/dropzone.min.js',
                'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
                //'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.ship.js',
                'lotsShip.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/transaction/shipments/newShipments.html');

            // Set active menu
            $this->smartyci->assign('active_shipments', 'active');
            $this->smartyci->assign('active_newShipment', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }      
	}
    
    public function select_cust()
	{
        $id = $this->input->get('id');
		$this->shipments_model->select_cust();
	}
    
    public function select_itemReceipt()
	{
        $id = $this->input->get('id');
		$this->shipments_model->select_itemReceipt($id);
	}
    public function select_shipto()
    {
        $id = $this->input->get('id');
        $this->shipments_model->selectShip($id);
    }
    public function select_renyReceipt()
	{
        $idc = $this->input->get('idc');
        $idi = $this->input->get('idi');
		$this->shipments_model->select_renyReceipt($idc,$idi);
	}
    public function select_renyLots()
	{
        $id = $this->input->get('id');
		$this->shipments_model->select_renyLots($id);
	}
    
    public function select_lots()
	{
		$this->shipments_model->select_lots();
	}
    
    public function reg_shipments()
	{
         if( $this->require_min_level(3) )
        { 
        //llamada al metodo regreceipts de receipts_model para insertar datos
            if ($this->shipments_model->createShipment() == true) {
                redirect('shipments/searchShip','refresh');
            }else
            {
                redirect('shipments/load_addShipments/','refresh');
                echo "<script> alert('registro existente');</script>";
            }
        }
	}

     public function showEdit($noShipment)
    {

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');

            $shipment=$this->shipments_model->select_shipments($noShipment);

            $this->smartyci->assign('shipment', $shipment);

            $this->smartyci->assign('noShipm', $noShipment);

            $images=$this->shipments_model->select_images($noShipment);
            $this->smartyci->assign('images', $images);

            $exten = array();

            $c = 0;

            foreach ($images as $ext) {

              if (strrpos($ext['name'] , 'jpg') || strrpos($ext['name'] , 'JPG') || strrpos($ext['name'] , 'png') || strrpos($ext['name'] , 'PNG') || strrpos($ext['name'] , 'jpeg') || strrpos($ext['name'] , 'JPEG')) {
              
                $exten[$c] = 'image'; 
              
              }elseif (strrpos($ext['name'] , 'pdf') || strrpos($ext['name'] , 'PDF')) {

                $exten[$c] = 'pdf';

              }elseif (strrpos($ext['name'] , 'doc') || strrpos($ext['name'] , 'DOC') || strrpos($ext['name'] , 'docx') || strrpos($ext['name'] , 'DOCX')) {

                $exten[$c] = 'doc';

              }elseif (strrpos($ext['name'] , 'xls') || strrpos($ext['name'] , 'XLS') || strrpos($ext['name'] , 'xlsx') || strrpos($ext['name'] , 'XLSX')) {

                $exten[$c] = 'xls';

              }
              $c = $c + 1;
              
            }
                 $this->smartyci->assign('extencion',  $exten);
        
                 $data=array();
                 $i=0;
                foreach ($shipment as $rt) 
                {
                    if ( array_key_exists('reny_ctrl', $rt)) {
                       
                        if ($rt['ht']=='si') 
                        {
                            $ht = 'checked';
                            $this->smartyci->assign('ht', $ht);
                        }
                        else
                        {
                            $ht = '';
                            $this->smartyci->assign('ht', $ht);
                        }
                        if ($rt['tags']=='si')
                        {
                            $tags = 'checked';
                            $this->smartyci->assign('tags', $tags);
                        }
                        else
                        {
                            $tags = '';
                            $this->smartyci->assign('tags', $tags);
                         }

                    $data[$i]= $rt['reny_ctrl'];
                    }
                    $date= $rt['start']/1000;
                    $datestring = '%d/%M/%Y';
                    $date2=mdate($datestring, $date);

                     $date_system=$rt['system_dateShip'];

                    $no=$rt['no_shipment'];
                    $carrier=$rt['carrier'];
                    $trailer=$rt['trailer'];
                    $seal=$rt['seal'];
                    $status=$rt['statusShip'];
                    $cust_code=$rt['customer_code'];
                    $first_name=$rt['first_name'];
                    $item_code=$rt['items_code'];
                    $item_name=$rt['item_name'];
                    $whse=$rt['whse_code'];
                    $id_out=$rt['id_out'];
                    $notes=$rt['notesShip'];
              
                    $i=$i+1;
                }
        }


                $this->smartyci->assign('date', $date2);
                $this->smartyci->assign('dateSyst', $date_system);
                $this->smartyci->assign('no', $no);
                $this->smartyci->assign('id_out', $id_out);
                $this->smartyci->assign('carrier', $carrier);
                $this->smartyci->assign('trailer', $trailer);
                $this->smartyci->assign('seal', $seal);
                $this->smartyci->assign('status', $status);
                $this->smartyci->assign('cust_code', $cust_code);
                $this->smartyci->assign('first_name', $first_name);
                $this->smartyci->assign('item_code', $item_code);
                $this->smartyci->assign('item_name', $item_name);
                $this->smartyci->assign('whse', $whse);
                $this->smartyci->assign('reny_ctr', $data);
                $this->smartyci->assign('notes', $notes);

                $shipTo = $this->shipments_model->showShip($noShipment);
                $this->smartyci->assign('shipTo', $shipTo);
                // Set title page
                $this->shipments_model->delete_notifications($noShipment);
                $whseOption=$this->shipments_model->select_whse();
                $this->smartyci->assign('whseOption',  $whseOption);

                $custom=$this->shipments_model->select_cust();
                $this->smartyci->assign('custom', $custom);

                $ship_to=$this->shipments_model->selectShipEdit($noShipment);
                $this->smartyci->assign('ship_to', $ship_to);


                $this->smartyci->assign('title', 'Editar Entrada');

                // Set CSS plugins
                $css_plugin = array(
                    'dropzone/downloads/css/dropzone.css',
                    'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                    'chosen_v1.2.0/chosen.min.css',
                    'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
                );
                $this->smartyci->assign('list_css_plugin',$css_plugin);

                // Set JS plugins
                $js_plugin = array(
                   'chosen_v1.2.0/chosen.jquery.min.js',
                    'jquery-mockjax/jquery.mockjax.js',
                    'jquery-validation/dist/jquery.validate.min.js',
                    'dropzone/downloads/dropzone.min.js',
                    'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                    'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
                );
                $this->smartyci->assign('list_js_plugin',$js_plugin);

                // Set JS page
                $js_page = array(
                    'lotsShip.js',
                    'reny.ship.js'
                );
                $this->smartyci->assign('list_js_page',$js_page);

                // Set content page
                $this->smartyci->assign('body', 'contents/transaction/shipments/editShipments.html');

                // Set active menu
                $this->smartyci->assign('active_shipments', 'active');

                // Render view on main layout
                $this->smartyci->display('contents/layout.html');
    }

    public function upShipment($noShipment){
        $this->shipments_model->upShipment($noShipment);
        redirect('shipments/showEdit/'.$noShipment,'refresh');
    }
    public function upShipmentnuevo($noShipment){
        $this->shipments_model->upShipmentnuevo($noShipment);
        redirect('shipments/showEdit/'.$noShipment,'refresh');
    }

    public function images($noShipment){
        $this->shipments_model->images($noShipment);
    }
     public function deleteImg($id, $name){
        if (strrpos($name , '_')) {

            $codename = str_replace('_','-',$name);

        }elseif (strrpos($name , '%20')) {
          
            $codename = str_replace('%20',' ',$name);
        }else{
            $codename = $name;
        }
        $this->shipments_model->deleteImg($id, $codename);
    }

    public function deleteLots($id)
    {
      $this->shipments_model->deleteLots($id);
    }
    public function select_lotsShip()
    {
        $id = $this->input->get('id');
        $this->shipments_model->select_lotsShip($id);
    }

    public function is_ship($noShipment)
    {
        $this->shipments_model->is_ship($noShipment);

        //$this->billOfLading($id);

        //redirect('packingList/load_packingList ','refresh');
    }
    public function notification()
    {
        $this->receipts_model->load_notifications();
    }
    public function billOfLading($id)
    {
        $packingList= $this->shipments_model->billOfLading($id);
        $data=array();

        $i=0;
        foreach ($packingList as $pck) {
            if (isset($packingList)) {

                $renyCtrl =str_replace('-','',$pck['reny_ctrl']);
                $data[$i]= $renyCtrl;
                $i=$i+1;
            }
        }
           $data['reny'] = $data;

        $data['list'] = $packingList;

        $data['title']='Bill Of Lading';


        foreach ($packingList as $rt) 
      {
            if ($rt['ht']=='si') 
            {
                 $data['ht'] = 'checked';
            }
            else
            {
                 $data['ht']  = '';
            }
            if ($rt['tags']=='si')
            {
                 $data['tags'] = 'checked';
            }
            else
            {
                 $data['tags'] = '';
             }
            $date= $rt['start']/1000;
            $datestring = '%d/%M/%Y';
            $date2=mdate($datestring, $date); 

            $data['no']=$rt['no_shipment'];
            $data['no']=$rt['no_shipment'];
            $data['date']=$date2;
            $data['dateSyst']=$rt['system_date'];
            $data['carrier']=$rt['carrier'];
            $data['trailer']=$rt['trailer'];
            $data['seal']=$rt['seal'];
            $data['status']=$rt['statusShip'];
            $data['cust']=$rt['first_name'];
            $data['item_name']=$rt['item_name'];
            $data['ship_name']=$rt['ship_name'];
            $data['ship_address']=$rt['ship_address'];
            $data['ship_state']=$rt['ship_state'];
            $data['ship_phone']=$rt['ship_phone'];
            $data['whse_address']=$rt['address'];
            $data['whse_name']=$rt['whse_name'];
            $data['billNum']=$rt['bill_number'];
            $data['po_no']=$rt['po_no'];
            $data['barcode']=$this->barcode->barcodePNG($rt['bill_number']);
      }

       //$this->load->view('templates/contents/transaction/shipments/billOfLading', $data);

       $this->createFolder();

        //importante el slash del final o no funcionará correctamente
        $this->html2pdf->folder('./files/billLading/');

        //establecemos el nombre del archivo
        $filename = 'billOfLading'.$data['no'].'.pdf';
        $this->html2pdf->filename($filename);

        //establecemos el tipo de papel
        $this->html2pdf->paper('a4', 'portrait');
       
      //load the view and saved it into $html variabl
 
       $this->html2pdf->html(utf8_decode($this->load->view('templates/contents/transaction/shipments/billOfLading', $data, true)));


        //si el pdf se guarda correctamente lo mostramos en pantalla
        if($this->html2pdf->create('save'))
        {
            $this->show($filename);
        }
    }

    private function createFolder()
    {
        if(!is_dir("./files"))
        {
            mkdir("./files", 0777);
            mkdir("./files/billLading", 0777);
        }
    }
     public function show($filename)
    {
        if(is_dir("./files/billLading"))
        {
            $route = base_url("files/billLading/".$filename);
            if(file_exists("./files/billLading/".$filename))
            {
                header('Content-type: application/pdf');
                readfile($route);
            }
        }
    }
      //funcion que ejecuta la descarga del pdf
    public function downloadPdf($filename)
    {
        //si existe el directorio
        if(is_dir("./files/billLading"))
        {
            //ruta completa al archivo
            $route = base_url("files/billLading/".$filename);
            //nombre del archivo
            
            //$filename = "test.pdf";
            //si existe el archivo empezamos la descarga del pdf
            if(file_exists("./files/billLading/".$filename))
            {
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header('Content-disposition: inline; filename='.basename($route));
                header("Content-Type: application/pdf");
                header("Content-Transfer-Encoding: binary");
                header('Content-Length: '. filesize($route));
                readfile($route);
            }
        }
    }

    public function load_adjustment(){
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');


            $this->smartyci->assign('dateSyst', mdate('%d/%M/%Y',time()));

            $this->smartyci->assign('dateRec', mdate('%d/%M/%Y',time()));

            $whseOption=$this->shipments_model->select_whse();
            $this->smartyci->assign('whseOption',  $whseOption);

            $custom=$this->shipments_model->select_cust();
            $this->smartyci->assign('custom', $custom);

            // Set title page
            $this->smartyci->assign('title', 'Nueva Salida');

            // Set CSS plugins
            $css_plugin = array(
                'dropzone/downloads/css/dropzone.css',
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'chosen_v1.2.0/chosen.min.css',
                'smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js',
                'dropzone/downloads/dropzone.min.js',
                'bootstrap-datepicker-vitalets/js/bootstrap-datepicker.js" charset="UTF-8',
                'bootstrap-datepicker-vitalets/js/locales/bootstrap-datepicker.es.js'
                //'smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.ship.js',
                'lotsShip.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/transaction/ajusteInv.html');

            // Set active menu
            $this->smartyci->assign('active_shipments', 'active');
            $this->smartyci->assign('active_adjust', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }      
    }
    public function adjustment()
    {
        $noShipment =$this->shipments_model->adjustment();
        redirect('shipments/searchShip','refresh');
    }
     public function email($noShipment)
    {
      $this->shipments_model->sendMail($noShipment); 
    }

    public function deleteshipment($id)
    {
        $this->shipments_model->deleteshipment($id);
    }
      public function buscar_duplicado()
  {

      $this->shipments_model->buscar_duplicado();

  }
}