<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Producer extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url'));
		$this->load->model('producer_model');
	}
	
	//cargar vista 
	public function search()
	{

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'PRODUCTORES');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.producer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);


            $this->smartyci->assign('list_producer',$this->producer_model->select());

             

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/producer/producer.html');

            // Set active menu
            $this->smartyci->assign('active_producer', 'active');
            $this->smartyci->assign('active_producer_search', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}

    function select()
    {
        $this->producer_model->select();
    }
    
	//metodo para el registro
	public function add_producer()
	{
     
    //guarda en array los datos enviados del formulario
		$datas = array(
			'name_producer' => $this->input->post('name_producer')
		);
    //llamada al metodo reg_customer de customer_model para insertar datos
    $this->producer_model->insert($datas);	

			redirect('producer/search','refresh');		
	}
    //Cargar vista agregar.
    public function load_addproducer()
    {

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');
          //cargar lista proveedores
          // Set title page
            $this->smartyci->assign('title', 'Nuevo Productor');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.producer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/producer/newproducer.html');

            // Set active menu
            $this->smartyci->assign('active_producer', 'active');
            $this->smartyci->assign('new_producer', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
    //carga vista editar 
    public function load_edit($id)
	{
       if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');


            $editProducer = $this->producer_model->select($id);
            $this->smartyci->assign('editproducer', $editProducer);
            print_r( $editProducer);
      
          // Set title page
            $this->smartyci->assign('title', 'Editar Productor');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.producer.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/producer/editProducer.html');

            // Set active menu
            $this->smartyci->assign('active_producer', 'active');
            $this->smartyci->assign('new_producer', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    
    //Editar
    public function edit_producer($id)
	{

        //llamada al metodo reg_customer de customer_model para insertar datos
		$this->producer_model->edit($id);		
		redirect('producer/search','refresh');		
    }
    
    public function delete_producer($id)
    {
        $this->producer_model->delete($id); 
    }

}