<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url'));
		$this->load->model('item_model');
	}
	
	//cargar vista 
	public function search()
	{

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'PRODUCTOS');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.item.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            $this->smartyci->assign('list_items',$this->item_model->select());

             

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/items/items.html');

            // Set active menu
            $this->smartyci->assign('active_item', 'active');
            $this->smartyci->assign('active_item_search', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}

    function select()
    {
        $this->item_model->select();
    }
    
	//metodo para el registro del Producto
	public function add_item()
	{
     
    //guarda en array los datos enviados del formulario
		$datas = array(
			'item_name' => $this->input->post('item_name'),
			'desc' => $this->input->post('item_desc'),
		);
    //llamada al metodo reg_customer de customer_model para insertar datos
    $this->item_model->insert($datas);	

    /*$idc=$this->item_model->id_item();
    $idv=$this->input->post('item_vendor');
    $this->item_model->insert_item_vendor($idv,$idc);*/

			redirect('item/search','refresh');		
	}
    //Cargar vista agregar producto.
    public function load_additem()
    {

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');
          //cargar lista proveedores
            $vendor =$this->item_model->select_vendor();

             $this->smartyci->assign('list_vendor',$vendor);
          // Set title page
            $this->smartyci->assign('title', 'Nuevo Producto');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.item.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/items/newItem.html');

            // Set active menu
            $this->smartyci->assign('active_item', 'active');
            $this->smartyci->assign('new_item', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
    //carga vista editar 
    public function load_edit($id)
	{
       if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);

            $this->smartyci->assign('employees', 'employees');
          //cargar lista proveedores
            $vendor =$this->item_model->select_vendor();

            $this->smartyci->assign('list_vendor',$vendor);

            $editItem = $this->item_model->select($id);
            $this->smartyci->assign('editItem', $editItem);
      
          // Set title page
            $this->smartyci->assign('title', 'Editar Producto');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.item.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/items/editItem.html');

            // Set active menu
            $this->smartyci->assign('active_item', 'active');
            $this->smartyci->assign('new_item', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
	}
    
    //Editar producto
    public function edit_item($id)
	{

        //llamada al metodo reg_customer de customer_model para insertar datos
		$this->item_model->edit($id);		
		redirect('item/search','refresh');		
    }
    
    public function delete_item($id)
    {
        $this->item_model->delete($id); 
    }
    
    public function select_vendor($id)
    {   
        $data['title']='Proveedores';
        $data['vendors']=$this->item_model->select_vendor_item($id); 
        $data['vendor_list']=$this->item_model->select_vendor($id); 
        $data['id']=$id;
        
        $this->load->view('templates/head',$data);
        $this->load->view('templates/header',$data);
        $this->load->view('reny/item_vendor_view',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('templates/modal',$data);
    }
    
    public function item_vendor($idv, $idc)
    {
        $this->item_model->insert_item_vendor($idv, $idc); 
        redirect('item/select_vendor/'.$idc,'refresh');
    }
       public function delete_vendor($idv,$idc)
    {
        $this->item_model->delete_item_vendor($idv,$idc); 
        redirect('item/select_vendor/'.$idc,'refresh');
    }
}