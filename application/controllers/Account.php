<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {

    public function __construct()
    {
        parent::__construct();

        // Force SSL
        //$this->force_ssl();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('account_model');


        // Load resources
        $this->load->model('validation_callables');
        $this->load->library('form_validation');
    }

    public function users_table()
    {
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Titulo de pagina
            $this->smartyci->assign('title', 'PRODUCTOS');

            // CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css',
                'bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'fuelux/dist/js/fuelux.min.js',
                'bootstrap-switch/dist/js/bootstrap-switch.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // JS de pagina
            $js_page = array(
                'reny.account.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            $this->smartyci->assign('list_users',$this->account_model->select_user());
            $this->smartyci->assign('list_usersClient',$this->account_model->select_userClient());

            // Establecer contenido de la pagina
            $this->smartyci->assign('body', 'contents/users_table.html');

            // Agregar clase menu activo
            $this->smartyci->assign('active_users', 'active');
            $this->smartyci->assign('active_users_table', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }


    public function login()
    {
        if( $this->uri->uri_string() == 'account/login')
            show_404();

        if( strtolower( $_SERVER['REQUEST_METHOD'] ) == 'post' )
            $this->require_min_level(1);

        $this->setup_login_form();

        $this->load->view('templates/contents/login');
    }

     /* This login method only serves to redirect a user to a 
     * location once they have successfully logged in. It does
     * not attempt to confirm that the user has permission to 
     * be on the page they are being redirected to.
     */
   /**
     * Log out
     */
    public function logout()
    {
        $this->authentication->logout();

        // Set redirect protocol
        $redirect_protocol = USE_SSL ? 'https' : NULL;

        redirect( site_url( LOGIN_PAGE . '?logout=1', $redirect_protocol ) );
    }
    public function load_signup(){
        if( $this->require_group('employees') )
        {  
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'Crear Usuario');
             // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set active menu
            $this->smartyci->assign('active_users', 'active');
            $this->smartyci->assign('active_newUsers', 'active');
            $this->smartyci->assign('Val_user', set_value('username'));
            $this->smartyci->assign('Val_email', set_value('email'));
            $this->smartyci->assign('Val_rol', set_select('auth_level'));

            // Set content page
            $this->smartyci->assign('body', 'contents/sign_up.html');

             // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    public function load_editUser($id){
        if( $this->require_group('employees') )
        {  
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
            $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'Crear Usuario');
             // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set active menu
            $this->smartyci->assign('active_users', 'active');

            $val= $this->account_model->select_user($id);
            $this->smartyci->assign('val',$val);
            // Set content page
            $this->smartyci->assign('body', 'contents/edit_users.html');

             // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }

    public function edit_user($id)
    {
        $pwd = $this->input->post('passwd');
        if ($pwd != NULL) {
            $user_data = array(
                'username'   => $this->input->post('username'),
                'passwd'     => $this->input->post('passwd'),
                'email'      => $this->input->post('email'),
                'auth_level' => $this->input->post('auth_level'), // 9 if you want to login @ examples/index.
            );
        }else{
            $user_data = array(
                'username'   => $this->input->post('username'),
                'email'      => $this->input->post('email'),
                'auth_level' => $this->input->post('auth_level'), // 9 if you want to login @ examples/index.
            );
        }
       

        $this->form_validation->set_data( $user_data );
        $this->form_validation->set_error_delimiters('<label id="username-error" class="error" for="username" style="display: inline-block;">', '</label>');

        $validation_rules = array(
            
            array(
                'field' => 'passwd2',
                'label' => 'passwd2',
                'rules' => 'trim|matches[passwd]',
                'errors' => array(
                    'matches' => 'Las contraseñas no coinciden'
                )
            )
        );

        $this->form_validation->set_rules( $validation_rules );

        if( $this->form_validation->run() )
        {
            if (isset($user_data['passwd'])) {
                $user_data['passwd']     = $this->authentication->hash_passwd($user_data['passwd']);
            }
            
        
            $this->account_model->edit_user($id,$user_data);
            redirect('account/users_table','refresh');
                
        }
        else
        {
            $this->smartyci->assign('user',form_error('username'));
            $this->smartyci->assign('passwd',form_error('passwd'));
            $this->smartyci->assign('passwd2',form_error('passwd2'));
            $this->smartyci->assign('email',form_error('email'));
            $this->smartyci->assign('auth_level',form_error('auth_level'));
            
            $this->smartyci->assign('error_val', 'Error de'.validation_errors());
            $this->edit_user($id);
        }
        
    }
    public function sign_up()
    {
        $user_data = array(
            'username'   => $this->input->post('username'),
            'passwd'     => $this->input->post('passwd'),
            'email'      => $this->input->post('email'),
            'auth_level' => $this->input->post('auth_level'), // 9 if you want to login @ examples/index.
        );
         
        $this->is_logged_in();

        // Load resources
        $this->load->model('examples_model');
        $this->load->model('validation_callables');
        $this->load->library('form_validation');

        $this->form_validation->set_data( $user_data );
        $this->form_validation->set_error_delimiters('<label id="username-error" class="error" for="username" style="display: inline-block;">', '</label>');

        $validation_rules = array(
            array(
                'field' => 'username',
                'label' => 'username',
                'rules' => 'required|max_length[12]|is_unique[' . config_item('user_table') . '.username]',
                'errors' => array(
                    'is_unique' => 'El usuario ya exite.',
                    'required' => 'Este Campo es requerido'
                )
            ),
            array(
                'field' => 'passwd',
                'label' => 'passwd',
                'rules' => 'trim|required|max_length[12]|min_length[8]',
                'errors' => array(
                    'required' => 'Este campo es requerido.',
                    'min_length' =>'La conteseña debe contar con 8 minimo caracteres',
                    'max_length' =>'La conteseña debe contar con 12 maximo caracteres'
                )
            ),
            array(
                'field' => 'passwd2',
                'label' => 'passwd2',
                'rules' => 'trim|matches[passwd]',
                'errors' => array(
                    'matches' => 'Las contraseñas no coinciden'
                )
            ),
            array(
                'field'  => 'email',
                'label'  => 'email',
                'rules'  => 'trim|required|valid_email|is_unique[' . config_item('user_table') . '.email]',
                'errors' => array(
                    'required' => 'Este Campo es requerido',
                    'is_unique' => 'Email ya esta en uso.'
                )
            ),
            array(
                'field' => 'auth_level',
                'label' => 'auth_level',
                'rules' => 'required|integer|in_list[1,3,5,6,9]',
                'errors' => array(
                    'required' => 'Este Campo es requerido')
            )
        );

        $this->form_validation->set_rules( $validation_rules );

        if( $this->form_validation->run() )
        {
            $user_data['passwd']     = $this->authentication->hash_passwd($user_data['passwd']);
            $user_data['user_id']    = $this->examples_model->get_unused_id();
            $user_data['created_at'] = date('Y-m-d H:i:s');

            // If username is not used, it must be entered into the record as NULL
            if( empty( $user_data['username'] ) )
            {
                $user_data['username'] = NULL;
            }

            $this->db->set($user_data)
                ->insert(config_item('user_table'));

            if( $this->db->affected_rows() == 1 )
                redirect('dashboard','refresh');
                
        }
        else
        {
            $this->smartyci->assign('user',form_error('username'));
            $this->smartyci->assign('passwd',form_error('passwd'));
            $this->smartyci->assign('passwd2',form_error('passwd2'));
            $this->smartyci->assign('email',form_error('email'));
            $this->smartyci->assign('auth_level',form_error('auth_level'));
            
             $this->smartyci->assign('error_val', 'Error de'.validation_errors());
            //echo '<h1>User Creation Error(s)</h1>' . ;
             $this->load_signup();
        }

        //
    }

    public function lock_screen()
    {
        // Set title page
        $this->smartyci->assign('title', 'LOCK SCREEN');

        // Set CSS page
        $css_page = array(
            'sign.css'
        );
        $this->smartyci->assign('list_css_page',$css_page);

        // Set content page
        $this->smartyci->assign('body', 'contents/sign/lock_screen.html');

        // Render view on main layout
        $this->smartyci->display('contents/account.html');
    }
    public function delete_account($id)
    {
         $this->account_model->delete_user($id);
        $this->users_table();
    }

    public function permitirDetalle()
    {
        $this->account_model->permitirDetalle();
    }

}
