<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends MY_Controller {

	function __construct()
	{
		parent::__construct();
        
        //carga de librerias y helpers
		$this->load->helper(array('url','language'));
		$this->load->model('whse_model');
		$this->load->helper('form');
	}
    
	//cargar vista 
	public function search()
    {

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            // Set title page
            $this->smartyci->assign('title', 'BODEGAS');

            // Set CSS plugins
            $css_plugin = array(
                'datatables/css/dataTables.bootstrap.css',
                'datatables/css/datatables.responsive.css',
                'fuelux/dist/css/fuelux.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'datatables/js/jquery.dataTables.js',
                'datatables/js/dataTables.bootstrap.js',
                'datatables/js/datatables.responsive.js',
                'fuelux/dist/js/fuelux.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.whse.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            $this->smartyci->assign('list_whse',$this->whse_model->select());
            $this->smartyci->assign('list_location',$this->whse_model->select_location());
            $this->smartyci->assign('list_lote_loc',$this->whse_model->select_location_lote());

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/warehouse/warehouse.html');

            // Set active menu
            $this->smartyci->assign('active_whse', 'active');
            $this->smartyci->assign('active_whseSearch', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
	//metodo para el registro de bodega
	public function add_warehouse()
	{
        //guarda en array los datos enviados del formulario
		$datas = array(
			'whse_name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
		);
            //llamada al metodo insert de whse_model para insertar datos
			$this->whse_model->insert($datas);		
			redirect('warehouse/search','refresh');		
	}
    //Cargar vista agregar bodega.
    public function load_add_whse()
    {
      
        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
          // Set title page
            $this->smartyci->assign('title', 'Registrar Bodega');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.whse.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/warehouse/newWhse.html');

            // Set active menu
            $this->smartyci->assign('active_whse', 'active');
            $this->smartyci->assign('new_whse', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
    //carga vista editar 
    public function load_edit($id)
	{

        if( $this->require_group('employees') )
        { 
            $this->smartyci->assign('username', $this->auth_username);
            $this->smartyci->assign('role', $this->auth_role);
             $this->smartyci->assign('employees', 'employees');
            
           $whse = $this->whse_model->select($id);
           $data['id'] = $id;
           $this->smartyci->assign('whse', $whse);
          
           $this->smartyci->assign('title', 'Editar Bodega');

            // Set CSS plugins
            $css_plugin = array(
                'bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css',
                'chosen_v1.2.0/chosen.min.css'
            );
            $this->smartyci->assign('list_css_plugin',$css_plugin);

            // Set JS plugins
            $js_plugin = array(
                'chosen_v1.2.0/chosen.jquery.min.js',
                'jquery-mockjax/jquery.mockjax.js',
                'jquery-validation/dist/jquery.validate.min.js'
            );
            $this->smartyci->assign('list_js_plugin',$js_plugin);

            // Set JS page
            $js_page = array(
                'reny.whse.js'
            );
            $this->smartyci->assign('list_js_page',$js_page);

            // Set content page
            $this->smartyci->assign('body', 'contents/catalogs/warehouse/editWhse.html');

            // Set active menu
            $this->smartyci->assign('active_whse', 'active');

            // Render view on main layout
            $this->smartyci->display('contents/layout.html');
        }
    }
    
    //Editar producto
    public function edit_whse($id)
	{
     
        //guarda en array los datos enviados del formulario
		$datas = array(
			'whse_name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
		);
         //llamada al metodo reg_customer de customer_model para insertar datos
			$this->whse_model->edit($id, $datas);		
			redirect('warehouse/search','refresh');	
    }
    
    public function delete_whse($id)
    {
        $this->whse_model->delete($id); 
    }
    
     public function select_vendor($id)
    {   
        $data['title']='Bodega';
        $data['vendors']=$this->item_model->select_vendor_item($id); 
        $data['vendor_list']=$this->item_model->select_vendor($id); 
        $data['id']=$id;
        $this->load->view('templates/head',$data);
        $this->load->view('templates/header',$data);
        $this->load->view('reny/item_vendor_view',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('templates/modal',$data);
    }
    
    public function item_vendor($idv, $idc)
    {
        $this->whse_model->insert_item_vendor($idv, $idc); 
        redirect('item/select_vendor/'.$idc,'refresh');
    }
       public function delete_vendor($idv,$idc)
    {
        $this->whse_model->delete_item_vendor($idv,$idc); 
        redirect('item/select_vendor/'.$idc,'refresh');
    }
    
    
    public function whse_map()
    {
        // Load the library
        $this->load->library('googlemaps');
        $config['center'] = '9650 Railroad Dr, El Paso, TX 79924';
        $config['zoom'] = 'auto';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = '9650 Railroad Dr, El Paso, TX 79924';
        $this->googlemaps->add_marker($marker);
        // Initialize our map. Here you can also pass in additional parameters for customising the map (see below)
        $this->googlemaps->initialize();
        // Create the map. This will return the Javascript to be included in our pages <head></head> section and the HTML code to be
        // placed where we want the map to appear.
        $this->googlemaps->create_map();
    }
     public function insert_location()
    {
         //llamada al metodo reg_customer de customer_model para insertar datos
            $this->whse_model->insert_location();       
            redirect('warehouse/search','refresh'); 
    }
}