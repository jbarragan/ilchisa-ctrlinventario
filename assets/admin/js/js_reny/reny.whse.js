var ItemForm = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ItemForm.jqueryValidation();
            ItemForm.datatable1();
            ItemForm.datatable2();
            ItemForm.removedWhse();
        },

        jqueryValidation: function () {
        if($('#whse').length){

                $('#whse').validate({
                    rules:{
                        name:{
                            required:true
                        },
                        address:{
                            required:true
                        }
                    },
                    highlight:function(element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function(element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    }
                });
            }
        },
        // =========================================================================
        // eliminar Entrada
        // =========================================================================
        removedWhse: function()
            {
                $('.delWhser').click(function(){
                    var id=$(this).attr('id');
                    var whse=$(this).data('whse');
                    resp= confirm('La Bodega ' + whse + ' Sera Eliminado');
                        if (resp == true) {
                            $.getJSON( "delete_whse/"+id, function(data){
                                if (data.res==true) {
                                    $( 'tr[id='+data.id+']' ).remove();
                                }else{
                                    alert(data.alert);
                                }
                            });
                        }
                });
            },
         // =========================================================================
        // DATATABLE
        // =========================================================================
        datatable1: function () {
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };
            var tablewh = $('#warehouse');
            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 1, 2, 5 ] } ]
            } );
            tablewh.dataTable({
                autoWidth        : false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tablewh, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });

        },
        datatable2: function () {
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };
            var tableloc= $('#location');
            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 1, 2, 5 ] } ]
            } );
            tableloc.dataTable({
                autoWidth        : false,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tableloc, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });

        }

    };

}();

// Call main app init
ItemForm.init();