var BlankonCalendar = function () {

    // =========================================================================
    // SETTINGS APP
    // =========================================================================
    var globalPluginsPath = BlankonApp.handleBaseURL()+'/assets/global/plugins/bower_components';
     console.log(globalPluginsPath);
    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonCalendar.calendar();
            BlankonCalendar.chosenSelect();
            BlankonCalendar.datatable();
        },
        // =========================================================================
        // DATATABLE
        // =========================================================================
        datatable: function () {
            var responsiveHelperAjax = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };

            var tableAjax = $('#appotList');

            // Using AJAX
            tableAjax.dataTable({
                autoWidth      : false,
                ajax           : BlankonApp.handleBaseURL()+'/appointment/getAppointmentList',
                columns        : [{ "data": 'id' },{'data':'appointment'}],
                order          : [],
                lengthMenu     : [15, 30, 60, 100],
                aoColumnDefs   : [ { "orderable": false, "aTargets": [ 1 ] } ],
                order          : [ 0, 'desc' ],
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperAjax) {
                        responsiveHelperAjax = new ResponsiveDatatablesHelper(tableAjax, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperAjax.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperAjax.respond();
                }
            });
        },

        // =========================================================================
        // CALENDAR
        // =========================================================================
        calendar: function () {
            "use strict";

            var options = {
                events_source: BlankonApp.handleBaseURL()+'/appointment/getAppointment',
                view: 'month',
                tmpl_path: globalPluginsPath+'/bootstrap-calendar/tmplsAppoint/',
                format12: true,
                tmpl_cache: false,
                language:'es-ES',
                onAfterEventsLoad: function(events) {
                    if(!events) {
                        return;
                    }
                    var list = $('#eventlist');
                    list.html('');

                    $.each(events, function(key, val) {
                        $(document.createElement('li'))
                            .html('<a href="' + val.url + val.id + '"><i class="fa fa-calendar mr-10"></i> ' + val.name + '<span class="label ' + val.class + '">' + val.status + '</span>&nbsp;</a>')
                            .appendTo(list);
                    });
                },
                onAfterViewLoad: function(view) {
                    $('.page-header h4').text(this.getTitle());
                    $('button').removeClass('active');
                    $('.calendar-menu-mobile ul li').removeClass('active');
                    $('button[data-calendar-view="' + view + '"]').addClass('active');
                    $('a[data-calendar-view="' + view + '"]').parent('li').addClass('active');
                },
                classes: {
                    months: {
                        general: 'label'
                    }
                }
            };

            var calendar = $('#calendar').calendar(options);

            $('[data-calendar-nav]').each(function() {
                var $this = $(this);
                $this.click(function() {
                    calendar.navigate($this.data('calendar-nav'));
                });
            });

            $('[data-calendar-view]').each(function() {
                var $this = $(this);
                $this.click(function() {
                    calendar.view($this.data('calendar-view'));
                });
            });
            $('#language').change(function(){
                calendar.setLanguage($(this).val());
                calendar.view();
            });

            $('#events-in-modal').change(function(){
                var val = $(this).is(':checked') ? $(this).val() : null;
                calendar.setOptions({modal: val});
            });
            $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
                //e.preventDefault();
                //e.stopPropagation();
            });
        },

        // =========================================================================
        // CHOSEN SELECT
        // =========================================================================
        chosenSelect: function () {
            $('.chosen-select').chosen();
        }

    };

}();

// Call main app init
BlankonCalendar.init();