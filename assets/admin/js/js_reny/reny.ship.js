var ItemForm = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ItemForm.chosenSelect();
            ItemForm.jqueryValidation();
            ItemForm.bootstrapDatepicker();
            ItemForm.removedLots();
            ItemForm.removedfile();
            ItemForm.removedshipment();
            ItemForm.enviarmail();
            ItemForm.shipment();
            ItemForm.save();
            ItemForm.dropzone();
            ItemForm.buscar();
        }, 

        buscar: function() {
            $("#ship_ctrlnew").keyup(function(event) {

            var textoBusqueda = $("#ship_ctrlnew").val();
                 console.log(textoBusqueda);
               $.post( BlankonApp.handleBaseURL()+"/shipments/buscar_duplicado/", {valorBusqueda: textoBusqueda}, function(mensaje) {
                    if (mensaje == 'true') {
                        $('#ship_ctrlnew').parents('.form-group').addClass('has-error has-feedback');
                        $('#errorDuplicado').text('Entrada Existente, Escribe un nuevo reny control').attr('style','color:red;font-style:italic;');
                        $('#save').attr('disabled', 'true');
                    }else{
                        $('#errorDuplicado').text('');
                        $('#save').removeAttr('disabled');
                    }
                 }); 
            });
        },
        // =
        // =========================================================================
        // Enviar correo
        // =========================================================================
        enviarmail: function()
            {
                //$( "#success" ).hide();
                //$( "#error" ).hide();
                $('.mail').click(function(){
                    var id=$(this).attr('id')
                    console.log(id);
                    resp= confirm('¿Deseas enviar correo?');
                        if (resp == true) {
                            $.getJSON( BlankonApp.handleBaseURL()+"/shipments/email/"+id, function(data){
                                if (data.res==true) {

                                   $("html, body").animate({ scrollTop: 0 }, 500);
                                   $( "#success" ).attr('style','dispaly:block;');
                                   $( "#success" ).html('<span class="alert-icon"><i class="fa fa-check"></i></span>'
                                            +' <strong>Correo electronico enviado</strong> con exito a  </a> '+ data.mail);

                                    $( "#success" ).show();
                                }else
                                {
                                     $( "#error" ).attr('style','dispaly:block;');
                                     $("html, body").animate({ scrollTop: 0 }, 500);
                                }
                            });
                        }
                });
            },
            save: function()
            {
                 var lots=[];


                $( '#shipments' ).submit(function( event ) {

                    for (var i = 1; i <= $( "#numLots" ).val(); i++) {
                            lots[i] = $('.lots'+i+'').val();
                            console.log(lots);
                    }
                        if ( lots.some(function(lots){return lots == 0; }) == true) {


                            for (var i = 1; i <= lots.length; i++) {

                                if ( lots[i] == 0 ) {
                                    console.log(i);
                                    $('.lots'+i+'').parents('.form-group').addClass('has-error has-feedback');
                                    $('#errorLote'+i+'').text('Selleciona un lote.').attr('style','color:red;font-style:italic;').fadeOut(5000);
                                }
                            }

                            event.preventDefault();

                        }else{

                            console.log('entro'+lots.some(function(lots){return lots != 0; }));
                            return true;
                        }
                   

                    
                });
            },
        // =========================================================================
        // Marcar Embarcado
        // =========================================================================
        shipment: function()
            {
                //$( "#success" ).hide();
                //$( "#error" ).hide();
                $('#embarcado').click(function(){
                    var ship=$(this).data('ship');
                    var id=$(this).data('bill');
                    $.post( BlankonApp.handleBaseURL()+"/shipments/is_ship/"+ship,$("#receipt").serialize()).done(function(data){
                        
                        if ($('#sendMail').prop("checked")) {
                            var resp = JSON.parse(data);
                            if (resp.res == true) {
                                    
                                $( "#stat" ).empty();
                                $( "#stat" ).removeClass('label-warning');
                                $( "#stat" ).addClass('label-primary');
                                $( "#stat" ).append(resp.status);
                                $("html, body").animate({ scrollTop: 0 }, 500);
                                $( "#success" ).attr('style','display:block;');
                                $( "#success" ).html('<span class="alert-icon"><i class="fa fa-check"></i></span>'
                                            +' <strong>Correo electronico enviado</strong> con exito a  </a> '+ resp.mail);
                                $( "#button" ).show();
                                $( "#boxMail" ).empty();
                                $( "#button" ).empty();
                                $( "#button" ).append('<a href="'+BlankonApp.handleBaseURL()+'/shipments/searchShip' +'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 79px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Cerrar</a>');
                                 window.open(BlankonApp.handleBaseURL()+"/shipments/billOfLading/"+id, '_blank');
                            }else if(resp.res == false)
                                {
                                    $( "#stat" ).empty();
                                    $( "#stat" ).removeClass('label-warning');
                                    $( "#stat" ).addClass('label-primary');
                                    $( "#stat" ).append(resp.status);
                                    $( "#error" ).attr('style','display:block;');
                                    $("html, body").animate({ scrollTop: 0 }, 500);
                                    $( "#button" ).show();
                                    $( "#boxMail" ).empty();
                                    $( "#button" ).empty();
                                    $( "#button" ).append('<a href="'+BlankonApp.handleBaseURL()+'/shipments/searchShip' +'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 79px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Cerrar</a>');
                                }

                        } else{

                            window.open(BlankonApp.handleBaseURL()+"/shipments/billOfLading/"+id, '_blank');
                            window.location.href = BlankonApp.handleBaseURL()+'/shipments/searchShip';
                        }
                    });
                    $('#embarcado').attr('disabled', 'true');
                });
            },
            //========================================================================
        // eliminar Entrada
        // =========================================================================
        removedshipment: function()
            {
                $('.delShip').click(function(){

                    var id=$(this).attr('id');

                    resp= confirm('¿Deseas Eliminar esta Salida?');
                        if (resp == true) {
                            $.getJSON( BlankonApp.handleBaseURL()+"/shipments/deleteshipment/"+id, function(data){
                                if (data.res==true) {
                                    window.location.href =BlankonApp.handleBaseURL()+"/shipments/searchShip";
                                }else{
                                    alert(data.res);
                                }
                            });
                        }
                });
            },

        // =========================================================================
        // CHOSEN SELECT
        // =========================================================================
        chosenSelect: function () {
            if($('.chosen-select').length){
                $('.chosen-select').chosen();
            }
        },
        bootstrapDatepicker: function () {

            var dt = new Date();
            // Display the month, day, and year. getMonth() returns a 0-based number.
            var month = dt.getMonth()+1;
            var day = dt.getDate();
            var year = dt.getFullYear();
            var dateF =  day + '-' + month + '-' + year;
            if($('#shipments').length){

                // Default datepicker (options)
                $('#date').datepicker({
                    language:'en',
                    format: "dd/M/yyyy",
                    todayBtn: 'linked',
                    autoclose: 'true',
                    endDate: dateF
                    /*showMeridian: true,
                    autoclose: true,
                    todayBtn: true*/
                });
            }
        },
        jqueryValidation: function () {
        if($('#shipments').length){

                $('#shipments').validate({
                    rules:{
                        date:{
                            required:true
                        },
                        whse:{
                            required:true,
                            minlength: 1
                        },
                        customer:{
                            required:true,
                            minlength: 1
                        },
                        lots:{
                            required:true,
                            minlength: 1
                        },
                        item:{
                            required:true,
                            minlength: 1
                        }
                    },
                    messages: {
                        bv_email: {
                            remote: jQuery.validator.format("{0} is already in use")
                        },
                        bv_username: {
                            remote: jQuery.validator.format("{0} is already in use")
                        }
                    },
                    highlight:function(element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function(element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    }
                });
            }
        },
        // =========================================================================
        // eliminar fila lotes 
        // =========================================================================
        removedLots: function()
            {
                $('.delLot').click(function(){

                    var id=$(this).attr('id');
                    var index= $( 'input[name=index]' ).val();
                    var i = parseFloat(index)-1;


                    $( 'input[name=index]' ).val(i);
                     console.log(i);


                    resp= confirm('¿Quieres Eliminar este lote?');
                        if (resp == true) {
                            $.getJSON( "../deleteLots/"+id, function(data){
                                if (data.res==true) {
                                    var c = parseFloat( $('input[name="lotShipment"]' ).val()) - 1;
                                    var classb = $( 'tr[id='+data.id+']' ).attr('class');
                                    var aunclass = classb;

                                    $( 'tr[id='+data.id+']' ).remove();
                                    $( 'input[name="index"]' ).val(i);
                                    $( 'input[name="lotShipment"]' ).val(c);

                                    for (y = classb; y < index; y++){

                                        aunclass = parseFloat(aunclass) + 1;
                                        
                                        $( 'select[id="renyCtrl'+aunclass+'"]' ).removeClass('renyCtrl'+aunclass).addClass('renyCtrl'+y);
                                        $( 'select[id="lot'+aunclass+'"]' ).removeClass('lots'+aunclass).addClass('lots'+y);
                                        console.log( $( 'input[id="lot'+aunclass+'"]' ).attr('class'));
                                        $( 'input[id="pallets'+aunclass+'"]' ).removeClass('pallets'+aunclass).addClass('pallets'+y);
                                        $( 'input[id="bags'+aunclass+'"]' ).removeClass('bags'+aunclass).addClass('bags'+y);
                                        $( 'input[id="ht'+aunclass+'"]' ).removeClass('ht'+aunclass).addClass('ht'+y);
                                        $( 'input[id="tags'+aunclass+'"]' ).removeClass('tags'+aunclass).addClass('tags'+y);
                                        $( 'input[id="storage'+aunclass+'"]' ).removeClass('storage'+aunclass).addClass('storage'+y);

                                    }
                                }
                            });
                        }
                });
            },

        // =========================================================================
        // eliminar imgen
        // =========================================================================
        removedfile: function()
            {
                $('.delImg').click(function(){
                    var id=$(this).attr('id')
                    console.log(id);
                    resp= confirm('¿Quieres Eliminar esta images?');
                        console.log(resp);
                        if (resp == true) {
                            $.getJSON( "../deleteImg/"+id, function(data){
                                if (data.res==true) {
                                    $( 'div[id='+data.id+']'  ).remove();
                                }
                            });
                        }
                });
            },

         // =========================================================================
        // DROPZONE UPLOAD
        // =========================================================================
        dropzone: function () {
            Dropzone.options.myDropzone = {
                maxFilesize: 2,
                init: function() {
                    
                    this.on("error", function(file) {
                         // Capture the Dropzone instance as closure.
                        var _this = this;
                            alert(file.name+"  excede el limite de 2MB o el servidor no responde" ); 
                            _this.removeFile(file);
                    });
                    
                }
            }
        },


    };

}();

// Call main app init
ItemForm.init();