'use strict';
var reports = function () {

	var getBaseURL = BlankonApp.handleBaseURL();

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            reports.jqueryValidation();
            reports.bootstrapDatepicker();
            reports.datatableReceipt();
            reports.datatableship();
            reports.datatableinvent();
            reports.filterReport();
            reports.genRepor();
            reports.genReporExcel();

        },
        jqueryValidation: function () {
        if($('#receipt').length){

                $('#receipt').validate({
                    rules:{
                        carrier:{
                            required:true
                        },
                        trailer:{
                            required:true
                        },
                        seal:{
                            required:true
                        }
                    },
                    messages: {
                        bv_email: {
                            remote: jQuery.validator.format("{0} is already in use")
                        },
                        bv_username: {
                            remote: jQuery.validator.format("{0} is already in use")
                        }
                    },
                    highlight:function(element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function(element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    }
                });
            }
        },
        bootstrapDatepicker: function () {
            if($('#receiptReport').length){

                // Default datepicker (options)
                $('#date').datepicker({
                    language:'en',
                    format: 'dd/M/yyyy',
                    todayBtn: 'linked',
                    autoclose: 'true'
                });
                $('#date2').datepicker({
                    language:'en',
                    format: 'dd/M/yyyy',
                    todayBtn: 'linked',
                    autoclose: 'true'
                });
            }
        }, 
        datatableReceipt: function () {
            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
            var status = $("#status").val(); 
            var idUser = $("#idUserCustom").val(); 
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1050,
                phone : 360
            };
            if (idUser) {

                var urlString = BlankonApp.handleBaseURL()+'/reports/getReceipt?iduserCustom='+idUser;
            }else{

                var urlString = BlankonApp.handleBaseURL()+'/reports/getReceipt';
            }

            var tableDom = $('#datatable');
            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
               "aoColumnDefs": [ { "bSortable": false, "aTargets": [0, 2, 8, 9, 10] } ],
               "order": [[ 3, 'desc' ]],
               "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ]
            } );
            var table = tableDom.DataTable({
                processing     : true,
                ajax           : urlString,
                columns        : [  { "data":"reny_ctrl"},
                                    { "data": "status" },
                                    { "data": "system_date" },
                                    { "data": "receipt_date" },
                                    { "data": "po_no" },
                                    { "data": "first_name" },
                                    { "data": "item_name" },
                                    { "data": "name" },
                                    { "data": "lot" },
                                    { "data": "pallets" },
                                    { "data": "bags_total" }],
                order           : [],
                autoWidth       : true,
                pageLength      : 100,

                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tableDom, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });

            $("#customer").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#date").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#date2").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#status").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#item").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

        }, 
        datatableship: function () {
            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
            var status = $("#status").val(); 
            var idUser = $("#idUserCustom").val(); 
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1050,
                phone : 360
            };

            if (idUser) {

                var urlString = BlankonApp.handleBaseURL()+'/reports/getshipment?iduserCustom='+idUser;
                var colum = [  { "data": "no_shipment"},
                                    { "data": "status" },
                                    { "data": "system_dateShip" },
                                    { "data": "start" },
                                    { "data": "carrier" },
                                    { "data": "trailer" },
                                    { "data": "seal" },
                                    { "data": "reny_contrl" },
                                    { "data": "first_name" },
                                    { "data": "item_name" },
                                    { "data": "lot" },
                                    { "data": "pallets" },
                                    { "data": "bags_total" }];
            }else{

                var urlString = BlankonApp.handleBaseURL()+'/reports/getshipment';
                var colum = [  { "data": "no_shipment"},
                                    { "data": "status" },
                                    { "data": "system_dateShip" },
                                    { "data": "start" },
                                    { "data": "carrier" },
                                    { "data": "trailer" },
                                    { "data": "seal" },
                                    { "data": "reny_contrl" },
                                    { "data": "first_name" },
                                    { "data": "item_name" },
                                    { "data": "lot" },
                                    { "data": "pallets" },
                                    { "data": "bags_total" },
                                    { "data": "storage" }];
            }
            var tableDom = $('#datatableShip');
            console.log(colum);
            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
               "aoColumnDefs": [ { "bSortable": false} ],
               "order": [[ 2, 'desc' ],[ 0, 'desc' ]],
               "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ]
            } );
            var table = tableDom.DataTable({
                processing     : true,
                ajax           : urlString,
                columns        : colum,
                order           : [],
                autoWidth       : true,
                pageLength      : 100,

                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tableDom, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });

            $("#customer").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getshipment?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#date").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getshipment?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#date2").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getshipment?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#status").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getshipment?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#item").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getshipment?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

        },
        datatableinvent: function () {
            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
            var status = $("#status").val(); 
            var idUser = $("#idUserCustom").val(); 
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1050,
                phone : 360
            };

            if (idUser) {

                var urlString = BlankonApp.handleBaseURL()+'/reports/getInvent?iduserCustom='+idUser;

                var colum = [   { "data":"reny_ctrl"},
                                { "data": "receipt_date" },
                                { "data": "first_name" },
                                { "data": "name" },
                                { "data": "item_name" },
                                { "data": "name_brand" },
                                { "data": "lot" },
                                { "data": "prod_date" },
                                { "data": "exp_date" },
                                { "data": "pallets" },
                                { "data": "stock" }];

                var targ = [ { "bSortable": false, "aTargets": [ 6, 7, 8]  } ];

            }else{
                var colum = [  { "data":"reny_ctrl"},
                                    { "data": "receipt_date" },
                                    { "data": "first_name" },
                                    { "data": "name" },
                                    { "data": "item_name" },
                                    { "data": "name_brand" },
                                    { "data": "lot" },
                                    { "data": "prod_date" },
                                    { "data": "exp_date" },
                                    { "data": "pallets" },
                                    { "data": "stock" },
                                    { "data": "location" },
                                    { "data": "storage" }];

                var urlString = BlankonApp.handleBaseURL()+'/reports/getInvent';
                var targ = [ { "bSortable": false, "aTargets": [ 6, 7, 8]  } ];
            }
            var tableDom = $('#datatableInvent');
            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
               "order": [[ 2, 'desc' ],[ 0, 'desc' ]],
               "lengthMenu": [ [25, 50, 100, -1], [25, 50, 100, "All"] ]
            } );
            var table = tableDom.DataTable({
                processing     : true,
                ajax           : urlString,
                columns        : colum,
                order           : [],
                autoWidth       : true,
                pageLength      : 100,
                aoColumnDefs    : targ,

                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tableDom, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });

            $("#customer").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val();
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#date").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val();
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#date2").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val();
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#whse").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val(); 
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#item").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var status = $("#status").val(); 
            var item = $("#item").val();
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#brand").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val(); 
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#palnt").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val(); 
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

            $("#clasification").change(function(event) {

            var customer = $("#customer").val();
            var dateIni = $("#date").val();
            var dateFinal = $("#date2").val();
            var whse = $("#whse").val(); 
            var item = $("#item").val(); 
            var brand = $("#brand").val(); 
            var plant = $("#plant").val(); 
            var clasification = $("#clasification").val(); 
                table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getInvent?dateIn='+dateIni+'&dateFin='+dateFinal+'&whse='+whse+'&customer='+customer+'&item='+item+'&brand='+brand+'&plant='+plant+'&clasification='+clasification+'' ).load();
               // table.ajax.url(BlankonApp.handleBaseURL()+'/reports/getReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'').load();
            });

        },
        filterReport: function(){

            $('#exportar').click(function () {
                $('#form').attr('target', '_blank');         
            }); 
            $('#generar').click(function () {
                $('#form').removeAttr('target');         
            }); 

            $('#exportar2').click(function () {
                $('#form').attr('target', '_blank');         
            }); 
            $('#generar2').click(function () {
                $('#form').removeAttr('target');         
            }); 
            
        },
        genRepor: function(){
            $('#exportar').click(function(){
                var customer = $("#customer").val();
                var dateIni = $("#date").val();
                var dateFinal = $("#date2").val();
                var status = $("#status").val(); 
                var item = $("#item").val();
                var status = $("#status").val(); 
                  //$.ajaxSetup({"cache":false});
                window.open(BlankonApp.handleBaseURL()+'/reports/selectReceipt?dateIn='+dateIni+'&dateFin='+dateFinal+'&status='+status+'&customer='+customer+'&item='+item+'', '_blank');
                
            });
        },
        genReporExcel: function(){

            $("#ToExcelReceipt").click(function(event) {
                var table =  $("<div>").append( $("#datatable").eq(0).clone()).html();
                $("#tablaExcelReceipt").val(table);
                $("#exportExcelReceipt").submit();
            });
            $("#ToExcelShip").click(function(event) {
                var table =  $("<div>").append( $("#datatableShip").eq(0).clone()).html();
                $("#tablaExcelShip").val(table);
                $("#exportExcelShip").submit();
            });
            $("#ToExcelInvent").click(function(event) {
                var table =  $("<div>").append( $("#datatableInvent").eq(0).clone()).html();
                $("#tablaExcelInvent").val(table);
                $("#exportExcelInvent").submit();
            });
        }
    };
        
}();
// Call main app init
reports.init();