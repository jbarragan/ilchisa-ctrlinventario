var receipt = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            receipt.datatable();
            receipt.removedReceipt();
        },
        // =========================================================================
        // eliminar Entrada
        // =========================================================================
        removedReceipt: function()
            {
                $('.delRec').click(function(){

                    var id=$(this).attr('id');

                    resp= confirm('¿Quieres Eliminar esta Entrada?');
                        if (resp == true) {
                            $.getJSON( "deleteReceipt/"+id, function(data){
                                if (data.res==true) {
                                    $( 'tr[id='+data.id+']' ).remove();
                                }else{
                                    alert(data.res);
                                }
                            });
                        }
                });
            },
         // =========================================================================
        // DATATABLE
        // =========================================================================
        datatable: function () {
            var responsiveHelperDom = undefined;
            var breakpointDefinition = {
                tablet: 1024,
                phone : 480
            };
            var tableDom = $('#datatable-dom');
            // Using DOM
            // Remove arrow datatable
            $.extend( true, $.fn.dataTable.defaults, {
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 1, 2, 5 ] } ],
                "order": [[ 2, 'desc' ],[ 0, 'desc' ]]
            } );
            tableDom.dataTable({
                autoWidth        : true,
                pageLength       : 100,
                preDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelperDom) {
                        responsiveHelperDom = new ResponsiveDatatablesHelper(tableDom, breakpointDefinition);
                    }
                },
                rowCallback    : function (nRow) {
                    responsiveHelperDom.createExpandIcon(nRow);
                },
                drawCallback   : function (oSettings) {
                    responsiveHelperDom.respond();
                }
            });

            // Repeater
            
            var dataSource, filtering;

            dataSource = function(options, callback){
                var items = filtering(options);
                var resp = {
                    count: items.length,
                    items: [],
                    page: options.pageIndex,
                    pages: Math.ceil(items.length/(options.pageSize || 50))
                };
                var i, items, l;

                i = options.pageIndex * (options.pageSize || 50);
                l = i + (options.pageSize || 50);
                l = (l <= resp.count) ? l : resp.count;
                resp.start = i + 1;
                resp.end = l;

                if(options.view==='list' || options.view==='thumbnail'){
                    if(options.view==='list'){
                        resp.columns = columns;
                        for(i; i<l; i++){
                            resp.items.push(items[i]);
                        }
                    }else{
                        for(i; i<l; i++){
                            resp.items.push({
                                name: items[i].name,
                                src: items[i].ThumbnailImage
                            });
                        }
                    }

                    setTimeout(function(){
                        callback(resp);
                    }, delays[Math.floor(Math.random() * 4)]);
                }
            };

        },
    }
}();
// Call main app init
receipt.init();