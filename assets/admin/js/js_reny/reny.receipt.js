var receipt = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            receipt.viewModal();
            receipt.chosenSelect();
            receipt.jqueryValidation();
            receipt.bootstrapDatepicker();
            receipt.dropzone();
            receipt.removedfile();
            receipt.removedLots();
            receipt.enviarmail();
            receipt.receipt();
            receipt.buscar();
        },

        // =========================================================================
        // CHOSEN SELECT
        // =========================================================================
        chosenSelect: function () {
            if($('.chosen-select').length){
                $('.chosen-select').chosen();
            }
        },
        buscar: function() {
            $("#reny_ctrlnew").keyup(function(event) {

            var textoBusqueda = $("#reny_ctrlnew").val();
                 console.log(textoBusqueda);
               $.post( BlankonApp.handleBaseURL()+"/receipts/buscar_duplicado/", {valorBusqueda: textoBusqueda}, function(mensaje) {
                    if (mensaje == 'true') {
                        $('reny_ctrlnew').parents('.form-group').addClass('has-error has-feedback');
                        $('#errorDuplicado').text('Entrada Existente, Escribe un nuevo reny control').attr('style','color:red;font-style:italic;');
                        $('#SaveBtn').attr('disabled', 'true');
                    }else{
                        $('#errorDuplicado').text('');
                        $('#SaveBtn').removeAttr('disabled');
                    }
                 }); 
            });
        },
        jqueryValidation: function () {
        if($('#receipt').length){

                $('#receipt').validate({
                    rules:{
                        date:{
                            required:true
                        },
                        whse:{
                            required:true,
                            minlength: 1
                        },
                        po:{
                            required:true
                        },
                        item:{
                            required:true,
                            minlength: 1
                        },
                        vendor:{
                            required:true,
                            minlength: 1
                        },
                        customer:{
                            required:true,
                            minlength: 1
                        },
                        producer:{
                            required:true,
                            minlength: 1
                        },
                        brand:{
                            required:true,
                            minlength: 1
                        },
                        plant:{
                            required:true,
                            minlength: 1
                        },
                        clasification:{
                            required:true,
                            minlength: 1
                        }

                    },
                    messages: {
                        bv_email: {
                            remote: jQuery.validator.format("{0} is already in use")
                        },
                        bv_username: {
                            remote: jQuery.validator.format("{0} is already in use")
                        }
                    },
                    highlight:function(element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function(element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    }
                });
            }
        },
        bootstrapDatepicker: function () {
            var dt = new Date();
            // Display the month, day, and year. getMonth() returns a 0-based number.
            var month = dt.getMonth()+1;
            var day = dt.getDate();
            var year = dt.getFullYear();
            var dateF =  day + '-' + month + '-' + year;
console.log(dateF);
            if($('#receipt').length){

                // Default datepicker (options)
                $('#date').datepicker({
                    language:'en',
                    format: 'dd/M/yyyy',
                    todayBtn: 'linked',
                    autoclose: 'true',
                    endDate: dateF
                });
            }
        }, 

        // =========================================================================
        // eliminar fila lotes 
        // =========================================================================
        removedLots: function()
            {


                $('.delLot').click(function(){

                    var id=$(this).attr('id');
                    var index= $( 'input[name=index]' ).val();
                    var i = parseFloat(index)-1;
                    
                    var lot = parseFloat($( 'input[id=numLots]' ).val()) - 1;
                    var camlot=parseFloat(lot)+1

                    $( 'input[name=index]' ).val(i);
                    
                    resp= confirm('¿Quieres Eliminar este lote?');
                        if (resp == true) {
                            $.getJSON( "../deleteLots/"+id, function(data){

                                if (data.res==true) {

                                    var c = parseFloat( $('input[name="lotShipment"]' ).val()) - 1;
                                    var classb = $( 'tr[id='+data.id+']' ).attr('class');
                                    var aunclass = classb;

                                    $( 'tr[id='+data.id+']' ).remove();
                                    $( 'input[name="index"]' ).val(i);
                                    $( 'input[id="numLots"]' ).val(lot);

                                    for (y = classb; y <= index; y++){

                                        aunclass = parseFloat(aunclass) + 1;
                                        console.log(aunclass);
                                        console.log( $( 'input[id="pallets'+aunclass+'"]' ).attr('class'));
                                        $( 'input[id="pallets'+aunclass+'"]' ).removeClass('pallets'+aunclass).addClass('pallets'+y);
                                        $( 'input[id="bags'+aunclass+'"]' ).removeClass('bags'+aunclass).addClass('bags'+y);

                                    }
                                }
                            });
                        }
                });
            },

        // =========================================================================
        // eliminar imgen
        // =========================================================================
        removedfile: function()
            {
                $('.delImg').click(function(){
                    var id=$(this).attr('id')
                    console.log(id);
                    resp= confirm('¿Quieres Eliminar esta images?');
                        console.log(resp);
                        if (resp == true) {
                            $.getJSON( "../deleteImg/"+id, function(data){
                                if (data.res==true) {
                                    $( 'div[id='+data.id+']'  ).remove();
                                }
                            });
                        }
                });
            },
        // =========================================================================
        // Enviar correo
        // =========================================================================
        enviarmail: function()
            {
                //$( "#success" ).hide();
                //$( "#error" ).hide();
                $('.mail').click(function(){
                    var id=$(this).attr('id')
                    console.log(id);
                    resp= confirm('¿Deseas enviar correo?');
                        if (resp == true) {
                            $.getJSON( BlankonApp.handleBaseURL()+"/receipts/email/"+id, function(data){
                                console.log(data);
                                console.log(data.res);
                                if (data.res==true) {

                                   $("html, body").animate({ scrollTop: 0 }, 500);
                                   $( "#success" ).attr('style','display:block;');
                                   $( "#success" ).html('<span class="alert-icon"><i class="fa fa-check"></i></span>'
                                            +' <strong>Correo electronico enviado</strong> con exito a  </a> '+ data.mail);

                                    $( "#success" ).show();
                                }else
                                {
                                     $( "#error" ).attr('style','display:block;');
                                     $("html, body").animate({ scrollTop: 0 }, 500);
                                }
                            });
                        }
                });
            },
        // =========================================================================
        // Marcar Recibido
        // =========================================================================
        receipt: function()
            {
                //$( "#success" ).hide();
                //$( "#error" ).hide();
                $('.check').click(function(){
                    $('.check').attr("disabled", "true");
                    $('.check').html("Guardando...");
                    var id=$(this).attr('id')
                    $.post( BlankonApp.handleBaseURL()+"/receipts/ready/"+id,$("#receipt").serialize()).done(function(data){
                        
                        if ($('#sendMail').prop("checked")) {
                            var resp = JSON.parse(data);
                            if (resp.res == true) {
                                    
                                $( "#stat" ).empty();
                                $( "#stat" ).removeClass('label-warning');
                                $( "#stat" ).addClass('label-primary');
                                $( "#stat" ).append(resp.status);
                                $("html, body").animate({ scrollTop: 0 }, 500);
                                $( "#success" ).attr('style','display:block;');
                                $( "#success" ).html('<span class="alert-icon"><i class="fa fa-check"></i></span>'
                                            +' <strong>Correo electronico enviado</strong> con exito a  </a> '+ resp.mail);
                                $( "#button" ).show();
                                $( "#boxMail" ).empty();
                                $( "#button" ).empty();
                                $( "#button" ).append('<a href="'+BlankonApp.handleBaseURL()+'/receipts/searchReceipt' +'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 79px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Cerrar</a>');
                            }else if(resp.res == false)
                                {
                                    $( "#stat" ).empty();
                                    $( "#stat" ).removeClass('label-warning');
                                    $( "#stat" ).addClass('label-primary');
                                    $( "#stat" ).append(resp.status);
                                    $( "#error" ).attr('style','display:block;');
                                    $("html, body").animate({ scrollTop: 0 }, 500);
                                    $( "#button" ).show();
                                    $( "#boxMail" ).empty();
                                    $( "#button" ).empty();
                                    $( "#button" ).append('<a href="'+BlankonApp.handleBaseURL()+'/receipts/searchReceipt' +'" class="btn btn-sm btn-theme btn-xs" style="height:32px;width: 79px;padding-top: 6px;padding-bottom: 6px;padding-left: 12px;padding-right: 12px;">Cerrar</a>');
                                }
                        } else{
                            location.reload();
                        }
                    });
                });
            },
        // =========================================================================
        // DROPZONE UPLOAD
        // =========================================================================
        dropzone: function () {
            Dropzone.options.myDropzone = {
                maxFilesize: 2,
                init: function() {
                    
                    this.on("error", function(file) {
                         // Capture the Dropzone instance as closure.
                        var _this = this;
                            alert(file.name+"  excede el limite de 2MB o el servidor no responde" ); 
                            _this.removeFile(file);
                    });
                    /*this.on("addedfile", function(file) {
                        // Create the remove button
                        //var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block btn-theme'>Eliminar</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function(e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();
                            // Remove the file preview.
                            
                            var name = file.name;

                            resp= confirm('¿Deseas eliminar '+name+'?');
                            if (resp == true) {

                                $.post( BlankonApp.handleBaseURL()+"/receipts/deleteImgServer/", {filename: name}, function(data) {
                                        if(data == 'true')
                                        {
                                            _this.removeFile(file);
                                            alert("El elemento fué eliminado: " + name); 
                                        }else{
                                            alert("No se puede eliminar: " + name); 
                                        }
                                });
                            }
                        });

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });*/
                }
            }
        },
        
        // =========================================================================
        // ACTION VIEW ROW DATATABLES
        // =========================================================================
        viewModal: function () {
            $('#datatable-dom').on('click', '.btn-view', function(){
            	
            	var vic_ctrl=$(this).attr('id');

                showModalDialog(vic_ctrl);
            });

            $('#receiptView').modal({ show: false });

            $('#receiptView').on('show.bs.modal', function (e){
                var $dlg = $(this);

                var idreceipt = $dlg.data('btn');

                //console.log(idreceipt);
                console.log('lotsReceipt/'+idreceipt);
                $.getJSON('lotsReceipt/'+idreceipt, function(resp) {
	                
	                
	                $.each(resp, function(code, desc) {
	                    console.log(desc.lot);

	                    $('.modal-title', $dlg).html(desc.reny_ctrl);
	                
	                var html ='<form class="form-horizontal">' +
                    '<div class="form-group">' +
                    	'<label class="col-sm-2 control-label">Fecha Registro :</label>' +
                    	'<div class="col-sm-4">' +
                    		'<p class="form-control-static">' + desc.system_date + '</p>'+
                    	'</div>' +
                    	'<label class="col-sm-2 control-label">Fecha Entrada :</label>' +
                    	'<div class="col-sm-4">' +
                    		'<p class="form-control-static">' + desc.receipt_date + '</p>'+
                    	'</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    	'<label class="col-sm-2 control-label">Po. No. :</label>' +
                    	'<div class="col-sm-4">' +
                    		'<p class="form-control-static">' + desc.po_no + '</p>'+
                    	'</div>' +
                    	'<label class="col-sm-2 control-label">Cliente :</label>' +
                    	'<div class="col-sm-4">' +
                    		'<p class="form-control-static">' + desc.first_name + '</p>'+
                    	'</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    	'<label class="col-sm-2 control-label">Producto :</label>' +
                    	'<div class="col-sm-4">' +
                    		'<p class="form-control-static">' + desc.item_name + '</p>'+
                    	'</div>' +
                    	'<label class="col-sm-2 control-label">Proveedor :</label>' +
                    	'<div class="col-sm-4">' +
                    		'<p class="form-control-static">' + desc.name + '</p>'+
                    	'</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    	'<label class="col-sm-2 control-label">Extn. :</label>' +
                    	'<div class="col-sm-10">' +
                    		'<p class="form-control-static"></p>'+
                    	'</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    	'<label class="col-sm-2 control-label">Start Date :</label>' +
                    	'<div class="col-sm-10">' +
                    		'<p class="form-control-static"></p>'+
                    	'</div>' +
                    '</div>' +
                    '<div class="form-group">' +
                    	'<label class="col-sm-2 control-label">Salary :</label>' +
                    	'<div class="col-sm-10">' +
                    		'<p class="form-control-static">' + '</p>'+
                    	'</div>' +
                    '</div>' +
                    '</form>';
                     $('.modal-body', $dlg).html(html);
					});


	               
	            });

               // $('.row-name', $dlg).html(data[1]);
            });

            function showModalDialog(elBtn){
                $('#receiptView').data('btn', elBtn);
                $('#receiptView').modal('show');
            }
        }

    };

}();

// Call main app init
receipt.init();