var BlankonFormAdvanced = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonFormAdvanced.bootstrapSwitch();
        },

        // =========================================================================
        // BOOTSTRAP SWITCH
        // =========================================================================
        bootstrapSwitch: function () {
            if($('.switch').length){
                $('.switch').bootstrapSwitch();
            }
            $('.switch').on('switchChange.bootstrapSwitch', function(event,state) {
                var id = $(this).attr('id');
                if(state == true){
                    valor = 1;
                }else{
                    valor = 0;
                }
                
                $.post(BlankonApp.handleBaseURL()+"/account/permitirDetalle",{id: id, valor: valor},function(){
                    console.log(id);
                    console.log(state);
                });
            });
        }

    };

}();

// Call main app init
BlankonFormAdvanced.init();