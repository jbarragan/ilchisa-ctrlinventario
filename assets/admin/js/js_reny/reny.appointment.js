var appoint = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            appoint.chosenSelect();
            appoint.jqueryValidation();
            appoint.bootstrapDatepicker();
            appoint.sendMail();
        },

        // =========================================================================
        // CHOSEN SELECT
        // =========================================================================
        chosenSelect: function () {
            if($('.chosen-select').length){
                $('.chosen-select').chosen();
            }
        },
        jqueryValidation: function () {
        if($('#appoint').length){

                $('#appoint').validate({
                    rules:{
                        date:{
                            required:true
                        },
                        whse:{
                            required:true,
                            minlength: 1
                        },
                        po:{
                            required:true
                        },
                        vendor:{
                            required:true,
                            minlength: 1
                        },
                        customer:{
                            required:true,
                            minlength: 1
                        }

                    },
                    messages: {
                        bv_email: {
                            remote: jQuery.validator.format("{0} is already in use")
                        },
                        bv_username: {
                            remote: jQuery.validator.format("{0} is already in use")
                        }
                    },
                    highlight:function(element) {
                        $(element).parents('.form-group').addClass('has-error has-feedback');
                    },
                    unhighlight: function(element) {
                        $(element).parents('.form-group').removeClass('has-error');
                    }
                });
            }
        },
        bootstrapDatepicker: function () {
            if($('#appoint').length){

                // Default datepicker (options)
                 $('#date').datetimepicker({
                    language:'en',
                    format: "dd/M/yyyy hh:ii",
                    showMeridian: true,
                    autoclose: true,
                    todayBtn: true,
                    minDate: new Date()
                });
            } 
        },
        /*sendMail: function(){

            $('#vendor').change(function(){
                var idVendor = $(this).val();

                $('#sendMail').removeAttr('disabled');
                $.getJSON(BlankonApp.handleBaseURL()+'/appointment/select_vendors/', {id:idVendor}, function(resp) {
                    if (resp.vendor_email == "") 
                        {
                            $('#sendMail').removeAttr('checked');
                            $('#sendMail').attr('disabled', 'true');
                            alert('El Proveedor no tiene asignado un Correo Electronico');
                        }
                });

            });     
        }*/

    };

}();

// Call main app init
appoint.init();